FROM php:8.0-fpm-alpine

COPY --from=composer:2 /usr/bin/composer /usr/bin/composer

RUN apk --no-cache add \
    icu-dev \
    gettext \
    gettext-dev

RUN docker-php-ext-configure intl \
    && docker-php-ext-configure gettext \
    && docker-php-ext-install \
    intl \
    gettext
