<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Misc;

use QBNK\JobQueue\Job\JobQueueException;

class IncompatibleDataTypesException extends JobQueueException
{
}
