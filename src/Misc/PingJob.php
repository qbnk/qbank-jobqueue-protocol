<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Misc;

use QBNK\JobQueue\Job\JobAbstract;

class PingJob extends JobAbstract
{
    public static function fromArray(array $parameters, bool $assignId = false): static
    {
        $parameters['class'] = __CLASS__;
        return parent::fromArray($parameters, $assignId);
    }

    public function getQueueName(): string
    {
        return $this->queueName ?? 'ping';
    }
}
