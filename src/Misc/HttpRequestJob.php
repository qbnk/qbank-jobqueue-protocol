<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Misc;

use InvalidArgumentException;
use JsonSerializable;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\JobQueueException;
use QBNK\JobQueue\Job\Storage\File;

class HttpRequestJob extends JobAbstract
{
    public const METHOD_GET = 'get';
    public const METHOD_POST = 'post';
    public const METHOD_DELETE = 'delete';

    public const DATATYPE_FORM = 'form';
    public const DATATYPE_JSON = 'json';
    public const DATATYPE_MULTIPART = 'multipart';

    /** @var string */
    protected $method;

    /** @var string */
    protected $uri;

    /** @var array */
    protected $headers = [];

    /** @var array|JsonSerializable */
    protected $httpData;

    /** @var string|null */
    protected $dataType;

    /** @var File[] */
    protected $files = [];

    /** @var float (seconds) */
    protected $timeout = 10.0;

    /** @var float (seconds) */
    protected $readTimeout = 10.0;

    /** @var int The HTTP status code returned */
    protected $responseStatusCode;

    /** @var string The HTTP status code reason returned */
    protected $responseReasonPhrase;

    /** @var int */
    protected $webhookId;

    /** @var bool */
    protected $saveResponse = false;

    /** @var string[][] */
    protected $responseHeaders;

    /**
     * @return string
     */
    public function getMethod(): ?string
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return $this
     */
    public function setMethod($method): HttpRequestJob
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return string
     */
    public function getUri(): ?string
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     * @return $this
     */
    public function setUri($uri): HttpRequestJob
    {
        $this->uri = $uri;
        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers ?? [];
    }

    /**
     * @param array $headers
     * @return $this
     */
    public function setHeaders($headers): HttpRequestJob
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function addHeader($key, $value): static
    {
        $this->headers[$key] = $value;
        return $this;
    }

    /**
     * @return array|JsonSerializable
     */
    public function getHttpData()
    {
        return $this->httpData ?? [];
    }

    /**
     * @param array|JsonSerializable $data
     * @param string $type
     * @return $this
     */
    public function setHttpData($data, $type = self::DATATYPE_FORM): HttpRequestJob
    {
        if ($type === self::DATATYPE_JSON) {
            if (!(is_array($data) || $data instanceof JsonSerializable)) {
                throw new InvalidArgumentException('It is not possible to interpret data as JSON.');
            }
        } else {
            if (!is_array($data)) {
                throw new InvalidArgumentException(
                    'Data must be of type array or JsonSerializable. ' . gettype($data) . ' given.'
                );
            }
        }
        $this->httpData = $data;
        $this->dataType = $type;
        return $this;
    }

    /**
     * @param string $key
     * @param string|array $value
     * @param string $dataType
     * @return $this
     * @throws IncompatibleDataTypesException
     */
    public function addHttpData(string $key, $value, $dataType = self::DATATYPE_FORM): HttpRequestJob
    {
        if (($this->dataType !== null && $this->dataType !== $dataType)) {
            throw new IncompatibleDataTypesException('Data type is incompatible with the one already set!');
        }
        if ($this->httpData instanceof JsonSerializable) {
            throw new IncompatibleDataTypesException('Can not add data to JSON object!');
        }
        if (!is_array($this->httpData)) {
            $this->httpData = [];
            $this->dataType = $dataType;
        }
        $this->httpData[$key] = $value;
        return $this;
    }

    /**
     * @return File[]
     */
    public function getFiles(): array
    {
        return $this->files ?? [];
    }

    /**
     * @param File[] $files
     * @return $this
     */
    public function setFiles($files): HttpRequestJob
    {
        foreach ($files as $file) {
            $file = is_array($file) ? File::fromArray($file) : $file;
            $this->files[$file->getFilename()] = $file;
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function clearFiles(): HttpRequestJob
    {
        $this->files = [];
        return $this;
    }

    /**
     * @param File $file
     * @return $this
     */
    public function addFile(File $file): HttpRequestJob
    {
        $this->files[$file->getFilename()] = $file;
        return $this;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->method = $this->getMethod();
        $json->uri = $this->getUri();
        $json->headers = $this->getHeaders();
        $json->httpData = $this->getHttpData();
        if (!empty($this->getDataType())) {
            $json->dataType = $this->getDataType();
        }
        $json->files = $this->getFiles();
        $json->timeout = $this->getTimeout();
        $json->readTimeout = $this->getReadTimeout();
        $json->responseStatusCode = $this->getResponseStatusCode();
        $json->responseReasonPhrase = $this->getResponseReasonPhrase();
        $json->webhookId = $this->getWebhookId();
        $json->saveResponse = $this->isSaveResponse();
        $json->responseHeaders = $this->getResponseHeaders();
        return $json;
    }

    public function getQueueName(): string
    {
        return $this->queueName ?? 'httprequest';
    }

    /**
     * @return string
     */
    public function getDataType(): ?string
    {
        return $this->dataType;
    }

    /**
     * @param string $dataType
     * @return $this
     */
    public function setDataType(?string $dataType): HttpRequestJob
    {
        $this->dataType = $dataType;
        return $this;
    }

    /**
     * @return float
     */
    public function getTimeout(): ?float
    {
        return $this->timeout;
    }

    /**
     * @param float $timeout
     * @return $this
     */
    public function setTimeout(float $timeout): HttpRequestJob
    {
        $this->timeout = $timeout;
        return $this;
    }

    /**
     * @return float
     */
    public function getReadTimeout(): ?float
    {
        return $this->readTimeout;
    }

    /**
     * @param float $readTimeout
     * @return $this
     */
    public function setReadTimeout(float $readTimeout): HttpRequestJob
    {
        $this->readTimeout = $readTimeout;
        return $this;
    }

    /**
     * @return int
     */
    public function getResponseStatusCode(): ?int
    {
        return $this->responseStatusCode;
    }

    /**
     * @param int $httpStatusCode
     * @return $this
     */
    public function setResponseStatusCode(int $httpStatusCode): HttpRequestJob
    {
        $this->responseStatusCode = $httpStatusCode;
        return $this;
    }

    /**
     * @return int
     */
    public function getWebhookId(): ?int
    {
        return $this->webhookId;
    }

    /**
     * @param int $webhookId
     * @return HttpRequestJob
     */
    public function setWebhookId(int $webhookId): HttpRequestJob
    {
        $this->webhookId = $webhookId;
        return $this;
    }

    /**
     * @return string
     */
    public function getResponseReasonPhrase(): string
    {
        return $this->responseReasonPhrase ?? '';
    }

    /**
     * @param string $responseReasonPhrase
     * @return HttpRequestJob
     */
    public function setResponseReasonPhrase(string $responseReasonPhrase): HttpRequestJob
    {
        $this->responseReasonPhrase = $responseReasonPhrase;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSaveResponse(): bool
    {
        return $this->saveResponse;
    }

    /**
     * @param bool $saveResponse
     * @return HttpRequestJob
     */
    public function setSaveResponse(bool $saveResponse): HttpRequestJob
    {
        $this->saveResponse = $saveResponse;
        if ($this->saveResponse) {
            $this->readTimeout = $this->timeout = 0;
        }
        return $this;
    }

    /**
     * @return string[][]
     */
    public function getResponseHeaders(): array
    {
        return $this->responseHeaders ?? [];
    }

    /**
     * @param string[][] $responseHeaders
     * @return HttpRequestJob
     */
    public function setResponseHeaders(array $responseHeaders): HttpRequestJob
    {
        $this->responseHeaders = $responseHeaders;
        return $this;
    }
}
