<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job;

use Doctrine\Instantiator\Exception\InvalidArgumentException;
use JsonSerializable;
use QBNK\JobQueue\Job\Enums\DataProcessingRegion;
use QBNK\JobQueue\Job\Enums\JobPriority;
use QBNK\JobQueue\Job\Enums\JobStatus;
use QBNK\JobQueue\Job\Storage\StorageConfig;
use stdClass;

abstract class JobAbstract implements JsonSerializable, JobInterface
{
    public const DEFAULT_CALLBACK_QUEUE = 'callback';

    protected string $id;

    protected ?int $userId = null;

    protected ?int $sessionId = null;

    protected ?string $customerDomain = null;

    protected JobPriority $priority = JobPriority::Normal;

    protected JobStatus $status = JobStatus::New;

    protected array $blockingJobIds = [];

    /**
     * Usually one of the $blockingJobIds, but can be any string identifier
     * @var string|null
     */
    protected ?string $triggeredByJobId = null;

    /**
     * Success details or error message
     * @var string|null
     */
    protected ?string $message = null;

    /**
     * The name of a callback queue, defaults to "callback" for ordinary callbacks to QBank
     * It might also be a "personal" callback queue for a blocking job
     * @var string|null
     */
    protected ?string $callbackQueue = null;

    /**
     * Class name, if any, that should handle this job once it is processed by callback queue
     * @var string|null
     */
    protected ?string $callbackHandler = null;

    /**
     * Class name, if any, that should prepare this job once its blocking jobs are completed
     * @var string|null
     */
    protected ?string $delayedPrepareHandler = null;

    /**
     * Will usually be hardcoded in extending classes getQueueName method, but may also be set to property for
     * some dynamic multi-use extenders.
     * @var string
     */
    protected string $queueName;

    protected array $callbackParameters = [];

    /**
     * Main storage communication protocol. How to retrieve and store working copies of files to and from the media original repository
     * @var StorageConfig|null
     */
    protected ?StorageConfig $storageConfig = null;

    protected DataProcessingRegion $dataProcessingRegion = DataProcessingRegion::Global;

    public function __construct()
    {
        $this->id = self::generateId();
    }

    public static function generateId(): string
    {
        return strtr(base64_encode(random_bytes(32)), '+/=', '._-');
    }

    /**
     * Array of queue names that should be considered to be blockers for this job
     * @return array
     */
    public static function isBlockedBy(): array
    {
        return [];
    }

    /**
     * @return string The domain of the customer's instance
     * @deprecated Use JobAbstract::getCustomerDomain() instead.
     */
    public function getCustomer(): ?string
    {
        return $this->getCustomerDomain();
    }

    /**
     * @return string The domain of the customer's instance
     */
    public function getCustomerDomain(): ?string
    {
        return $this->customerDomain;
    }

    /**
     * @param string $customerDomain The domain of the customer's instance
     * @return $this
     */
    public function setCustomerDomain(string $customerDomain): static
    {
        $this->customerDomain = $customerDomain;
        return $this;
    }

    /**
     * @param string $customer The domain of the customer's instance
     * @deprecated Use JobAbstract::setCustomerDomain() instead.
     * @return $this
     */
    public function setCustomer(string $customer): static
    {
        return $this->setCustomerDomain($customer);
    }

    public function setQueueName(string $queueName): static
    {
        $this->queueName = $queueName;
        return $this;
    }

    public function addBlockingJobId(string $blockingJobId): static
    {
        $this->blockingJobIds[] = $blockingJobId;
        return $this;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = new stdClass();
        $json->id = $this->getId();
        $json->userId = $this->getUserId();
        $json->sessionId = $this->getSessionId();
        $json->class = get_class($this);
        $json->priority = $this->getPriority();
        $json->status = $this->getStatus();
        $json->queueName = $this->getQueueName();
        $json->message = $this->getMessage();
        $json->customerDomain = $this->getCustomerDomain();
        $json->storageConfig = $this->getStorageConfig();
        $json->callbackQueue = $this->getCallbackQueue();
        $json->callbackHandler = $this->getCallbackHandler();
        $json->delayedPrepareHandler = $this->getDelayedPrepareHandler();
        $json->callbackParameters = $this->getCallbackParameters();
        $json->blockingJobIds = $this->getBlockingJobIds();
        $json->triggeredByJobId = $this->getTriggeredByJobId();
        $json->dataProcessingRegion = $this->getDataProcessingRegion();
        return $json;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): static
    {
        $this->userId = $userId;
        return $this;
    }

    public function getSessionId(): ?int
    {
        return $this->sessionId;
    }

    public function setSessionId(int $sessionId): static
    {
        $this->sessionId = $sessionId;
        return $this;
    }

    public function getPriority(): JobPriority
    {
        return $this->priority;
    }

    public function setPriority(JobPriority $priority): static
    {
        $this->priority = $priority;
        return $this;
    }

    public function getStatus(): JobStatus
    {
        return $this->status;
    }

    public function setStatus(JobStatus $status): static
    {
        $this->status = $status;
        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): static
    {
        $this->message = $message;
        return $this;
    }

    public function getStorageConfig(): ?StorageConfig
    {
        return $this->storageConfig;
    }

    public function setStorageConfig(StorageConfig $config): static
    {
        $this->storageConfig = $config;
        return $this;
    }

    public function getCallbackQueue(): ?string
    {
        return $this->callbackQueue;
    }

    public function setCallbackQueue(?string $callbackQueue): static
    {
        $this->callbackQueue = $callbackQueue;
        return $this;
    }

    public function getCallbackHandler(): ?string
    {
        return $this->callbackHandler;
    }

    public function setCallbackHandler(?string $callbackHandler): static
    {
        $this->callbackHandler = $callbackHandler;
        return $this;
    }

    public function getDelayedPrepareHandler(): ?string
    {
        return $this->delayedPrepareHandler;
    }

    public function setDelayedPrepareHandler(string $delayedPrepareHandler): static
    {
        $this->delayedPrepareHandler = $delayedPrepareHandler;
        return $this;
    }

    public function getCallbackParameters(): array
    {
        return $this->callbackParameters;
    }

    public function setCallBackParameters(array $parameters): static
    {
        $this->callbackParameters = $parameters;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getBlockingJobIds(): array
    {
        return $this->blockingJobIds;
    }

    /**
     * @param string[] $blockingJobIds
     */
    public function setBlockingJobIds(array $blockingJobIds): static
    {
        $this->blockingJobIds = $blockingJobIds;
        return $this;
    }

    public function getTriggeredByJobId(): ?string
    {
        return $this->triggeredByJobId;
    }

    public function setTriggeredByJobId(?string $identifier): static
    {
        $this->triggeredByJobId = $identifier;
        return $this;
    }

    public function getDataProcessingRegion(): DataProcessingRegion
    {
        return $this->dataProcessingRegion;
    }

    public function setDataProcessingRegion(DataProcessingRegion $region): static
    {
        $this->dataProcessingRegion = $region;
        return $this;
    }

    /**
     * Reset a job, erasing any results from previous execution and making it
     * suitable to be added back to queue as a new job with an identical payload
     * @return JobAbstract Returns a "cloned" JobAbstract
     * @throws JobQueueException
     */
    public function reset(): static
    {
        $jobSerialized = json_decode(json_encode($this), true);
        $jobSerialized['id'] = $this::generateId();
        $jobSerialized['status'] = JobStatus::New;
        $jobSerialized['message'] = null;
        $jobSerialized['blockingJobIds'] = [];
        $jobSerialized['callbackQueue'] = null;

        return self::fromArray($jobSerialized);
    }

    /**
     * @param array $parameters
     * @return static
     * @throws JobQueueException
     */
    public static function fromArray(array $parameters, bool $assignId = false): static
    {
        try {
            if (isset($parameters['storageConfig']) && !$parameters['storageConfig'] instanceof StorageConfig) {
                $parameters['storageConfig'] = StorageConfig::fromArray($parameters['storageConfig']);
            }

            $instance = InstanceHelper::fromArray($parameters);

            if ($instance instanceof JobAbstract && (!empty($parameters['id']) || $assignId)) {
                $instance->id = $parameters['id'] ?? self::generateId();
            }
            return $instance;
        } catch (InvalidArgumentException $iie) {
            throw new JobQueueException('Could not instantiate job: ' . $iie->getMessage());
        }
    }
}
