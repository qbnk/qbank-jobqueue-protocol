<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Integration;

class DeployNewVersionJob extends DeployJob
{
    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? 'integration_deploy_new_version_job';
    }
}
