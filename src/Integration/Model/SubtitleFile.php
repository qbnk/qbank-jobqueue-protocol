<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Integration\Model;

use JsonSerializable;
use stdClass;

class SubtitleFile implements JsonSerializable
{
    protected int $id;

    protected int $mediaId;

    protected string $path;

    protected string $language;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getMediaId(): int
    {
        return $this->mediaId;
    }

    public function setMediaId(int $mediaId): self
    {
        $this->mediaId = $mediaId;
        return $this;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;
        return $this;
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;
        return $this;
    }

    public function jsonSerialize(): stdClass
    {
        $json = new stdClass();
        $json->id = $this->getId();
        $json->mediaId = $this->getMediaId();
        $json->path = $this->getPath();
        $json->language = $this->getLanguage();
        return $json;
    }

    public static function fromArray(array $parameters): SubtitleFile
    {
        $subtitleFile = new static();
        foreach (['id', 'mediaId', 'path', 'language'] as $param) {
            if (isset($parameters[$param])) {
                $subtitleFile->{'set' . $param}($parameters[$param]);
            }
        }
        return $subtitleFile;
    }
}
