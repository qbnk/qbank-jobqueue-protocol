<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Integration\Model;

use JsonSerializable;
use stdClass;

class SubChannel implements JsonSerializable
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $definition;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): SubChannel
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return array
     */
    public function getDefinition(): array
    {
        return $this->definition ?? [];
    }

    /**
     * @param array $definition
     * @return $this
     */
    public function setDefinition(array $definition): SubChannel
    {
        $this->definition = $definition;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): SubChannel
    {
        $this->name = $name;
        return $this;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = new stdClass();
        $json->id = $this->getId();
        $json->name = $this->getName();
        $json->definition = $this->getDefinition();
        return $json;
    }

    public static function fromArray(array $parameters): SubChannel
    {
        return (new self())
            ->setId($parameters['id'])
            ->setName($parameters['name'])
            ->setDefinition($parameters['definition'] ?? [])
        ;
    }
}
