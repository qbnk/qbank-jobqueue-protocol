<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Integration\Model;

use JsonSerializable;
use stdClass;

class ImportFile implements JsonSerializable
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $filename;

    /**
     * @var array
     */
    protected $metadata;

    /**
     * @var string
     */
    protected $pageUrl;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): ImportFile
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     * @return $this
     */
    public function setFilename(string $filename): ImportFile
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return array
     */
    public function getMetadata(): array
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     * @return $this
     */
    public function setMetadata(array $metadata): ImportFile
    {
        $this->metadata = $metadata;
        return $this;
    }

    /**
     * @return string
     */
    public function getPageUrl(): string
    {
        return $this->pageUrl;
    }

    /**
     * @param string $pageUrl
     * @return $this
     */
    public function setPageUrl(string $pageUrl): ImportFile
    {
        $this->pageUrl = $pageUrl;
        return $this;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = new stdClass();
        $json->name = $this->getName();
        $json->filename = $this->getFilename();
        $json->metadata = $this->getMetadata();
        $json->pageUrl = $this->getPageUrl();
        return $json;
    }

    public static function fromArray(array $parameters): ImportFile
    {
        return (new self())
            ->setName($parameters['name'])
            ->setFilename($parameters['filename'])
            ->setMetadata($parameters['metadata'])
            ->setPageUrl($parameters['pageUrl'])
        ;
    }
}
