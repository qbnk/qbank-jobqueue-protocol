<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Integration\Model;

use DateTime;
use Exception;
use JsonSerializable;
use QBNK\JobQueue\Job\JobQueueException;
use QBNK\JobQueue\Job\Storage\CompressedFile;
use stdClass;

/**
 * Represents a MediaUsage of a Media in QBank.
 * @author Kristian Sandström
 * @since 2016-04-05
 */
class MediaUsage implements JsonSerializable
{
    /**
     * The id of the media usage
     * @var int
     */
    protected $id;

    /**
     * The session ID used to create this MediaUsage
     * @var int
     */
    protected $sessionId;

    /**
     * @var int
     */
    protected $mediaId;

    /**
     * Optional SocialMedia ID that this usage has originated through
     * @var int
     */
    protected $integrationId;

    /**
     * The URL to the media on the remote site
     * @var string
     */
    protected $mediaUrl;

    /**
     * The URL to the page where the media may be visible
     * @var string
     */
    protected $pageUrl;

    /**
     * A JSON containing any semi-defined contextual data relevant for the usage
     * Suggested keys:
     *  * localData: Any relevant external ID's or such that a third party may want to track if the usage was created externally
     *  * cropCoords: If the usage is a cropped version of the media the coords are saved here
     *  * cropId: A unique but recreatable string representation of the media with it's cropping parameters.
     *        Used to identify a duplicate crop and enable reuse of an existing version instead of creating a duplicate
     *  * createdByName: Name of whoever used the media. Useful for contacting in case of expired rights
     *  * createdByEmail: E-mail of whoever used the media. Useful for contacting in case of expired rights
     *  * pageTitle: When used in some sort of CMS environment a the page title may be supplied
     * @var array
     */
    protected $context;

    /**
     * The time this usage was added
     * @var DateTime
     */
    protected $created;

    /**
     * The time this usage was updated
     * @var DateTime
     */
    protected $updated;

    /**
     * The time this usage was deleted
     * @var DateTime
     */
    protected $deleted;

    /**
     * Main language (three letter ISO) of the environment of the usage. Defaults to "eng"
     * @var string
     */
    protected $language;

    /**
     * Last QBank {@link User} ID that updated this row
     * @var int
     */
    protected $updatedBy;

    /**
     * Get the contextual data for this usage
     * @param string $key Optional context key to get through "dot access". Returns null on "not found"
     * @param mixed $default Default value when $key was specified and not found
     * @return mixed
     */
    public function getContext($key = null, $default = null)
    {
        return ($key ? $this->dotAccess($key, $this->context, $default) : $this->context);
    }

    /**
     * @param $key
     * @param array $data
     * @param null $default
     * @return array|mixed|null
     */
    private function dotAccess($key, array $data, $default = null)
    {
        if (!is_string($key) || empty($key) || !count($data)) {
            return $default;
        }

        if (strpos($key, '.') !== false) {
            $keys = explode('.', $key);

            foreach ($keys as $innerKey) {
                if (!is_array($data) || !array_key_exists($innerKey, $data)) {
                    return $default;
                }

                $data = $data[$innerKey];
            }

            return $data;
        }

        return array_key_exists($key, $data) ? $data[$key] : $default;
    }

    /**
     * Set a JSON containing any semi-defined contextual data relevant for the usage
     * Suggested keys:
     *  * localData: Any relevant external ID's or such that a third party may want to track if the usage was created externally
     *  * cropCoords: If the usage is a cropped version of the media the coords are saved here
     *  * cropId: A unique but recreatable string representation of the media with it's cropping parameters.
     *        Used to identify a duplicate crop and enable reuse of an existing version instead of creating a duplicate
     *  * createdByName: Name of whoever used the media. Useful for contacting in case of expired rights
     *  * createdByEmail: E-mail of whoever used the media. Useful for contacting in case of expired rights
     *  * pageTitle: When used in some sort of CMS environment a the page title may be supplied
     * @param array $context
     * @return $this
     */
    public function setContext(array $context): MediaUsage
    {
        $this->context = $context;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): MediaUsage
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getSessionId(): int
    {
        return $this->sessionId;
    }

    /**
     * @param int $sessionId
     * @return $this
     */
    public function setSessionId(int $sessionId): MediaUsage
    {
        $this->sessionId = $sessionId;
        return $this;
    }

    /**
     * @return int
     */
    public function getMediaId(): int
    {
        return $this->mediaId;
    }

    /**
     * @param int $mediaId
     * @return $this
     */
    public function setMediaId(int $mediaId): MediaUsage
    {
        $this->mediaId = $mediaId;
        return $this;
    }

    /**
     * @return int
     */
    public function getIntegrationId(): int
    {
        return $this->integrationId;
    }

    /**
     * @param int $integrationId
     * @return $this
     */
    public function setIntegrationId(int $integrationId): MediaUsage
    {
        $this->integrationId = $integrationId;
        return $this;
    }

    /**
     * @return string
     */
    public function getMediaUrl(): ?string
    {
        return $this->mediaUrl;
    }

    /**
     * @param string $mediaUrl
     * @return $this
     */
    public function setMediaUrl(string $mediaUrl): MediaUsage
    {
        $this->mediaUrl = $mediaUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getPageUrl(): ?string
    {
        return $this->pageUrl;
    }

    /**
     * @param string $pageUrl
     * @return $this
     */
    public function setPageUrl(string $pageUrl): MediaUsage
    {
        $this->pageUrl = $pageUrl;
        return $this;
    }

    /**
     * @param string|array|DateTime $dateTime
     * @return DateTime
     * @throws Exception
     */
    protected function interpretDate($dateTime)
    {
        if (is_array($dateTime)) {
            return CompressedFile::dateTimeFromArray($dateTime);
        }
        if (is_string($dateTime)) {
            return new DateTime($dateTime);
        }
        return $dateTime;
    }

    /**
     * @return DateTime
     */
    public function getCreated(): DateTime
    {
        return $this->created;
    }

    /**
     * @param DateTime|array $created
     * @return $this
     */
    public function setCreated($created): MediaUsage
    {
        try {
            $this->created = $this->interpretDate($created);
        } catch (Exception $e) {
            throw new JobQueueException('Faulty date passed to MediaUsage->setCreated: ' . json_encode($created));
        }
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdated(): ?DateTime
    {
        return $this->updated;
    }

    /**
     * @param DateTime|array $updated
     * @return $this
     */
    public function setUpdated($updated): MediaUsage
    {
        try {
            $this->updated = $this->interpretDate($updated);
        } catch (Exception $e) {
            throw new JobQueueException('Faulty date passed to MediaUsage->setUpdated: ' . json_encode($updated));
        }
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDeleted(): ?DateTime
    {
        return $this->deleted;
    }

    /**
     * @param DateTime|array $deleted
     * @return $this
     */
    public function setDeleted($deleted): MediaUsage
    {
        try {
            $this->deleted = $this->interpretDate($deleted);
        } catch (Exception $e) {
            throw new JobQueueException('Faulty date passed to MediaUsage->setDeleted: ' . json_encode($deleted));
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return $this
     */
    public function setLanguage(string $language): MediaUsage
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return int
     */
    public function getUpdatedBy(): ?int
    {
        return $this->updatedBy;
    }

    /**
     * @param int $updatedBy
     * @return $this
     */
    public function setUpdatedBy(int $updatedBy): MediaUsage
    {
        $this->updatedBy = $updatedBy;
        return $this;
    }

    public static function fromArray(array $parameters): MediaUsage
    {
        $mediaUsage = new static();
        foreach (get_object_vars($mediaUsage) as $param => $v) {
            if (isset($parameters[$param])) {
                $mediaUsage->{'set' . $param}($parameters[$param]);
            }
        }
        return $mediaUsage;
    }

    /**
     * Provides a JSON representation of the MediaUsage.
     * @return stdClass
     */
    public function jsonSerialize(): \stdClass
    {
        $json = new stdClass();
        $json->id = $this->getId();
        $json->sessionId = $this->getSessionId();
        $json->created = $this->getCreated();
        $json->updated = $this->getUpdated();
        $json->deleted = $this->getDeleted();
        $json->pageUrl = $this->getPageUrl();
        $json->mediaUrl = $this->getMediaUrl();
        $json->context = $this->getContext();
        $json->mediaId = $this->getMediaId();
        $json->language = $this->getLanguage();
        $json->updatedBy = $this->getUpdatedBy();
        $json->integrationId = $this->getIntegrationId();
        return $json;
    }

    /**
     * Provides a JSON representation of the media suitable for audits.
     * @return stdClass
     */
    public function jsonAuditSerialize()
    {
        $json = new stdClass();
        $json->id = $this->getId();
        $json->pageUrl = $this->getPageUrl();
        $json->context = $this->getContext();
        return $json;
    }
}
