<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Integration\Model;

use JsonSerializable;
use stdClass;

class Statistic implements JsonSerializable
{
    /**
     * @var int
     */
    protected $mediaId;

    /**
     * @var array
     */
    protected $total;

    /**
     * @var array
     */
    protected $details;

    /**
     * @return int
     */
    public function getMediaId(): int
    {
        return $this->mediaId;
    }

    /**
     * @param int $mediaId
     * @return $this
     */
    public function setMediaId(int $mediaId): Statistic
    {
        $this->mediaId = $mediaId;
        return $this;
    }

    /**
     * @return array
     */
    public function getTotal(): array
    {
        return $this->total;
    }

    /**
     * @param array $total
     * @return $this
     */
    public function setTotal(array $total): Statistic
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return array
     */
    public function getDetails(): ?array
    {
        return $this->details;
    }

    /**
     * @param array $details
     * @return $this
     */
    public function setDetails(array $details): Statistic
    {
        $this->details = $details;
        return $this;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = new stdClass();
        $json->mediaId = $this->getMediaId();
        $json->total = $this->getTotal();
        $json->details = $this->getDetails();
        return $json;
    }

    public static function fromArray(array $parameters): Statistic
    {
        return (new self())
            ->setMediaId($parameters['mediaId'])
            ->setTotal($parameters['total'] ?? [])
            ->setDetails($parameters['details'] ?? [])
        ;
    }
}
