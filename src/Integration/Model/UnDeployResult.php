<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Integration\Model;

use JsonSerializable;
use stdClass;

class UnDeployResult implements JsonSerializable
{
    /**
     * @var MediaUsage
     */
    protected $usage;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var bool
     */
    protected $success;

    /**
     * @return MediaUsage
     */
    public function getUsage(): MediaUsage
    {
        return $this->usage;
    }

    /**
     * @param MediaUsage $usage
     * @return $this
     */
    public function setUsage(MediaUsage $usage): UnDeployResult
    {
        $this->usage = $usage;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage(string $message = null): UnDeployResult
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success ?? false;
    }

    /**
     * @param bool $success
     * @return $this
     */
    public function setSuccess(bool $success): UnDeployResult
    {
        $this->success = $success;
        return $this;
    }


    public function jsonSerialize(): \stdClass
    {
        $json = new stdClass();
        $json->usage = $this->getUsage();
        $json->message = $this->getMessage();
        $json->success = $this->isSuccess();
        return $json;
    }

    public static function fromArray(array $parameters): UnDeployResult
    {
        return (new self())
            ->setUsage(
                is_array($parameters['usage']) ? MediaUsage::fromArray($parameters['usage']) : $parameters['usage']
            )
            ->setMessage($parameters['message'] ?? null)
            ->setSuccess($parameters['success'] ?? false)
        ;
    }
}
