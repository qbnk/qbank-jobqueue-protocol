<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Integration\Model;

use JsonSerializable;
use stdClass;

class IntegratedFile implements JsonSerializable
{
    /**
     * @var bool
     */
    protected $success;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var array
     */
    protected $payload;

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return $this->payload ?? [];
    }

    /**
     * @param array $payload
     * @return $this
     */
    public function setPayload(array $payload): IntegratedFile
    {
        $this->payload = $payload;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success ?? false;
    }

    /**
     * @param bool $success
     * @return $this
     */
    public function setSuccess(bool $success): IntegratedFile
    {
        $this->success = $success;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage(string $message): IntegratedFile
    {
        $this->message = $message;
        return $this;
    }


    public function jsonSerialize(): \stdClass
    {
        $json = new stdClass();
        $json->success = $this->isSuccess();
        $json->message = $this->getMessage();
        $json->payload = $this->getPayload();
        return $json;
    }

    public static function fromArray(array $parameters): IntegratedFile
    {
        return (new self())
            ->setSuccess($parameters['success'] ?? false)
            ->setMessage($parameters['message'] ?? '')
            ->setPayload($parameters['payload'] ?? [])
        ;
    }
}
