<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Integration;

use QBNK\JobQueue\Job\Deploy\Protocol\ProtocolAbstract;
use QBNK\JobQueue\Job\Integration\Model\SubChannel;
use QBNK\JobQueue\Job\JobAbstract;

abstract class IntegrationJobAbstract extends JobAbstract
{
    protected int $integrationId;

    protected string $authData;

    protected array $jobParameters = [];

    /**
     * Optional storage protocol to use when some file output destination is elsewhere than the main media storage.
     * Integration equivalent of DeployJob::$protocol
     * @var ProtocolAbstract|null
     */
    protected ?ProtocolAbstract $publishProtocol = null;

    /**
     * @var SubChannel[]
     */
    protected array $subChannels = [];

    public function getIntegrationId(): int
    {
        return $this->integrationId;
    }

    public function setIntegrationId(int $integrationId): IntegrationJobAbstract
    {
        $this->integrationId = $integrationId;
        return $this;
    }

    public function getAuthData(): string
    {
        return $this->authData;
    }

    public function setAuthData(string $authData): IntegrationJobAbstract
    {
        $this->authData = $authData;
        return $this;
    }

    public function getJobParameters(): array
    {
        return $this->jobParameters;
    }

    public function setJobParameters(array $jobParameters): IntegrationJobAbstract
    {
        $this->jobParameters = $jobParameters;
        return $this;
    }

    /**
     * @return SubChannel[]
     */
    public function getSubChannels(): array
    {
        return $this->subChannels;
    }

    /**
     * @param SubChannel[] $subChannels
     * @return $this
     */
    public function setSubChannels(array $subChannels): IntegrationJobAbstract
    {
        foreach ($subChannels as &$subChannel) {
            if (is_array($subChannel)) {
                $subChannel = SubChannel::fromArray($subChannel);
            }
        }
        unset($subChannel);
        $this->subChannels = $subChannels;
        return $this;
    }

    public function getPublishProtocol(): ?ProtocolAbstract
    {
        return $this->publishProtocol;
    }

    /**
     * @param array|ProtocolAbstract $publishProtocol
     * @return $this
     */
    public function setPublishProtocol($publishProtocol): static
    {
        if (!($publishProtocol instanceof ProtocolAbstract) && !is_array($publishProtocol)) {
            throw new \TypeError(
                sprintf(
                    'Incorrect type, ProtocolAbstract or array expected, got %s instead',
                    json_encode($publishProtocol)
                )
            );
        }

        if (is_array($publishProtocol)) {
            $publishProtocol = ProtocolAbstract::fromArray($publishProtocol);
        }

        $this->publishProtocol = $publishProtocol;
        return $this;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->integrationId = $this->getIntegrationId();
        $json->authData = $this->getAuthData();
        $json->jobParameters = $this->getJobParameters();
        $json->subChannels = $this->getSubChannels();
        $json->publishProtocol = $this->getPublishProtocol();
        return $json;
    }
}
