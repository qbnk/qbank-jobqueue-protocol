<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Integration;

use QBNK\JobQueue\Job\Convert\Model\TargetAbstract;
use QBNK\JobQueue\Job\Integration\Model\IntegratedFile;
use QBNK\JobQueue\Job\Integration\Model\MediaUsage;
use QBNK\JobQueue\Job\Integration\Model\SubtitleFile;
use QBNK\JobQueue\Job\JobQueueException;
use QBNK\JobQueue\Job\Storage\File;

class DeployJob extends IntegrationJobAbstract
{
    /**
     * @var File[]
     */
    protected $sourceFiles;

    /**
     * Optional targets to convert source into
     * @var TargetAbstract[]
     */
    protected $convertTargets;

    /**
     * @var IntegratedFile[][]
     */
    protected $integratedFiles;

    /**
     * @var SubtitleFile[]
     */
    protected $subtitleFiles;

    /**
     * @var MediaUsage[]
     */
    protected $mediaUsages;

    /**
     * Adds a source with optional properties.
     * @param int $mediaId Optional mediaId to associate the properties and source with
     * @param File $file
     * @return $this
     */
    public function addSourceFile(int $mediaId, File $file)
    {
        if ($mediaId) {
            $this->sourceFiles[$mediaId] = $file;
        } else {
            $this->sourceFiles[] = $file;
        }

        return $this;
    }

    /**
     * @return File[]
     */
    public function getSourceFiles(): array
    {
        return $this->sourceFiles;
    }

    /**
     * @param File[] $sourceFiles
     * @return $this
     */
    public function setSourceFiles(array $sourceFiles): DeployJob
    {
        foreach ($sourceFiles as &$file) {
            if (is_array($file)) {
                $file = File::fromArray($file);
            }
        }
        $this->sourceFiles = $sourceFiles;
        return $this;
    }

    /**
     * @param TargetAbstract $target
     * @return $this
     */
    public function addConvertTarget(TargetAbstract $target)
    {
        $this->convertTargets[] = $target;
        return $this;
    }

    /**
     * @return TargetAbstract[]
     */
    public function getConvertTargets(): array
    {
        return $this->convertTargets ?? [];
    }

    /**
     * @param TargetAbstract[] $convertTargets
     * @return $this
     * @throws JobQueueException
     */
    public function setConvertTargets(array $convertTargets): DeployJob
    {
        foreach ($convertTargets as &$target) {
            if (is_array($target)) {
                $target = TargetAbstract::fromArray($target);
            }
        }
        unset($target);
        $this->convertTargets = $convertTargets;
        return $this;
    }

    /**
     * @return IntegratedFile[][]
     */
    public function getIntegratedFiles(): array
    {
        return $this->integratedFiles ?? [];
    }

    /**
     * @param IntegratedFile[][] $integratedFiles
     * @return $this
     */
    public function setIntegratedFiles(array $integratedFiles): DeployJob
    {
        foreach ($integratedFiles as $mediaId => &$files) {
            foreach ($files as &$file) {
                if (is_array($file)) {
                    $file = IntegratedFile::fromArray($file);
                }
            }
        }
        $this->integratedFiles = $integratedFiles;
        return $this;
    }

    /**
     * @return SubtitleFile[]
     */
    public function getSubtitleFiles(): array
    {
        return $this->subtitleFiles ?? [];
    }

    /**
     * @param SubtitleFile[] $subtitleFiles
     *
     * @return $this
     */
    public function setSubtitleFiles(array $subtitleFiles): DeployJob
    {
        foreach ($subtitleFiles as &$file) {
            if (is_array($file)) {
                $file = SubtitleFile::fromArray($file);
            }
        }

        $this->subtitleFiles = $subtitleFiles;

        return $this;
    }

    /**
     * @return MediaUsage[]
     */
    public function getMediaUsages(): array
    {
        return $this->mediaUsages ?? [];
    }

    /**
     * @param MediaUsage[] $mediaUsages
     * @return $this
     */
    public function setMediaUsages(array $mediaUsages)
    {
        foreach ($mediaUsages as &$mediaUsage) {
            if (is_array($mediaUsage)) {
                $mediaUsage = MediaUsage::fromArray($mediaUsage);
            }
        }
        $this->mediaUsages = $mediaUsages;
        return $this;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? 'integration_deploy_job';
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->sourceFiles = $this->getSourceFiles();
        $json->convertTargets = $this->getConvertTargets();
        $json->integratedFiles = $this->getIntegratedFiles();
        $json->subtitleFiles = $this->getSubtitleFiles();
        $json->mediaUsages = $this->getMediaUsages();

        return $json;
    }

    public static function isBlockedBy(): array
    {
        return \QBNK\JobQueue\Job\DeployJob::isBlockedBy();
    }
}
