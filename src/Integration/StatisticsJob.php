<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Integration;

use DateTime;
use QBNK\JobQueue\Job\Integration\Model\Statistic;
use QBNK\JobQueue\Job\Storage\CompressedFile;

class StatisticsJob extends IntegrationJobAbstract
{
    /**
     * @var array
     */
    protected $context;

    /**
     * @var int[]
     */
    protected $mediaIds;

    /**
     * @var DateTime
     */
    protected $startDate;

    /**
     * @var DateTime
     */
    protected $endDate;

    /**
     * @var Statistic[]
     */
    protected $statistics;

    /**
     * @return array
     */
    public function getContext(): array
    {
        return $this->context ?? [];
    }

    /**
     * @param array $context
     * @return $this
     */
    public function setContext(array $context): StatisticsJob
    {
        $this->context = $context;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getStartDate(): DateTime
    {
        return $this->startDate;
    }

    /**
     * @param DateTime|array $startDate
     * @return $this
     */
    public function setStartDate($startDate): StatisticsJob
    {
        $this->startDate = is_array($startDate) ? CompressedFile::dateTimeFromArray($startDate) : $startDate;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getEndDate(): DateTime
    {
        return $this->endDate;
    }

    /**
     * @param DateTime|array $endDate
     * @return $this
     */
    public function setEndDate($endDate): StatisticsJob
    {
        $this->endDate = is_array($endDate) ? CompressedFile::dateTimeFromArray($endDate) : $endDate;
        return $this;
    }

    /**
     * @return Statistic[]
     */
    public function getStatistics(): array
    {
        return $this->statistics ?? [];
    }

    /**
     * @param Statistic[] $statistics
     * @return $this
     */
    public function setStatistics(array $statistics): StatisticsJob
    {
        foreach ($statistics as &$statistic) {
            if (is_array($statistic)) {
                $statistic = Statistic::fromArray($statistic);
            }
        }
        $this->statistics = $statistics;
        return $this;
    }

    /**
     * @return int[]
     */
    public function getMediaIds(): array
    {
        return $this->mediaIds;
    }

    /**
     * @param int[] $mediaIds
     * @return $this
     */
    public function setMediaIds(array $mediaIds): StatisticsJob
    {
        $this->mediaIds = $mediaIds;
        return $this;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? 'integration_statistics_job';
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->mediaIds = $this->getMediaIds();
        $json->context = $this->getContext();
        $json->startDate = $this->getStartDate();
        $json->endDate = $this->getEndDate();
        $json->statistics = $this->getStatistics();
        return $json;
    }
}
