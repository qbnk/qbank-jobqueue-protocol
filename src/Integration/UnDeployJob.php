<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Integration;

use QBNK\JobQueue\Job\Integration\Model\MediaUsage;
use QBNK\JobQueue\Job\Integration\Model\UnDeployResult;

class UnDeployJob extends IntegrationJobAbstract
{
    /**
     * @var MediaUsage[]
     */
    protected $mediaUsages;

    /**
     * @var UnDeployResult[]
     */
    protected $result;

    /**
     * @return MediaUsage[]
     */
    public function getMediaUsages(): array
    {
        return $this->mediaUsages;
    }

    /**
     * @param MediaUsage[] $mediaUsages
     * @return $this
     */
    public function setMediaUsages(array $mediaUsages)
    {
        foreach ($mediaUsages as &$mediaUsage) {
            if (is_array($mediaUsage)) {
                $mediaUsage = MediaUsage::fromArray($mediaUsage);
            }
        }
        $this->mediaUsages = $mediaUsages;
        return $this;
    }

    /**
     * @param $mediaUsage
     * @return $this
     */
    public function addMediaUsage($mediaUsage)
    {
        $this->mediaUsages[] = $mediaUsage;
        return $this;
    }

    /**
     * @return UnDeployResult[]
     */
    public function getResult(): array
    {
        return $this->result ?? [];
    }

    /**
     * @param UnDeployResult[] $results
     * @return $this
     */
    public function setResult(array $results): UnDeployJob
    {
        // Unpack any results in array form, in place
        array_walk($results, function (&$result) {
            if (is_array($result)) {
                $result = UnDeployResult::fromArray($result);
            }
        });

        $this->result = $results;
        return $this;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? 'integration_undeploy_job';
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->mediaUsages = $this->getMediaUsages();
        $json->result = $this->getResult();
        return $json;
    }
}
