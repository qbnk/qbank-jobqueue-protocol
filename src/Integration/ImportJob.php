<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Integration;

use QBNK\JobQueue\Job\Integration\Model\ImportFile;

class ImportJob extends IntegrationJobAbstract
{
    /**
     * @var array
     */
    protected $externalIds;

    /**
     * @var array
     */
    protected $filters;

    /**
     * @var ImportFile[]
     */
    protected $importedFiles;

    /**
     * @return array
     */
    public function getExternalIds(): array
    {
        return $this->externalIds ?? [];
    }

    /**
     * @param array $externalIds
     * @return $this
     */
    public function setExternalIds(array $externalIds): ImportJob
    {
        $this->externalIds = $externalIds;
        return $this;
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return $this->filters ?? [];
    }

    /**
     * @param array $filters
     * @return $this
     */
    public function setFilters(array $filters): ImportJob
    {
        $this->filters = $filters;
        return $this;
    }

    /**
     * @return ImportFile[]
     */
    public function getImportedFiles(): array
    {
        return $this->importedFiles ?? [];
    }

    /**
     * @param ImportFile[] $importedFiles
     * @return $this
     */
    public function setImportedFiles(array $importedFiles): ImportJob
    {
        foreach ($importedFiles as &$file) {
            if (is_array($file)) {
                $file = ImportFile::fromArray($file);
            }
        }
        $this->importedFiles = $importedFiles;
        return $this;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? 'integration_import_job';
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->externalIds = $this->getExternalIds();
        $json->filters = $this->getFilters();
        $json->importedFiles = $this->getImportedFiles();
        return $json;
    }
}
