<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job;

use QBNK\JobQueue\Job\Convert\AudioThumbnailJob;
use QBNK\JobQueue\Job\Convert\FFmpegJob;
use QBNK\JobQueue\Job\Convert\ImageConvertJob;
use QBNK\JobQueue\Job\Convert\PdfExtractPagesJob;
use QBNK\JobQueue\Job\Convert\VideoThumbnailJob;
use QBNK\JobQueue\Job\Deploy\DeployMedia;
use QBNK\JobQueue\Job\Deploy\Protocol\ProtocolAbstract;
use QBNK\JobQueue\Job\Enums\JobStatus;
use QBNK\JobQueue\Job\Examination\ExifToolJob;
use QBNK\JobQueue\Job\Examination\MimeTypeJob;
use QBNK\JobQueue\Job\Storage\CombineChunksJob;
use TypeError;

class DeployJob extends JobAbstract
{
    /** @var DeployMedia[] */
    protected array $sources = [];

    protected ?ProtocolAbstract $protocol = null;

    protected ?int $deploymentSiteId = null;

    protected array $mediaIds = [];

    protected array $metadata = [];

    /**
     * @param DeployMedia[] $sources
     * @return $this
     */
    public function setSources(array $sources): DeployJob
    {
        $this->sources = [];
        foreach ($sources as $source) {
            if (is_array($source)) {
                $source = DeployMedia::fromArray($source);
            }

            if ($source instanceof DeployMedia) {
                $this->sources[] = $source;
            }
        }
        return $this;
    }

    public function addSource(DeployMedia $source): DeployJob
    {
        $this->sources[] = $source;
        return $this;
    }

    /**
     * @return DeployMedia[]
     */
    public function getSources(): array
    {
        return $this->sources;
    }

    /**
     * @param ProtocolAbstract|array $protocol
     * @return $this
     */
    public function setProtocol($protocol): DeployJob
    {
        if (!($protocol instanceof ProtocolAbstract) && !is_array($protocol)) {
            throw new TypeError(
                sprintf('Incorrect type, ProtocolAbstract or array expected, got %s instead', json_encode($protocol))
            );
        }

        if (is_array($protocol)) {
            $protocol = ProtocolAbstract::fromArray($protocol);
        }

        $this->protocol = $protocol;
        return $this;
    }

    public function getProtocol(): ?ProtocolAbstract
    {
        return $this->protocol;
    }

    public function setDeploymentSiteId(int $deploymentSiteId): DeployJob
    {
        $this->deploymentSiteId = $deploymentSiteId;
        return $this;
    }

    public function getDeploymentSiteId(): ?int
    {
        return $this->deploymentSiteId;
    }

    /**
     * @return int[]
     */
    public function getMediaIds(): array
    {
        if (!empty($this->mediaIds)) {
            return $this->mediaIds;
        }
        foreach ($this->sources as $source) {
            $this->mediaIds[] = $source->getMediaId();
        }

        return $this->mediaIds;
    }

    /**
     * @param int[] $mediaIds
     */
    public function setMediaIds(array $mediaIds): DeployJob
    {
        $this->mediaIds = $mediaIds;

        return $this;
    }

    public function getMetadata(): array
    {
        return $this->metadata;
    }

    public function setMetadata(array $metadata): DeployJob
    {
        $this->metadata = $metadata;

        return $this;
    }

    public function getQueueName(): string
    {
        return $this->queueName ?? 'deploy';
    }

    public function jsonSerialize(): \stdClass
    {
        $data = parent::jsonSerialize();
        $data->protocol = $this->getProtocol();
        $data->deploymentSiteId = $this->getDeploymentSiteId();
        $data->mediaIds = $this->getMediaIds();
        $data->sources = $this->getSources();
        $data->metadata = $this->getMetadata();
        return $data;
    }

    public static function fromArray(array $parameters, bool $assignId = false): static
    {
        /** @var DeployJob $instance */
        $instance = parent::fromArray($parameters, $assignId);

        if (isset($parameters['sources'])) {
            $sources = [];
            foreach ($parameters['sources'] as $source) {
                if ($source instanceof DeployMedia) {
                    $sources[] = $source;
                } else {
                    $sources[] = DeployMedia::fromArray($source);
                }
            }
            $instance->setSources($sources);
        }

        return $instance;
    }

    public function asLegacy(): array
    {
        $data = [
            'deploymentSiteId' => $this->getDeploymentSiteId(),
            'deployedFiles' => [],
            'success' => $this->getStatus() === JobStatus::Success
        ];

        foreach ($this->getSources() as $i => $source) {
            $data['deployedFiles'][$i] = [
                'mediaId' => $source->getMediaId(),
                'files' => []
            ];
            foreach ($source->getDeployedFiles() as $deployedFile) {
                $data['deployedFiles'][$i]['files'][] = (array)$deployedFile->jsonSerialize();
            }
        }

        return $data;
    }

    public static function isBlockedBy(): array
    {
        return [
            CombineChunksJob::QUEUE_NAME,
            ExifToolJob::QUEUE_NAME,
            VideoThumbnailJob::QUEUE_NAME,
            ImageConvertJob::QUEUE_NAME,
            PdfExtractPagesJob::QUEUE_NAME,
            FFmpegJob::QUEUE_NAME,
            AudioThumbnailJob::QUEUE_NAME,
            MimeTypeJob::QUEUE_NAME
        ];
    }

    public function reset(): static
    {
        $resetJob = parent::reset();
        foreach ($resetJob->getSources() ?? [] as $source) {
            $resetConvertJobs = [];

            foreach ($source->getConvertJobs() ?? [] as $convertJob) {
                $resetConvertJobs[] = $convertJob->reset();
            }

            $source
                ->setDeployedFiles([])
                ->setErrorMessage(null)
                ->setConvertJobs($resetConvertJobs);
        }

        return $resetJob;
    }
}
