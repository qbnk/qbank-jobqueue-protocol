<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Enums;

enum DataProcessingRegion: string
{
    case Global = 'global';
    case US = 'us';
    case EU = 'eu';
}
