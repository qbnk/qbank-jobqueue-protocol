<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Enums;

enum JobPriority: int
{
    case Low = 1;
    case Normal = 2;
    case High = 3;
}
