<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Enums;

enum JobStatus: int
{
    case New = 1;
    case Running = 2;
    case Success = 3;
    case Failure = 4;
    case Shutdown = 5;
    case Unblocked = 6;
    case UnblockedFailure = 7;
}
