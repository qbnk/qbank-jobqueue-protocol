<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job;

use Exception;

/**
 * Base class for JobQueue exceptions.
 * @author Björn Hjortsten
 * @since 2012-10-09
 * @package JobQueue
 * @subpackage Model
 */
class JobQueueException extends Exception
{
    public const ERR_TARGETEXISTS = 1;
    public const ERR_REGISTERJOB = 2;
    public const ERR_SESSION = 3;
    public const ERR_UNKNOWNJOB = 4;
    public const ERR_FORMAT = 5;
    public const ERR_JOBFAILED = 6;
    public const ERR_NOMEDIA = 7;
    public const ERR_RETRIEVEJOBS = 8;
    public const ERR_OTHER = 9;
}
