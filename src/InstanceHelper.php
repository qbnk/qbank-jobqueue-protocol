<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job;

use Doctrine\Instantiator\Instantiator;
use QBNK\JobQueue\Job\Enums\JobPriority;
use QBNK\JobQueue\Job\Enums\JobStatus;

class InstanceHelper
{
    private const ENUM_NAMESPACE = 'QBNK\\JobQueue\\Job\\Enums\\';

    public static function fromArray(array $parameters)
    {
        $object = (new Instantiator())->instantiate($parameters['class']);

        unset($parameters['class']);
        foreach ($parameters as $property => $value) {
            $property = ucfirst($property);

            if ($object instanceof JobAbstract && !($value instanceof \UnitEnum)) {
                $value = match (strtolower($property)) {
                    'priority' => JobPriority::tryFrom($value),
                    'status' => JobStatus::tryFrom($value),
                    default => $value
                };
            }

            $maybeEnum = self::ENUM_NAMESPACE . $property;
            if (enum_exists($maybeEnum) && !($value instanceof $maybeEnum)) {
                $value = $maybeEnum::tryFrom($value);
            }

            if ($value !== null && method_exists($object, 'set' . $property)) {
                $object->{"set{$property}"}($value);
            }
        }

        return $object;
    }
}
