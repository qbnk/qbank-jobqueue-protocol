<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination;

use Generator;
use InvalidArgumentException;
use QBNK\JobQueue\Job\Examination\Model\ExternalUsage\ExternalUsageAbstract;
use QBNK\JobQueue\Job\Examination\Model\ExternalUsage\Image;
use QBNK\JobQueue\Job\Examination\Model\ExternalUsage\Page;
use QBNK\JobQueue\Job\JobAbstract;

class ExternalUsageJob extends GoogleVisionJob
{
    public const ENTITY_TYPE_WEB = 'webEntities';
    public const ENTITY_TYPE_FULLMATCHINGIMAGES = 'fullMatchingImages';
    public const ENTITY_TYPE_PARTIALMATCHINGIMAGES = 'partialMatchingImages';
    public const ENTITY_TYPE_PAGESWITHMATCHINGIMAGES = 'pagesWithMatchingImages';
    public const ENTITY_TYPE_VISUALLYSIMILARIMAGES = 'visuallySimilarImages';
    public const ENTITY_TYPE_BESTGUESSLABELS = 'bestGuessLabels';

    /** @var array */
    protected const ENTITY_TYPE_IMAGE_MAPPING = [
        self::ENTITY_TYPE_FULLMATCHINGIMAGES => Image::TYPE_FULL_MATCH,
        self::ENTITY_TYPE_PARTIALMATCHINGIMAGES => Image::TYPE_PARTIAL_MATCH,
        self::ENTITY_TYPE_VISUALLYSIMILARIMAGES => Image::TYPE_VISUALLY_SIMILAR
    ];

    protected $analysisFeatures = [self::FEATURE_WEB_DETECTION];

    /**
     * Will not actually change features.
     * @param string[] $features
     * @return $this
     */
    public function setAnalysisFeatures(array $features): static
    {
        return $this;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? 'externalusage';
    }

    public function setCallbackQueue(?string $callbackQueue): static
    {
        if (preg_match('/callback_' . $this->getQueueName() . '(\d)/', $callbackQueue, $matches)) {
            $parent = new GoogleVisionJob();
            $this->callbackQueue = 'callback_' . $parent->getQueueName() . $matches[1];
            return $this;
        }
        return parent::setCallbackQueue($callbackQueue);
    }

    /**
     * @return ExternalUsageAbstract[]|Generator An array of Page and Image instances.
     */
    public function getExternalUseResult(): Generator
    {
        foreach ($this->getAnalysisData()[self::FEATURE_WEB_DETECTION] as $entityType => $list) {
            switch ($entityType) {
                case self::ENTITY_TYPE_PAGESWITHMATCHINGIMAGES:
                    yield from $this->generatePages($list);
                    break;
                case self::ENTITY_TYPE_FULLMATCHINGIMAGES:
                case self::ENTITY_TYPE_PARTIALMATCHINGIMAGES:
                case self::ENTITY_TYPE_VISUALLYSIMILARIMAGES:
                    yield from $this->generateImages($list, $entityType);
                    break;
            }
        }
    }

    /**
     * @param array $rawPages
     * @return Page[]|Generator
     */
    protected function generatePages(array $rawPages): Generator
    {
        foreach ($rawPages as $rawPage) {
            /** @var Image[] $images */
            $images = array_merge(
                iterator_to_array(
                    $this->generateImages(
                        $rawPage[Page::FIELD_FULLMATCHINGIMAGES] ?? [],
                        Page::FIELD_FULLMATCHINGIMAGES
                    ),
                    false
                ),
                iterator_to_array(
                    $this->generateImages(
                        $rawPage[Page::FIELD_PARTIALMATCHINGIMAGES] ?? [],
                        Page::FIELD_PARTIALMATCHINGIMAGES
                    ),
                    false
                )
            );
            yield (new Page())
                ->setUrl($rawPage[Page::FIELD_URL])
                ->setTitle($rawPage[Page::FIELD_TITLE])
                ->setImages($images);
        }
    }

    /**
     * @param array $rawImages
     * @param string $entityType
     * @return Image[]|Generator
     * @throws InvalidArgumentException Thrown when The entity type is not supported.
     */
    protected function generateImages(array $rawImages, string $entityType): Generator
    {
        if (!array_key_exists($entityType, self::ENTITY_TYPE_IMAGE_MAPPING)) {
            throw new InvalidArgumentException('Bad image entity type! ' . $entityType . ' is not supported!');
        }

        foreach ($rawImages as $rawImage) {
            yield (new Image())
                ->setUrl($rawImage['url'])
                ->setType(self::ENTITY_TYPE_IMAGE_MAPPING[$entityType]);
        }
    }

    public function reset(): static
    {
        $resetJob = parent::reset();
        return $resetJob->setAnalysisData([]);
    }
}
