<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination\Model;

use JsonSerializable;
use stdClass;

class Segment implements JsonSerializable
{
    /** @var int seconds */
    protected $startTime;

    /** @var int seconds */
    protected $endTime;

    public function __construct(int $startTime, int $endTime)
    {
        $this->setStartTime($startTime)
            ->setEndTime($endTime)
        ;
    }

    public static function fromArray($parameters): Segment
    {
        return (new self($parameters['startTime'], $parameters['endTime']));
    }

    /** @return int */
    public function getStartTime(): int
    {
        return $this->startTime;
    }

    /** @return int */
    public function getEndTime(): int
    {
        return $this->endTime;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = new stdClass();
        $json->startTime = $this->getStartTime();
        $json->endTime = $this->getEndTime();
        return $json;
    }

    /**
     * @param int $startTime
     * @return Segment
     */
    public function setStartTime(int $startTime): Segment
    {
        $this->startTime = $startTime;
        return $this;
    }

    /**
     * @param int $endTime
     * @return Segment
     */
    public function setEndTime(int $endTime): Segment
    {
        $this->endTime = $endTime;
        return $this;
    }
}
