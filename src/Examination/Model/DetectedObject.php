<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination\Model;

use InvalidArgumentException;
use JsonSerializable;
use stdClass;

class DetectedObject implements JsonSerializable
{
    /** @var float[] */
    protected $normalizedBoundingBox;

    /** @var int */
    protected $timeOffset;

    public function __construct(array $normalizedBoundingBox, int $timeOffset)
    {
        $this->setTimeOffset($timeOffset)
            ->setNormalizedBoundingBox($normalizedBoundingBox)
        ;
    }

    public static function fromArray(array $parameters): DetectedObject
    {
        return (new self($parameters['normalizedBoundingBox'], $parameters['timeOffset']));
    }

    /**
     * @return float[]
     */
    public function getNormalizedBoundingBox(): array
    {
        return $this->normalizedBoundingBox;
    }

    /**
     * @return int
     */
    public function getTimeOffset(): int
    {
        return $this->timeOffset;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = new stdClass();
        $json->normalizedBoundingBox = $this->getNormalizedBoundingBox();
        $json->timeOffset = $this->getTimeOffset();
        return $json;
    }

    /**
     * @param float[] $normalizedBoundingBox
     * @return DetectedObject
     */
    public function setNormalizedBoundingBox(array $normalizedBoundingBox): DetectedObject
    {
        $errorMessage = '';
        if (!self::validateBoundingBox($normalizedBoundingBox, $errorMessage)) {
            throw new InvalidArgumentException(
                'setNormalizedBoundingBox() requires an array with the following keys: "left", "top", "right", "bottom". ' .
                $errorMessage
            );
        }

        $this->normalizedBoundingBox = $normalizedBoundingBox;

        return $this;
    }

    public static function validateBoundingBox(array $boundingBox, string &$message = ''): bool
    {
        $requiredKeys = array_flip(['left', 'top', 'right', 'bottom']);

        $boundingBox = array_filter($boundingBox, static function ($item) {
            return filter_var($item, FILTER_VALIDATE_FLOAT) !== false;
        });

        $diff = array_diff_key($requiredKeys, $boundingBox);
        if (empty($diff)) {
            return true;
        }

        $message = 'Missing keys: '. implode(', ', array_keys($diff));
        return false;
    }

    /**
     * @param int $timeOffset
     * @return DetectedObject
     */
    public function setTimeOffset(int $timeOffset): DetectedObject
    {
        $this->timeOffset = $timeOffset;
        return $this;
    }
}
