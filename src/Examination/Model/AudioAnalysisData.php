<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination\Model;

use JsonSerializable;
use stdClass;

class AudioAnalysisData implements JsonSerializable
{
    /** @var array */
    protected $audioWords;

    /** @var array */
    protected $audioTranscripts;

    /**
     * @return array
     */
    public function getAudioWords(): array
    {
        return $this->audioWords ?? [];
    }

    /**
     * @param array $audioWords
     * @return $this
     */
    public function setAudioWords(array $audioWords): AudioAnalysisData
    {
        $this->audioWords = $audioWords;
        return $this;
    }

    /**
     * @return array
     */
    public function getAudioTranscripts(): array
    {
        return $this->audioTranscripts ?? [];
    }

    /**
     * @param array $audioTranscripts
     * @return $this
     */
    public function setAudioTranscripts(array $audioTranscripts): AudioAnalysisData
    {
        $this->audioTranscripts = $audioTranscripts;
        return $this;
    }

    public static function fromArray(array $parameters): AudioAnalysisData
    {
        return (new self())
            ->setAudioTranscripts($parameters['audioTranscripts'] ?? [])
            ->setAudioWords($parameters['audioWords'] ?? [])
        ;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = new stdClass();
        $json->audioTranscripts = $this->getAudioTranscripts();
        $json->audioWords = $this->getAudioWords();
        return $json;
    }
}
