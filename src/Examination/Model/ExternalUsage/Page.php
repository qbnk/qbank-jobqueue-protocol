<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination\Model\ExternalUsage;

class Page extends ExternalUsageAbstract
{
    public const FIELD_URL = 'url';
    public const FIELD_TITLE = 'pageTitle';
    public const FIELD_FULLMATCHINGIMAGES = 'fullMatchingImages';
    public const FIELD_PARTIALMATCHINGIMAGES = 'partialMatchingImages';

    /** @var string */
    protected $title;

    /** @var Image[] */
    protected $images;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return self
     */
    public function setTitle(string $title): static
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return Image[]
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @param Image[] $images
     * @return self
     */
    public function setImages(array $images): static
    {
        $this->images = $images;
        return $this;
    }

    public function jsonSerialize(): \stdClass
    {
        $jsonObject = parent::jsonSerialize();
        $jsonObject->title = $this->getTitle();
        $jsonObject->images = $this->getImages();
        return $jsonObject;
    }
}
