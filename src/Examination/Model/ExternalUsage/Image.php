<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination\Model\ExternalUsage;

class Image extends ExternalUsageAbstract
{
    public const TYPE_FULL_MATCH = 'full match';
    public const TYPE_PARTIAL_MATCH = 'partial match';
    public const TYPE_VISUALLY_SIMILAR = 'visually similar';

    /** @var string */
    protected $type;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return self
     */
    public function setType(string $type): static
    {
        $this->type = $type;
        return $this;
    }

    public function jsonSerialize(): \stdClass
    {
        $jsonObject = parent::jsonSerialize();
        $jsonObject->type = $this->getType();
        return $jsonObject;
    }
}
