<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination\Model\ExternalUsage;

use Doctrine\Instantiator\Exception\InvalidArgumentException;
use JsonSerializable;
use QBNK\JobQueue\Job\InstanceHelper;
use QBNK\JobQueue\Job\JobQueueException;
use stdClass;

abstract class ExternalUsageAbstract implements JsonSerializable
{
    /** @var string */
    protected $url;

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return self
     */
    public function setUrl(string $url): static
    {
        $this->url = $url;
        return $this;
    }

    public function jsonSerialize(): \stdClass
    {
        $jsonObject = new stdClass();
        $jsonObject->class = get_class($this);
        $jsonObject->url = $this->getUrl();
        return $jsonObject;
    }

    /**
     * @param array $parameters
     * @return ExternalUsageAbstract
     * @throws JobQueueException
     */
    public static function fromArray(array $parameters): static
    {
        try {
            return InstanceHelper::fromArray($parameters);
        } catch (InvalidArgumentException $iie) {
            throw new JobQueueException('Could not instantiate external usage: ' . $iie->getMessage());
        }
    }
}
