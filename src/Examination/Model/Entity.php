<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination\Model;

use JsonSerializable;
use stdClass;

class Entity implements JsonSerializable
{
    /** @var string */
    protected $value;

    /** @var Segment[] */
    protected $segments;

    /** @var float */
    protected $confidence;

    /** @var DetectedObject[] */
    protected $detectedObjects;

    public function __construct(string $value)
    {
        $this->setValue($value);
    }

    public static function fromArray(array $parameters): Entity
    {
        $instance = new self($parameters['value']);

        if (isset($parameters['confidence'])) {
            $instance->setConfidence($parameters['confidence']);
        }

        if ($parameters['detectedObjects']) {
            foreach ($parameters['detectedObjects'] as $detectedObject) {
                $instance->addDetectedObject(
                    is_array($detectedObject)
                        ? DetectedObject::fromArray($detectedObject)
                        : $detectedObject
                );
            }
        }

        if ($parameters['segments']) {
            foreach ($parameters['segments'] as $segment) {
                $instance->addSegment(
                    is_array($segment)
                        ? Segment::fromArray($segment)
                        : $segment
                );
            }
        }

        return $instance;
    }


    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return Entity
     */
    public function setValue(string $value): Entity
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return Segment[]
     */
    public function getSegments(): array
    {
        return $this->segments;
    }

    /**
     * @param Segment[] $segments
     * @return Entity
     */
    public function setSegments(array $segments): Entity
    {
        $this->segments = $segments;
        return $this;
    }

    public function addSegment(Segment $segment): Entity
    {
        if ($this->segments === null) {
            $this->segments = [];
        }

        $this->segments[] = $segment;
        return $this;
    }

    /**
     * @return DetectedObject[]
     */
    public function getDetectedObjects(): array
    {
        return $this->detectedObjects ?? [];
    }

    /**
     * @param DetectedObject[] $detectedObjects
     * @return Entity
     */
    public function setDetectedObjects(array $detectedObjects): Entity
    {
        $this->detectedObjects = $detectedObjects;
        return $this;
    }

    /**
     * @param DetectedObject $detectedObject
     * @return Entity
     */
    public function addDetectedObject(DetectedObject $detectedObject): Entity
    {
        if ($this->detectedObjects === null) {
            $this->detectedObjects = [];
        }
        $this->detectedObjects[] = $detectedObject;
        return $this;
    }

    /**
     * @return ?float
     */
    public function getConfidence(): ?float
    {
        return $this->confidence ?? null;
    }

    /**
     * @param float $confidence
     * @return Entity
     */
    public function setConfidence(float $confidence): Entity
    {
        $this->confidence = $confidence;
        return $this;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = new stdClass();
        $json->value = $this->getValue();
        $json->segments = $this->getSegments();
        $json->detectedObjects = $this->getDetectedObjects();
        $json->confidence = $this->getConfidence();
        return $json;
    }
}
