<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination\Model;

use JsonSerializable;
use stdClass;

class VideoAnalysisData implements JsonSerializable
{
    /** @var array */
    protected $videoData;

    /** @var Entity[] */
    protected $logos;

    /** @var Entity[] */
    protected $texts;

    /** @var int[] */
    protected $explicit;

    public function getVideoData(): array
    {
        return $this->videoData ?? [];
    }

    /**
     * @param array $videoData
     * @return $this
     */
    public function setVideoData(array $videoData): VideoAnalysisData
    {
        $this->videoData = $videoData;
        return $this;
    }

    /**
     * @return Entity[]
     */
    public function getLogos(): array
    {
        return $this->logos ?? [];
    }

    /**
     * @param Entity[] $data
     * @return $this
     */
    public function setLogos(array $data): VideoAnalysisData
    {
        $this->logos = $data;
        return $this;
    }

    /**
     * @return Entity[]
     */
    public function getTexts(): array
    {
        return $this->texts ?? [];
    }

    /**
     * @param Entity[] $data
     * @return $this
     */
    public function setTexts(array $data): VideoAnalysisData
    {
        $this->texts = $data;
        return $this;
    }

    public static function fromArray(array $parameters): VideoAnalysisData
    {
        $texts = $logos = [];
        $instance = (new self())
            ->setVideoData($parameters['videoData'] ?? [])
            ->setExplicit($parameters['explicit'] ?? []);

        foreach ($parameters['logos'] ?? [] as $logo) {
            $logos[] = is_array($logo) ? Entity::fromArray($logo) : $logo;
        }

        foreach ($parameters['texts'] ?? [] as $text) {
            $texts[] = is_array($text) ? Entity::fromArray($text) : $text;
        }

        return $instance
            ->setLogos($logos)
            ->setTexts($texts)
        ;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = new stdClass();
        $json->videoData = $this->getVideoData();
        $json->explicit = $this->getExplicit();
        $json->logos = $this->getLogos();
        $json->texts = $this->getTexts();
        return $json;
    }

    /**
     * @return int[] - associative array with {time offset in seconds} => \Google\Cloud\VideoIntelligence\V1\Likelihood::const
     */
    public function getExplicit(): array
    {
        return $this->explicit ?? [];
    }

    /**
     * @param array $explicit
     * @return VideoAnalysisData
     */
    public function setExplicit(array $explicit): VideoAnalysisData
    {
        $this->explicit = $explicit;
        return $this;
    }
}
