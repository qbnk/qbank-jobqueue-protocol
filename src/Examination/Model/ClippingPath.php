<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination\Model;

use JsonSerializable;
use stdClass;

class ClippingPath implements JsonSerializable
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var bool
     */
    protected $isPrimaryPath;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $pngFile;

    public static function fromArray(array $parameters): ClippingPath
    {
        return (new self())
            ->setName($parameters['name'])
            ->setIsPrimaryPath($parameters['isPrimaryPath'] ?? false)
            ->setPath($parameters['path'])
            ->setPngFile($parameters['pngFile'] ?? null)
        ;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = new stdClass();
        $json->name = $this->getName();
        $json->isPrimaryPath = $this->isPrimaryPath();
        $json->path = $this->getPath();
        $json->pngFile = $this->getPngFile();
        return $json;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ClippingPath
     */
    public function setName(string $name): ClippingPath
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPrimaryPath(): bool
    {
        return $this->isPrimaryPath ?? false;
    }

    /**
     * @param bool $isPrimaryPath
     * @return ClippingPath
     */
    public function setIsPrimaryPath(bool $isPrimaryPath): ClippingPath
    {
        $this->isPrimaryPath = $isPrimaryPath;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return ClippingPath
     */
    public function setPath(string $path): ClippingPath
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return string
     */
    public function getPngFile(): ?string
    {
        return $this->pngFile;
    }

    /**
     * @param string $pngFile
     * @return ClippingPath
     */
    public function setPngFile(string $pngFile = null): ClippingPath
    {
        $this->pngFile = $pngFile;
        return $this;
    }
}
