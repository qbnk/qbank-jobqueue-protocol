<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination;

use QBNK\JobQueue\Job\Document\PresentationInformation;

class PowerPointExtractJob extends ExaminationAbstract
{
    /** @var PresentationInformation */
    protected $presentationInformation;

    /**
     * @param PresentationInformation $presentationInformation
     * @return $this
     */
    public function setPresentationInformation(PresentationInformation $presentationInformation): PowerPointExtractJob
    {
        $this->presentationInformation = $presentationInformation;
        return $this;
    }

    /**
     * @return PresentationInformation
     */
    public function getPresentationInformation(): PresentationInformation
    {
        return $this->presentationInformation;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? 'pptextract';
    }

    public function jsonSerialize(): \stdClass
    {
        $data = parent::jsonSerialize();
        $data->presentationInformation = $this->getPresentationInformation();
        return $data;
    }

    public static function fromArray(array $parameters, bool $assignId = false): static
    {
        $instance = parent::fromArray($parameters, $assignId);
        if (isset($parameters['presentationInformation'])) {
            $instance->setPresentationInformation(
                PresentationInformation::fromArray($parameters['presentationInformation'])
            );
        }
        return $instance;
    }
}
