<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination;

/**
 * A job that processes an image through the Google Cloud Vision API, providing image analysis.
 * @author Björn Hjortsten
 * @since 2018-04-19
 * @package QBNK\JobQueue\Model\Image
 */
class GoogleVisionJob extends ExaminationAbstract
{
    public const FEATURE_UNSPECIFIED = 'TYPE_UNSPECIFIED';
    public const FEATURE_FACE_DETECTION = 'FACE_DETECTION';
    public const FEATURE_LANDMARK_DETECTION = 'LANDMARK_DETECTION';
    public const FEATURE_LOGO_DETECTION = 'LOGO_DETECTION';
    public const FEATURE_LABEL_DETECTION = 'LABEL_DETECTION';
    public const FEATURE_TEXT_DETECTION = 'TEXT_DETECTION';
    public const FEATURE_DOCUMENT_TEXT_DETECTION = 'DOCUMENT_TEXT_DETECTION';
    public const FEATURE_SAFE_SEARCH_DETECTION = 'SAFE_SEARCH_DETECTION';
    public const FEATURE_IMAGE_PROPERTIES = 'IMAGE_PROPERTIES';
    public const FEATURE_CROP_HINTS = 'CROP_HINTS';
    public const FEATURE_WEB_DETECTION = 'WEB_DETECTION';

    /**
     * Determines how large the image derivate created for google vision API should be
     * @var int
     */
    public const IMAGE_DERIVATE_SIZE = 2000;

    /**
     * Features to detect
     * @var array
     */
    protected $analysisFeatures = [self::FEATURE_LABEL_DETECTION, self::FEATURE_LANDMARK_DETECTION];

    /**
     * @var array
     */
    protected $analysisData;

    /**
     * @return array
     */
    public function getAnalysisFeatures(): array
    {
        return $this->analysisFeatures ?? [];
    }

    /**
     * @param string[] $features
     * @return $this
     */
    public function setAnalysisFeatures(array $features)
    {
        $this->analysisFeatures = $features;
        return $this;
    }

    /**
     * @return array
     */
    public function getAnalysisData(): array
    {
        return $this->analysisData ?? [];
    }

    /**
     * @param array $analysisData
     * @return $this
     */
    public function setAnalysisData($analysisData)
    {
        $this->analysisData = $analysisData;
        return $this;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? 'googlevision';
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->analysisFeatures = $this->getAnalysisFeatures();
        $json->analysisData = $this->getAnalysisData();
        return $json;
    }

    public function reset(): static
    {
        $resetJob = parent::reset();
        return $resetJob->setAnalysisData([]);
    }
}
