<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination;

use QBNK\JobQueue\Job\Examination\Model\VideoAnalysisData;

class VideoAnalysisJob extends ExaminationAbstract
{
    /** @var int[] */
    protected $features;

    /** @var VideoAnalysisData */
    protected $analysisData;

    /**
     * Gets the features that should be used during analysis.
     * @return int[]
     */
    public function getFeatures(): array
    {
        return $this->features ?? [];
    }

    /**
     * Sets the features that should be used during analysis. See Google\Cloud\VideoIntelligence\V1\Feature constants.
     * @param int[] $features
     * @return $this
     */
    public function setFeatures(array $features): VideoAnalysisJob
    {
        $this->features = $features;
        return $this;
    }

    /**
     * @return VideoAnalysisData
     */
    public function getAnalysisData(): ?VideoAnalysisData
    {
        return $this->analysisData;
    }

    /**
     * @param VideoAnalysisData|array $videoAnalysisData
     * @return $this
     */
    public function setAnalysisData($videoAnalysisData): VideoAnalysisJob
    {
        $this->analysisData = is_array($videoAnalysisData) ? VideoAnalysisData::fromArray(
            $videoAnalysisData
        ) : $videoAnalysisData;
        return $this;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? 'videoanalysis';
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->features = $this->getFeatures();
        $json->analysisData = $this->getAnalysisData();
        return $json;
    }

    public function reset(): static
    {
        $resetJob = parent::reset();
        return $resetJob->setAnalysisData([]);
    }
}
