<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination;

class VirusScanJob extends ExaminationAbstract
{
    public const QUEUE_NAME = 'virusscan';

    private bool $malwareFound = false;
    private ?string $malwareName = null;

    public function getQueueName(): string
    {
        return $this->queueName ?? self::QUEUE_NAME;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->malwareFound = $this->isMalwareFound();
        $json->malwareName = $this->getMalwareName();

        return $json;
    }

    public function isMalwareFound(): bool
    {
        return $this->malwareFound;
    }

    public function setMalwareFound(bool $malwareFound): static
    {
        $this->malwareFound = $malwareFound;
        return $this;
    }

    public function reset(): static
    {
        /** @var VirusScanJob $resetJob */
        $resetJob = parent::reset();
        return $resetJob
            ->setMalwareFound(false)
            ->setMalwareName(null);
    }

    public function getMalwareName(): ?string
    {
        return $this->malwareName;
    }

    public function setMalwareName(?string $malwareName): static
    {
        $this->malwareName = $malwareName;
        return $this;
    }
}
