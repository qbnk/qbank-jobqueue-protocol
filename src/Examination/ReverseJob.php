<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination;

use QBNK\JobQueue\Job\JobAbstract;

/**
 * A job that gets information from a latitude/longitude pair
 * @author Henrik Malmberg
 * @since 2017-05-30
 * @package JobQueue
 * @subpackage Model\Location
 */
class ReverseJob extends JobAbstract
{
    /**
     * @var float
     */
    protected $longitude;

    /**
     * @var float
     */
    protected $latitude;

    /**
     * Returned location details if any
     * @var array
     */
    protected $locationDetails;

    /**
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     * @return $this
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     * @return $this
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
        return $this;
    }

    /**
     * @return array
     */
    public function getLocationDetails(): array
    {
        return $this->locationDetails ?? [];
    }

    /**
     * @param array $locationDetails
     * @return $this
     */
    public function setLocationDetails(array $locationDetails)
    {
        $this->locationDetails = $locationDetails;
        return $this;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? 'locationreverse';
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->latitude = $this->getLatitude();
        $json->longitude = $this->getLongitude();
        $json->locationDetails = $this->getLocationDetails();
        return $json;
    }
}
