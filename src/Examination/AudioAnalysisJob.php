<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination;

use QBNK\JobQueue\Job\Examination\Model\AudioAnalysisData;
use RuntimeException;

class AudioAnalysisJob extends ExaminationAbstract
{
    /** @var int[] */
    protected $features;

    /** @var AudioAnalysisData */
    protected $analysisData;

    /** @var string */
    protected $selectedLanguage;

    public const ML_MODEL_VIDEO = 'video';
    public const ML_MODEL_DEFAULT = 'default';
    public const AVAILABLE_LANGUAGES = [
        self::ML_MODEL_VIDEO => [
            'en-US' => 'English (United States)'
        ],
        self::ML_MODEL_DEFAULT => [
            'af-ZA' => 'Afrikaans (South Africa)',
            'sq-AL' => 'Albanian (Albania)',
            'am-ET' => 'Amharic (Ethiopia)',
            'ar-DZ' => 'Arabic (Algeria)',
            'ar-BH' => 'Arabic (Bahrain)',
            'ar-EG' => 'Arabic (Egypt)',
            'ar-IQ' => 'Arabic (Iraq)',
            'ar-IL' => 'Arabic (Israel)',
            'ar-JO' => 'Arabic (Jordan)',
            'ar-KW' => 'Arabic (Kuwait)',
            'ar-LB' => 'Arabic (Lebanon)',
            'ar-MA' => 'Arabic (Morocco)',
            'ar-OM' => 'Arabic (Oman)',
            'ar-QA' => 'Arabic (Qatar)',
            'ar-SA' => 'Arabic (Saudi Arabia)',
            'ar-PS' => 'Arabic (State of Palestine)',
            'ar-TN' => 'Arabic (Tunisia)',
            'ar-AE' => 'Arabic (United Arab Emirates)',
            'ar-YE' => 'Arabic (Yemen)',
            'hy-AM' => 'Armenian (Armenia)',
            'az-AZ' => 'Azerbaijani (Azerbaijan)',
            'en-GB' => 'English (United Kingdom)',
            'eu-ES' => 'Basque (Spain)',
            'bn-BD' => 'Bengali (Bangladesh)',
            'bn-IN' => 'Bengali (India)',
            'bs-BA' => 'Bosnian (Bosnia and Herzegovina)',
            'bg-BG' => 'Bulgarian (Bulgaria)',
            'my-MM' => 'Burmese (Myanmar)',
            'ca-ES' => 'Catalan (Spain)',
            'yue-Hant-HK' => 'Chinese, Cantonese (Traditional Hong Kong)',
            'zh' => 'Chinese Mandarin (Simplified, China)',
            'zh-TW' => 'Chinese, Mandarin (Traditional, Taiwan)',
            'hr-HR' => 'Croatian (Croatia)',
            'cs-CZ' => 'Czech (Czech Republic)',
            'da-DK' => 'Danish (Denmark)',
            'nl-BE' => 'Dutch (Belgium)',
            'nl-NL' => 'Dutch (Netherlands)',
            'en-AU' => 'English (Australia)',
            'en-CA' => 'English (Canada)',
            'en-GH' => 'English (Ghana)',
            'en-HK' => 'English (Hong Kong)',
            'en-IN' => 'English (India)',
            'en-IE' => 'English (Ireland)',
            'en-KE' => 'English (Kenya)',
            'en-NZ' => 'English (New Zealand)',
            'en-NG' => 'English (Nigeria)',
            'en-PK' => 'English (Pakistan)',
            'en-PH' => 'English (Philippines)',
            'en-SG' => 'English (Singapore)',
            'en-ZA' => 'English (South Africa)',
            'en-TZ' => 'English (Tanzania)',
            'et-EE' => 'Estonian (Estonia)',
            'fil-PH' => 'Filipino (Philippines)',
            'fi-FI' => 'Finnish (Finland)',
            'fr-BE' => 'French (Belgium)',
            'fr-CA' => 'French (Canada)',
            'fr-FR' => 'French (France)',
            'fr-CH' => 'French (Switzerland)',
            'gl-ES' => 'Galician (Spain)',
            'ka-GE' => 'Georgian (Georgia)',
            'de-AT' => 'German (Austria)',
            'de-DE' => 'German (Germany)',
            'de-CH' => 'German (Switzerland)',
            'el-GR' => 'Greek (Greece)',
            'gu-IN' => 'Gujarati (India)',
            'iw-IL' => 'Hebrew (Israel)',
            'hi-IN' => 'Hindi (India)',
            'hu-HU' => 'Hungarian (Hungary)',
            'is-IS' => 'Icelandic (Iceland)',
            'id-ID' => 'Indonesian (Indonesia)',
            'it-IT' => 'Italian (Italy)',
            'it-CH' => 'Italian (Switzerland)',
            'ja-JP' => 'Japanese (Japan)',
            'jv-ID' => 'Javanese (Indonesia)',
            'kn-IN' => 'Kannada (India)',
            'km-KH' => 'Khmer (Cambodia)',
            'ko-KR' => 'Korean (South Korea)',
            'lo-LA' => 'Lao (Laos)',
            'lv-LV' => 'Latvian (Latvia)',
            'lt-LT' => 'Lithuanian (Lithuania)',
            'mk-MK' => 'Macedonian (North Macedonia)',
            'ms-MY' => 'Malay (Malaysia)',
            'ml-IN' => 'Malayalam (India)',
            'mr-IN' => 'Marathi (India)',
            'mn-MN' => 'Mongolian (Mongolia)',
            'ne-NP' => 'Nepali (Nepal)',
            'no-NO' => 'Norwegian Bokmål (Norway)',
            'fa-IR' => 'Persian (Iran)',
            'pl-PL' => 'Polish (Poland)',
            'pt-BR' => 'Portuguese (Brazil)',
            'pt-PT' => 'Portuguese (Portugal)',
            'pa-Guru-IN' => 'Punjabi (Gurmukhi India)',
            'ro-RO' => 'Romanian (Romania)',
            'ru-RU' => 'Russian (Russia)',
            'sr-RS' => 'Serbian (Serbia)',
            'si-LK' => 'Sinhala (Sri Lanka)',
            'sk-SK' => 'Slovak (Slovakia)',
            'sl-SI' => 'Slovenian (Slovenia)',
            'es-AR' => 'Spanish (Argentina)',
            'es-BO' => 'Spanish (Bolivia)',
            'es-CL' => 'Spanish (Chile)',
            'es-CO' => 'Spanish (Colombia)',
            'es-CR' => 'Spanish (Costa Rica)',
            'es-DO' => 'Spanish (Dominican Republic)',
            'es-EC' => 'Spanish (Ecuador)',
            'es-SV' => 'Spanish (El Salvador)',
            'es-GT' => 'Spanish (Guatemala)',
            'es-HN' => 'Spanish (Honduras)',
            'es-MX' => 'Spanish (Mexico)',
            'es-NI' => 'Spanish (Nicaragua)',
            'es-PA' => 'Spanish (Panama)',
            'es-PY' => 'Spanish (Paraguay)',
            'es-PE' => 'Spanish (Peru)',
            'es-PR' => 'Spanish (Puerto Rico)',
            'es-ES' => 'Spanish (Spain)',
            'es-UY' => 'Spanish (Uruguay)',
            'es-VE' => 'Spanish (Venezuela)',
            'su-ID' => 'Sundanese (Indonesia)',
            'sw-KE' => 'Swahili (Kenya)',
            'sw-TZ' => 'Swahili (Tanzania)',
            'sv-SE' => 'Swedish (Sweden)',
            'ta-IN' => 'Tamil (India)',
            'ta-MY' => 'Tamil (Malaysia)',
            'ta-SG' => 'Tamil (Singapore)',
            'ta-LK' => 'Tamil (Sri Lanka)',
            'te-IN' => 'Telugu (India)',
            'th-TH' => 'Thai (Thailand)',
            'tr-TR' => 'Turkish (Turkey)',
            'uk-UA' => 'Ukrainian (Ukraine)',
            'ur-IN' => 'Urdu (India)',
            'ur-PK' => 'Urdu (Pakistan)',
            'uz-UZ' => 'Uzbek (Uzbekistan)',
            'vi-VN' => 'Vietnamese (Vietnam)',
            'zu-ZA' => 'Zulu (South Africa)'
        ],
    ];

    public const BCP47_TO_ISO6391 = [
        'af-ZA' => 'af',
        'sq-AL' => 'sq',
        'am-ET' => 'am',
        'ar-DZ' => 'ar',
        'ar-BH' => 'ar',
        'ar-EG' => 'ar',
        'ar-IQ' => 'ar',
        'ar-IL' => 'ar',
        'ar-JO' => 'ar',
        'ar-KW' => 'ar',
        'ar-LB' => 'ar',
        'ar-MA' => 'ar',
        'ar-OM' => 'ar',
        'ar-QA' => 'ar',
        'ar-SA' => 'ar',
        'ar-PS' => 'ar',
        'ar-TN' => 'ar',
        'ar-AE' => 'ar',
        'ar-YE' => 'ar',
        'hy-AM' => 'hy',
        'az-AZ' => 'az',
        'en-GB' => 'en',
        'eu-ES' => 'eu',
        'bn-BD' => 'bn',
        'bn-IN' => 'bn',
        'bs-BA' => 'bs',
        'bg-BG' => 'bg',
        'my-MM' => 'my',
        'ca-ES' => 'ca',
        'yue-Hant-HK' => 'zh-TW',
        'zh-CN' => 'zh',
        'zh-TW' => 'zh',
        'hr-HR' => 'hr',
        'cs-CZ' => 'cs',
        'da-DK' => 'da',
        'nl-BE' => 'nl',
        'nl-NL' => 'nl',
        'en-AU' => 'en',
        'en-CA' => 'en',
        'en-GH' => 'en',
        'en-HK' => 'en',
        'en-IN' => 'en',
        'en-IE' => 'en',
        'en-KE' => 'en',
        'en-NZ' => 'en',
        'en-NG' => 'en',
        'en-PK' => 'en',
        'en-PH' => 'en',
        'en-SG' => 'en',
        'en-ZA' => 'en',
        'en-TZ' => 'en',
        'et-EE' => 'et',
        'fil-PH' => 'tl',
        'fi-FI' => 'fi',
        'fr-BE' => 'fr',
        'fr-CA' => 'fr',
        'fr-FR' => 'fr',
        'fr-CH' => 'fr',
        'gl-ES' => 'gl',
        'ka-GE' => 'ka',
        'de-AT' => 'de',
        'de-DE' => 'de',
        'de-CH' => 'de',
        'el-GR' => 'el',
        'gu-IN' => 'gu',
        'iw-IL' => 'iw',
        'hi-IN' => 'hi',
        'hu-HU' => 'hu',
        'is-IS' => 'is',
        'id-ID' => 'id',
        'it-IT' => 'it',
        'it-CH' => 'it',
        'ja-JP' => 'ja',
        'jv-ID' => 'jv',
        'kn-IN' => 'kn',
        'km-KH' => 'km',
        'ko-KR' => 'ko',
        'lo-LA' => 'lo',
        'lv-LV' => 'lv',
        'lt-LT' => 'lt',
        'mk-MK' => 'mk',
        'ms-MY' => 'ms',
        'ml-IN' => 'ml',
        'mr-IN' => 'mr',
        'mn-MN' => 'mn',
        'ne-NP' => 'ne',
        'no-NO' => 'no',
        'fa-IR' => 'fa',
        'pl-PL' => 'pl',
        'pt-BR' => 'pt',
        'pt-PT' => 'pt',
        'pa-Guru-IN' => 'pa',
        'ro-RO' => 'ro',
        'ru-RU' => 'ru',
        'sr-RS' => 'sr',
        'si-LK' => 'si',
        'sk-SK' => 'sk',
        'sl-SI' => 'sl',
        'es-AR' => 'es',
        'es-BO' => 'es',
        'es-CL' => 'es',
        'es-CO' => 'es',
        'es-CR' => 'es',
        'es-DO' => 'es',
        'es-EC' => 'es',
        'es-SV' => 'es',
        'es-GT' => 'es',
        'es-HN' => 'es',
        'es-MX' => 'es',
        'es-NI' => 'es',
        'es-PA' => 'es',
        'es-PY' => 'es',
        'es-PE' => 'es',
        'es-PR' => 'es',
        'es-ES' => 'es',
        'es-UY' => 'es',
        'es-VE' => 'es',
        'su-ID' => 'su',
        'sw-KE' => 'sw',
        'sw-TZ' => 'sw',
        'sv-SE' => 'sv',
        'ta-IN' => 'ta',
        'ta-MY' => 'ta',
        'ta-SG' => 'ta',
        'ta-LK' => 'ta',
        'te-IN' => 'te',
        'th-TH' => 'th',
        'tr-TR' => 'tr',
        'uk-UA' => 'uk',
        'ur-IN' => 'ur',
        'ur-PK' => 'ur',
        'uz-UZ' => 'uz',
        'vi-VN' => 'vi',
        'zu-ZA' => 'zu',
    ];

    /**
     * @return AudioAnalysisData
     */
    public function getAnalysisData(): ?AudioAnalysisData
    {
        return $this->analysisData;
    }

    /**
     * @param AudioAnalysisData|array $AudioAnalysisData
     * @return $this
     */
    public function setAnalysisData($AudioAnalysisData): AudioAnalysisJob
    {
        $this->analysisData = is_array($AudioAnalysisData) ? AudioAnalysisData::fromArray(
            $AudioAnalysisData
        ) : $AudioAnalysisData;
        return $this;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? 'audioanalysis';
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->analysisData = $this->getAnalysisData();
        $json->selectedLanguage = $this->getSelectedLanguage();
        return $json;
    }

    /**
     * @return string
     */
    public function getSelectedLanguage(): string
    {
        return $this->selectedLanguage ?? 'en-US';
    }

    /**
     * @param string $selectedLanguage
     * @return AudioAnalysisJob
     */
    public function setSelectedLanguage(string $selectedLanguage): AudioAnalysisJob
    {
        foreach (self::AVAILABLE_LANGUAGES as $ml => $languages) {
            if (isset($languages[$selectedLanguage])) {
                $this->selectedLanguage = $selectedLanguage;
                return $this;
            }
        }

        throw new RuntimeException(sprintf('Unable to set language "%s"', $selectedLanguage));
    }

    /**
     * @return string[]
     */
    public static function getAvailableLanguages(): array
    {
        return array_merge(
            self::AVAILABLE_LANGUAGES[self::ML_MODEL_DEFAULT],
            self::AVAILABLE_LANGUAGES[self::ML_MODEL_VIDEO]
        );
    }

    public function getAppropriateMLForLanguage(string $lang)
    {
        if (isset(self::AVAILABLE_LANGUAGES[self::ML_MODEL_VIDEO][$lang])) {
            return self::ML_MODEL_VIDEO;
        }

        return self::ML_MODEL_DEFAULT;
    }

    public function reset(): static
    {
        $resetJob = parent::reset();
        return $resetJob->setAnalysisData([]);
    }
}
