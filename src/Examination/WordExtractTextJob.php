<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination;

class WordExtractTextJob extends ExaminationAbstract
{
    /**
     * Extracted text, if any
     * @var string
     */
    protected $extractedText;

    /**
     * @return string
     */
    public function getExtractedText(): ?string
    {
        return $this->extractedText;
    }

    /**
     * @param string $extractedText
     * @return $this
     */
    public function setExtractedText($extractedText)
    {
        $this->extractedText = $extractedText;
        return $this;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? 'wordextracttext';
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->extractedText = $this->getExtractedText();
        return $json;
    }
}
