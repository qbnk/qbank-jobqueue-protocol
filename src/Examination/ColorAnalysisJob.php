<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination;

/**
 * Extract structured color data for this image, to be used to search for images containing certain colors, and looking for similiar images
 * @author Kristian Sandström
 * @since 2018-08-31
 * @package QBNK\JobQueue\Model\Image
 */
class ColorAnalysisJob extends ExaminationAbstract
{
    public const PIXEL_SAMPLES = 100;

    /**
     * Width to scale the source image down to before extracting colors.
     * The downsizing will automatically "group" similar colors, reducing number of unique colors.
     * @var int
     */
    protected $scaleWidth = 300;

    /**
     * Height to scale the source image down to before extracting colors.
     * The downsizing will automatically "group" similar colors, reducing number of unique colors.
     * @var int
     */
    protected $scaleHeight = 300;

    /**
     * Number of quadrants to split the downsized image into. When comparing images each quadrant is compared with its
     * respective to find a more visually similiar image, instead of just finding images with the same overall color profile.
     * @var int
     */
    protected $quadrants = 36;

    /**
     * Number of colors extracted in each $quadrant. Colors are sorted according to amount/quadrant descending
     * @var int
     */
    protected $topColors = 5;

    /**
     * Output color data if any
     * @var array
     */
    protected $colorData;

    /**
     * @return int
     */
    public function getScaleWidth(): int
    {
        return $this->scaleWidth;
    }

    /**
     * @return int
     */
    public function getScaleHeight(): int
    {
        return $this->scaleHeight;
    }

    /**
     * @return int
     */
    public function getQuadrants(): int
    {
        return $this->quadrants;
    }

    /**
     * @return int
     */
    public function getTopColors(): int
    {
        return $this->topColors;
    }

    /**
     * @return array
     */
    public function getColorData(): array
    {
        return $this->colorData ?? [];
    }

    /**
     * @param array $colorData
     * @return $this
     */
    public function setColorData($colorData)
    {
        $this->colorData = $colorData;
        return $this;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? 'coloranalysis';
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->colorData = $this->getColorData();
        return $json;
    }

    public function reset(): static
    {
        $resetJob = parent::reset();
        return $resetJob->setColorData([]);
    }
}
