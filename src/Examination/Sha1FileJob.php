<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination;

/**
 * A job that calculates the sha1 hash of a file
 * @author Henrik Malmberg
 * @since 2014-04-10
 * @package JobQueue
 * @subpackage Model
 */
class Sha1FileJob extends ExaminationAbstract
{
    protected ?string $sha1sum = null;

    protected bool $calculateDuplicates = true;

    public function getSha1sum(): ?string
    {
        return $this->sha1sum;
    }

    public function setSha1sum(?string $sha1sum): Sha1FileJob
    {
        $this->sha1sum = $sha1sum;
        return $this;
    }

    public function getQueueName(): string
    {
        return $this->queueName ?? 'sha1file';
    }

    public function isCalculateDuplicates(): bool
    {
        return $this->calculateDuplicates;
    }

    public function setCalculateDuplicates(bool $calculateDuplicates): Sha1FileJob
    {
        $this->calculateDuplicates = $calculateDuplicates;
        return $this;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->sha1sum = $this->getSha1sum();
        $json->calculateDuplicates = $this->isCalculateDuplicates();
        return $json;
    }

    public function reset(): static
    {
        $resetJob = parent::reset();
        return $resetJob->setSha1sum(null);
    }
}
