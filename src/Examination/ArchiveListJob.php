<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination;

use QBNK\JobQueue\Job\Storage\CompressedFile;

/**
 * A job that fetches the file path contents of an archive file.
 * @author Björn Hjortsten
 * @since 2013-12-11
 */
class ArchiveListJob extends ExaminationAbstract
{
    public const FORMAT_ZIP = 'zip';
    public const FORMAT_TAR = 'tar';
    public const FORMAT_PHAR = 'phar';

    /**
     * The archive format.
     * Should be one of the format constants.
     * @var string
     */
    protected $format;

    /**
     * @var CompressedFile[]
     */
    protected $compressedFiles;

    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * @param string $format
     * @return $this
     */
    public function setFormat(string $format): ArchiveListJob
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @return CompressedFile[]
     */
    public function getCompressedFiles(): array
    {
        return $this->compressedFiles ?? [];
    }

    /**
     * @param CompressedFile[] $compressedFiles
     * @return $this
     */
    public function setCompressedFiles(array $compressedFiles): ArchiveListJob
    {
        foreach ($compressedFiles as &$file) {
            if (is_array($file)) {
                $file = CompressedFile::fromArray($file);
            }
        }
        unset($file);
        $this->compressedFiles = $compressedFiles;
        return $this;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? 'archivelist';
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->format = $this->getFormat();
        $json->compressedFiles = $this->getCompressedFiles();
        return $json;
    }

    public function reset(): static
    {
        $resetJob = parent::reset();
        return $resetJob->setCompressedFiles([]);
    }
}
