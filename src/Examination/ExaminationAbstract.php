<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination;

use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\Storage\File;

abstract class ExaminationAbstract extends JobAbstract
{
    /**
     * @var File
     */
    protected $source;

    /**
     * @return File
     */
    public function getSource(): File
    {
        return $this->source;
    }

    /**
     * @param File $source
     * @return $this
     */
    public function setSource($source)
    {
        $this->source = is_array($source) ? File::fromArray($source) : $source;
        return $this;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->source = $this->source;
        return $json;
    }
}
