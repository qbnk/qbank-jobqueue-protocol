<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination;

class MimeTypeJob extends ExaminationAbstract
{
    public const QUEUE_NAME = 'mimetype';
    protected ?string $extractedMimeType = null;

    protected ?string $extractedDescription = null;

    public function getExtractedMimeType(): ?string
    {
        return $this->extractedMimeType;
    }

    public function setExtractedMimeType(?string $extractedMimeType): MimeTypeJob
    {
        $this->extractedMimeType = $extractedMimeType;
        return $this;
    }

    public function getExtractedDescription(): ?string
    {
        return $this->extractedDescription;
    }

    public function setExtractedDescription(?string $extractedDescription): MimeTypeJob
    {
        $this->extractedDescription = $extractedDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? self::QUEUE_NAME;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->extractedMimeType = $this->getExtractedMimeType();
        $json->extractedDescription = $this->getExtractedDescription();
        return $json;
    }

    public function reset(): static
    {
        $resetJob = parent::reset();
        return $resetJob
            ->setExtractedMimeType(null)
            ->setExtractedDescription(null);
    }
}
