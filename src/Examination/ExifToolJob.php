<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Examination;

use QBNK\JobQueue\Job\Examination\Model\ClippingPath;

/**
 * A job that extracts embedded meta-data from files.
 * @author Björn Hjortsten
 * @since 2012-10-30
 * @package JobQueue
 * @package Model
 */
class ExifToolJob extends ExaminationAbstract
{
    public const QUEUE_NAME = 'exiftool';

    /**
     * @var string[]
     */
    protected array $extractedExifData = [];

    /**
     * Any (clipping) paths that this image may contain
     * @var string[]
     */
    protected array $extractedPaths = [];

    protected ?ReverseJob $locationReverseJob = null;

    /**
     * @return array
     */
    public function getExtractedExifData(): array
    {
        return $this->extractedExifData;
    }

    /**
     * @param array $extractedExifData
     * @return $this
     */
    public function setExtractedExifData(?array $extractedExifData): static
    {
        $this->extractedExifData = $extractedExifData;
        return $this;
    }

    /**
     * @return ClippingPath[]
     */
    public function getExtractedPaths(): array
    {
        return $this->extractedPaths;
    }

    /**
     * @param ClippingPath[] $extractedPaths
     * @return $this
     */
    public function setExtractedPaths(?array $extractedPaths): static
    {
        foreach ($extractedPaths as &$path) {
            if (!$path instanceof ClippingPath) {
                $path = ClippingPath::fromArray($path);
            }
        }
        $this->extractedPaths = $extractedPaths;
        return $this;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? self::QUEUE_NAME;
    }

    /**
     * @return ReverseJob
     */
    public function getLocationReverseJob(): ?ReverseJob
    {
        return $this->locationReverseJob;
    }

    /**
     * @param array|ReverseJob|null $locationReverseJob
     * @return ExifToolJob
     */
    public function setLocationReverseJob($locationReverseJob): static
    {
        $this->locationReverseJob = is_array($locationReverseJob)
            ? ReverseJob::fromArray($locationReverseJob)
            : $locationReverseJob;
        return $this;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->extractedExifData = $this->getExtractedExifData();
        $json->extractedPaths = $this->getExtractedPaths();
        $json->locationReverseJob = $this->getLocationReverseJob();
        return $json;
    }

    public function reset(): static
    {
        $resetJob = parent::reset();
        return $resetJob
            ->setExtractedExifData([])
            ->setExtractedPaths([])
            ->setLocationReverseJob(null);
    }
}
