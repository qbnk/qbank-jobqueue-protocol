<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Document;

use JsonSerializable;
use stdClass;

class PresentationInformation implements JsonSerializable
{
    /** @var string */
    protected $title;

    /** @var string */
    protected $subject;

    /** @var string */
    protected $keywords;

    /** @var string[] */
    protected $slideText;

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title): PresentationInformation
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $subject
     * @return $this
     */
    public function setSubject(string $subject): PresentationInformation
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @param string $keywords
     * @return $this
     */
    public function setKeywords(string $keywords): PresentationInformation
    {
        $this->keywords = $keywords;
        return $this;
    }

    /**
     * @return string
     */
    public function getKeywords(): string
    {
        return $this->keywords;
    }

    /**
     * @param string[] $slideText
     * @return $this
     */
    public function setSlideText(array $slideText): PresentationInformation
    {
        $this->slideText = $slideText;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getSlideText(): array
    {
        return $this->slideText;
    }

    public function jsonSerialize(): \stdClass
    {
        $data = new stdClass();
        $data->title = $this->getTitle();
        $data->subject = $this->getSubject();
        $data->keywords = $this->getKeywords();
        $data->slideText = $this->getSlideText();
        return $data;
    }

    public static function fromArray(array $parameters): PresentationInformation
    {
        $instance = new static();
        if (isset($parameters['title'])) {
            $instance->setTitle($parameters['title']);
        }
        if (isset($parameters['subject'])) {
            $instance->setSubject($parameters['subject']);
        }
        if (isset($parameters['keywords'])) {
            $instance->setKeywords($parameters['keywords']);
        }
        if (isset($parameters['slideText'])) {
            $instance->setSlideText($parameters['slideText']);
        }
        return $instance;
    }
}
