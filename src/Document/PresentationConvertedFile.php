<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Document;

use JsonSerializable;
use QBNK\JobQueue\Job\Storage\File;
use stdClass;

class PresentationConvertedFile implements JsonSerializable
{
    /** @var string */
    protected $templateName;

    /** @var File */
    protected $file;

    /**
     * @param string $templateName
     * @return $this
     */
    public function setTemplateName(string $templateName): PresentationConvertedFile
    {
        $this->templateName = $templateName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTemplateName(): string
    {
        return $this->templateName;
    }

    /**
     * @param File $file
     * @return $this
     */
    public function setFile(File $file): PresentationConvertedFile
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return File
     */
    public function getFile(): File
    {
        return $this->file;
    }

    public function jsonSerialize(): \stdClass
    {
        $data = new stdClass();
        $data->templateName = $this->getTemplateName();
        $data->file = $this->getFile();
        return $data;
    }

    public static function fromArray(array $parameters): PresentationConvertedFile
    {
        $instance = new static();
        if (isset($parameters['templateName'])) {
            $instance->setTemplateName($parameters['templateName']);
        }
        if (isset($parameters['file'])) {
            $instance->setFile(File::fromArray($parameters['file']));
        }
        return $instance;
    }
}
