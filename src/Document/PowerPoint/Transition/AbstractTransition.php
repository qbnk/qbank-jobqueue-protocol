<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Document\PowerPoint\Transition;

use DirectoryIterator;
use RuntimeException;

class AbstractTransition
{
    public function __construct(array $parameters = [])
    {
        foreach ($parameters as $key => $value) {
            $method = 'set' . $key;
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
    }

    public static function getName()
    {
        $nameSpace = explode('\\', static::class());
        return array_pop($nameSpace);
    }

    public function getProperties()
    {
        return [];
    }

    /**
     * Gets an instance of a transition by the transitions type id.
     *
     * @param int $typeId
     * @param array $params The constructor parameters for the transition
     * @return EmptyTransition
     * @throws RuntimeException Thrown when no transition with the supplied type id exists.
     * @since 2016-10-24
     * @author Björn Hjortsten
     */
    public static function factory($typeId, array $params = [])
    {
        if ($typeId === 0) {
            return new EmptyTransition($params);
        }

        foreach (new DirectoryIterator(__DIR__) as $fileInfo) {
            if (!$fileInfo->isFile() || $fileInfo->getBasename('.php') == 'AbstractTransition') {
                continue;
            }
            $className = 'QBNK\\JobQueue\\Model\\PowerPoint\\Transition\\' . $fileInfo->getBasename('.php');
            if (constant($className . '::TYPE') == $typeId) {
                return new $className($params);
            }
        }

        throw new RuntimeException('Could not find a transition with the type "' . $typeId . '".');
    }
}
