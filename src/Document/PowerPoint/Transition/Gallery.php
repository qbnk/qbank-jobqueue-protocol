<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Document\PowerPoint\Transition;

use QBNK\JobQueue\Job\Document\PowerPoint\Transition\Type\LeftRightDirectionType;

class Gallery extends LeftRightDirectionType
{
    public const TYPE = 34;

    public function jsonSerialize(): \stdClass
    {
        return (object) array_merge((array) parent::jsonSerialize(), [
            'type' => self::TYPE
        ]);
    }

    public static function getName(): string
    {
        return gettext('slidebuilder.transition.gallery');
    }
}
