<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Document\PowerPoint\Transition;

use JsonSerializable;

class EmptyTransition extends AbstractTransition implements TransitionInterface, JsonSerializable
{
    /**
     * @var int
     */
    protected $advanceAfterTime = 0;

    /**
     * @var boolean
     */
    protected $advanceOnClick = true;

    /**
     * @var int
     */
    protected $speed;

    /**
     * Gets the time to advance after in milliseconds.
     * @return int
     */
    public function getAdvanceAfterTime(): int
    {
        return $this->advanceAfterTime;
    }

    /**
     * Sets the time to advance slides after in milliseconds.
     * @param int $advanceAfterTime
     * @return $this
     */
    public function setAdvanceAfterTime(int $advanceAfterTime)
    {
        $this->advanceAfterTime = $advanceAfterTime;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isAdvanceOnClick(): bool
    {
        return $this->advanceOnClick;
    }

    /**
     * @param boolean $advanceOnClick
     * @return $this
     */
    public function setAdvanceOnClick(bool $advanceOnClick)
    {
        $this->advanceOnClick = $advanceOnClick;
        return $this;
    }

    /**
     * Gets the transition speed in milliseconds
     * @return int
     */
    public function getSpeed(): int
    {
        return $this->speed;
    }

    /**
     * Sets the transition speed in milliseconds
     * @param int $speed
     * @return $this
     */
    public function setSpeed(int $speed)
    {
        $this->speed = $speed;
        return $this;
    }

    public static function getName(): string
    {
        return gettext('slidebuilder.transition.empty');
    }

    public function jsonSerialize(): \stdClass
    {
        $className = self::class;
        $className = substr($className, strrpos($className, '\\') + 1);
        return (object) [
            'qlass' => $className,
            'advanceAfterTime' => $this->advanceAfterTime,
            'advanceOnClick' => $this->advanceOnClick,
            'speed' => $this->speed,
            'type' => 0
        ];
    }
}
