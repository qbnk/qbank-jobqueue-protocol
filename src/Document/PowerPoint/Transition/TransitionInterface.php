<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Document\PowerPoint\Transition;

interface TransitionInterface
{
    public function getAdvanceAfterTime(): int;

    public function setAdvanceAfterTime(int $advanceAfterTime);

    public function isAdvanceOnClick(): bool;

    public function setAdvanceOnClick(bool $advanceOnClick);

    public function getSpeed(): int;

    public function setSpeed(int $speed);

    public static function getName(): string;
}
