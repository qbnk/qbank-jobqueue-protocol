<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Document\PowerPoint\Transition\Type;

use QBNK\QBank\Object\PropertyType\Model\PropertyType;

class FlyThroughType extends InOutType
{
    /**
     * @var boolean
     */
    protected $bounce;

    /**
     * @return boolean
     */
    public function hasBounce()
    {
        return $this->bounce;
    }

    /**
     * @param boolean $bounce
     * @return $this
     */
    public function setBounce($bounce)
    {
        $this->bounce = $bounce;
        return $this;
    }

    public function getProperties()
    {
        return array_merge(
            parent::getProperties(),
            [
                [
                    'name' => gettext('powerpoint.effect.bounce'),
                    'systemname' => 'bounce',
                    'datatype_id' => PropertyType::BOOLEAN
                ]
            ]
        );
    }

    public function jsonSerialize(): \stdClass
    {
        $className = self::class;
        $className = substr($className, strrpos($className, '\\') + 1);
        return (object) array_merge((array) parent::jsonSerialize(), [
            'qlass' => $className,
            'bounce' => $this->bounce
        ]);
    }
}
