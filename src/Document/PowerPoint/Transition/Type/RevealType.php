<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Document\PowerPoint\Transition\Type;

use QBNK\QBank\Object\PropertyType\Model\PropertyType;

class RevealType extends OptionalBlackType
{
    public const DIRECTION_LEFT = 0;
    public const DIRECTION_RIGHT = 1;

    /**
     * @var int
     */
    protected $direction;

    /**
     * @return int
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param int $direction
     * @return $this
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
        return $this;
    }

    public function getProperties()
    {
        return array_merge(parent::getProperties(), [
            [
                'name' => gettext('powerpoint.effect.direction'),
                'systemname' => 'direction',
                'datatype_id' => PropertyType::INTEGER,
                'definition' => [
                    'mandatory' => false,
                    'array' => true,
                    'options' => [
                        ['key' => self::DIRECTION_LEFT, 'value' => gettext('powerpoint.effect.left')],
                        ['key' => self::DIRECTION_RIGHT, 'value' => gettext('powerpoint.effect.right')]
                    ]
                ]
            ]
        ]);
    }

    public function jsonSerialize(): \stdClass
    {
        $className = self::class;
        $className = substr($className, strrpos($className, '\\') + 1);
        return (object) array_merge((array) parent::jsonSerialize(), [
            'qlass' => $className,
            'direction' => $this->direction
        ]);
    }
}
