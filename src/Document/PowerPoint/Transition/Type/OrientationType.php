<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Document\PowerPoint\Transition\Type;

use QBNK\JobQueue\Job\Document\PowerPoint\Transition\EmptyTransition;
use QBNK\QBank\Object\PropertyType\Model\PropertyType;

class OrientationType extends EmptyTransition
{
    public const DIRECTION_HORIZONTAL = 0;
    public const DIRECTION_VERTICAL = 1;

    /**
     * @var int
     */
    protected $direction;

    /**
     * @return int
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param int $direction
     *
     * @return $this
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
        return $this;
    }

    public function getProperties()
    {
        return array_merge([
            [
                'name' => gettext('powerpoint.effect.direction'),
                'systemname' => 'direction',
                'datatype_id' => PropertyType::INTEGER,
                'definition' => [
                    'mandatory' => false,
                    'array' => true,
                    'options' => [
                        ['key' => self::DIRECTION_HORIZONTAL, 'value' => gettext('powerpoint.effect.horizontal')],
                        ['key' => self::DIRECTION_VERTICAL, 'value' => gettext('powerpoint.effect.vertical')]
                    ]
                ]
            ]
        ], parent::getProperties());
    }

    public function jsonSerialize(): \stdClass
    {
        $className = self::class;
        $className = substr($className, strrpos($className, '\\') + 1);
        return (object) array_merge((array) parent::jsonSerialize(), [
            'qlass' => $className,
            'direction' => $this->direction
        ]);
    }
}
