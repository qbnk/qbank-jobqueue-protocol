<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Document\PowerPoint\Transition\Type;

use QBNK\JobQueue\Job\Document\PowerPoint\Transition\EmptyTransition;
use QBNK\QBank\Object\PropertyType\Model\PropertyType;

class EightDirectionType extends EmptyTransition
{
    public const DIRECTION_LEFT_DOWN = 0;
    public const DIRECTION_LEFT_UP = 1;
    public const DIRECTION_RIGHT_DOWN = 2;
    public const DIRECTION_RIGHT_UP = 3;
    public const DIRECTION_LEFT = 4;
    public const DIRECTION_UP = 5;
    public const DIRECTION_DOWN = 6;
    public const DIRECTION_RIGHT = 7;

    /**
     * @var int
     */
    protected $direction;

    /**
     * @return int
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param int $direction
     * @return $this
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
        return $this;
    }

    public function getProperties()
    {
        return array_merge([
            [
                'name' => gettext('powerpoint.effect.direction'),
                'systemname' => 'direction',
                'datatype_id' => PropertyType::INTEGER,
                'definition' => [
                    'mandatory' => false,
                    'array' => true,
                    'options' => [
                        ['key' => self::DIRECTION_LEFT_DOWN, 'value' => gettext('powerpoint.effect.left_down')],
                        ['key' => self::DIRECTION_LEFT_UP, 'value' => gettext('powerpoint.effect.left_up')],
                        ['key' => self::DIRECTION_RIGHT_DOWN, 'value' => gettext('powerpoint.effect.right_down')],
                        ['key' => self::DIRECTION_RIGHT_UP, 'value' => gettext('powerpoint.effect.right_up')],
                        ['key' => self::DIRECTION_LEFT, 'value' => gettext('powerpoint.effect.left')],
                        ['key' => self::DIRECTION_UP, 'value' => gettext('powerpoint.effect.up')],
                        ['key' => self::DIRECTION_DOWN, 'value' => gettext('powerpoint.effect.down')],
                        ['key' => self::DIRECTION_RIGHT, 'value' => gettext('powerpoint.effect.right')]
                    ]
                ]
            ]
        ], parent::getProperties());
    }

    public function jsonSerialize(): \stdClass
    {
        $className = self::class;
        $className = substr($className, strrpos($className, '\\') + 1);
        return (object) array_merge((array) parent::jsonSerialize(), [
            'qlass' => $className,
            'direction' => $this->direction
        ]);
    }
}
