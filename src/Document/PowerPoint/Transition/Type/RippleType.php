<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Document\PowerPoint\Transition\Type;

class RippleType extends CornerAndCenterDirectionType
{
    public function jsonSerialize(): \stdClass
    {
        $className = self::class;
        $className = substr($className, strrpos($className, '\\') + 1);
        return (object) array_merge((array) parent::jsonSerialize(), [
            'qlass' => $className,
            'direction' => $this->direction
        ]);
    }
}
