<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Document\PowerPoint\Transition\Type;

use QBNK\QBank\Object\PropertyType\Model\PropertyType;

class SplitType extends InOutType
{
    public const ORIENTATION_HORIZONTAL = 0;
    public const ORIENTATION_VERTICAL = 1;

    /**
     * @var int
     */
    protected $orientation;

    /**
     * @return int
     */
    public function getOrientation()
    {
        return $this->orientation;
    }

    /**
     * @param int $orientation
     * @return $this
     */
    public function setOrientation($orientation)
    {
        $this->orientation = $orientation;
        return $this;
    }

    public function getProperties()
    {
        return array_merge(parent::getProperties(), [
            [
                'name' => gettext('powerpoint.effect.orientation'),
                'systemname' => 'orientation',
                'datatype_id' => PropertyType::INTEGER,
                'definition' => [
                    'mandatory' => false,
                    'array' => true,
                    'options' => [
                        ['key' => self::ORIENTATION_HORIZONTAL, 'value' => gettext('powerpoint.effect.horizontal')],
                        ['key' => self::ORIENTATION_VERTICAL, 'value' => gettext('powerpoint.effect.vertical')]
                    ]
                ]
            ]
        ]);
    }

    public function jsonSerialize(): \stdClass
    {
        $className = self::class;
        $className = substr($className, strrpos($className, '\\') + 1);
        return (object) array_merge((array) parent::jsonSerialize(), [
            'qlass' => $className,
            'orientation' => $this->orientation
        ]);
    }
}
