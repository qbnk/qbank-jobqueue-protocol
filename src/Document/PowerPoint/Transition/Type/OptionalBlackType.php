<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Document\PowerPoint\Transition\Type;

use QBNK\JobQueue\Job\Document\PowerPoint\Transition\EmptyTransition;
use QBNK\QBank\Object\PropertyType\Model\PropertyType;

class OptionalBlackType extends EmptyTransition
{
    /**
     * @var boolean
     */
    protected $fromBlack;

    /**
     * @return boolean
     */
    public function isFromBlack()
    {
        return $this->fromBlack;
    }

    /**
     * @param boolean $fromBlack
     * @return $this
     */
    public function setFromBlack($fromBlack)
    {
        $this->fromBlack = $fromBlack;
        return $this;
    }

    public function getProperties()
    {
        return array_merge([
            [
                'name' => gettext('powerpoint.effect.fromblack'),
                'systemname' => 'direction',
                'datatype_id' => PropertyType::BOOLEAN,
            ]
        ], parent::getProperties());
    }

    public function jsonSerialize(): \stdClass
    {
        $className = self::class;
        $className = substr($className, strrpos($className, '\\') + 1);
        return (object) array_merge((array) parent::jsonSerialize(), [
            'qlass' => $className,
            'fromBlack' => $this->fromBlack
        ]);
    }
}
