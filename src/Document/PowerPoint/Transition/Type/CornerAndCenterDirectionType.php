<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Document\PowerPoint\Transition\Type;

use QBNK\QBank\Object\PropertyType\Model\PropertyType;

class CornerAndCenterDirectionType extends CornerDirectionType
{
    public const DIRECTION_CENTER = 4;

    public function getProperties()
    {
        return array_merge([
            [
                'name' => gettext('powerpoint.effect.direction'),
                'systemname' => 'direction',
                'datatype_id' => PropertyType::INTEGER,
                'definition' => [
                    'mandatory' => false,
                    'array' => true,
                    'options' => [
                        ['key' => self::DIRECTION_LEFT_DOWN, 'value' => gettext('powerpoint.effect.left_down')],
                        ['key' => self::DIRECTION_LEFT_UP, 'value' => gettext('powerpoint.effect.left_up')],
                        ['key' => self::DIRECTION_RIGHT_DOWN, 'value' => gettext('powerpoint.effect.right_down')],
                        ['key' => self::DIRECTION_RIGHT_UP, 'value' => gettext('powerpoint.effect.right_up')],
                        ['key' => self::DIRECTION_CENTER, 'value' => gettext('powerpoint.effect.center')],
                    ]
                ]
            ]
        ], parent::getProperties());
    }

    public function jsonSerialize(): \stdClass
    {
        $className = self::class;
        $className = substr($className, strrpos($className, '\\') + 1);
        return (object) array_merge((array) parent::jsonSerialize(), [
            'qlass' => $className,
            'direction' => $this->direction
        ]);
    }
}
