<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Document\PowerPoint\Transition\Type;

use QBNK\QBank\Object\PropertyType\Model\PropertyType;

class GlitterType extends SideDirectionType
{
    /**
     * Diamond tile pattern
     */
    public const PATTERN_DIAMOND = 0;

    /**
     * Hexagon tile pattern
     */
    public const PATTERN_HEXAGON = 1;

    /**
     * @var int
     */
    protected $pattern;

    /**
     * @return int
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * @param int $pattern
     * @return $this
     */
    public function setPattern($pattern)
    {
        $this->pattern = $pattern;
        return $this;
    }

    public function getProperties()
    {
        return array_merge(
            parent::getProperties(),
            [
                [
                    'name' => gettext('powerpoint.effect.pattern'),
                    'systemname' => 'pattern',
                    'datatype_id' => PropertyType::INTEGER,
                    'definition' => [
                        'mandatory' => false,
                        'array' => true,
                        'options' => [
                            ['key' => self::PATTERN_DIAMOND, 'value' => gettext('powerpoint.effect.diamond')],
                            ['key' => self::PATTERN_HEXAGON, 'value' => gettext('powerpoint.effect.hexagon')]
                        ]
                    ]
                ]
            ]
        );
    }

    public function jsonSerialize(): \stdClass
    {
        $className = self::class;
        $className = substr($className, strrpos($className, '\\') + 1);
        return (object) array_merge((array) parent::jsonSerialize(), [
            'qlass' => $className,
            'pattern' => $this->pattern
        ]);
    }
}
