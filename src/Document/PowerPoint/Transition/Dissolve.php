<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Document\PowerPoint\Transition;

class Dissolve extends EmptyTransition
{
    public const TYPE = 8;

    public function jsonSerialize(): \stdClass
    {
        return (object) array_merge((array) parent::jsonSerialize(), [
            'type' => self::TYPE
        ]);
    }

    public static function getName(): string
    {
        return gettext('slidebuilder.transition.dissolve');
    }
}
