<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Document\PowerPoint\Transition;

use QBNK\JobQueue\Job\Document\PowerPoint\Transition\Type\SideDirectionType;

class Orbit extends SideDirectionType
{
    public const TYPE = 30;

    public function jsonSerialize(): \stdClass
    {
        return (object) array_merge((array) parent::jsonSerialize(), [
            'type' => self::TYPE
        ]);
    }

    public static function getName(): string
    {
        return gettext('slidebuilder.transition.orbit');
    }
}
