<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Document;

use JsonSerializable;
use QBNK\JobQueue\Job\Document\PowerPoint\Transition\AbstractTransition;
use QBNK\JobQueue\Job\Document\PowerPoint\Transition\TransitionInterface;

class CombineSlideStructure implements JsonSerializable
{
    /**
     * @var string
     */
    private $filename;

    /**
     * @var int
     */
    private $slide;

    /**
     * @var TransitionInterface
     */
    private $transition;

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param string $filename The presentation to use a slide from
     *
     * @return $this
     */
    public function setFilename(string $filename): CombineSlideStructure
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return int The slide index to use (0 is the first slide)
     */
    public function getSlide(): int
    {
        return $this->slide;
    }

    /**
     * @param int $slide
     *
     * @return $this
     */
    public function setSlide(int $slide): CombineSlideStructure
    {
        $this->slide = $slide;
        return $this;
    }

    /**
     * @return TransitionInterface
     */
    public function getTransition(): TransitionInterface
    {
        return $this->transition;
    }

    /**
     * @param TransitionInterface $transition
     *
     * @return $this
     */
    public function setTransition(TransitionInterface $transition)
    {
        $this->transition = $transition;
        return $this;
    }

    public function jsonSerialize(): \stdClass
    {
        return [
            'filename' => $this->getFilename(),
            'slide' => $this->getSlide(),
            'transition' => $this->getTransition()
        ];
    }

    public static function fromArray(array $parameters): CombineSlideStructure
    {
        $instance = new static();
        if (isset($parameters['filename'])) {
            $instance->setFilename($parameters['filename']);
        }
        if (isset($parameters['slide'])) {
            $instance->setSlide((int)$parameters['slide']);
        }
        if (isset($parameters['transition'])) {
            $instance->setTransition(
                AbstractTransition::factory($parameters['transition']['type'] ?? 0, $parameters['transition'])
            );
        }
        return $instance;
    }
}
