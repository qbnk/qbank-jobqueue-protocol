<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Storage;

use DateTime;
use DateTimeZone;
use Exception;
use JsonSerializable;
use stdClass;

class CompressedFile implements JsonSerializable
{
    /**
     * @var string
     */
    protected $archivePath;

    /**
     * @var int
     */
    protected $compressedSize;

    /**
     * @var int
     */
    protected $realSize;

    /**
     * @var DateTime
     */
    protected $created;

    /**
     * @var DateTime
     */
    protected $modified;

    /**
     * See Phar:: compression constants
     * @var int
     */
    protected $compressionAlgorithm;

    /**
     * @param array $parameters
     * @return DateTime
     * @throws Exception
     */
    public static function dateTimeFromArray(array $parameters)
    {
        return new DateTime($parameters['date'], new DateTimeZone($parameters['timezone']));
    }

    /**
     * @return string
     */
    public function getArchivePath(): string
    {
        return $this->archivePath;
    }

    /**
     * @param string $archivePath
     * @return $this
     */
    public function setArchivePath(string $archivePath): CompressedFile
    {
        $this->archivePath = $archivePath;
        return $this;
    }

    /**
     * @return int
     */
    public function getCompressedSize(): int
    {
        return $this->compressedSize;
    }

    /**
     * @param int $compressedSize
     * @return $this
     */
    public function setCompressedSize(int $compressedSize): CompressedFile
    {
        $this->compressedSize = $compressedSize;
        return $this;
    }

    /**
     * @return int
     */
    public function getRealSize(): int
    {
        return $this->realSize;
    }

    /**
     * @param int $realSize
     * @return $this
     */
    public function setRealSize(int $realSize): CompressedFile
    {
        $this->realSize = $realSize;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreated(): DateTime
    {
        return $this->created;
    }

    /**
     * @param DateTime|array|string $created
     * @return $this
     */
    public function setCreated($created): CompressedFile
    {
        if ($created instanceof DateTime) {
            $this->created = $created;
        }
        if (is_array($created)) {
            $this->created = self::dateTimeFromArray($created);
        } else {
            $this->created = new DateTime($created);
        }
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getModified(): ?DateTime
    {
        return $this->modified;
    }

    /**
     * @param DateTime|array|string $modified
     * @return $this
     * @throws Exception
     */
    public function setModified($modified): CompressedFile
    {
        if ($modified instanceof DateTime) {
            $this->modified = $modified;
        }
        if (is_array($modified)) {
            $this->modified = self::dateTimeFromArray($modified);
        } else {
            $this->modified = new DateTime($modified);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getCompressionAlgorithm(): int
    {
        return $this->compressionAlgorithm;
    }

    /**
     * @param int $compressionAlgorithm
     * @return $this
     */
    public function setCompressionAlgorithm(int $compressionAlgorithm): CompressedFile
    {
        $this->compressionAlgorithm = $compressionAlgorithm;
        return $this;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = new stdClass();
        $json->archivePath = $this->getArchivePath();
        $json->compressedSize = $this->getCompressedSize();
        $json->realSize = $this->getRealSize();
        $json->created = $this->getCreated();
        $json->modified = $this->getModified();
        $json->compressionAlgorithm = $this->getCompressionAlgorithm();
        return $json;
    }

    public static function fromArray(array $parameters): CompressedFile
    {
        return (new self())
            ->setArchivePath($parameters['archivePath'])
            ->setCompressedSize($parameters['compressedSize'])
            ->setRealSize($parameters['realSize'])
            ->setCreated($parameters['created'])
            ->setModified($parameters['modified'])
            ->setCompressionAlgorithm($parameters['compressionAlgorithm'])
        ;
    }
}
