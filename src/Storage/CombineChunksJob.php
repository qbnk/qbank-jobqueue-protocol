<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Storage;

use QBNK\JobQueue\Job\Examination\MimeTypeJob;
use QBNK\JobQueue\Job\JobAbstract;

class CombineChunksJob extends JobAbstract
{
    public const QUEUE_NAME = 'combinechunks';

    /**
     * Directory with chunks to combine into a single file
     * string
     */
    protected $chunkDirectory;

    /**
     * Optional checksum that the combined file is expected to match
     * @var string
     */
    protected $checkSum;

    /**
     * When uploading new versions of media, the uploader may want to keep the existing thumbnails instead of overwriting
     * @var bool
     */
    protected $createThumbnails;

    /**
     * Path to combined file
     * @var File
     */
    protected $combinedFile;

    /**
     * Mimetype examination result of combined file
     * @var MimeTypeJob
     */
    protected $mimeTypeJob;

    public function getQueueName(): string
    {
        return self::QUEUE_NAME;
    }

    /**
     * @return string
     */
    public function getChunkDirectory(): string
    {
        return $this->chunkDirectory;
    }

    /**
     * @param string $chunkDirectory
     * @return self
     */
    public function setChunkDirectory(string $chunkDirectory): static
    {
        $this->chunkDirectory = $chunkDirectory;
        return $this;
    }

    /**
     * @return bool
     */
    public function shouldCreateThumbnails(): bool
    {
        return $this->createThumbnails ?? true;
    }

    /**
     * @param bool $createThumbnails
     * @return self
     */
    public function setCreateThumbnails(bool $createThumbnails): static
    {
        $this->createThumbnails = $createThumbnails;
        return $this;
    }

    /**
     * @return string
     */
    public function getCheckSum(): ?string
    {
        return $this->checkSum;
    }

    /**
     * @param string $checkSum
     * @return self
     */
    public function setCheckSum(string $checkSum = null): static
    {
        $this->checkSum = $checkSum;
        return $this;
    }

    /**
     * @return File
     */
    public function getCombinedFile(): ?File
    {
        return $this->combinedFile;
    }

    /**
     * @param array|File $combinedFile
     * @return self
     */
    public function setCombinedFile($combinedFile): static
    {
        $this->combinedFile = is_array($combinedFile) ? File::fromArray($combinedFile) : $combinedFile;
        return $this;
    }

    /**
     * @return MimeTypeJob
     */
    public function getMimeTypeJob(): ?MimeTypeJob
    {
        return $this->mimeTypeJob;
    }

    /**
     * @param array|MimeTypeJob $mimeTypeJob
     * @return self
     */
    public function setMimeTypeJob($mimeTypeJob): static
    {
        $this->mimeTypeJob = is_array($mimeTypeJob) ? MimeTypeJob::fromArray($mimeTypeJob) : $mimeTypeJob;
        return $this;
    }

    public function jsonSerialize(): \stdClass
    {
        $jsonData = parent::jsonSerialize();
        $jsonData->chunkDirectory = $this->getChunkDirectory();
        $jsonData->checkSum = $this->getCheckSum();
        $jsonData->combinedFile = $this->getCombinedFile();
        $jsonData->mimeTypeJob = $this->getMimeTypeJob();
        $jsonData->createThumbnails = $this->shouldCreateThumbnails();
        return $jsonData;
    }
}
