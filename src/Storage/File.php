<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Storage;

use JsonSerializable;
use QBNK\JobQueue\Job\JobQueueException;
use SplFileInfo;
use stdClass;

class File extends SplFileInfo implements JsonSerializable
{
    /**
     * Simple media properties, [(string)'systemname' => (mixed)value]
     * @var array
     */
    protected array $properties;

    /**
     * QBank type classification - image/video/etc..
     * @var string|null
     */
    protected ?string $classification;

    protected ?string $mimeType;

    protected ?string $workingCopy;

    public function __construct(
        string $file,
        string $mimeType = null,
        string $classification = null,
        array $properties = []
    ) {
        parent::__construct($file);
        $this->mimeType = $mimeType;
        $this->classification = $classification;
        $this->properties = $properties;
    }

    public function getClassification(): ?string
    {
        return $this->classification;
    }

    public function setClassification(string $classification): static
    {
        $this->classification = $classification;
        return $this;
    }

    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    public function setMimeType(string $mimeType): static
    {
        $this->mimeType = $mimeType;
        return $this;
    }

    public function getProperties(): array
    {
        return $this->properties;
    }

    public function setProperties(array $properties): static
    {
        $this->properties = $properties;
        return $this;
    }

    public function getWorkingCopy(): string
    {
        if (!isset($this->workingCopy)) {
            throw new JobQueueException('WorkingCopy not set for file');
        }
        return $this->workingCopy;
    }

    /**
     * @param string $path
     * @return $this
     */
    public function setWorkingCopy(string $path): static
    {
        $this->workingCopy = $path;
        return $this;
    }

    public function jsonSerialize(): \stdClass
    {
        $jsonData = new stdClass();
        $jsonData->source = $this->getPathname();
        $jsonData->mimeType = $this->getMimeType();
        $jsonData->classification = $this->getClassification();
        $jsonData->properties = $this->getProperties();
        return $jsonData;
    }

    public static function fromArray(array $parameters): File
    {
        return new static(
            $parameters['source'],
            $parameters['mimeType'] ?? null,
            $parameters['classification'] ?? null,
            $parameters['properties'] ?? []
        );
    }
}
