<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Storage;

/**
 * Describes the Flysystem protocol that a job should use to work with any associated files
 * @package QBNK\JobQueue\Job
 */
class StorageConfig
{
    public const TYPE_LOCAL = 'FileSystem';

    public const TYPE_S3 = 'S3';

    public const TYPE_UNKNOWN = 'unknown';

    public string $type;

    public string $sharedTempDirectory;

    public array $config;

    public static function fromArray(array $parameters): static
    {
        $self = new self();
        $self->type = $parameters['type'] ?? self::TYPE_UNKNOWN;
        $self->sharedTempDirectory = $parameters['sharedTempDirectory'] ?? '/';
        $self->config = $parameters['config'] ?? [];
        return $self;
    }
}
