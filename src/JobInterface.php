<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job;

interface JobInterface
{
    /**
     * Name of queue to place this job in
     * @return string
     */
    public function getQueueName(): string;
}
