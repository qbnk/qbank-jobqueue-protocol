<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert\Model;

class NoConvertTarget extends TargetAbstract
{
    public static function fromArray($parameters): NoConvertTarget
    {
        $parameters['class'] = self::class;
        return parent::fromArray($parameters);
    }
}
