<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert\Model;

class ImageConvertTarget extends TargetAbstract
{
    /**
     * While imageMagick might support additional output formats, we'll limit our convert support to the common formats
     * so that we don't have customers expectations running amok if they would see svg, psd etc as valid output.
     * @var string[]
     */
    public const SUPPORTED_FORMATS = [
        'image/tiff' => 'tiff',
        'image/jpeg' => 'jpg',
        'image/png' => 'png',
        'image/gif' => 'gif',
        'image/webp' => 'webp',
        'image/avif' => 'avif',
        'image/svg' => 'svg',
    ];

    public static function fromArray(array $parameters): ImageConvertTarget
    {
        $parameters['class'] = self::class;
        return parent::fromArray($parameters);
    }
}
