<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert\Model;

use JsonSerializable;
use QBNK\JobQueue\Job\Convert\Html2PdfJob;
use QBNK\JobQueue\Job\InstanceHelper;

class PdfText implements JsonSerializable
{
    protected const ALIGNMENT_MAP = [
      'text-align' => [
        Html2PdfJob::ALIGNMENT_LEFT => 'start',
        Html2PdfJob::ALIGNMENT_RIGHT => 'end',
        Html2PdfJob::ALIGNMENT_CENTER => 'center'
      ],
      'float' => [
        Html2PdfJob::ALIGNMENT_LEFT => 'left',
        Html2PdfJob::ALIGNMENT_RIGHT => 'right'
      ]
    ];
    protected string $content = '';
    protected string $alignment = 'left';
    protected bool $float = false;

    protected string $alignmentStyles = 'float: left;';


    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;
        return $this;
    }

    public function getAlignment(): string
    {
        return $this->alignment;
    }

    public function setAlignment(string $alignment): self
    {
        $this->alignment = $alignment;
        $cssAttribute = $this->float ? 'float ' : 'text-align ';
        $cssValue = self::ALIGNMENT_MAP[$cssAttribute][$alignment] ?? null;
        if ($cssValue === null) {
            $this->alignmentStyles = $alignment === 'left' ? 'float: left;' : ($alignment === 'right' ? 'float: right;' : 'text-align: center;');
        } else {
            throw new \InvalidArgumentException('Invalid $alignment argument passed');
        }
        return $this;
    }

    public function getFloat(): bool
    {
        return $this->float;

    }

    public function setFloat(bool $float): self
    {
        $this->float = $float;
        $this->setAlignment($this->alignment);
        return $this;
    }

    public function getAlignmentStyles(): string
    {
        return $this->alignmentStyles;

    }

    public static function fromArray(array $parameters): self
    {
        if (!isset($parameters['class'])) {
            $parameters['class'] = self::class;
        }
        return InstanceHelper::fromArray($parameters);
    }


    public function jsonSerialize(): array
    {
        $data = [];
        $data['class'] = get_class($this);
        $data['content'] = $this->content;
        $data['float'] = $this->float;
        $data['alignment'] = $this->alignment;
        $data['alignmentStyles'] = $this->alignmentStyles;

        return $data;
    }
}
