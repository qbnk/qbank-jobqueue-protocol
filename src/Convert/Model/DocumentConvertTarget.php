<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert\Model;

/**
 * Represents a target during a document conversion. Contains all the possible settings.
 * @author Kristian Sandström
 * @since 2016-09-07
 */
class DocumentConvertTarget extends TargetAbstract
{
    public static function fromArray(array $parameters): DocumentConvertTarget
    {
        $parameters['class'] = self::class;
        return parent::fromArray($parameters);
    }
}
