<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert\Model;

use JsonSerializable;
use QBNK\JobQueue\Job\InstanceHelper;

class PdfHeader implements JsonSerializable
{
    protected string $id = 'header';
    /** @var PdfText[] */
    protected array $texts = [];
    protected string $baseStyles = 'position: fixed; left: 0; right: 0; padding: 40px 0px; height: 100px; top: -100px;';
    protected string $extraStyles = '';


    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    /** @return PdfText[] */
    public function getTexts(): array
    {
        return $this->texts;
    }

    public function setTexts(array $texts): self
    {
        if (!empty($texts) && is_array($texts[0])) {
            $texts = array_map(fn ($text) => PdfText::fromArray($text), $texts);
        }
        $this->texts = $texts;
        return $this;
    }

    public function getBaseStyles(): string
    {
        return $this->baseStyles;
    }

    public function setBaseStyles(string $styles): self
    {
        $this->baseStyles = $styles;
        return $this;
    }

    public function getExtraStyles(): string
    {
        return $this->extraStyles;
    }

    public function setExtraStyles(string $styles): self
    {
        $this->extraStyles = $styles;
        return $this;
    }

    public static function fromArray(array $parameters): self
    {
        if (!isset($parameters['class'])) {
            $parameters['class'] = self::class;
        }
        return InstanceHelper::fromArray($parameters);
    }


    public function jsonSerialize(): array
    {
        $data = [];
        $data['class'] = get_class($this);
        $data['id'] = $this->id;
        $data['texts'] = $this->texts;
        $data['baseStyles'] = $this->baseStyles;
        $data['extraStyles'] = $this->extraStyles;

        return $data;
    }
}
