<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert\Model;

class VideoConvertTarget extends TargetAbstract
{
    public static function fromArray(array $parameters): VideoConvertTarget
    {
        $parameters['class'] = self::class;
        return parent::fromArray($parameters);
    }
}
