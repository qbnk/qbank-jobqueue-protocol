<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert\Model;

class AudioConvertTarget extends TargetAbstract
{
    public static function fromArray(array $parameters): AudioConvertTarget
    {
        $parameters['class'] = self::class;
        return parent::fromArray($parameters);
    }
}
