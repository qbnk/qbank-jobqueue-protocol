<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert\Model;

use Doctrine\Instantiator\Exception\InvalidArgumentException;
use JsonSerializable;
use QBNK\JobQueue\Job\Convert\Command\CommandAbstract;
use QBNK\JobQueue\Job\Convert\Command\CommandFactory;
use QBNK\JobQueue\Job\InstanceHelper;
use QBNK\JobQueue\Job\JobQueueException;
use QBNK\JobQueue\Job\Storage\File;
use stdClass;

abstract class TargetAbstract implements JsonSerializable
{
    protected ?string $identifier = null;

    protected ?int $templateId = null;

    protected ?File $target = null;

    /**
     * May carry any message related to the processing of the target
     * @var string|null
     */
    protected ?string $message = null;

    /**
     * @var CommandAbstract[]
     */
    protected array $commands = [];

    /**
     * Constructs a new {@link ImageConvertTarget}
     * @param string|null $identifier The name of the target. Usually {<templatetype>_<templateid>} If not supplied, a random identifier will be generated.
     * @author Björn Hjortsten
     * @since 2012-10-19
     */
    public function __construct(string $identifier = null)
    {
        if ($identifier === null) {
            mt_srand();
            $identifier = 'target' . mt_rand();
        }
        $this->identifier = $identifier;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): static
    {
        $this->identifier = $identifier;
        return $this;
    }

    /**
     * @return CommandAbstract[]
     */
    public function getCommands(): array
    {
        return $this->commands;
    }

    /**
     * Slightly more complex setter that auto-casts commands to their respective class if they're json serialized.
     * Depending on the extending class, the namespace for where to construct the commands will differ
     * @param CommandAbstract[] $commands
     * @return $this
     */
    public function setCommands(array $commands): static
    {
        $commandFactory = null;
        $this->commands = array_map(static function ($command) use (&$commandFactory) {
            if (is_array($command)) {
                if (!$commandFactory) {
                    $commandFactory = new CommandFactory();
                }
                $command = $commandFactory->fromArray($command);
            }
            return $command;
        }, $commands);

        return $this;
    }

    public function getTarget(): ?File
    {
        return $this->target;
    }

    public function setTarget(File $target): static
    {
        $this->target = $target;
        return $this;
    }

    public function getTemplateId(): ?int
    {
        return $this->templateId;
    }

    public function setTemplateId(int $templateId): static
    {
        $this->templateId = $templateId;
        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): static
    {
        $this->message = $message;
        return $this;
    }

    public static function fromArray(array $parameters): TargetAbstract
    {
        try {
            $parameters['target'] = is_array($parameters['target'])
                ? File::fromArray($parameters['target'])
                : $parameters['target'];
            return InstanceHelper::fromArray($parameters);
        } catch (InvalidArgumentException $iae) {
            throw new JobQueueException('Could not instantiate TargetAbstract: ' . $iae->getMessage());
        }
    }

    public function jsonSerialize(): \stdClass
    {
        $json = new stdClass();
        $json->templateId = $this->getTemplateId();
        $json->class = get_class($this);
        $json->identifier = $this->getIdentifier();
        $json->target = $this->getTarget();
        $json->commands = $this->getCommands();
        return $json;
    }
}
