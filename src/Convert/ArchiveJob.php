<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert;

use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\JobQueueException;
use QBNK\JobQueue\Job\Storage\File;

/**
 * Create an archive from a given set of source/convert jobs
 * @author Kristian Sandström
 * @since 2021-09-14
 */
class ArchiveJob extends JobAbstract
{
    public const FORMAT_ZIP = 'zip';
    public const FORMAT_TAR = 'tar';

    /**
     * The archive format.
     * Should be one of the format constants.
     * @var string
     */
    protected string $format;

    /**
     * Prepared files to include in zip
     * @var File[]
     */
    protected array $sourceFiles = [];

    /**
     * Any files that need to be converted before being included in zip
     * @var ConvertAbstract[]
     */
    protected array $convertJobs = [];

    protected string $notifyEmail = '';

    /**
     * Created archive
     * @var File|null
     */
    protected ?File $archive = null;

    public function getFormat(): string
    {
        return $this->format ?? self::FORMAT_ZIP;
    }

    public function setFormat(string $format): static
    {
        $this->format = $format;
        return $this;
    }

    public function getQueueName(): string
    {
        return $this->queueName ?? 'archive';
    }

    /**
     * @return File[]
     */
    public function getSourceFiles(): array
    {
        return $this->sourceFiles;
    }

    /**
     * @param File[] $sourceFiles
     * @return ArchiveJob
     */
    public function setSourceFiles(array $sourceFiles): static
    {
        foreach ($sourceFiles as &$file) {
            if (is_array($file)) {
                $file = File::fromArray($file);
            }
        }

        $this->sourceFiles = $sourceFiles;
        return $this;
    }

    /**
     * @return ConvertAbstract[]
     */
    public function getConvertJobs(): array
    {
        return $this->convertJobs;
    }

    /**
     * @param ConvertAbstract[] $convertJobs
     * @return ArchiveJob
     */
    public function setConvertJobs(array $convertJobs): static
    {
        foreach ($convertJobs as &$convertJob) {
            if (is_array($convertJob)) {
                $convertJob = ConvertAbstract::fromArray($convertJob);
            } elseif (!$convertJob instanceof ConvertAbstract) {
                throw new JobQueueException('Passing non-ConvertJob to ArchiveJob; "' . get_class($convertJob) . '"');
            }
        }

        $this->convertJobs = $convertJobs;
        return $this;
    }

    /**
     * @return File
     */
    public function getArchive(): ?File
    {
        return $this->archive;
    }

    /**
     * @param File|array $archive
     * @return ArchiveJob
     */
    public function setArchive($archive): static
    {
        $this->archive = is_array($archive) ? File::fromArray($archive) : $archive;
        return $this;
    }

    public function getNotifyEmail(): string
    {
        return $this->notifyEmail;
    }

    /**
     * @throws JobQueueException if trying to set a bad email.
     */
    public function setNotifyEmail(string $notifyEmail): static
    {
        if (!empty($notifyEmail) && !filter_var($notifyEmail, FILTER_VALIDATE_EMAIL)) {
            throw new JobQueueException(
                'Bad email format: ' . $notifyEmail,
                JobQueueException::ERR_FORMAT
            );
        }
        $this->notifyEmail = $notifyEmail;
        return $this;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->format = $this->getFormat();
        $json->sourceFiles = $this->getSourceFiles();
        $json->convertJobs = $this->getConvertJobs();
        $json->archive = $this->getArchive();
        $json->notifyEmail = $this->getNotifyEmail();
        return $json;
    }
}
