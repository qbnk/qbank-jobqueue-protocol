<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert;

use QBNK\JobQueue\Job\Convert\Model\NoConvertTarget;
use QBNK\JobQueue\Job\JobQueueException;

class NoConvertJob extends ConvertAbstract
{
    /**
     * A list of different conversions that should be performed.
     * @var NoConvertTarget[]
     */
    protected $targets;

    /**
     * Adds a new target to the job.
     * @param NoConvertTarget $target
     * @return $this
     * @throws JobQueueException Thrown if a target with the same name already is attached to the job.
     * @author Björn Hjortsten
     * @since 2012-10-19
     */
    public function addTarget(NoConvertTarget $target): static
    {
        if ($this->targets === null) {
            $this->targets = [];
        }

        if (isset($this->targets[$target->getIdentifier()])) {
            throw new JobQueueException(
                'Could not add NoConvertTarget to NoConvertJob.'
                . 'Another target with the same name is already added.',
                JobQueueException::ERR_TARGETEXISTS
            );
        }

        $this->targets[$target->getIdentifier()] = $target;
        return $this;
    }

    /**
     * @return NoConvertTarget[]
     */
    public function getTargets(): array
    {
        return $this->targets ?? [];
    }

    /**
     * @param NoConvertTarget[] $targets
     * @return self
     */
    public function setTargets(array $targets): static
    {
        $this->targets = $this->ensureIndexedTargets($targets, NoConvertTarget::class);
        return $this;
    }

    public function getQueueName(): string
    {
        return $this->queueName ?? 'noqueue';
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->targets = [];
        foreach ($this->getTargets() as $identifier => $target) {
            $json->targets[$identifier] = $target->jsonSerialize();
        }
        return $json;
    }
}
