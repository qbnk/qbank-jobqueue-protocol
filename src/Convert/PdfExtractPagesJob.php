<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert;

use QBNK\JobQueue\Job\JobQueueException;
use QBNK\JobQueue\Job\Storage\File;

class PdfExtractPagesJob extends ConvertAbstract
{
    public const QUEUE_NAME = 'pdfextractpages';

    /**
     * The extension to use for the extracted pages, will be <filename>-<page>.<extension>
     * @var string
     */
    protected string $targetExtension;

    /**
     * If not null, indicate which page we should extract
     * @var int|null
     */
    protected ?int $page = null;

    /**
     * If we are extracting only one page, indicate the width you want for the extracted page
     * @var int|null
     */
    protected ?int $width = null;

    /**
     * If we are extracting only one page, indicate the height you want for the extracted page
     * @var int|null
     */
    protected ?int $height = null;

    /**
     * @var File[]
     */
    protected array $outputPages = [];

    public function getPage(): ?int
    {
        return $this->page;
    }

    /**
     * Sets the page to extract, do not set this if you want to extract all pages.
     * @param int $page The page to extract
     * @return PdfExtractPagesJob
     * @throws JobQueueException Thrown if $page is not greater than 0
     * @author Henrik Malmberg
     * @since 2013-12-10
     */
    public function setPage(int $page): PdfExtractPagesJob
    {
        if ($page === 0) {
            throw new JobQueueException('Page must be greater than 0');
        }
        $this->page = $page;
        return $this;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    /**
     * Sets the wanted width of extracted pages, only used if we are extracting a single page.
     * @param int $width The width of the extracted page
     * @return PdfExtractPagesJob
     * @throws JobQueueException
     * @since 2013-12-10
     * @author Henrik Malmberg
     */
    public function setWidth(int $width): PdfExtractPagesJob
    {
        if ($width === 0) {
            throw new JobQueueException('Width must be greater than 0');
        }
        $this->width = $width;
        return $this;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    /**
     * Sets the wanted height of extracted pages, only used if we are extracting a single page.
     * @param int $height The height of the extraced page
     * @return PdfExtractPagesJob
     * @throws JobQueueException
     * @since 2013-12-10
     * @author Henrik Malmberg
     */
    public function setHeight(int $height): PdfExtractPagesJob
    {
        if ($height === 0) {
            throw new JobQueueException('Height must be greater than 0');
        }
        $this->height = $height;
        return $this;
    }

    /**
     * @return File[]
     */
    public function getOutputPages(): array
    {
        return $this->outputPages;
    }

    /**
     * @param File[] $outputPages
     * @return PdfExtractPagesJob
     */
    public function setOutputPages(array $outputPages): PdfExtractPagesJob
    {
        foreach ($outputPages as &$page) {
            if (is_array($page)) {
                $page = File::fromArray($page);
            }
        }

        $this->outputPages = $outputPages;
        return $this;
    }

    public function getTargetExtension(): string
    {
        return $this->targetExtension;
    }

    /**
     * Sets the extension to use for extracted pages, directory and filename are set separately
     * @param string $extension The extension to use for extracted pages
     * @return PdfExtractPagesJob
     * @author Henrik Malmberg
     * @since 2013-12-10
     */
    public function setTargetExtension(string $extension): PdfExtractPagesJob
    {
        $this->targetExtension = $extension;
        return $this;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? self::QUEUE_NAME;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->targetExtension = $this->getTargetExtension();
        $json->page = $this->getPage();
        $json->width = $this->getWidth();
        $json->height = $this->getHeight();
        $json->outputPages = $this->getOutputPages();
        return $json;
    }

    public function reset(): static
    {
        $resetJob = parent::reset();
        return $resetJob->setOutputPages([]);
    }
}
