<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert;

interface PersistConvertInterface
{
    public function getPersistProtocol(): ?array;

    public function setPersistProtocol(?array $persistProtocol): static;
}
