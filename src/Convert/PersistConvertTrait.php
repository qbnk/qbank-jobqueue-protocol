<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert;

trait PersistConvertTrait
{
    /**
     * The protocol settings for the filesystem that should persist converted assets
     * @var array
     */
    protected array $persistProtocol;

    public function getPersistProtocol(): ?array
    {
        return $this->persistProtocol ?? null;
    }

    public function setPersistProtocol(?array $persistProtocol): static
    {
        $this->persistProtocol = $persistProtocol;
        return $this;
    }
}
