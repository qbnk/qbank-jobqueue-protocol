<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert;

use QBNK\JobQueue\Job\Convert\Model\ImageConvertTarget;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\JobQueueException;
use QBNK\JobQueue\Job\Storage\File;

class ImageConvertJob extends ConvertAbstract implements PersistConvertInterface
{
    use PersistConvertTrait;

    public const QUEUE_NAME = 'imageconvert';

    protected bool $sourceIsVector = false;

    /**
     * A list of different conversions that should be performed.
     * @var ImageConvertTarget[]
     */
    protected array $targets = [];

    /**
     * Carrying the converted images if any
     * @var File[]
     */
    protected array $convertedImages = [];

    public function getSourceIsVector(): bool
    {
        return $this->sourceIsVector;
    }

    public function setSourceIsVector(bool $sourceIsVector): static
    {
        $this->sourceIsVector = $sourceIsVector;
        return $this;
    }

    /**
     * Adds a new target to the job.
     * @param ImageConvertTarget $target
     * @return $this
     * @throws JobQueueException Thrown if a target with the same name already is attached to the job.
     * @author Björn Hjortsten
     * @since 2012-10-19
     */
    public function addTarget(ImageConvertTarget $target): static
    {
        if (isset($this->targets[$target->getIdentifier()])) {
            throw new JobQueueException(
                'Could not add ImageConvertTarget to ImageConvertJob.'
                . 'Another target with the same name is already added.',
                JobQueueException::ERR_TARGETEXISTS
            );
        }

        $this->targets[$target->getIdentifier()] = $target;
        return $this;
    }

    /**
     * @return ImageConvertTarget[]
     */
    public function getTargets(): array
    {
        return $this->targets;
    }

    /**
     * @param ImageConvertTarget[] $targets
     * @return ImageConvertJob
     */
    public function setTargets(array $targets): static
    {
        $this->targets = $this->ensureIndexedTargets($targets);
        return $this;
    }

    /**
     * @return File[]
     */
    public function getConvertedImages(): array
    {
        return $this->convertedImages;
    }

    /**
     * @param File[] $convertedImages
     * @return ImageConvertJob
     */
    public function setConvertedImages(array $convertedImages): static
    {
        foreach ($convertedImages as $name => &$file) {
            if (is_array($file)) {
                $file = File::fromArray($file);
            }
        }
        $this->convertedImages = $convertedImages;
        return $this;
    }

    public function getQueueName(): string
    {
        return $this->queueName ?? self::QUEUE_NAME;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->sourceIsVector = $this->sourceIsVector;
        $json->targets = [];
        foreach ($this->getTargets() as $templateName => $target) {
            $json->targets[$templateName] = $target->jsonSerialize();
        }
        $json->convertedImages = [];
        foreach ($this->getConvertedImages() as $templateName => $file) {
            $json->convertedImages[$templateName] = $file->jsonSerialize();
        }
        return $json;
    }

    public function reset(): static
    {
        $resetJob = parent::reset();
        return $resetJob->setConvertedImages([]);
    }
}
