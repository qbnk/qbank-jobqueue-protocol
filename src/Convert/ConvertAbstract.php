<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert;

use QBNK\JobQueue\Job\Convert\Model\AudioConvertTarget;
use QBNK\JobQueue\Job\Convert\Model\DocumentConvertTarget;
use QBNK\JobQueue\Job\Convert\Model\ImageConvertTarget;
use QBNK\JobQueue\Job\Convert\Model\NoConvertTarget;
use QBNK\JobQueue\Job\Convert\Model\TargetAbstract;
use QBNK\JobQueue\Job\Convert\Model\VideoConvertTarget;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\JobQueueException;
use QBNK\JobQueue\Job\Storage\File;

abstract class ConvertAbstract extends JobAbstract
{
    /**
     * @var File
     */
    protected $source;

    /**
     * @var bool
     */
    protected $removeSource;

    /**
     * @return File
     */
    public function getSource(): File
    {
        return $this->source;
    }

    /**
     * @param File $source
     * @return $this
     */
    public function setSource($source)
    {
        $this->source = is_array($source) ? File::fromArray($source) : $source;
        return $this;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        if ($this instanceof PersistConvertInterface) {
            $json->persistProtocol = $this->getPersistProtocol();
        }
        $json->source = $this->source;
        $json->removeSource = $this->removeSource;
        return $json;
    }

    /**
     * @return bool
     */
    public function isRemoveSource(): bool
    {
        return $this->removeSource ?? false;
    }

    /**
     * @param bool $removeSource
     * @return $this
     */
    public function setRemoveSource(bool $removeSource)
    {
        $this->removeSource = $removeSource;
        return $this;
    }

    /**
     * Targets are expected to be indexed for easier manipulation by handlers. This method helps any
     * sloppy implementations where the target identifier isn't relevant for the caller.
     * @param array $targets
     * @return TargetAbstract[]
     * @throws JobQueueException
     */
    protected function ensureIndexedTargets(array $targets, string $targetClass = ImageConvertTarget::class): array
    {
        $indexedTargets = [];

        foreach ($targets as $index => $target) {
            if (is_array($target)) {
                switch ($targetClass) {
                    case ImageConvertTarget::class:
                        $target = ImageConvertTarget::fromArray($target);
                        break;
                    case NoConvertTarget::class:
                        $target = NoConvertTarget::fromArray($target);
                        break;
                    case VideoConvertTarget::class:
                        $target = VideoConvertTarget::fromArray($target);
                        break;
                    case DocumentConvertTarget::class:
                        $target = DocumentConvertTarget::fromArray($target);
                        break;
                    case AudioConvertTarget::class:
                        $target = AudioConvertTarget::fromArray($target);
                        break;
                    default:
                        $target = ConvertAbstract::fromArray($target);
                }
            }

            /** @var TargetAbstract $target */
            if (empty($target->getIdentifier())) {
                $target->setIdentifier(uniqid('target-'));
            }

            if (is_int($index)) {
                //Assume unassigned index
                $index = $target->getIdentifier();
            }

            $indexedTargets[$index] = $target;
        }

        return $indexedTargets;
    }
}
