<?php

namespace QBNK\JobQueue\Job\Convert\Command;

use Doctrine\Instantiator\Exception\ExceptionInterface;
use Doctrine\Instantiator\Instantiator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class CommandFactory
{
    /**
     * @var Instantiator
     */
    protected $instantiator;

    /**
     * When looping "fromArray:s" it is likely that commands in the same namespace are being constructed, so we can often
     * guess the namespace right away instead of looking in the directories each time
     * @var string
     */
    protected $lastMatchedDirectory;

    /**
     * @var string[]
     */
    protected $directoryCache;

    public function __construct()
    {
        $this->instantiator = new Instantiator();
    }

    /**
     * @param array $parameters
     * @return CommandAbstract
     * @throws ExceptionInterface
     */
    public function fromArray(array $parameters): CommandAbstract
    {
        $namespace = $this->getNamespace($parameters['class']);
        $command = $this->instantiator->instantiate($namespace . $parameters['class']);
        unset($parameters['class']);

        foreach ($parameters as $property => $value) {
            if ($value !== null && method_exists($command, 'set' . $property)) {
                $command->{"set{$property}"}($value);
            }
        }

        return $command;
    }

    protected function getNamespace($class)
    {
        $namespace = __NAMESPACE__ . '\\';

        if ($this->lastMatchedDirectory && file_exists(
            __DIR__ . DIRECTORY_SEPARATOR . $this->lastMatchedDirectory . DIRECTORY_SEPARATOR . $class . '.php'
        )) {
            return str_replace(DIRECTORY_SEPARATOR, '\\', $namespace . $this->lastMatchedDirectory . '\\');
        }

        if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . $class . '.php')) {
            $this->lastMatchedDirectory = '';
        } else {
            foreach ($this->getDirectories() as $directory) {
                if (file_exists($directory . DIRECTORY_SEPARATOR . $class . '.php')) {
                    $directory = str_replace(__DIR__ . DIRECTORY_SEPARATOR, '', $directory);
                    $this->lastMatchedDirectory = $directory;
                    $namespace .= str_replace(DIRECTORY_SEPARATOR, '\\', $directory) . '\\';
                    break;
                }
            }
        }

        return str_replace(DIRECTORY_SEPARATOR, '\\', $namespace);
    }

    /**
     * @return string[]
     */
    protected function getDirectories(): array
    {
        if (empty($this->directoryCache)) {
            foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator(__DIR__)) as $iterator) {
                if ($iterator->isDir()) {
                    $this->directoryCache[] = $iterator->getRealPath();
                }
            }
        }

        return $this->directoryCache;
    }
}
