<?php

namespace QBNK\JobQueue\Job\Convert\Command;

use JsonSerializable;

abstract class CommandAbstract implements JsonSerializable
{
    abstract public function getName(): string;

    abstract public function getProperties();

    public function jsonSerialize(): \stdClass
    {
        $array = get_object_vars($this);
        $array['class'] = substr(get_class($this), strrpos(get_class($this), '\\') + 1);

        return (object) $array;
    }
}
