<?php

namespace QBNK\JobQueue\Job\Convert\Command;

interface CommandInterface
{
    public function getProperties();

    public function getName(): string;

    public function getDescription(): string;
}
