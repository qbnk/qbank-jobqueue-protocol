<?php

namespace QBNK\JobQueue\Job\Convert\Command;

final class PropertyTypeEnum
{
    /**
     * A boolean datatype
     * @var integer - represents the id of this datatype in the database
     */
    public const BOOLEAN = 1;

    /**
     * A datetime datatype
     * @var integer - represents the id of this datatype in the database
     */
    public const DATETIME = 2;

    /**
     * A decimal datatype
     * @var integer - represents the id of this datatype in the database
     */
    public const DECIMAL = 3;

    /**
     * A float datatype
     * @var integer - represents the id of this datatype in the database
     */
    public const FLOAT = 4;

    /**
     * A integer datatype
     * @var integer - represents the id of this datatype in the database
     */
    public const INTEGER = 5;

    /**
     * A string datatype
     * @var integer - represents the id of this datatype in the database
     */
    public const STRING = 6;
}
