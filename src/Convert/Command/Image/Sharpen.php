<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class Sharpen extends ImageCommandAbstract
{
    /** @var  float The sigma value */
    protected $sharpen;

    public function __construct($percent)
    {
        $this->setSharpen($percent);
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        return '-sharpen 0x' . $this->sharpen;
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('image_template.command.sharpen'),
                'systemname' => 'sharpen',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => true,
                    'min' => 0,
                    'max' => 100
                ]
            ]
        ];
    }

    /**
     * @return int
     */
    public function getSharpen()
    {
        return round($this->sharpen * 100 / 3.0);
    }

    /**
     * @param float|int $percent
     */
    public function setSharpen($percent)
    {
        if (is_float($percent) && $percent >= 0 && $percent <= 3.0) {
            $this->sharpen = $percent;
        } else {
            $this->sharpen = 3.0 / 100 * (int)$percent;
        }
    }

    public function getName(): string
    {
        return gettext('image_template.command.sharpen');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.sharpen.description');
    }
}
