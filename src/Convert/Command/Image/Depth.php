<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

/**
 * This the number of bits in a color sample within a pixel. Use this option to specify the depth of raw images whose
 * depth is unknown such as GRAY, RGB, or CMYK.
 * @package QBNK\JobQueue\Model\Image\Command
 */
class Depth extends ImageCommandAbstract
{
    /**
     * @var int
     */
    protected $depth;

    public function __construct($depth)
    {
        $this->setDepth($depth);
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        return '-depth ' . $this->getDepth();
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('image_template.command.depth'),
                'systemname' => 'depth',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => [8, 16, 32]
                ]
            ]
        ];
    }

    /**
     * @return int
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * @param int $depth
     */
    public function setDepth($depth)
    {
        $this->depth = (int)$depth;
    }

    public function getName(): string
    {
        return gettext('image_template.command.depth');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.depth.description');
    }
}
