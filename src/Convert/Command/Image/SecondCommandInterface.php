<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

/**
 * Interface SecondCommandInterface
 * @package QBNK\JobQueue\Model\Image\Command
 * @deprecated Try to use LateBindingCommandInterface or command chaining in the main "convert" command instead!
 */
interface SecondCommandInterface
{
    public function getAdditionalArgument($outputFilename, array $targetParameters);
}
