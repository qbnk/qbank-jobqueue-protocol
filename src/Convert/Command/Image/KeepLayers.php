<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

class KeepLayers extends ImageCommandAbstract
{
    public function __construct()
    {
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        return '';
    }

    public function getProperties()
    {
        return false;
    }

    public function getName(): string
    {
        return gettext('image_template.command.keeplayers');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.keeplayers.description');
    }
}
