<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

/**
 * Restrict the maximum JPEG file size, for example -define jpeg:extent=400KB. The JPEG encoder will search for the
 * highest compression quality level that results in an output file that does not exceed the value. The -quality option
 * also will be respected starting with version 6.9.2-5. Between 6.9.1-0 and 6.9.2-4, add -quality 100 in order for the
 * jpeg:extent to work properly. Prior to 6.9.1-0, the -quality setting was ignored.
 * @package QBNK\JobQueue\Model\Image\Command
 */
class MaxJpegFilesize extends ImageCommandAbstract
{
    /**
     * @var int Max filesize in bytes
     */
    protected int $filesize = 0;

    public function __construct(int $filesize)
    {
        $this->setFilesize($filesize);
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        return '-define jpeg:extent=' . $this->getFilesize() . 'b';
    }

    public function getFilesize(): int
    {
        return $this->filesize;
    }

    public function setFilesize(int $filesize): MaxJpegFilesize
    {
        $this->filesize = $filesize;
        return $this;
    }

    public function getProperties(): array
    {
        return [
            [
                'name' => gettext('image_template.command.define:jpegextent'),
                'systemname' => 'filesize',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => true,
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('image_template.command.define:jpegextent');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.define:jpegextent.description');
    }
}
