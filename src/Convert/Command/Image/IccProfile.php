<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

/**
 * Manage ICM profiles in an image.
 * @package QBNK\JobQueue\Model\Image\Command
 */
class IccProfile extends ImageCommandAbstract
{
    public const PROFILE_CMYK_ISOCOATED_V2_300_ECI = 'ISO Coated v2 300% (ECI)';
    public const PROFILE_SRGB = 'sRGB IEC61966-2-1';
    public const PROFILE_SRGB_BLACK_SCALED = 'sRGB IEC61966-2-1 black scaled';

    private $iccProfiles = [
        'CMYK' => 'ISOcoated_v2_300_eci.icc',
        'RGB' => 'sRGB_IEC61966-2-1.icc',
        'SRGB' => 'sRGB_IEC61966-2-1.icc'
    ];

    private $iccProfileFilename = [
        self::PROFILE_CMYK_ISOCOATED_V2_300_ECI => 'ISOcoated_v2_300_eci.icc',
        self::PROFILE_SRGB => 'sRGB_IEC61966-2-1.icc',
        self::PROFILE_SRGB_BLACK_SCALED => 'sRGB_IEC61966-2-1.icc'
    ];

    /** @var bool */
    protected $isBound;

    /**
     * @var string
     */
    protected $iccProfile;

    /**
     * @var bool
     */
    protected $blackPointCompensation;

    /**
     * @var string
     */
    protected $intent;

    public function __construct(
        $iccProfile,
        $intent = ImagickConstants::INTENT_DEFAULT,
        $blackPointCompensation = false
    ) {
        $this->setIccProfile($iccProfile);
        $this->setBlackPointCompensation($blackPointCompensation);
        $this->setIntent($intent);
        $this->isBound = false;
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        $hasProfile = false;
        $colorspace = 'unknown';

        if (!empty($identify['Profile-icc'])) {
            $hasProfile = true;
        }
        if (isset($identify['Colorspace'])) {
            $colorspace = strtoupper($identify['Colorspace']);
        }

        if (!$hasProfile && isset($this->iccProfiles[$colorspace])) {
            // We have to set a input ICC Profile or a colorspace, this is just a "best guess" kind of scenario..
            $argument[] = '-profile ' . escapeshellarg(
                realpath(__DIR__) . '/profiles/' . $this->iccProfiles[$colorspace]
            );
        } else {
            if (!$hasProfile) {
                // Let's go with the sRGB colorspace..
                $argument[] = '-colorspace sRGB';
            }
        }

        if ($this->getIntent()) {
            $argument[] = '-intent ' . $this->getIntent();
        }
        if ($this->isBlackPointCompensation()) {
            $argument[] = '-black-point-compensation';
        }

        // Apply target profile
        if ($this->getIccProfile() && isset($this->iccProfileFilename[$this->getIccProfile()])) {
            $argument[] = '-profile ' . escapeshellarg(
                realpath(__DIR__) . '/profiles/' . $this->iccProfileFilename[$this->getIccProfile()]
            );
        }

        return join(' ', $argument);
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('image_template.command.iccprofile'),
                'systemname' => 'iccprofile',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => []
                ]
            ],
            [
                'name' => gettext('image_template.command.iccprofile.blackpointcompensation'),
                'systemname' => 'blackpointcompensation',
                'datatype_id' => PropertyTypeEnum::BOOLEAN
            ],
            [
                'name' => gettext('image_template.command.iccprofile.intent'),
                'systemname' => 'intent',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => false,
                    'array' => true,
                    'options' => [
                        ImagickConstants::INTENT_ABSOLUTE,
                        ImagickConstants::INTENT_PERCEPTUAL,
                        ImagickConstants::INTENT_RELATIVE,
                        ImagickConstants::INTENT_SATURATION
                    ]
                ]
            ]
        ];
    }

    /**
     * @return string
     */
    public function getIccProfile()
    {
        return $this->iccProfile;
    }

    /**
     * @param string $iccProfile
     */
    public function setIccProfile($iccProfile)
    {
        $this->iccProfile = (string)$iccProfile;
    }

    /**
     * @return boolean
     */
    public function isBlackPointCompensation()
    {
        return $this->blackPointCompensation;
    }

    /**
     * @param boolean $blackPointCompensation
     */
    public function setBlackPointCompensation($blackPointCompensation)
    {
        $this->blackPointCompensation = (bool)$blackPointCompensation;
    }

    /**
     * @return string
     */
    public function getIntent()
    {
        return $this->intent;
    }

    /**
     * @param string $intent
     */
    public function setIntent($intent)
    {
        $this->intent = (string)$intent;
    }

    public function getName(): string
    {
        return gettext('image_template.command.iccprofile');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.iccprofile.description');
    }
}
