<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

/**
 * Strip the image of any profiles or comments.
 * @package QBNK\JobQueue\Model\Image\Command
 */
class Strip extends ImageCommandAbstract
{
    public function __construct()
    {
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        return '-strip';
    }

    public function getProperties()
    {
        return false;
    }

    public function getName(): string
    {
        return gettext('image_template.command.stripprofiles');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.stripprofiles.description');
    }
}
