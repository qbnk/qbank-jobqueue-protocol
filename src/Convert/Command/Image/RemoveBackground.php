<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

/**
 * Sets pixels that are within fuzz distance of top-left pixel to transparent
 * @package QBNK\JobQueue\Model\Image\Command
 */
class RemoveBackground extends ImageCommandAbstract
{
    /**
     * @var integer
     */
    protected $fuzz;

    public function __construct($fuzz)
    {
        $this->setFuzz($fuzz);
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        return '-fuzz ' . escapeshellarg($this->fuzz . '%') . ' -fill none -draw "matte 0,0 floodfill"';
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('image_template.command.removebackground.fuzz'),
                'systemname' => 'fuzz',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => true
                ]
            ]
        ];
    }

    /**
     * @return int
     */
    public function getFuzz()
    {
        return $this->fuzz;
    }

    /**
     * @param int $fuzz
     * @return $this
     */
    public function setFuzz($fuzz)
    {
        $this->fuzz = $fuzz;
        return $this;
    }

    public function getName(): string
    {
        return gettext('image_template.command.removebackground');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.removebackground.description');
    }
}
