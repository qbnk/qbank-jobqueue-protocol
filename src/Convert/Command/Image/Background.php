<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use Primal\Color\Color;
use Primal\Color\Parser;
use Primal\Color\UnknownFormatException;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

/**
 * Set the background color.
 * @package QBNK\JobQueue\Model\Image\Command
 */
class Background extends ImageCommandAbstract
{
    public const TYPE_COLOR = 'color';
    public const TYPE_SPECIAL = 'special';
    public const TYPE_NONE = 'none';

    public const BACKGROUND_CHECKERED = 'checkered';
    public const BACKGROUND_TRANSPARENT = 'transparent';

    protected $type;

    /**
     * @var string
     */
    protected $background;

    /**
     * @var Color
     */
    private $backgroundParsed;

    public function __construct(string $background)
    {
        $this->setBackground($background);
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        switch ($this->type) {
            case self::TYPE_COLOR:
                return '-background ' . escapeshellarg($this->getBackgroundParsed()->toHex()) . ' +repage -flatten';

            case self::TYPE_SPECIAL:
                switch ($this->backgroundParsed) {
                    case self::BACKGROUND_CHECKERED:
                        return
                            '-set option:my:h \'%[h]\' ' .
                            '-set option:my:w \'%[w]\' ' .
                            '\( ' .
                            'pattern:checkerboard -set option:distort:viewport \'%[my:w]x%[my:h]\' ' .
                            '-virtual-pixel tile -filter point -distort SRT 0 ' .
                            '\) ' .
                            '+swap -compose over -composite';

                    case self::BACKGROUND_TRANSPARENT:
                        return '-background \'rgba(0,0,0,0)\'';
                }
                break;

            default:
                return '';
        }
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('image_template.command.background.color'),
                'systemname' => 'background',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'colorpicker' => true
                ]
            ]
        ];
    }

    /**
     * @return string
     */
    public function getBackground()
    {
        return $this->background;
    }

    /**
     * @param string $background
     */
    public function setBackground(string $background)
    {
        $this->background = str_replace('%23', '#', $background);

        try {
            $this->backgroundParsed = Parser::Parse($this->background);
            $this->type = self::TYPE_COLOR;
        } catch (UnknownFormatException $ufe) {
            $this->type = self::TYPE_SPECIAL;
            switch (strtolower($this->background)) {
                case self::BACKGROUND_CHECKERED:
                    $this->backgroundParsed = self::BACKGROUND_CHECKERED;
                    break;

                case self::BACKGROUND_TRANSPARENT:
                    $this->backgroundParsed = self::BACKGROUND_TRANSPARENT;
                    break;

                default:
                    $this->type = self::TYPE_NONE;
            }
        }
    }

    public function jsonSerialize(): \stdClass
    {
        $object = parent::jsonSerialize();
        $object->background = $this->background;
        if ($this->type === self::TYPE_COLOR) {
            $object->backgroundParsed = $this->backgroundParsed->toCss();
        } else {
            $object->backgroundParsed = $this->backgroundParsed;
        }
        return $object;
    }

    public function getName(): string
    {
        return gettext('image_template.command.background');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.background.description');
    }

    /**
     * @return Color
     */
    private function getBackgroundParsed()
    {
        return $this->backgroundParsed;
    }
}
