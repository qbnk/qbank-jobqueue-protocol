<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

/**
 * Removes indicated profile(s).
 * Use standard filename globbing, so wildcard expressions may be used to remove more than one profile.
 * Here we remove all profiles from the image except for the XMP profile: "!xmp,*"
 * @package QBNK\JobQueue\Model\Image\Command
 */
class RemoveProfile extends ImageCommandAbstract
{
    /**
     * @var string[]
     */
    protected $profiles;

    public function __construct(array $profiles)
    {
        $this->setProfiles($profiles);
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        return '+profile ' . escapeshellarg(implode(',', $this->getProfiles()));
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('image_template.command.removeprofile.profiles'),
                'systemname' => 'profiles',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'array' => true
                ]
            ]
        ];
    }

    /**
     * @return string[]
     */
    public function getProfiles()
    {
        return $this->profiles;
    }

    /**
     * @param string[] $profiles
     */
    public function setProfiles(array $profiles)
    {
        $this->profiles = $profiles;
    }

    public function getName(): string
    {
        return gettext('image_template.command.removeprofiles');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.removeprofiles.description');
    }
}
