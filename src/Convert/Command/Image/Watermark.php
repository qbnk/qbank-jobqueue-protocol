<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

/**
 * Overlay a watermark on the image.
 * @package QBNK\JobQueue\Model\Image\Command
 */
class Watermark extends ImageCommandAbstract
{
    /**
     * @var ?string
     */
    private $watermark;

    /**
     * @var bool
     */
    private $tiled;

    /**
     * @var int
     */
    protected $gravity;

    /**
     * @var int
     */
    protected $opacity;

    /**
     * Geometry string specifying watermark size & placement according to format: '<width>x<height>[+/-]<posX>[+/-]<posY>'.
     * For example, _FIT_ watermark in 300x300px and place it 50px down-right from specified $gravity: '300x300+50+50'
     * @var string
     */
    protected $geometry;

    /**
     * Path to watermark file
     * @var string
     */
    protected $source;

    /** @var int */
    protected $offsetX;

    /** @var int */
    protected $offsetY;

    public function __construct(
        $watermark,
        $tiled,
        $gravity,
        $opacity,
        $geometry = '',
        $source = '',
        int $offsetX = 0,
        int $offsetY = 0
    ) {
        $this->setWatermark($watermark);
        $this->setTiled($tiled);
        $this->setGravity($gravity);
        $this->setOpacity($opacity);
        $this->setGeometry($geometry);
        $this->setSource($source);
        $this->setOffsetX($offsetX);
        $this->setOffsetY($offsetY);
    }

    public function getOffsetX()
    {
        return $this->offsetX;
    }

    public function setOffsetX($x)
    {
        return $this->offsetX = (int) $x;
    }

    public function getOffsetY()
    {
        return $this->offsetY;
    }

    public function setOffsetY($y)
    {
        return $this->offsetY = (int) $y;
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        if ($this->isTiled()) {
            return sprintf(
                '\( -clone 0 -tile %s  -draw "color 0,0 reset" \) -compose dissolve -define compose:args=%d %s -composite',
                escapeshellarg($this->getSource()),
                $this->getOpacity(),
                (!empty($this->getGeometry()) ? '-geometry ' . escapeshellarg($this->getGeometry()) : '')
            );
        }

        return sprintf(
            '%s -compose dissolve -define compose:args=%d -gravity %s %s -composite',
            escapeshellarg($this->getSource()),
            $this->getOpacity(),
            ImagickConstants::GravityConstantToString($this->getGravity()),
            (!empty($this->getGeometry()) ? '-geometry ' . escapeshellarg($this->getGeometry()) : '')
        );
    }

    public function getAdditionalArgument($outputFilename, array $targetParameters)
    {
        return sprintf(
            'composite -dissolve %d %s -gravity %s %s %s %s %s',
            $this->getOpacity(),
            ($this->isTiled() ? '-tile' : ''),
            ImagickConstants::GravityConstantToString($this->getGravity()),
            (!empty($this->getGeometry()) ? '-geometry ' . escapeshellarg($this->getGeometry()) : ''),
            escapeshellarg($this->getSource()),
            escapeshellarg($outputFilename),
            escapeshellarg($outputFilename)
        );
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('image_template.command.watermark.media'),
                'description' => gettext('image_template.command.watermark.media_description'),
                'systemname' => 'watermark',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'mediapicker' => true,
                    'custom_settings' => json_encode([
                        'filters_changeable' => true,
                        'default_filters' => []
                    ])
                ]
            ],
            [
                'name' => gettext('image_template.command.watermark.opacity'),
                'description' => gettext('image_template.command.watermark.opacity_description'),
                'systemname' => 'opacity',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => ['mandatory' => true]
            ],
            [
                'name' => gettext('image_template.command.watermark.repeat'),
                'systemname' => 'tiled',
                'datatype_id' => PropertyTypeEnum::BOOLEAN
            ],
            $this->getGravityProperty(),
            [
                'name' => gettext('image_template.command.watermark.offsetX'),
                'systemname' => 'offsetx',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => ['min' => 0]
            ],
            [
                'name' => gettext('image_template.command.watermark.offsetY'),
                'systemname' => 'offsety',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => ['min' => 0]
            ]
        ];
    }

    public function jsonSerialize(): \stdClass
    {
        $object = parent::jsonSerialize();
        $object->watermark = $this->getWatermark();
        $object->tiled = $this->isTiled();
        $object->gravity = $this->getGravity();
        $object->geometry = $this->getGeometry();
        $object->source = $this->getSource();
        return $object;
    }

    public function getName(): string
    {
        return gettext('image_template.command.watermark');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.watermark.description');
    }

    /**
     * @return string
     */
    public function getWatermark()
    {
        return $this->watermark;
    }

    /**
     * @param string|string[] $watermark
     */
    public function setWatermark($watermark): static
    {
        if (is_array($watermark)) {
            $arrayFiltered = array_filter($watermark);
            $watermark = array_shift($arrayFiltered);
        }

        $this->watermark = $watermark;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isTiled()
    {
        return $this->tiled;
    }

    /**
     * @param boolean $tiled
     * @return $this
     */
    public function setTiled($tiled)
    {
        $this->tiled = $tiled;
        return $this;
    }

    /**
     * @return int
     */
    public function getGravity()
    {
        return $this->gravity;
    }

    /**
     * @param int $gravity
     * @return $this
     */
    public function setGravity($gravity)
    {
        $this->gravity = (int)$gravity;
        return $this;
    }

    /**
     * @return int
     */
    public function getOpacity()
    {
        return $this->opacity;
    }

    /**
     * @param int $opacity
     * @return $this
     */
    public function setOpacity($opacity)
    {
        $this->opacity = (int)$opacity;
        return $this;
    }

    /**
     * @return string
     */
    public function getGeometry()
    {
        $offset = '';

        if ($this->offsetX > 0 || $this->offsetY > 0) {
            $offset = sprintf('+%d+%d', $this->offsetX, $this->offsetY);
        }

        if (empty($this->geometry) && empty($offset)) {
            return '';
        }

        return implode([' ', $this->geometry, $offset]);
    }

    /**
     * @param string $geometry
     * @return $this
     */
    public function setGeometry($geometry)
    {
        $this->geometry = $geometry;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     * @return $this
     */
    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }
}
