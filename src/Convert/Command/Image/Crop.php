<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

/**
 * Cut out one or more rectangular regions of the image.
 * @package QBNK\JobQueue\Model\Image\Command
 */
class Crop extends ImageCommandAbstract
{
    /**
     * @var int
     */
    protected $x;

    /**
     * @var int
     */
    protected $y;

    /**
     * @var int
     */
    protected $width;

    /**
     * @var int
     */
    protected $height;

    /**
     * @var int
     */
    protected $gravity;

    /** @var bool */
    protected $usepoint;

    public function __construct(
        int $x,
        int $y,
        int $width,
        int $height,
        int $gravity = ImagickConstants::GRAVITY_NORTHWEST
    ) {
        $this->setX($x);
        $this->setY($y);
        $this->setWidth($width);
        $this->setHeight($height);
        $this->setGravity($gravity);
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        $return = '+repage -gravity ' . ImagickConstants::GravityConstantToString($this->getGravity());
        if ($this->isUsepoint()) {
            $return = '+repage';
        }
        $return .= ' -crop ' . $this->getWidth() . 'x' . $this->getHeight() . '+' . $this->getX() . '+' . $this->getY(
        ) . ' +repage';

        return $return;
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('image_template.command.crop.x'),
                'systemname' => 'x',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => ['mandatory' => true]
            ],
            [
                'name' => gettext('image_template.command.crop.y'),
                'systemname' => 'y',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => ['mandatory' => true]
            ],
            [
                'name' => gettext('image_template.command.crop.width'),
                'systemname' => 'width',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => ['mandatory' => true]
            ],
            [
                'name' => gettext('image_template.command.crop.height'),
                'systemname' => 'height',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => ['mandatory' => true]
            ],
            $this->getGravityProperty(),
            [
                'name' => gettext('image_template.command.crop.use_crop_point'),
                'description' => gettext('image_template.command.crop.use_crop_point.description'),
                'systemname' => 'usepoint',
                'datatype_id' => PropertyTypeEnum::BOOLEAN,
                'definition' => ['mandatory' => false]
            ]
        ];
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $height
     * @return $this
     */
    public function setHeight(int $height)
    {
        if ($height < 1) {
            throw new \InvalidArgumentException('Cannot crop with height less than 1');
        }
        $this->height = $height;
        return $this;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @param int $width
     * @return $this
     */
    public function setWidth(int $width)
    {
        if ($width < 1) {
            throw new \InvalidArgumentException('Cannot crop with width less than 1');
        }
        $this->width = $width;
        return $this;
    }

    /**
     * @return int
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * @param int $x
     * @return $this
     */
    public function setX(int $x)
    {
        $this->x = $x;
        return $this;
    }

    /**
     * @return int
     */
    public function getY(): int
    {
        return $this->y;
    }

    /**
     * @param int $y
     * @return $this
     */
    public function setY(int $y)
    {
        $this->y = $y;
        return $this;
    }

    /**
     * @return int
     */
    public function getGravity(): int
    {
        return $this->gravity;
    }

    /**
     * @param int $gravity
     * @return $this
     */
    public function setGravity(int $gravity)
    {
        $this->gravity = $gravity;
        return $this;
    }

    public function getName(): string
    {
        return gettext('image_template.command.crop');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.crop.description');
    }

    /**
     * @param bool $usepoint
     */
    public function setUsepoint(bool $usepoint)
    {
        $this->usepoint = $usepoint;
    }

    public function isUsepoint(): bool
    {
        return $this->usepoint ?? false;
    }
}
