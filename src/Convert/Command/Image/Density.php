<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class Density extends ImageCommandAbstract
{
    /**
     * @var int
     */
    protected $density;

    public function __construct($density)
    {
        $this->setDensity($density);
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        $density = $this->getDensity() . 'x' . $this->getDensity();
        return '-density ' . $density . ' -resample ' . $density;
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('image_template.command.density'),
                'systemname' => 'density',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => [72, 96, 150, 300]
                ]
            ]
        ];
    }

    /**
     * @return int
     */
    public function getDensity()
    {
        return $this->density;
    }

    /**
     * @param int $density
     */
    public function setDensity($density)
    {
        $this->density = (int)$density;
    }

    public function getName(): string
    {
        return gettext('image_template.command.density');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.density.description');
    }
}
