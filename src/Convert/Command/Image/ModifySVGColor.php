<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use Primal\Color\Color;
use Primal\Color\Parser;
use Primal\Color\UnknownFormatException;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

/**
 * Modify svg color
 * @package QBNK\JobQueue\Model\Image\Command
 */
class ModifySVGColor extends ImageCommandAbstract implements ModifiesSourceInterface
{
    protected string $svgColor;
    protected string $svgReplaceColor;
    protected Color $parsedSvgColor;
    protected Color $parsedSvgReplaceColor;
    protected bool $svgIgnoreColor = false;
    protected string $positiveSVGPattern = "/((fill|stroke)\s*=\s*(?<quote1>\"|')(?!\k<quote1>)(?<color1>%s))(?=\k<quote1>)/i";
    protected string $positiveCSSPattern = "/((fill|stroke)\s*:\s*(?<color1>%s))(?=;)/i";
    protected string $negativeSVGPattern = "/((fill|stroke)\s*=\s*(?<quote1>\"|')(?!\k<quote1>)(?:(?!%s)(?<color1>.*?))(?=\k<quote1>))/i";
    protected string $negativeCSSPattern = "/((fill|stroke)\s*:\s*(?:(?!%s|none)(?<color1>.*?))(?=;))/i";

    public function __construct(string $svgColor, string $svgReplaceColor, bool $svgIgnoreColor = false)
    {
        $this->setSvgColor($svgColor);
        $this->setSvgReplaceColor($svgColor);
        $this->setSvgIgnoreColor($svgIgnoreColor);
    }

    public function modifySource(string $contents): string
    {
        $svgMatches = [];
        $cssMatches = [];
        if ($this->svgIgnoreColor) {
            $regexSVGPattern = sprintf($this->negativeSVGPattern, str_replace(",", "|", $this->getSvgColor()));
            $regexCSSPattern = sprintf($this->negativeCSSPattern, str_replace(",", "|", $this->getSvgColor()));
        } else {
            $regexSVGPattern = sprintf($this->positiveSVGPattern, $this->getSvgColor() === "" ? "(.*?)" : $this->getSvgColor());
            $regexCSSPattern = sprintf($this->positiveCSSPattern, $this->getSvgColor() === "" ? "(.*?)" : $this->getSvgColor());
        }
        preg_match_all($regexSVGPattern, $contents, $svgMatches);
        preg_match_all($regexCSSPattern, $contents, $cssMatches);
        $svgValues = array_unique($svgMatches["color1"]);
        $cssValues = array_unique($cssMatches["color1"]);
        $modifiedContent = str_replace($svgValues, $this->svgReplaceColor, $contents);
        return str_replace($cssValues, $this->svgReplaceColor, $modifiedContent);
    }

    public function setSvgColor($svgColor): ModifySVGColor
    {
        $this->svgColor = str_replace('%23', '#', $svgColor);

        try {
            $this->parsedSvgColor = Parser::Parse($this->svgColor);
        } catch (UnknownFormatException $ufe) {
        }
        return $this;
    }

    public function getSvgColor(): string
    {
        return $this->svgColor;
    }

    public function setSvgReplaceColor($svgReplaceColor): ModifySVGColor
    {
        $this->svgReplaceColor = str_replace('%23', '#', $svgReplaceColor);

        try {
            $this->parsedSvgReplaceColor = Parser::Parse($this->svgReplaceColor);
        } catch (UnknownFormatException $ufe) {
        }
        return $this;
    }

    public function getSvgReplaceColor(): string
    {
        return $this->svgReplaceColor;
    }

    public function getSvgIgnoreColor(): bool
    {
        return $this->svgIgnoreColor;
    }

    public function setSvgIgnoreColor(bool $svgIgnoreColor): ModifySVGColor
    {
        $this->svgIgnoreColor = $svgIgnoreColor;
        return $this;
    }

    public function isSvgIgnoreColor(): bool
    {
        return $this->svgIgnoreColor;
    }

    public function getProgram()
    {
        return null;
    }

    public function getArgument(array $identify = [])
    {
        return [];
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('image_template.command.svgcolor'),
                'systemname' => 'svgcolor',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'colorpicker' => true
                ]
            ],
            [
                'name' => gettext('image_template.command.svgreplacecolor'),
                'systemname' => 'svgreplacecolor',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'colorpicker' => true
                ]
            ],
            [
                'name' => gettext('image_template.command.svgignorecolor'),
                'systemname' => 'svgignorecolor',
                'datatype_id' => PropertyTypeEnum::BOOLEAN,
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('image_template.command.modify_svg_color');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.svgcolor.description');
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        unset($json->positiveSVGPattern);
        unset($json->positiveCSSPattern);
        unset($json->negativeSVGPattern);
        unset($json->negativeCSSPattern);
        $json->svgColor = $this->getSvgColor();
        $json->svgReplaceColor = $this->getSvgReplaceColor();
        $json->svgIgnoreColor = $this->getSvgIgnoreColor();

        return $json;
    }
}
