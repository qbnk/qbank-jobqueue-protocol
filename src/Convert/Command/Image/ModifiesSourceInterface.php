<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

interface ModifiesSourceInterface
{
    public function modifySource(string $contents): string;
}
