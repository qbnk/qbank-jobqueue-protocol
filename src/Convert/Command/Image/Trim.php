<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

/**
 * This option removes any edges that are exactly the same color as the corner pixels.
 * Use -fuzz to make -trim remove edges that are nearly the same color as the corner pixels.
 *
 * @package QBNK\JobQueue\Model\Image\Command
 */
class Trim extends ImageCommandAbstract
{
    /**
     * @var int
     */
    protected $fuzz;

    public function __construct($fuzz)
    {
        $this->setFuzz($fuzz);
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        if (!empty($this->fuzz)) {
            return '-fuzz ' . $this->fuzz . '% -trim';
        }

        return '-trim';
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('image_template.command.fuzz'),
                'systemname' => 'fuzz',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'min' => 0,
                    'max' => 100
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('image_template.command.trim');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.trim.description');
    }

    /**
     * @return int
     */
    public function getFuzz()
    {
        return $this->fuzz;
    }

    /**
     * @param int $fuzz
     */
    public function setFuzz($fuzz)
    {
        $this->fuzz = (int)$fuzz;
    }
}
