<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

/**
 * Set the image size and offset.
 * @package QBNK\JobQueue\Model\Image\Command
 */
class Extent extends ImageCommandAbstract
{
    /**
     * @var int
     */
    protected $width;

    /**
     * @var int
     */
    protected $height;

    /**
     * @var int
     */
    protected $gravity;

    public function __construct($width, $height, $gravity = ImagickConstants::GRAVITY_CENTER)
    {
        $this->setWidth($width);
        $this->setHeight($height);
        $this->setGravity($gravity);
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        $return = '-gravity ' . ImagickConstants::GravityConstantToString($this->getGravity());
        $return .= ' -extent ' . $this->getWidth() . 'x' . $this->getHeight();

        return $return;
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('image_template.command.extent.width'),
                'systemname' => 'width',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => ['mandatory' => true]
            ],
            [
                'name' => gettext('image_template.command.extent.height'),
                'systemname' => 'height',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => ['mandatory' => true]
            ],
            $this->getGravityProperty()
        ];
    }

    /**
     * @return int
     */
    public function getGravity()
    {
        return $this->gravity;
    }

    /**
     * @param int $gravity
     */
    public function setGravity($gravity)
    {
        $this->gravity = (int)$gravity;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->height = (int)$height;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth($width)
    {
        $this->width = (int)$width;
    }

    public function getName(): string
    {
        return gettext('image_template.command.extent');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.extent.description');
    }
}
