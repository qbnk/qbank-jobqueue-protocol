<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

/**
 * Write properties to the file as metadata
 * @package QBNK\JobQueue\Model\Image\Command
 */
class WriteMetadata extends ImageCommandAbstract
{
    /**
     * @var string
     */
    protected $tagName;

    /**
     * @var string
     */
    protected $propertySystemName;

    // TODO: refactor this into a dependency
    protected string $propertyTypeControllerName = 'QBNK\QBank\Objects\PropertyType\Controller\PropertyTypeController';

    public function __construct($propertySystemName, $tagName)
    {
        $this->setPropertySystemName($propertySystemName);
        $this->setTagName($tagName);
    }

    public function getProgram()
    {
        return self::PROGRAM_EXIFTOOL;
    }

    public function getArgument(array $identify = [])
    {
        return '-' . $this->getTagName() . '=';
    }

    public function getProperties()
    {
        $propertyTypes = [];
        if (class_exists($this->propertyTypeControllerName)) {
            foreach ((new $this->propertyTypeControllerName())->getPropertyTypes() as $propertyType) {
                $propertyTypes[] = [
                    'key' => $propertyType->getSystemName(),
                    'value' => $propertyType->getName()
                ];
            }
        }
        return [
            [
                'name' => gettext('image_template.command.writemetadata.propertySystemName'),
                'systemname' => 'propertySystemName',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => $propertyTypes
                ]
            ],
            [
                'name' => gettext('image_template.command.writemetadata.tagName'),
                'systemname' => 'tagName',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => ['mandatory' => true]
            ]
        ];
    }

    /**
     * @return string
     */
    public function getTagName()
    {
        return $this->tagName;
    }

    /**
     * @param string $tagName
     */
    public function setTagName($tagName)
    {
        $this->tagName = $tagName;
    }

    /**
     * @return string
     */
    public function getPropertySystemName()
    {
        return $this->propertySystemName;
    }

    /**
     * @param string $propertySystemName
     */
    public function setPropertySystemName($propertySystemName)
    {
        $this->propertySystemName = $propertySystemName;
    }

    public function getName(): string
    {
        return gettext('image_template.command.writemetadata');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.writemetadata.description');
    }
}
