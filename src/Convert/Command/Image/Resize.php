<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

/**
 * Resize an image.
 * @package QBNK\JobQueue\Model\Image\Command
 */
class Resize extends ImageCommandAbstract
{
    /**
     * @var int
     */
    protected $width;

    /**
     * @var int
     */
    protected $height;

    /**
     * @var bool
     */
    protected $proportional;

    /**
     * @var bool
     */
    protected $scaleUp;

    /** @var  bool */
    protected $allowOverflow;

    /**
     * @param int $width Width if resized image
     * @param int $height Height of resized image
     * @param bool $proportional Indicates if we should keep proportions when resizing image
     * @param bool $scaleUp Indicates if we should allow scaling a image up when resizing
     * @param bool $allowOverflow Indicates that overflowing the set width and height is allowed to fill the canvas. Usally followed by some sort of cropping.
     */
    public function __construct($width, $height, $proportional = true, $scaleUp = false, $allowOverflow = false)
    {
        $this->setWidth($width);
        $this->setHeight($height);
        $this->setProportional($proportional);
        $this->setScaleUp($scaleUp);
        $this->setAllowOverflow($allowOverflow);
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        if ($this->getWidth() > 0 && $this->getHeight() > 0) {
            return '-resize ' . $this->getWidth() . 'x' . $this->getHeight() .
                (!$this->isProportional() ? '\!' : '') .
                (!$this->isScaleUp() ? '\>' : '') .
                ($this->isAllowOverflow() ? '^' : '');
        } else {
            if ($this->getWidth() === 0 && $this->getHeight() > 0) {
                return '-resize x' . $this->getHeight();
            } else {
                if ($this->getHeight() === 0 && $this->getWidth() > 0) {
                    return '-resize ' . $this->getWidth() . 'x';
                }
            }
        }
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('image_template.command.resize.width'),
                'systemname' => 'width',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => ['mandatory' => true, 'min' => 0],
                'sortorder' => 1
            ],
            [
                'name' => gettext('image_template.command.resize.height'),
                'systemname' => 'height',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => ['mandatory' => true, 'min' => 0],
                'sortorder' => 2
            ],
            [
                'name' => gettext('image_template.command.resize.proportional'),
                'systemname' => 'proportional',
                'datatype_id' => PropertyTypeEnum::BOOLEAN,
                'sortorder' => 3
            ],
            [
                'name' => gettext('image_template.command.resize.scaleup'),
                'systemname' => 'scaleUp',
                'datatype_id' => PropertyTypeEnum::BOOLEAN,
                'sortorder' => 4
            ],
            [
                'name' => gettext('image_template.command.resize.allow_overflow'),
                'systemname' => 'allowoverflow',
                'datatype_id' => PropertyTypeEnum::BOOLEAN,
                'sortorder' => 5
            ]
        ];
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->height = (int)$height;
    }

    /**
     * @return boolean
     */
    public function isProportional()
    {
        return $this->proportional;
    }

    /**
     * @param boolean $proportional
     */
    public function setProportional($proportional)
    {
        $this->proportional = (bool)$proportional;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth($width)
    {
        $this->width = (int)$width;
    }

    /**
     * @return boolean
     */
    public function isScaleUp()
    {
        return $this->scaleUp;
    }

    /**
     * @param boolean $allowUpScale
     */
    public function setScaleUp($allowUpScale)
    {
        $this->scaleUp = (bool)$allowUpScale;
    }

    /**
     * @param bool $allowOverflow
     */
    public function setAllowOverflow($allowOverflow)
    {
        $this->allowOverflow = (bool)$allowOverflow;
    }

    /** @return bool */
    public function isAllowOverflow()
    {
        return $this->allowOverflow;
    }

    public function getName(): string
    {
        return gettext('image_template.command.resize');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.resize.description');
    }
}
