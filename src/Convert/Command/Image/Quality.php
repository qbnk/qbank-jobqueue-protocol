<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

/**
 * JPEG/MIFF/PNG compression level.
 * @package QBNK\JobQueue\Model\Image\Command
 */
class Quality extends ImageCommandAbstract
{
    /**
     * @var int
     */
    protected $quality;

    public function __construct($quality)
    {
        $this->setQuality($quality);
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        return '-quality ' . $this->getQuality();
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('image_template.command.quality'),
                'systemname' => 'quality',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => true,
                    'min' => 0,
                    'max' => 100
                ]
            ]
        ];
    }

    /**
     * @return int
     */
    public function getQuality()
    {
        return $this->quality;
    }

    /**
     * @param int $quality
     */
    public function setQuality($quality)
    {
        $this->quality = (int)$quality;
    }

    public function getName(): string
    {
        return gettext('image_template.command.quality');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.quality.description');
    }
}
