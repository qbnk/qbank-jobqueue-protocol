<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

/**
 * Create a mirror image.
 * @package QBNK\JobQueue\Model\Image\Command
 */
class Mirror extends ImageCommandAbstract
{
    public const DIRECTION_VERTICAL = 'flip';
    public const DIRECTION_HORIZONTAL = 'flop';

    protected $direction;

    public function __construct($direction)
    {
        $this->setDirection($direction);
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        switch ($this->getDirection()) {
            case self::DIRECTION_HORIZONTAL:
                return '-flop';

            case self::DIRECTION_VERTICAL:
                return '-flip';

            default:
                return;
        }
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('image_template.command.mirror.direction'),
                'systemname' => 'direction',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => [
                        ['key' => self::DIRECTION_VERTICAL, 'value' => gettext('image_template.mirror.vertical')],
                        ['key' => self::DIRECTION_HORIZONTAL, 'value' => gettext('image_template.mirror.horizontal')]
                    ]
                ]
            ]
        ];
    }

    /**
     * @return string
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param string $direction
     */
    public function setDirection($direction)
    {
        $this->direction = (string)$direction;
    }

    public function getName(): string
    {
        return gettext('image_template.command.mirror');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.mirror.description');
    }
}
