<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

class ImagickConstants
{
    public const GRAVITY_NORTHWEST = 1;
    public const GRAVITY_NORTH = 2;
    public const GRAVITY_NORTHEAST = 3;
    public const GRAVITY_WEST = 4;
    public const GRAVITY_CENTER = 5;
    public const GRAVITY_EAST = 6;
    public const GRAVITY_SOUTHWEST = 7;
    public const GRAVITY_SOUTH = 8;
    public const GRAVITY_SOUTHEAST = 9;

    public const INTENT_DEFAULT = null;
    public const INTENT_ABSOLUTE = 'Absolute';
    public const INTENT_PERCEPTUAL = 'Perceptual';
    public const INTENT_RELATIVE = 'Relative';
    public const INTENT_SATURATION = 'Saturation';

    public static function GravityConstantToString($gravity)
    {
        return [
            self::GRAVITY_CENTER => 'Center',
            self::GRAVITY_EAST => 'East',
            self::GRAVITY_NORTH => 'North',
            self::GRAVITY_NORTHEAST => 'NorthEast',
            self::GRAVITY_NORTHWEST => 'NorthWest',
            self::GRAVITY_SOUTH => 'South',
            self::GRAVITY_SOUTHEAST => 'SouthEast',
            self::GRAVITY_SOUTHWEST => 'SouthWest',
            self::GRAVITY_WEST => 'West'
        ][$gravity];
    }
}
