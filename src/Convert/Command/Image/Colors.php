<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

/**
 * Defines the maximum numbers of colors in an image. Useful only for images with a palette (ie. indexed images).
 * @package QBNK\JobQueue\Model\Image\Command
 */
class Colors extends ImageCommandAbstract
{
    protected $colors;

    public function __construct($colors)
    {
        $this->setColors($colors);
    }

    public function setColors($colors)
    {
        $this->colors = (int)$colors;
    }

    public function getColors(): int
    {
        return $this->colors;
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        return '-colors ' . $this->getColors();
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('image_template.command.colors'),
                'systemname' => 'colors',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => true,
                    'array' => false
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('image_template.command.colors');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.colors.description');
    }
}
