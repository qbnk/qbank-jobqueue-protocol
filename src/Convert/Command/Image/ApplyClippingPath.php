<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

/**
 * Applies a clipping path if one is found. Background turns transparent if background is not set.
 * @package QBNK\JobQueue\Model\Image\Command
 */
class ApplyClippingPath extends ImageCommandAbstract
{
    /**
     * @var Background
     */
    protected $background;

    /**
     * @var bool
     */
    protected $bailOnManyPaths = false;

    /**
     * @var string
     */
    protected $primaryPath = '';

    /**
     * @var string
     */
    public const ARGUMENT_PLACEHOLDER = '_SVG_PATH_PLACEHOLDER_';

    public function __construct($background, bool $bailOnManyPaths = false, string $primaryPath = '')
    {
        $this->setBackground($background);
        $this->setBailOnManyPaths($bailOnManyPaths);
        $this->setPrimaryPath($primaryPath);
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        // apply path as mask to image
        return '\( ' . self::ARGUMENT_PLACEHOLDER . ' \) -alpha off -compose copy_opacity -composite ' . str_replace(
            '+repage -flatten',
            '',
            $this->background->getArgument()
        ) . ' -alpha remove';
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('image_template.command.background.color'),
                'systemname' => 'background',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'colorpicker' => true
                ]
            ],
            [
                'name' => gettext('image_template.command.applyclippingpath.bail_on_many_paths'),
                'description' => gettext('image_template.command.applyclippingpath.bail_on_many_paths.description'),
                'systemname' => 'bailonmanypaths',
                'datatype_id' => PropertyTypeEnum::BOOLEAN
            ]
        ];
    }

    /**
     * @return string
     */
    public function getBackground(): string
    {
        return $this->background->getBackground();
    }

    public function getName(): string
    {
        return gettext('image_template.command.applyclippingpath');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.applyclippingpath.description');
    }

    public function setBackground($background): ApplyClippingPath
    {
        $this->background = new Background((is_array($background) ? $background['background'] : $background));
        return $this;
    }

    /**
     * @return bool
     */
    public function isBailOnManyPaths(): bool
    {
        return $this->bailOnManyPaths;
    }

    /**
     * @param bool $bailOnManyPaths
     * @return $this
     */
    public function setBailOnManyPaths($bailOnManyPaths): ApplyClippingPath
    {
        $this->bailOnManyPaths = $bailOnManyPaths;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrimaryPath(): string
    {
        return $this->primaryPath;
    }

    /**
     * @param string $primaryPath
     * @return $this
     */
    public function setPrimaryPath($primaryPath): ApplyClippingPath
    {
        $this->primaryPath = $primaryPath;
        return $this;
    }
}
