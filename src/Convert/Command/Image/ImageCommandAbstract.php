<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\CommandAbstract;

abstract class ImageCommandAbstract extends CommandAbstract
{
    public const PROGRAM_IMAGEMAGICK_CONVERT = 'convert';
    public const PROGRAM_EXIFTOOL = 'exiftool';

    abstract public function getProgram();

    abstract public function getArgument(array $identify = []);

    abstract public function getProperties();

    abstract public function getName(): string;

    abstract public function getDescription(): string;

    public function getGravityProperty()
    {
        return [
            'name' => gettext('image_template.command.crop.gravity'),
            'systemname' => 'gravity',
            'datatype_id' => 6,
            'definition' => [
                'mandatory' => true,
                'array' => true,
                'options' => [
                    [
                        'key' => ImagickConstants::GRAVITY_CENTER,
                        'value' => gettext('image_template.gravity.center'),
                        'icon' => 'icon-fullscreen'
                    ],
                    [
                        'key' => ImagickConstants::GRAVITY_NORTHWEST,
                        'value' => gettext('image_template.gravity.northwest'),
                        'icon' => 'icon-arrow-up icon-rotate-nw'
                    ],
                    [
                        'key' => ImagickConstants::GRAVITY_NORTH,
                        'value' => gettext('image_template.gravity.north'),
                        'icon' => 'icon-arrow-up icon-rotate-n'
                    ],
                    [
                        'key' => ImagickConstants::GRAVITY_NORTHEAST,
                        'value' => gettext('image_template.gravity.northeast'),
                        'icon' => 'icon-arrow-up icon-rotate-ne'
                    ],
                    [
                        'key' => ImagickConstants::GRAVITY_EAST,
                        'value' => gettext('image_template.gravity.east'),
                        'icon' => 'icon-arrow-up icon-rotate-e'
                    ],
                    [
                        'key' => ImagickConstants::GRAVITY_SOUTHEAST,
                        'value' => gettext('image_template.gravity.southeast'),
                        'icon' => 'icon-arrow-up icon-rotate-se'
                    ],
                    [
                        'key' => ImagickConstants::GRAVITY_SOUTH,
                        'value' => gettext('image_template.gravity.south'),
                        'icon' => 'icon-arrow-up icon-rotate-s'
                    ],
                    [
                        'key' => ImagickConstants::GRAVITY_SOUTHWEST,
                        'value' => gettext('image_template.gravity.southwest'),
                        'icon' => 'icon-arrow-up icon-rotate-sw'
                    ],
                    [
                        'key' => ImagickConstants::GRAVITY_WEST,
                        'value' => gettext('image_template.gravity.west'),
                        'icon' => 'icon-arrow-up icon-rotate-w'
                    ]
                ]
            ]
        ];
    }
}
