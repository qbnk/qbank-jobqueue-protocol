<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

/**
 * Apply Paeth image rotation (using shear operations) to the image.
 * @package QBNK\JobQueue\Model\Image\Command
 */
class Rotate extends ImageCommandAbstract
{
    /**
     * @var int
     */
    protected $degrees;

    public function __construct($degrees)
    {
        $this->setDegrees($degrees);
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        return '-rotate ' . $this->getDegrees();
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('image_template.command.rotate.degrees'),
                'systemname' => 'degrees',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => true,
                    'min' => 0,
                    'max' => 360
                ]
            ]
        ];
    }

    /**
     * @return mixed
     */
    public function getDegrees()
    {
        return $this->degrees;
    }

    /**
     * @param mixed $degrees
     */
    public function setDegrees($degrees)
    {
        $this->degrees = (int)$degrees;
    }

    public function getName(): string
    {
        return gettext('image_template.command.rotate');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.rotate.description');
    }
}
