<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

/**
 * Adjusts an image so that its orientation is suitable for viewing (i.e. top-left orientation).
 * @package QBNK\JobQueue\Model\Image\Command
 */
class AutoOrient extends ImageCommandAbstract
{
    public function __construct()
    {
    }

    public function getProgram()
    {
        return self::PROGRAM_IMAGEMAGICK_CONVERT;
    }

    public function getArgument(array $identify = [])
    {
        return '-auto-orient';
    }

    public function getProperties()
    {
        return false;
    }

    public function getName(): string
    {
        return gettext('image_template.command.autoorient');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.autoorient.description');
    }
}
