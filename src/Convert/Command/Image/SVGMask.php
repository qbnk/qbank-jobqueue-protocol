<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use DOMElement;
use DOMXPath;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class SVGMask extends ImageCommandAbstract implements ModifiesSourceInterface
{
    protected string $maskElement;
    protected string $maskId;

    public function __construct(string $maskElement, string $maskId)
    {
        $this->maskElement = $maskElement;
        $this->maskId = $maskId;
    }

    public function getProgram()
    {
        return null;
    }

    public function getArgument(array $identify = [])
    {
    }

    public function getProperties(): array
    {
        return [
            [
                'name' => gettext('image_template.command.svgmask.maskElement'),
                'systemname' => 'maskElement',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => ['mandatory' => true]
            ],
            [
                'name' => gettext('image_template.command.svgmask.maskId'),
                'systemname' => 'maskId',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => ['mandatory' => true]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('image_template.command.svgmask');
    }

    public function getDescription(): string
    {
        return gettext('image_template.command.svgmask.description');
    }

    public function getMaskElement(): string
    {
        return $this->maskElement;
    }

    public function getMaskId(): string
    {
        return $this->maskId;
    }

    public function setMaskElement(string $maskElement): SVGMask
    {
        $this->maskElement = $maskElement;
        return $this;
    }

    public function setMaskId(string $maskId): SVGMask
    {
        $this->maskId = $maskId;
        return $this;
    }

    public function modifySource(string $contents): string
    {
        try {
            $dom = dom_import_simplexml(simplexml_load_string($contents));
            $xpath = new DOMXPath($dom->ownerDocument);
            $background = $xpath->query('//*[@id="' . $this->maskElement . '"]')->item(0);
            $background->setAttribute('mask', 'url(#mask)');

            $icon = $xpath->query('//*[@id="' . $this->maskId . '"]')->item(0);
            $wrappedIcon = new DOMElement('mask');
            $icon->parentNode->replaceChild($wrappedIcon, $icon);
            $wrappedIcon->setAttribute('id', 'mask');
            $rect = new DOMElement('rect');
            $wrappedIcon->appendChild($rect);
            $rect->setAttribute('x', '0');
            $rect->setAttribute('y', '0');
            $rect->setAttribute('width', '100%');
            $rect->setAttribute('height', '100%');
            $rect->setAttribute('fill', 'white');
            $wrappedIcon->appendChild($icon);

            foreach (
                $xpath->query(
                    '//*[@id="' . $this->maskId . '" and @fill] | //*[@id="' . $this->maskId . '"]/*[@fill]'
                ) as $node
            ) {
                $node->setAttribute('fill', 'black');
            }

            return $dom->ownerDocument->saveXML();
        } catch (\Throwable) {
        }

        return $contents;
    }
}
