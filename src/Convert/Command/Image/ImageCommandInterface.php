<?php

namespace QBNK\JobQueue\Job\Convert\Command\Image;

use QBNK\JobQueue\Job\Convert\Command\CommandInterface;

interface ImageCommandInterface extends CommandInterface
{
    public function getProgram();

    public function getArgument();
}
