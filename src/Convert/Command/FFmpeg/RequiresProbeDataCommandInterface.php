<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg;

/**
 * Interface for commands that requires probe data.
 * @package QBNK\JobQueue\Job\Convert\Command\FFmpeg
 */
interface RequiresProbeDataCommandInterface
{
    public const PROBE_SOURCE_ORIGINAL = 'original';

    /**
     * @return string[] An array of paths to get probe data for. Use constant PROBE_SOURCE_ORIGINAL if probe data is needed for the original file.
     */
    public function getProbeSources();

    /**
     * Return function for the probe data.
     * @param array $probeData A key => value array where the key is the required source and the value is the probe data.
     * @return void
     */
    public function setProbeData($probeData);
}
