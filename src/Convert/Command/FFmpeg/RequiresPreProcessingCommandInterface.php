<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg;

use Generator;

/**
 * Interface for commands that require pre-processing. Typically used for complex filters and such.
 * @package QBNK\JobQueue\Job\Convert\Command\FFmpeg
 */
interface RequiresPreProcessingCommandInterface
{
    public const DEFAULT_QUALITY = ' -codec:v \'libx264\' -preset \'fast\' -crf 18  -codec:a \'libmp3lame\' -b:a \'320k\' -f mp4';

    /**
     * Pre-process the source by running it through FFmpeg with specific arguments.
     * The result will be passed to the main conversion as the source.
     * @param mixed $worker The worker running this command (FFMpeg)
     * @param string|null $templateSource
     * @return array An array of arrays with keys and preprocessing instructions.
     * [
     *     'commands' => array Array of FFmpeg arguments
     *     'source'   => string Optional! Path to replacement source. If supplied, result will not be passed to the main conversion.
     *     'callback' => callable Optional! Called when preprocessing is complete with path to the resulting file.
     * ]
     */
    public function getPreProcessingCommands($worker, ?string $templateSource = null): Generator;
}
