<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg;

abstract class BitRateAbstract extends FFmpegCommandAbstract
{
    /**
     * @var string
     */
    protected $bitrate;

    /**
     * Sets the bit rate.
     * @param string $bitrate
     */
    public function __construct($bitrate)
    {
        $this->setBitrate($bitrate);
    }

    /**
     * @return string
     */
    public function getBitrate()
    {
        return $this->bitrate;
    }

    /**
     * @param string $bitrate
     * @return $this
     */
    public function setBitrate($bitrate)
    {
        $this->bitrate = $bitrate;
        return $this;
    }
}
