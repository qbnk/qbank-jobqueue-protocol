<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\FFmpegCommandAbstract;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class Profile extends FFmpegCommandAbstract
{
    public const PROFILE_BASELINE = 'baseline';
    public const PROFILE_MAIN = 'main';
    public const PROFILE_HIGH = 'high';

    public const LEVEL_30 = '3.0';
    public const LEVEL_31 = '3.1';
    public const LEVEL_40 = '4.0';
    public const LEVEL_41 = '4.1';
    public const LEVEL_42 = '4.2';

    /**
     * @var string
     */
    protected $profile;

    /**
     * @var string
     */
    protected $level;

    public function __construct($profile, $level)
    {
        $this->setProfile($profile);
        $this->setLevel($level);
    }

    public function getConvertArgument()
    {
        return sprintf(
            '-profile:v %s -level %s',
            escapeshellarg($this->getProfile()),
            escapeshellarg($this->getLevel())
        );
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('video_template.command.profile'),
                'systemname' => 'profile',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => [
                        self::PROFILE_BASELINE,
                        self::PROFILE_MAIN,
                        self::PROFILE_HIGH
                    ]
                ]
            ],
            [
                'name' => gettext('video_template.command.profile.level'),
                'systemname' => 'level',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => [
                        self::LEVEL_30,
                        self::LEVEL_31,
                        self::LEVEL_40,
                        self::LEVEL_41,
                        self::LEVEL_42
                    ]
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.profile');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.profile.description');
    }

    /**
     * @return string
     */
    public function getProfile(): string
    {
        return $this->profile;
    }

    /**
     * @param string $profile
     * @return $this
     */
    public function setProfile(string $profile): Profile
    {
        $this->profile = $profile;
        return $this;
    }

    /**
     * @return string
     */
    public function getLevel(): string
    {
        return $this->level;
    }

    /**
     * @param string $level
     * @return $this
     */
    public function setLevel(string $level): Profile
    {
        $this->level = $level;
        return $this;
    }
}
