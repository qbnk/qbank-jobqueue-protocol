<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\FFmpegCommandAbstract;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class Scale extends FFmpegCommandAbstract
{
    public const RESOLUTION_240P = '426x240';
    public const RESOLUTION_360P = '640x360';
    public const RESOLUTION_480P = '854x480';
    public const RESOLUTION_720P = '1280x720';
    public const RESOLUTION_1080P = '1920x1080';
    public const OPTION_FASTBILINEAR = 'fast_bilinear';
    public const OPTION_BILINEAR = 'bilinear';
    public const OPTION_BICUBIC = 'bicubic';
    public const OPTION_NEIGHBOR = 'neighbor';
    public const OPTION_AREA = 'area';
    public const OPTION_BICUBLIN = 'bicublin';
    public const OPTION_GAUSS = 'gauss';
    public const OPTION_SINC = 'sinc';
    public const OPTION_LANCZOS = 'lanczos';
    public const OPTION_SPLINE = 'spline';
    public const OPTION_ACCURATERND = 'accurate_rnd';
    public const OPTION_FULLCHROMAINT = 'full_chroma_int';
    public const OPTION_FULLCHROMAINP = 'full_chroma_inp';
    public const OPTION_BITEXACT = 'bitexact';

    /**
     * @var int
     */
    protected $width;

    /**
     * @var int
     */
    protected $height;

    /**
     * @var string
     */
    protected $scalingOption;

    public function __construct($width, $height, $scalingOption = self::OPTION_BICUBIC)
    {
        $this->setWidth($width);
        $this->setHeight($height);
        $this->setScalingOption($scalingOption);
    }

    public function getConvertArgument()
    {
        /*
         * Goal of command is to scale input video dimensions to fit inside given dimensions without scaling up,
         * and while maintaining aspect ratio. Final numbers have to be divisable by 2 or h264 will fail
         * (
         * 	floor( //Floor the final divided width
         * 		in_w * min( //multiply the input video width with the smallest number of ..
         * 			1\,	//1, i.e. output width (out_w) will be same as original
         * 			if(
         * 				gt(in_w\,in_h)\, //if video width is greater than height..
         * 				%d/in_w\, //divide the specified template width (%d) with video width
         * 				(%d*sar)/in_h //scale the portrait mode video in aspect ratio according to the template height
          * 			)
         * 		)
         * 		/2 //divide the final width before flooring
         * 	)
         * )*2 //multiply the floored final width, to make sure we have an even number
         * :
         * (
         * 	floor( //floor half the output width we computed in the previous statement with the display aspect ratio
         * 		(out_w/dar)
         * 		/2
         * 	)
         * )*2 //multiply it with 2 to end up with an even height number
         */


        $argument = sprintf(
            '-vf "scale=(floor(in_w*min(1\,if(gt(in_w\,in_h)\,%d/in_w\,(%d*sar)/in_h))/2))*2:(floor((out_w/dar)/2))*2"',
            $this->getWidth(),
            $this->getHeight()
        );

        if (!empty($this->scalingOption)) {
            $argument .= ' -sws_flags ' . $this->scalingOption;
        }

        return $argument;
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('video_template.command.scale.width'),
                'systemname' => 'width',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => true,
                    'min' => 1
                ]
            ],
            [
                'name' => gettext('video_template.command.scale.height'),
                'systemname' => 'height',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => true,
                    'min' => 1
                ]
            ],
            [
                'name' => gettext('video_template.command.scale.option'),
                'systemname' => 'scalingOption',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => false,
                    'array' => true,
                    'options' => [
                        ['key' => self::OPTION_FASTBILINEAR, 'value' => 'Fast Bilinear'],
                        ['key' => self::OPTION_BILINEAR, 'value' => 'Bilinear'],
                        ['key' => self::OPTION_BICUBIC, 'value' => 'Bicubic'],
                        ['key' => self::OPTION_NEIGHBOR, 'value' => 'Nearest neighbor'],
                        ['key' => self::OPTION_AREA, 'value' => 'Average area'],
                        ['key' => self::OPTION_BICUBLIN, 'value' => 'Bicubic luma, Bilinear chroma'],
                        ['key' => self::OPTION_GAUSS, 'value' => 'Gaussian'],
                        ['key' => self::OPTION_SINC, 'value' => 'Sinc'],
                        ['key' => self::OPTION_LANCZOS, 'value' => 'Lanczos'],
                        ['key' => self::OPTION_SPLINE, 'value' => 'Spline'],
                        ['key' => self::OPTION_ACCURATERND, 'value' => 'Accurate rounding'],
                        ['key' => self::OPTION_FULLCHROMAINT, 'value' => 'Full chroma interpolation'],
                        ['key' => self::OPTION_FULLCHROMAINP, 'value' => 'Full chroma input'],
                        ['key' => self::OPTION_BITEXACT, 'value' => 'Bit exact']
                    ]
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.scale');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.scale.description');
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @param int $width
     * @return $this
     */
    public function setWidth(int $width): Scale
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $height
     * @return $this
     */
    public function setHeight(int $height): Scale
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return string
     */
    public function getScalingOption(): string
    {
        return $this->scalingOption;
    }

    /**
     * @param string $scalingOption
     * @return $this
     */
    public function setScalingOption(string $scalingOption): Scale
    {
        $this->scalingOption = $scalingOption;
        return $this;
    }
}
