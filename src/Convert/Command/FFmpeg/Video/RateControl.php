<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\FFmpegCommandAbstract;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class RateControl extends FFmpegCommandAbstract
{
    /** @var  int -bufsize */
    protected $bufferSize;

    /** @var  int -maxrate */
    protected $maxRate;

    /** @var  int -minrate */
    protected $minRate;

    public function __construct($bufferSize, $maxRate, $minRate)
    {
        $this->bufferSize = (int)$bufferSize;
        $this->maxRate = (int)$maxRate;
        $this->minRate = (int)$minRate;
    }

    public function getConvertArgument()
    {
        $argument = '';
        if ($this->bufferSize !== null) {
            $argument .= ' -bufsize ' . $this->bufferSize . 'k';
        }
        if ($this->maxRate !== null) {
            $argument .= ' -maxrate ' . $this->maxRate . 'k';
        }
        if ($this->minRate !== null) {
            $argument .= ' -minrate ' . $this->minRate . 'k';
        }
        return $argument;
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('video_template.command.ratecontrol.buffersize'),
                'systemname' => 'buffersize',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => true,
                    'min' => 0
                ]
            ],
            [
                'name' => gettext('video_template.command.ratecontrol.maxrate'),
                'systemname' => 'maxrate',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => false,
                    'min' => 0
                ]
            ],
            [
                'name' => gettext('video_template.command.ratecontrol.minrate'),
                'systemname' => 'minrate',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => false,
                    'min' => 0
                ]
            ],
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.ratecontrol');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.ratecontrol.description');
    }

    /**
     * @param int $bufferSize
     * @return $this
     */
    public function setBufferSize($bufferSize)
    {
        $this->bufferSize = (int)$bufferSize;
        return $this;
    }

    /**
     * @return int
     */
    public function getBufferSize()
    {
        return $this->bufferSize;
    }

    /**
     * @param int $maxRate
     * @return $this
     */
    public function setMaxRate($maxRate)
    {
        $this->maxRate = (int)$maxRate;
        return $this;
    }

    /**
     * @return int
     */
    public function getMaxRate()
    {
        return $this->maxRate;
    }

    /**
     * @param int $minRate
     * @return $this
     */
    public function setMinRate($minRate)
    {
        $this->minRate = (int)$minRate;
        return $this;
    }

    /**
     * @return int
     */
    public function getMinRate()
    {
        return $this->minRate;
    }
}
