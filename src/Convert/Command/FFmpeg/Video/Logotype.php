<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\FFmpegCommandAbstract;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\RequiresFilePlaceholderInterface;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\RequiresPreProcessingCommandInterface;
use QBNK\JobQueue\Job\Convert\Command\Image\AutoOrient;
use QBNK\JobQueue\Job\Convert\Command\Image\Background;
use QBNK\JobQueue\Job\Convert\Command\Image\Resize;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;
use QBNK\JobQueue\Job\Convert\ImageConvertJob;
use QBNK\JobQueue\Job\Convert\Model\ImageConvertTarget;
use QBNK\JobQueue\Job\Enums\JobStatus;
use QBNK\JobQueue\Job\Storage\File;
use RuntimeException;

class Logotype extends FFmpegCommandAbstract implements
    RequiresFilePlaceholderInterface,
    RequiresPreProcessingCommandInterface
{
    public const MODE_ID = 2;
    public const MODE_MEDIAPROPERTY = 1;

    public const PLACEMENT_TOPRIGHT = 'top_right';
    public const PLACEMENT_BOTTOMRIGHT = 'bottom_right';
    public const PLACEMENT_BOTTOMLEFT = 'bottom_left';
    public const PLACEMENT_TOPLEFT = 'top_left';
    public const PLACEMENT_CENTER = 'center';

    /** @var  int */
    protected $mode;

    /** @var  string */
    protected $logotypevalue;

    /** @var  string */
    protected $placement;

    /** @var  int */
    protected $sizepercent;

    /**
     * @var array
     * Keys are "width" and "height"
     */
    protected $sizeAbsolute;

    /** @var  string */
    protected $logotypePath;

    public function getConvertArgument()
    {
        // TODO: Implement getConvertArgument() method.
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('video_template.command.logotype.mode'),
                'systemname' => 'mode',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => [
                        ['key' => self::MODE_ID, 'value' => gettext('video_template.command.logotype.mode.id')],
                        [
                            'key' => self::MODE_MEDIAPROPERTY,
                            'value' => gettext('video_template.command.logotype.mode.mediaproperty')
                        ]
                    ]
                ]
            ],
            [
                'name' => gettext('video_template.command.logotype.value'),
                'systemname' => 'logotypevalue',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                ]
            ],
            [
                'name' => gettext('video_template.command.logotype.placement'),
                'systemname' => 'placement',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => [
                        [
                            'key' => self::PLACEMENT_TOPRIGHT,
                            'value' => gettext('video_template.command.logotype.placement.top_right')
                        ],
                        [
                            'key' => self::PLACEMENT_BOTTOMRIGHT,
                            'value' => gettext('video_template.command.logotype.placement.bottom_right')
                        ],
                        [
                            'key' => self::PLACEMENT_BOTTOMLEFT,
                            'value' => gettext('video_template.command.logotype.placement.bottom_left')
                        ],
                        [
                            'key' => self::PLACEMENT_TOPLEFT,
                            'value' => gettext('video_template.command.logotype.placement.top_left')
                        ],
                        [
                            'key' => self::PLACEMENT_CENTER,
                            'value' => gettext('video_template.command.logotype.placement.center')
                        ]
                    ]
                ]
            ],
            [
                'name' => gettext('video_template.command.logotype.size'),
                'systemname' => 'sizepercent',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => true,
                    'min' => 1,
                    'max' => 100
                ]
            ],
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.logotype');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.logotype.description');
    }

    public function getFilePlaceholder(): string
    {
        return '%logotypeFilePath%';
    }

    public function getPlaceholderFilePath(): string
    {
        return $this->getLogotypePath();
    }

    /**
     * @param $worker - The FFMPeg worker, with protected properties made available through logger(), qbankJob() and runSynchronous()
     * @param string|null $templateSource
     * @return array<array<string, mixed>>
     */
    public function getPreProcessingCommands($worker, ?string $templateSource = null): \Generator
    {
        if (empty($this->sizeAbsolute)) {
            throw new RuntimeException('Missing size arguments for logotype scaling.');
        }

        $worker->logger()->debug('Logotype command resizing image according to video size');

        /** @var ImageConvertJob $conversionResult */
        $conversionResult = $worker->runSynchronous(
            (new ImageConvertJob())
                ->setStorageConfig($worker->qbankJob()->getStorageConfig())
                ->setSource(new File($this->logotypePath))
                ->setTargets([
                    (new ImageConvertTarget())
                        ->setIdentifier('logoresize')
                        ->setTarget(
                            new File(
                                uniqid('videologotype-', true) . '.png',
                                'image/png'
                            )
                        )
                        ->setCommands([
                            new AutoOrient(),
                            new Background(Background::BACKGROUND_TRANSPARENT),
                            new Resize($this->sizeAbsolute['width'], $this->sizeAbsolute['height'])
                        ])
                ])
        );

        if ($conversionResult->getStatus() !== JobStatus::Success) {
            throw new RuntimeException('Could not resize logotype; ' . $conversionResult->getMessage());
        }

        switch ($this->getPlacement()) {
            case self::PLACEMENT_BOTTOMLEFT:
                $overlay = 'overlay=5:main_h-overlay_h';
                break;
            case self::PLACEMENT_BOTTOMRIGHT:
                $overlay = 'overlay=main_w-overlay_w-5:main_h-overlay_h-5';
                break;
            case self::PLACEMENT_TOPLEFT:
                $overlay = 'overlay=5:5';
                break;
            case self::PLACEMENT_TOPRIGHT:
            default:
                $overlay = 'overlay=main_w-overlay_w-5:5';
                break;
        }

        $tmpLogoResize = $conversionResult->getConvertedImages()['logoresize']->getPathname();

        yield [
            'commands' => [
                '-i ' . $this->getFilePlaceholder(),
                '-filter_complex \'' . $overlay . '\'',
                self::DEFAULT_QUALITY
            ],
            'callback' => [$this, 'preProcessingCallback'],
            'callbackParameters' => ['path' => $tmpLogoResize]
        ];
    }

    public function preProcessingCallback(string $processingPath, array $callbackParameters): void
    {
        if (file_exists($callbackParameters['path'])) {
            unlink($callbackParameters['path']);
        }
    }

    /**
     * @param int $mode
     * @return $this
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
        return $this;
    }

    /**
     * @return int
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $logotypeValue
     * @return $this
     */
    public function setLogotypeValue($logotypeValue)
    {
        $this->logotypevalue = $logotypeValue;
        return $this;
    }

    /**
     * @return string
     */
    public function getLogotypeValue()
    {
        return $this->logotypevalue;
    }

    /**
     * @param string $placement
     * @return $this
     */
    public function setPlacement($placement)
    {
        $this->placement = $placement;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlacement()
    {
        return $this->placement;
    }

    /**
     * @param int $sizePercent
     * @return $this
     */
    public function setSizePercent($sizePercent)
    {
        $this->sizepercent = $sizePercent;
        return $this;
    }

    /**
     * @return int
     */
    public function getSizePercent()
    {
        return $this->sizepercent;
    }

    /**
     * @param array $sizeAbsolute An array with the keys "width" and "height"
     * @return $this
     */
    public function setSizeAbsolute($sizeAbsolute)
    {
        $this->sizeAbsolute = $sizeAbsolute;
        return $this;
    }

    /**
     * @return array An array with the keys "width" and "height"
     */
    public function getSizeAbsolute()
    {
        return $this->sizeAbsolute;
    }

    /**
     * @param string $logotypePath
     * @return $this
     */
    public function setLogotypePath($logotypePath)
    {
        $this->logotypePath = $logotypePath;
        return $this;
    }

    /**
     * @return string
     */
    public function getLogotypePath()
    {
        return $this->logotypePath;
    }
}
