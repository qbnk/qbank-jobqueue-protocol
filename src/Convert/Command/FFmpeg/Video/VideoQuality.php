<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\QualityAbstract;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class VideoQuality extends QualityAbstract
{
    public function getConvertArgument(): string
    {
        return '-qscale:v ' . $this->getQuality();
    }

    public function getProperties(): array
    {
        return [
            [
                'name' => gettext('video_template.command.videoquality'),
                'systemname' => 'videoQuality',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => [
                        ['key' => 1, 'value' => gettext('common.best')],
                        ['key' => 2, 'value' => gettext('common.high')],
                        ['key' => 3, 'value' => gettext('gettext.medium')],
                        ['key' => 4, 'value' => gettext('gettext.low')],
                        ['key' => 5, 'value' => gettext('common.worst')]
                    ]
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.videoquality');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.videoquality.description');
    }

    public function getVideoQuality(): ?int
    {
        return $this->getQuality();
    }

    public function setVideoQuality(int $quality)
    {
        return $this->setQuality($quality);
    }
}
