<?php

/**
 * This class is an alias for the Extend command to keep the backward compatibility
 */

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class Outro extends Extend
{
    public function __construct($mode, $outroValue)
    {
        parent::__construct($mode, $outroValue, false);
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('video_template.command.outro.mode'),
                'systemname' => 'mode',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => [
                        ['key' => self::MODE_ID, 'value' => gettext('video_template.command.outro.mode.id')],
                        [
                            'key' => self::MODE_MEDIAPROPERTY,
                            'value' => gettext('video_template.command.outro.mode.mediaproperty')
                        ]
                    ]
                ]
            ],
            [
                'name' => gettext('video_template.command.outro.value'),
                'systemname' => 'outrovalue',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.outro');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.outro.description');
    }
}
