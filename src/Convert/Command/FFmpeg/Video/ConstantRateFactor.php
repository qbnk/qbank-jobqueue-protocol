<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\FFmpegCommandAbstract;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class ConstantRateFactor extends FFmpegCommandAbstract
{
    /**
     * @var int
     */
    protected $crf;

    public function __construct($crf)
    {
        $this->setCrf($crf);
    }

    public function getConvertArgument()
    {
        return '-crf ' . $this->getCrf();
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('video_template.command.crf'),
                'systemname' => 'crf',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => true,
                    'min' => 0,
                    'max' => 51
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.crf');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.crf.description');
    }

    /**
     * @return int
     */
    public function getCrf(): int
    {
        return $this->crf;
    }

    /**
     * @param int $crf
     * @return $this
     */
    public function setCrf(int $crf): ConstantRateFactor
    {
        $this->crf = $crf;
        return $this;
    }
}
