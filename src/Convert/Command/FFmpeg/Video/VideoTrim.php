<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\TrimAbstract;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class VideoTrim extends TrimAbstract
{
    public function getProperties()
    {
        return [
            [
                'name' => gettext('video_template.command.trim.seek'),
                'systemname' => 'seek',
                'datatype_id' => PropertyTypeEnum::FLOAT,
                'definition' => [
                    'mandatory' => true,
                    'min' => 0
                ]
            ],
            [
                'name' => gettext('video_template.command.trim.length'),
                'systemname' => 'length',
                'datatype_id' => PropertyTypeEnum::FLOAT,
                'definition' => [
                    'mandatory' => true,
                    'min' => 1
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.trim');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.trim.description');
    }
}
