<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\FFmpegCommandAbstract;

class FastStart extends FFmpegCommandAbstract
{
    public function __construct()
    {
    }

    public function getConvertArgument()
    {
        return '-movflags +faststart';
    }

    public function getProperties()
    {
        return false;
    }

    public function getName(): string
    {
        return gettext('video_template.command.faststart');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.faststart.description');
    }
}
