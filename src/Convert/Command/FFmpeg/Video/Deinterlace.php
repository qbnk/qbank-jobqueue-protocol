<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\FFmpegCommandAbstract;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class Deinterlace extends FFmpegCommandAbstract
{
    public const MODE_SENDFRAME = 0;
    public const MODE_SENDFIELD = 1;
    public const MODE_SENDFRAMENOSPATIAL = 2;
    public const MODE_SENDFIELDNOSPATIAL = 3;

    public const PARITY_TOPFIELDFIRST = 0;
    public const PARITY_BOTTOMFIELDFIRST = 1;
    public const PARITY_AUTO = -1;

    /** @var  int */
    protected $mode;

    /** @var  int */
    protected $parity;

    /** @var  bool */
    protected $force;

    /**
     * Deinterlace constructor.
     * @param int $mode
     * @param int $parity
     * @param bool $force
     */
    public function __construct($mode = self::MODE_SENDFRAME, $parity = self::PARITY_AUTO, $force = false)
    {
        $this->setMode($mode);
        $this->setParity($parity);
        $this->setForce($force);
    }

    public function getConvertArgument()
    {
        return sprintf('-vf yadif=%d:%d:%d', $this->mode, $this->parity, (int)$this->force);
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('video_template.command.deinterlace.mode'),
                'systemname' => 'mode',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => false,
                    'array' => true,
                    'options' => [
                        ['key' => self::MODE_SENDFRAME, 'value' => 'One frame per frame'],
                        ['key' => self::MODE_SENDFIELD, 'value' => 'One frame per field'],
                        [
                            'key' => self::MODE_SENDFRAMENOSPATIAL,
                            'value' => 'One frame per frame without spatial check'
                        ],
                        ['key' => self::MODE_SENDFIELDNOSPATIAL, 'value' => 'One frame per field without spatial check']
                    ]
                ]
            ],
            [
                'name' => gettext('video_template.command.deinterlace.parity'),
                'systemname' => 'parity',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => false,
                    'array' => true,
                    'options' => [
                        ['key' => self::PARITY_TOPFIELDFIRST, 'value' => 'Top field first'],
                        ['key' => self::PARITY_BOTTOMFIELDFIRST, 'value' => 'Bottom field first'],
                        ['key' => self::PARITY_AUTO, 'value' => 'Automatic detection']
                    ]
                ]
            ],
            [
                'name' => gettext('video_template.command.deinterlace.force'),
                'systemname' => 'force',
                'datatype_id' => PropertyTypeEnum::BOOLEAN,
                'definition' => [
                    'mandatory' => false,
                    'default' => false
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.deinterlace');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.deinterlace.description');
    }

    /**
     * @param int $mode
     * @return $this
     */
    public function setMode($mode)
    {
        $this->mode = (int)$mode;
        return $this;
    }

    /**
     * @return int
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param int $parity
     * @return $this
     */
    public function setParity($parity)
    {
        $this->parity = (int)$parity;
        return $this;
    }

    /**
     * @return int
     */
    public function getParity()
    {
        return $this->parity;
    }

    /**
     * @param bool $force
     * @return $this
     */
    public function setForce($force)
    {
        $this->force = (bool)$force;
        return $this;
    }

    /**
     * @return bool
     */
    public function isForce()
    {
        return $this->force;
    }
}
