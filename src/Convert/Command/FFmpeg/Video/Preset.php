<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\FFmpegCommandAbstract;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class Preset extends FFmpegCommandAbstract
{
    public const PRESET_ULTRAFAST = 'ultrafast';
    public const PRESET_SUPERFAST = 'superfast';
    public const PRESET_VERYFAST = 'veryfast';
    public const PRESET_FASTER = 'faster';
    public const PRESET_FAST = 'fast';
    public const PRESET_MEDIUM = 'medium';
    public const PRESET_SLOW = 'slow';
    public const PRESET_SLOWER = 'slower';
    public const PRESET_VERYSLOW = 'veryslow';

    /**
     * @var string
     */
    protected $preset;

    public function __construct($preset)
    {
        $this->setPreset($preset);
    }

    public function getConvertArgument()
    {
        return '-preset ' . escapeshellarg($this->getPreset());
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('video_template.command.preset'),
                'systemname' => 'preset',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => [
                        self::PRESET_ULTRAFAST,
                        self::PRESET_SUPERFAST,
                        self::PRESET_VERYFAST,
                        self::PRESET_FASTER,
                        self::PRESET_FAST,
                        self::PRESET_MEDIUM,
                        self::PRESET_SLOW,
                        self::PRESET_SLOWER,
                        self::PRESET_VERYSLOW
                    ]
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.preset');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.preset.description');
    }

    /**
     * @return string
     */
    public function getPreset(): string
    {
        return $this->preset;
    }

    /**
     * @param string $preset
     * @return $this
     */
    public function setPreset(string $preset): Preset
    {
        $this->preset = $preset;
        return $this;
    }
}
