<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\BitRateAbstract;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class VideoBitRate extends BitRateAbstract
{
    public function getConvertArgument()
    {
        return '-b:v ' . escapeshellarg($this->getBitrate() . 'k');
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('video_template.command.videobitrate.value'),
                'systemname' => 'bitrate',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => true,
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.videobitrate');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.videobitrate.description');
    }
}
