<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\FFmpegCommandAbstract;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class Aspect extends FFmpegCommandAbstract
{
    public const ASPECT_WIDESCREEN = '16:9';       // Standard for HD video
    public const ASPECT_STANDARD = '4:3';          // Old television standard
    public const ASPECT_ULTRAWIDE = '21:9';        // CinemaScope or "UltraWide"
    public const ASPECT_SQUARE = '1:1';            // Square, common for social media
    public const ASPECT_VERTICAL = '9:16';         // Vertical video, common for mobile
    public const ASPECT_PHOTOGRAPHY = '3:2';       // Standard photography aspect ratio
    public const ASPECT_MOBILE_STORY = '2:3';             // Common for mobile stories
    public const ASPECT_PORTRAIT = '4:5';                 // Often used for social media posts

    /**
     * @var string
     */
    protected $aspect;

    public function __construct($aspect)
    {
        $this->setAspect($aspect);
    }

    public function getConvertArgument()
    {
        return '-aspect ' . $this->getAspect();
    }

    /**
     * @return string
     */
    public function getAspect(): string
    {
        return $this->aspect;
    }

    /**
     * @param string $aspect
     * @return $this
     */
    public function setAspect(string $aspect): Aspect
    {
        $this->aspect = $aspect;
        return $this;
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('video_template.command.aspect'),
                'systemname' => 'aspect',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => [
                        self::ASPECT_STANDARD,
                        self::ASPECT_WIDESCREEN,
                        self::ASPECT_ULTRAWIDE,
                        self::ASPECT_SQUARE,
                        self::ASPECT_VERTICAL,
                        self::ASPECT_PHOTOGRAPHY,
                        self::ASPECT_MOBILE_STORY,
                        self::ASPECT_PORTRAIT
                    ]
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.aspect');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.aspect.description');
    }
}
