<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\FFmpegCommandAbstract;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class Pass extends FFmpegCommandAbstract
{
    /**
     * @var int
     */
    protected $pass;

    public function __construct($pass)
    {
        $this->setPass($pass);
    }

    public function getConvertArgument()
    {
        return '-pass ' . $this->getPass();
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('video_template.command.pass'),
                'systemname' => 'pass',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => [1, 2]
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.pass');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.pass.description');
    }

    /**
     * @return int
     */
    public function getPass(): int
    {
        return $this->pass;
    }

    /**
     * @param int $pass
     * @return $this
     */
    public function setPass(int $pass): Pass
    {
        $this->pass = $pass;
        return $this;
    }
}
