<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\FFmpegCommandAbstract;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\RequiresProbeDataCommandInterface;

/**
 * Automatically trims the video to a certain length based on a scoring system and a baseline score for 1080p@10min. Intended for previews.
 */
class ScoreTrim extends FFmpegCommandAbstract implements RequiresProbeDataCommandInterface
{
    /**
     * @var int
     */
    protected $width;

    /**
     * @var int
     */
    protected $height;

    /**
     * Length in seconds
     * @var int
     */
    protected $originalLength;

    public function getConvertArgument()
    {
        $length = 120;
        if (!(empty($this->width) || empty($this->height) || empty($this->originalLength))) {
            $length = $this->getVideoLengthByScoring($this->width, $this->height, $this->originalLength);
        }
        return sprintf(
            '-ss %d -t \'%1.2F\'',
            0,
            $length
        );
    }

    public function getProperties()
    {
        return [];
    }

    public function getName(): string
    {
        return gettext('video_template.command.scoretrim');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.scoretrim.description');
    }

    /**
     * Gets the recommended length of the converted video determined by the score and baseline.
     * @param int $width The width in pixels of the source video.
     * @param int $height The height in pixels of the source video.
     * @param int $length The length of the source video in seconds.
     * @return int The recommended length in seconds.
     */
    protected function getVideoLengthByScoring($width, $height, $length)
    {
        $score = $this->scoring($width, $height, $length);
        $baseline = $this->scoring(1920, 1080, 10 * 60);
        if ($score <= $baseline) {
            return $length;
        }

        $candidateLength = round($length);
        do {
            $candidateLength -= 60;
            $score = $this->scoring($width, $height, $candidateLength);
        } while ($score > $baseline);

        return $candidateLength;
    }


    /**
     * Arbitrary scoring algorithm used to determine the comparative "load" converting would inflict.
     * @param int $width The width in pixels of the source video.
     * @param int $height The height in pixels of the source video.
     * @param int $length The length of the source video in seconds.
     * @return int The score.
     */
    protected function scoring($width, $height, $length)
    {
        $length = min($length, 240 * 60);    // No video is longer then 240 min right? Avoiding overflow issues....
        return (int)($width * $height * (round(pow(2, round($length / 600, 2)), 2) * (pow($length / 600, 2))));
    }

    /**
     * @return string[] An array of paths to get probe data for. Use constant PROBE_SOURCE_ORIGINAL if probe data is needed for the original file.
     */
    public function getProbeSources()
    {
        return [RequiresProbeDataCommandInterface::PROBE_SOURCE_ORIGINAL];
    }

    /**
     * Return function for the probe data.
     * @param array $probeData A key => value array where the key is the required source and the value is the probe data.
     * @return void
     */
    public function setProbeData($probeData)
    {
        if (isset($probeData[RequiresProbeDataCommandInterface::PROBE_SOURCE_ORIGINAL])) {
            $probeData = $probeData[RequiresProbeDataCommandInterface::PROBE_SOURCE_ORIGINAL];
        }
        $this->setWidth($probeData['width'])->setHeight($probeData['height'])->setOriginalLength(
            $probeData['duration']
        );
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @param int $width
     * @return ScoreTrim
     */
    public function setWidth(int $width): ScoreTrim
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $height
     * @return ScoreTrim
     */
    public function setHeight(int $height): ScoreTrim
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return int
     */
    public function getOriginalLength(): int
    {
        return $this->originalLength;
    }

    /**
     * @param int $originalLength
     * @return ScoreTrim
     */
    public function setOriginalLength(int $originalLength): ScoreTrim
    {
        $this->originalLength = $originalLength;
        return $this;
    }
}
