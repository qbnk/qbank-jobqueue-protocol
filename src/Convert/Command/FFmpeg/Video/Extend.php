<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\FFmpegCommandAbstract;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\RequiresFilePlaceholderInterface;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\RequiresPreProcessingCommandInterface;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\RequiresProbeDataCommandInterface;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class Extend extends FFmpegCommandAbstract implements
    RequiresFilePlaceholderInterface,
    RequiresPreProcessingCommandInterface,
    RequiresProbeDataCommandInterface
{
    public const MODE_ID = 0;
    public const MODE_MEDIAPROPERTY = 1;

    public const EXTENDMODE_PRE = 0;
    public const EXTENDMODE_POST = 1;

    /** @var  int */
    protected $mode;

    /** @var  string */
    protected $outrovalue;

    /** @var  string The path to the file that is to be appended as the outro */
    protected $outroFilePath;

    /** @var  int */
    protected $extendMode;

    /** @var array */
    protected $probeData;

    public function __construct($mode, $outroValue, $extendMode = self::EXTENDMODE_POST)
    {
        $this
            ->setMode($mode)
            ->setOutrovalue($outroValue)
            ->setExtendmode($extendMode);
    }

    /** @inheritdoc */
    public function getConvertArgument()
    {
        return '';  // Everything is done in pre-processing
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('video_template.command.outro.mode'),
                'systemname' => 'mode',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => [
                        ['key' => self::MODE_ID, 'value' => gettext('video_template.command.outro.mode.id')],
                        [
                            'key' => self::MODE_MEDIAPROPERTY,
                            'value' => gettext('video_template.command.outro.mode.mediaproperty')
                        ]
                    ]
                ]
            ],
            [
                'name' => gettext('video_template.command.outro.value'),
                'systemname' => 'outrovalue',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                ]
            ],
            [
                'name' => gettext('video_template.command.extend.mode'),
                'systemname' => 'extendmode',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => [
                        ['key' => self::EXTENDMODE_PRE, 'value' => gettext('video_template.command.extend.mode.pre')],
                        ['key' => self::EXTENDMODE_POST, 'value' => gettext('video_template.command.extend.mode.post')]
                    ]
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.extend');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.extend.description');
    }

    public function getFilePlaceholder(): string
    {
        return '%outroFilePath%';
    }

    public function getPlaceholderFilePath(): string
    {
        return $this->getOutroFilePath();
    }

    public function getPreProcessingCommands($worker, ?string $templateSource = null): \Generator
    {
        $originalProbeData = $this->probeData[RequiresProbeDataCommandInterface::PROBE_SOURCE_ORIGINAL];
        $outroProbeData = $this->probeData[$this->outroFilePath];

        if (($originalProbeData['width'] != $outroProbeData['width'])
            || ($originalProbeData['height'] != $outroProbeData['height'])) {
            // Adapt the outro to the main files format
            yield [
                'source' => $this->outroFilePath,
                'commands' => [
                    '-i \'' . $this->getFilePlaceholder() . '\'',
                    sprintf(
                        '-vf "scale=\'min(%1$d,iw)\':min\'(%2$d,ih)\':force_original_aspect_ratio=disable,pad=%1$d:%2$d:(ow-iw)/2:(oh-ih)/2,setdar=dar=%3$s,setsar=sar=%4$s"',
                        $originalProbeData['width'],
                        $originalProbeData['height'],
                        number_format(
                            $outroProbeData['width'] / $outroProbeData['height'],
                            5,
                            '.',
                            ''
                        ),
                        $originalProbeData['sar'] ?? '1/1',
                    ),
                    self::DEFAULT_QUALITY
                ],
                'callback' => [$this, 'setOutroFilePath']
            ];
        }

        if ($this->getExtendmode() === self::EXTENDMODE_PRE) {
            // Concatenate the intro to the start of the main file. Done as a pre-process as it interferes with standard video filters (-vf)
            yield [
                'intro' => true,
                'source' => $this->outroFilePath,
                'commands' => [
                    '-i \'' . $templateSource . '\' -filter_complex ' . $this->getFilterComplexArg() . ' -vsync 2',
                    self::DEFAULT_QUALITY
                ]
            ];
        } else {
            // Concatenate the outro to the end of the main file. Done as a pre-process as it interferes with standard video filters (-vf)
            yield [
                'commands' => [
                    '-i \'' . $this->getFilePlaceholder() . '\' -filter_complex ' . $this->getFilterComplexArg() . ' -vsync 2',
                    self::DEFAULT_QUALITY
                ]
            ];
        }
    }

    /**
     * @inheritdoc
     */
    public function getProbeSources()
    {
        return [RequiresProbeDataCommandInterface::PROBE_SOURCE_ORIGINAL, $this->outroFilePath];
    }

    /**
     * Return function for the probe data.
     * @param array $probeData A key => value array where the key is the required source and the value is the probe data.
     * @return void
     */
    public function setProbeData($probeData = null)
    {
        if ($probeData === null || !is_array($probeData)) {
            $probeData = [];
        }
        $this->probeData = $probeData;
    }

    /**
     * @param int $mode
     * @return $this
     */
    public function setMode($mode)
    {
        $this->mode = (int)$mode;
        return $this;
    }

    /**
     * @return int
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $outrovalue
     * @return $this
     */
    public function setOutrovalue($outrovalue)
    {
        $this->outrovalue = (string)$outrovalue;
        return $this;
    }


    /**
     * @return string
     */
    public function getOutrovalue()
    {
        return $this->outrovalue;
    }

    /**
     * @param int $extendMode
     * @return $this
     */
    public function setExtendmode(int $extendMode = null)
    {
        if ($extendMode === null) {
            $this->extendMode = self::EXTENDMODE_POST;
        } else {
            $this->extendMode = $extendMode;
        }
        return $this;
    }

    /**
     * @return int
     */
    public function getExtendmode()
    {
        return ($this->extendMode ?? self::EXTENDMODE_POST);
    }

    /**
     * @param string $outroFilePath
     * @return $this
     */
    public function setOutroFilePath($outroFilePath)
    {
        $this->outroFilePath = (string)$outroFilePath;
        return $this;
    }

    /**
     * @return string
     */
    public function getOutroFilePath()
    {
        return $this->outroFilePath;
    }

    /**
     * @return string
     */
    private function getFilterComplexArg()
    {
        $onlyVideo = !$this->probeData[RequiresProbeDataCommandInterface::PROBE_SOURCE_ORIGINAL]['has_audio']
            || !$this->probeData[$this->outroFilePath]['has_audio'];

        return $onlyVideo
            ? '\'[0:v:0] [1:v:0] concat=n=2:v=1:a=0 [v]\' -map \'[v]\''
            : '\'[0:v:0] [0:a:0] [1:v:0] [1:a:0] concat=n=2:v=1:a=1 [v] [a]\' -map \'[v]\' -map \'[a]\'';
    }
}
