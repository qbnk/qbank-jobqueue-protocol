<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\FFmpegCommandAbstract;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class FrameRate extends FFmpegCommandAbstract
{
    /**
     * @var int
     */
    protected $framerate;

    public function __construct($rate)
    {
        $this->setFrameRate($rate);
    }

    public function getConvertArgument()
    {
        return '-r ' . $this->getFrameRate();
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('video_template.command.framerate'),
                'systemname' => 'framerate',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => true,
                    'min' => 1
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.framerate');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.framerate.description');
    }

    /**
     * @return int
     */
    public function getFramerate(): int
    {
        return $this->framerate;
    }

    /**
     * @param int $framerate
     * @return $this
     */
    public function setFramerate(int $framerate): FrameRate
    {
        $this->framerate = $framerate;
        return $this;
    }
}
