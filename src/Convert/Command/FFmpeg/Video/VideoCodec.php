<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\CodecAbstract;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class VideoCodec extends CodecAbstract
{
    public const CODEC_COPY_SOURCE = 'copy';
    public const CODEC_LIBX264 = 'libx264';
    public const CODEC_LIBVPX = 'libvpx';
    public const CODEC_LIBTHEORA = 'libtheora';
    public const CODEC_MPEG2VIDEO = 'mpeg2video';
    public const CODEC_MPEG1VIDEO = 'mpeg1video';

    public function getConvertArgument(): string
    {
        return $this->getCodec() === null ? '' : '-codec:v ' . escapeshellarg($this->getCodec());
    }

    public function getProperties(): array
    {
        return [
            [
                'name' => gettext('video_template.command.videocodec'),
                'systemname' => 'codec',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => [
                        ['key' => self::CODEC_COPY_SOURCE, 'value' => 'Copy from source video'],
                        ['key' => self::CODEC_LIBX264, 'value' => 'libx264 H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10'],
                        ['key' => self::CODEC_LIBVPX, 'value' => 'libvpx VP8'],
                        ['key' => self::CODEC_LIBTHEORA, 'value' => 'libtheora Theora encoder'],
                        ['key' => self::CODEC_MPEG2VIDEO, 'value' => 'MPEG-2'],
                        ['key' => self::CODEC_MPEG1VIDEO, 'value' => 'MPEG-1']
                    ]
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.videocodec');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.videocodec.description');
    }
}
