<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\FFmpegCommandAbstract;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class CodecOptionsX264 extends FFmpegCommandAbstract
{
    /** @var  int --keyint */
    protected $keyInt;

    /** @var  int --min-keyint */
    protected $minKeyInt;

    /** @var  int --scenecut */
    protected $sceneCut;

    /** @var  int --bframes */
    protected $bFrames;

    /** @var  int --aq-mode */
    protected $aqMode;

    /** @var  bool --weightb */
    protected $weightB;

    /** @var  bool --no-cabac */
    protected $noCabac;

    /** @var  array */
    private $commandMapping = [
        'keyInt' => 'keyint',
        'minKeyInt' => 'min-keyint',
        'sceneCut' => 'scenecut',
        'bFrames' => 'bframes',
        'aqMode' => 'aq-mode',
        'weightB' => 'weightb',
        'noCabac' => 'no-cabac'
    ];

    public function __construct(array $options)
    {
        foreach ($options as $key => $value) {
            $key = 'set' . str_replace('-', '', $key);
            if (method_exists($this, $key)) {
                $this->{$key}($value);
            }
        }
    }

    public function getConvertArgument()
    {
        $argumentValues = [];
        foreach ($this->commandMapping as $property => $argument) {
            if ($this->{$property} !== null) {
                if (is_bool($this->{$property})) {
                    $argumentValues[] = $argument;
                } else {
                    $argumentValues[] = $argument . '=' . $this->{$property};
                }
            }
        }
        if (empty($argumentValues)) {
            return '';
        }
        return '-x264opts \'' . implode(':', $argumentValues) . '\'';
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('video_template.command.codecoptions.x264.keyint'),
                'systemname' => 'keyint',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => false,
                    'min' => 0,
                    'default' => 250
                ]
            ],
            [
                'name' => gettext('video_template.command.codecoptions.x264.minkeyint'),
                'systemname' => 'minkeyint',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => false,
                    'min' => 0,
                    'default' => 25
                ]
            ],
            [
                'name' => gettext('video_template.command.codecoptions.x264.scenecut'),
                'systemname' => 'scenecut',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => false,
                    'min' => -1,
                    'default' => 40
                ]
            ],
            [
                'name' => gettext('video_template.command.codecoptions.x264.bframes'),
                'systemname' => 'bframes',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => false,
                    'min' => 0,
                    'default' => 0
                ]
            ],
            [
                'name' => gettext('video_template.command.codecoptions.x264.aqmode'),
                'systemname' => 'aqmode',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => false,
                    'min' => 0,
                    'max' => 3,
                    'default' => 1
                ]
            ],
            [
                'name' => gettext('video_template.command.codecoptions.x264.weightb'),
                'systemname' => 'weightb',
                'datatype_id' => PropertyTypeEnum::BOOLEAN,
                'definition' => [
                    'mandatory' => false,
                    'default' => false
                ]
            ],
            [
                'name' => gettext('video_template.command.codecoptions.x264.nocabac'),
                'systemname' => 'nocabac',
                'datatype_id' => PropertyTypeEnum::BOOLEAN,
                'definition' => [
                    'mandatory' => false,
                    'default' => false
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.codecoptions.x264');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.codecoptions.x264.description');
    }

    /**
     * @param int $keyInt
     * @return $this
     */
    public function setKeyInt($keyInt)
    {
        $this->keyInt = (int)$keyInt;
        return $this;
    }

    /**
     * @return int
     */
    public function getKeyInt()
    {
        return $this->keyInt;
    }

    /**
     * @param int $minKeyInt
     * @return $this
     */
    public function setMinKeyInt($minKeyInt)
    {
        $this->minKeyInt = (int)$minKeyInt;
        return $this;
    }

    /**
     * @return int
     */
    public function getMinKeyInt()
    {
        return $this->minKeyInt;
    }

    /**
     * @param int $sceneCut
     * @return $this
     */
    public function setSceneCut($sceneCut)
    {
        $this->sceneCut = (int)$sceneCut;
        return $this;
    }

    /**
     * @return int
     */
    public function getSceneCut()
    {
        return $this->sceneCut;
    }

    /**
     * @param int $bFrames
     * @return $this
     */
    public function setBFrames($bFrames)
    {
        $this->bFrames = (int)$bFrames;
        return $this;
    }

    /**
     * @return int
     */
    public function getBFrames()
    {
        return $this->bFrames;
    }

    /**
     * @param int $aqMode
     * @return $this
     */
    public function setAqMode($aqMode)
    {
        $this->aqMode = (int)$aqMode;
        return $this;
    }

    /**
     * @return int
     */
    public function getAqMode()
    {
        return $this->aqMode;
    }

    /**
     * @param bool $weightB
     * @return $this
     */
    public function setWeightB($weightB)
    {
        $this->weightB = (bool)$weightB;
        return $this;
    }

    /**
     * @return bool
     */
    public function isWeightB()
    {
        return $this->weightB;
    }

    /**
     * @param bool $noCabac
     * @return $this
     */
    public function setNoCabac($noCabac)
    {
        $this->noCabac = (bool)$noCabac;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNoCabac()
    {
        return $this->noCabac;
    }
}
