<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg;

use QBNK\JobQueue\Job\Convert\Command\CommandAbstract;

abstract class FFmpegCommandAbstract extends CommandAbstract implements FFmpegCommandInterface
{
}
