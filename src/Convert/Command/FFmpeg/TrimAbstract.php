<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg;

use Generator;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

abstract class TrimAbstract extends FFmpegCommandAbstract implements RequiresPreProcessingCommandInterface
{
    /**
     * @var float
     */
    protected $seek;

    /**
     * @var float
     */
    protected $length;

    public function __construct($seek, $length)
    {
        $this->setSeek($seek);
        $this->setLength($length);
    }

    public function getConvertArgument()
    {
        return '';
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('ffmpeg_template.command.trim.seek'),
                'systemname' => 'seek',
                'datatype_id' => PropertyTypeEnum::FLOAT,
                'definition' => [
                    'mandatory' => true,
                    'min' => 0
                ]
            ],
            [
                'name' => gettext('ffmpeg_template.command.trim.length'),
                'systemname' => 'length',
                'datatype_id' => PropertyTypeEnum::FLOAT,
                'definition' => [
                    'mandatory' => true,
                    'min' => 1
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('ffmpeg_template.command.trim');
    }

    public function getDescription(): string
    {
        return gettext('ffmpeg_template.command.trim.description');
    }

    /**
     * @return int
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param float $length
     * @return $this
     */
    public function setLength($length)
    {
        $this->length = (float)$length;
        return $this;
    }

    /**
     * @return int
     */
    public function getSeek()
    {
        return $this->seek;
    }

    /**
     * @param float $seek
     * @return $this
     */
    public function setSeek($seek)
    {
        $this->seek = (float)$seek;
        return $this;
    }

    public function getPreProcessingCommands($worker, ?string $templateSource = null): Generator
    {
        // If length is beyond end of video, FFMpeg will cut at end of video.
        yield [
            'commands' => [
                sprintf(
                    '-ss %F -t %F' .
                    self::DEFAULT_QUALITY,
                    $this->getSeek(),
                    $this->getLength()
                )
            ]
        ];
    }
}
