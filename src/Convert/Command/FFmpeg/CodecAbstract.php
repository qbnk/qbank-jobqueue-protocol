<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg;

abstract class CodecAbstract extends FFmpegCommandAbstract
{
    /** @var  string */
    protected $codec;

    public function __construct($codec)
    {
        $this->setCodec($codec);
    }

    /**
     * @return string
     */
    public function getCodec(): ?string
    {
        return $this->codec;
    }

    /**
     * @param string $codec
     * @return $this
     */
    public function setCodec(string $codec): CodecAbstract
    {
        $this->codec = $codec;
        return $this;
    }
}
