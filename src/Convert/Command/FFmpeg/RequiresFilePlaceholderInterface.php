<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg;

/**
 * Interface for commands that requires a file placeholder in the command string.
 * @package QBNK\JobQueue\Job\Convert\Command\FFmpeg
 */
interface RequiresFilePlaceholderInterface
{
    /**
     * @return string The placeholder to use in the command string.
     */
    public function getFilePlaceholder(): string;

    /**
     * @return string The path to the file to use as placeholder.
     */
    public function getPlaceholderFilePath(): string;
}
