<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Audio;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\FFmpegCommandAbstract;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

/**
 * Represents the number of audio channels a stream shall have.
 * @package QBNK\JobQueue\Job\Convert\Command\FFmpeg\Audio
 */
class AudioChannels extends FFmpegCommandAbstract
{
    public const CHANNELS_MONO = 1;
    public const CHANNELS_STEREO = 2;
    public const CHANNELS_51_SURROUND = 6;

    /** @var int */
    protected $channels;

    public function __construct(int $channels)
    {
        $this->setChannels($channels);
    }

    public function getConvertArgument(): string
    {
        return '-ac ' . escapeshellarg($this->getChannels());
    }

    public function getProperties(): array
    {
        return [
            [
                'name' => gettext('video_template.command.audiochannels'),
                'systemname' => 'samplingrate',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => [
                        ['key' => self::CHANNELS_MONO, 'value' => 'Mono'],
                        ['key' => self::CHANNELS_STEREO, 'value' => 'Stereo'],
                        ['key' => self::CHANNELS_51_SURROUND, 'value' => '5.1 Surround']
                    ]
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.audiochannels');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.audiochannels.description');
    }

    /**
     * @return int
     */
    public function getChannels(): int
    {
        return $this->channels;
    }

    /**
     * @param int $channels
     * @return $this
     */
    public function setChannels(int $channels): static
    {
        $this->channels = $channels;
        return $this;
    }
}
