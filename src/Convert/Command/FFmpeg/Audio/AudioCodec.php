<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Audio;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\CodecAbstract;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class AudioCodec extends CodecAbstract
{
    public const AAC = 'aac';
    public const AAC_FDK = 'libfdk_aac';
    public const MP3 = 'libmp3lame';
    public const OPUS = 'libopus';
    public const VORBIS = 'libvorbis';

    public function getConvertArgument(): string
    {
        return $this->getCodec() === null ? '' : '-codec:a ' . escapeshellarg($this->getCodec());
    }

    public function getProperties(): array
    {
        return [
            [
                'name' => gettext('video_template.command.audiocodec'),
                'systemname' => 'codec',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => [
                        ['key' => 'copy', 'value' => 'Copy from source video'],
                        ['key' => self::OPUS, 'value' => 'Opus Interactive Audio Codec'],
                        ['key' => self::AAC, 'value' => 'AAC'],
                        ['key' => self::AAC_FDK, 'value' => 'Fraunhofer FDK AAC'],
                        ['key' => self::MP3, 'value' => 'LAME MP3'],
                        ['key' => self::VORBIS, 'value' => 'Vorbis audio'],
                        ['key' => 'mp2', 'value' => 'MPEG 2']
                    ]
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.audiocodec');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.audiocodec.description');
    }
}
