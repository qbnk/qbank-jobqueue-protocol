<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Audio;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\FFmpegCommandAbstract;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class AudioSamplingRate extends FFmpegCommandAbstract
{
    public const SAMPLERATE_11K = 11025;
    public const SAMPLERATE_22K = 22050;
    public const SAMPLERATE_41K = 44100;
    public const SAMPLERATE_48K = 48000;
    public const SAMPLERATE_88K = 88200;

    /**
     * @var int
     */
    protected $samplingRate;

    public function __construct($samplingRate)
    {
        $this->setSamplingRate($samplingRate);
    }

    public function getConvertArgument()
    {
        return '-ar ' . $this->getSamplingRate();
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('video_template.command.samplingrate'),
                'systemname' => 'samplingrate',
                'datatype_id' => PropertyTypeEnum::INTEGER,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => [
                        ['key' => self::SAMPLERATE_11K, 'value' => '11,025 Hz'],
                        ['key' => self::SAMPLERATE_22K, 'value' => '22,050 Hz'],
                        ['key' => self::SAMPLERATE_41K, 'value' => '44,100 Hz - Best case for most cases'],
                        ['key' => self::SAMPLERATE_48K, 'value' => '48,000 Hz'],
                        ['key' => self::SAMPLERATE_88K, 'value' => '88,200 Hz']
                    ]
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.samplingrate');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.samplingrate.description');
    }

    /**
     * @return int
     */
    public function getSamplingRate()
    {
        return $this->samplingRate;
    }

    /**
     * @param int $samplingRate
     */
    public function setSamplingRate($samplingRate)
    {
        $this->samplingRate = (int)$samplingRate;
    }
}
