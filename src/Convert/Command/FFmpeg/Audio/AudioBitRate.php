<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg\Audio;

use QBNK\JobQueue\Job\Convert\Command\FFmpeg\BitRateAbstract;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class AudioBitRate extends BitRateAbstract
{
    public function getConvertArgument()
    {
        return '-b:a ' . escapeshellarg($this->getBitrate());
    }

    public function getProperties()
    {
        return [
            [
                'name' => gettext('video_template.audiobitrate.profile'),
                'systemname' => 'bitrate',
                'datatype_id' => PropertyTypeEnum::STRING,
                'definition' => [
                    'mandatory' => true,
                    'array' => true,
                    'options' => [
                        ['key' => '32k', 'value' => '32 kbps – generally acceptable only for speech'],
                        ['key' => '96k', 'value' => '96 kbps – generally used for speech or low-quality streaming'],
                        ['key' => '128k', 'value' => '128 kbps – mid-range bitrate quality'],
                        ['key' => '160k', 'value' => '160 kbps – mid-range bitrate quality'],
                        ['key' => '192k', 'value' => '192 kbps – a commonly used high-quality bitrate'],
                        ['key' => '320k', 'value' => '320 kbps - very high quality bitrate']
                    ]
                ]
            ]
        ];
    }

    public function getName(): string
    {
        return gettext('video_template.command.audiobitrate');
    }

    public function getDescription(): string
    {
        return gettext('video_template.command.audiobitrate.description');
    }
}
