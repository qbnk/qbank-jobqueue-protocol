<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg;

use QBNK\JobQueue\Job\Convert\Command\CommandInterface;

interface FFmpegCommandInterface extends CommandInterface
{
    public function getConvertArgument();
}
