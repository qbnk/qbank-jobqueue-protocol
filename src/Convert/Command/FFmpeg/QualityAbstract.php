<?php

namespace QBNK\JobQueue\Job\Convert\Command\FFmpeg;

abstract class QualityAbstract extends FFmpegCommandAbstract
{
    /** @var  int */
    protected $quality;

    public function __construct(int $quality)
    {
        $this->setQuality($quality);
    }

    /**
     * @return int
     */
    public function getQuality(): ?int
    {
        return $this->quality;
    }

    /**
     * @param int $quality
     * @return $this
     */
    public function setQuality(int $quality): QualityAbstract
    {
        $this->quality = $quality;
        return $this;
    }
}
