<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert;

use QBNK\JobQueue\Job\Convert\Model\DocumentConvertTarget;
use QBNK\JobQueue\Job\JobQueueException;
use QBNK\JobQueue\Job\Storage\File;

/**
 * A job that usually just passed the original document untouched, but may apply commands
 * @author Kristian Sandström
 * @since 2016-09-07
 */
class DocumentConvertJob extends ConvertAbstract implements PersistConvertInterface
{
    use PersistConvertTrait;

    /**
     * A list of different conversions that should be performed.
     * @var DocumentConvertTarget[]
     */
    protected array $targets = [];

    /**
     * @var File[]
     */
    protected array $convertedDocuments = [];

    /**
     * Adds a new target to the job.
     * @param DocumentConvertTarget $target
     * @return $this
     * @throws JobQueueException Thrown if a target with the same name already is attached to the job.
     */
    public function addTarget(DocumentConvertTarget $target): static
    {
        if (isset($this->targets[$target->getIdentifier()])) {
            throw new JobQueueException(
                'Could not add DocumentConvertTarget to DocumentConvertJob.'
                . 'Another target with the same name is already added.',
                JobQueueException::ERR_TARGETEXISTS
            );
        }
        $this->targets[$target->getIdentifier()] = $target;
        return $this;
    }

    /**
     * @return DocumentConvertTarget[]
     */
    public function getTargets(): array
    {
        return $this->targets;
    }

    /**
     * @param DocumentConvertTarget[] $targets
     * @return $this
     */
    public function setTargets(array $targets): DocumentConvertJob
    {
        $this->targets = $this->ensureIndexedTargets($targets, DocumentConvertTarget::class);
        return $this;
    }

    /**
     * @return File[]
     */
    public function getConvertedDocuments(): array
    {
        return $this->convertedDocuments;
    }

    /**
     * @param File[] $convertedFiles
     * @return $this
     */
    public function setConvertedDocuments(array $convertedFiles): DocumentConvertJob
    {
        foreach ($convertedFiles as $targetName => &$file) {
            if (is_array($file)) {
                $file = File::fromArray($file);
            }
        }
        $this->convertedDocuments = $convertedFiles;
        return $this;
    }

    public function getQueueName(): string
    {
        return $this->queueName ?? 'documentconvert';
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->targets = $this->getTargets();
        $json->convertedDocuments = [];
        foreach ($this->getConvertedDocuments() as $templateName => $file) {
            $json->convertedDocuments[$templateName] = $file->jsonSerialize();
        }
        return $json;
    }

    public function reset(): static
    {
        $resetJob = parent::reset();
        return $resetJob->setConvertedDocuments([]);
    }
}
