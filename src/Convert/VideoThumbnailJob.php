<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert;

use QBNK\JobQueue\Job\Storage\File;

/**
 * A job that created thumbnail from videos
 * @author Henrik Malmberg
 * @since 2012-12-17
 * @package JobQueue
 * @subpackage Model\Video
 */
class VideoThumbnailJob extends ConvertAbstract
{
    public const QUEUE_NAME = 'videothumbnail';

    /**
     * The width of the generated thumbnail (total width of each generated thumbnail)
     * @var int|null
     */
    protected ?int $width = null;

    /**
     * The height of the generated thumbnail (total height of each generated thumbnail)
     * @var int|null
     */
    protected ?int $height = null;

    /**
     * Number of thumbs to generate, if set to more than 1, numColumns and numRows will be ignored
     * @var int
     */
    protected int $numThumbs = 1;

    /**
     * @var File[]
     */
    protected array $convertedThumbnails = [];

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function setWidth(int $width): VideoThumbnailJob
    {
        $this->width = $width;
        return $this;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function setHeight(int $height): VideoThumbnailJob
    {
        $this->height = $height;
        return $this;
    }

    public function getNumThumbs(): int
    {
        return $this->numThumbs;
    }

    public function setNumThumbs(int $numThumbs): VideoThumbnailJob
    {
        $this->numThumbs = $numThumbs;
        return $this;
    }

    /**
     * @return File[]
     */
    public function getConvertedThumbnails(): array
    {
        return $this->convertedThumbnails;
    }

    /**
     * @param File[] $convertedThumbnails
     * @return VideoThumbnailJob
     */
    public function setConvertedThumbnails(array $convertedThumbnails): VideoThumbnailJob
    {
        foreach ($convertedThumbnails as &$thumbnail) {
            if (is_array($thumbnail)) {
                $thumbnail = File::fromArray($thumbnail);
            }
        }

        $this->convertedThumbnails = $convertedThumbnails;
        return $this;
    }

    public function getQueueName(): string
    {
        return $this->queueName ?? self::QUEUE_NAME;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->width = $this->getWidth();
        $json->height = $this->getHeight();
        $json->numThumbs = $this->getNumThumbs();
        $json->convertedThumbnails = $this->getConvertedThumbnails();
        return $json;
    }

    public function reset(): static
    {
        return parent::reset()->setConvertedThumbnails([]);
    }
}
