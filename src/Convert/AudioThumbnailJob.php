<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert;

use QBNK\JobQueue\Job\Storage\File;

/**
 * A job that creates waveform from audio
 * @author Henrik Malmberg
 * @since 2017-04-10
 * @package JobQueue
 * @subpackage Model\Audio
 */
class AudioThumbnailJob extends ConvertAbstract
{
    public const QUEUE_NAME = 'audiothumbnail';

    protected ?File $convertedWaveformImage = null;

    public function getConvertedWaveformImage(): ?File
    {
        return $this->convertedWaveformImage;
    }

    /**
     * @param File|array $convertedWaveformImage
     * @return $this
     */
    public function setConvertedWaveformImage($convertedWaveformImage): AudioThumbnailJob
    {
        $this->convertedWaveformImage = is_array($convertedWaveformImage)
            ? File::fromArray($convertedWaveformImage)
            : $convertedWaveformImage;
        return $this;
    }

    public function getQueueName(): string
    {
        return $this->queueName ?? self::QUEUE_NAME;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->convertedWaveformImage = $this->getConvertedWaveformImage();
        return $json;
    }
}
