<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert;

use QBNK\JobQueue\Job\Convert\Model\PdfFooter;
use QBNK\JobQueue\Job\Convert\Model\PdfHeader;
use QBNK\JobQueue\Job\Storage\File;

/**
 * Class Html2PdfJob
 * @package QBNK\JobQueue\Model\Pdf
 * @author Kristian Sandström
 * @since 2015-11-05
 */
class Html2PdfJob extends ConvertAbstract
{
    public const ORIENTATION_LANDSCAPE = 'Landscape';
    public const ORIENTATION_PORTRAIT = 'Portrait';
    public const ALIGNMENT_LEFT = 'left';
    public const ALIGNMENT_CENTER = 'center';
    public const ALIGNMENT_RIGHT = 'right';

    protected File $outputPdf;

    /**
     * Html2PdfJob::ORIENTATION_LANDSCAPE or Html2PdfJob::ORIENTATION_PORTRAIT
     */
    protected ?string $orientation = null;
    protected ?PdfHeader $header = null;
    protected ?PdfFooter $footer = null;

    /**
     * A3/A4/A5 etc
     */
    protected ?string $documentSize = null;

    /**
     * 0 is default for all
     * array(
     *    'top' = '45mm',
     *  'right' = '40mm',
     *  'bottom' = '23mm',
     *  'left' => '40mm'
     * )
     * @var array
     */
    protected $margins = [];


    /**
     * For a full list of supported pages sizes please see http://qt-project.org/doc/qt-4.8/qprinter.html#PaperSize-enum
     * @param string $size A3,A4,A5,Letter etc
     * @return $this
     */
    public function setDocumentSize($size): Html2PdfJob
    {
        $this->documentSize = $size;
        return $this;
    }

    /**
     * Sets the pdf margins
     * @param array $margins array('top' => 'Xmm', 'right' => 'Xmm', 'bottom' => 'Xmm', 'left' => 'Xmm')
     * @return $this
     */
    public function setMargins(array $margins): Html2PdfJob
    {
        $this->margins = $margins;
        return $this;
    }

    /**
     * Sets the pdf orientation
     * @param string $orientation Html2PdfJob::ORIENTATION_LANDSCAPE or Html2PdfJob::ORIENTATION_PORTRAIT
     * @return $this
     */
    public function setOrientation(string $orientation): Html2PdfJob
    {
        $this->orientation = $orientation;
        return $this;
    }

    /**
     * @return File
     */
    public function getOutputPdf(): ?File
    {
        return $this->outputPdf;
    }

    /**
     * @param File $outputPdf
     * @return Html2PdfJob
     */
    public function setOutputPdf($outputPdf): Html2PdfJob
    {
        $this->outputPdf = is_array($outputPdf) ? File::fromArray($outputPdf) : $outputPdf;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrientation(): ?string
    {
        return $this->orientation;
    }

    public function getHeader(): ?PdfHeader
    {
        return $this->header;
    }

    public function setHeader(PdfHeader|array $header): Html2PdfJob
    {
        if (is_array($header)) {
            $header = PdfHeader::fromArray($header);
        }
        $this->header = $header;
        return $this;
    }

    public function getFooter(): ?PdfFooter
    {
        return $this->footer;
    }

    public function setFooter(PdfFooter|array $footer): Html2PdfJob
    {
        if (is_array($footer)) {
            $footer = PdfFooter::fromArray($footer);
        }
        $this->footer = $footer;
        return $this;
    }

    /**
     * @return string
     */
    public function getDocumentSize(): ?string
    {
        return $this->documentSize;
    }

    /**
     * @return array
     */
    public function getMargins(): array
    {
        return $this->margins;
    }

    public function getQueueName(): string
    {
        return $this->queueName ?? 'htmltopdf';
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->class = get_class($this);
        $json->header = $this->getHeader();
        $json->footer = $this->getFooter();
        $json->documentSize = $this->getDocumentSize();
        $json->orientation = $this->getOrientation();
        $json->margins = $this->getMargins();
        $json->outputPdf = $this->getOutputPdf();
        return $json;
    }
}
