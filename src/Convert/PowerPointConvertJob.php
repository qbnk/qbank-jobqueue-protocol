<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert;

use QBNK\JobQueue\Job\Document\PresentationConvertedFile;
use QBNK\JobQueue\Job\Storage\File;

class PowerPointConvertJob extends ConvertAbstract
{
    public const FORMAT_PDF = 'PDF';
    public const FORMAT_TIFF = 'TIFF';
    public const FORMAT_JPG = 'JPG';

    /** @var File */
    protected $target;

    /** @var string[] */
    protected $fontPaths;

    /** @var string */
    protected $format;

    /** @var int[] */
    protected $slidesToConvert;

    /** @var PresentationConvertedFile[] */
    protected $convertedFiles;

    /**
     * @param File $target
     * @return PowerPointConvertJob
     */
    public function setTarget(File $target): PowerPointConvertJob
    {
        $this->target = $target;
        return $this;
    }

    /**
     * @return File
     */
    public function getTarget(): File
    {
        return $this->target;
    }

    /**
     * @param string[] $fontPaths
     * @return PowerPointConvertJob
     */
    public function setFontPaths(array $fontPaths): PowerPointConvertJob
    {
        $this->fontPaths = $fontPaths;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getFontPaths(): array
    {
        return $this->fontPaths ?? [];
    }

    /**
     * @param string $format
     * @return PowerPointConvertJob
     */
    public function setFormat(string $format): PowerPointConvertJob
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * @param int[] $slidesToConvert
     * @return PowerPointConvertJob
     */
    public function setSlidesToConvert(array $slidesToConvert): PowerPointConvertJob
    {
        $this->slidesToConvert = $slidesToConvert;
        return $this;
    }

    /**
     * @return int[]
     */
    public function getSlidesToConvert(): array
    {
        return $this->slidesToConvert ?? [];
    }

    /**
     * @param PresentationConvertedFile[] $convertedFiles
     * @return PowerPointConvertJob
     */
    public function setConvertedFiles(array $convertedFiles): PowerPointConvertJob
    {
        $this->convertedFiles = [];
        foreach ($convertedFiles as $file) {
            if ($file instanceof PresentationConvertedFile) {
                $this->convertedFiles[] = $file;
            }
        }
        return $this;
    }

    /**
     * @return PresentationConvertedFile[]
     */
    public function getConvertedFiles(): array
    {
        return $this->convertedFiles;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? 'pptconvert';
    }

    public function jsonSerialize(): \stdClass
    {
        $data = parent::jsonSerialize();
        $data->target = $this->getTarget();
        $data->fontPaths = $this->getFontPaths();
        $data->format = $this->getFormat();
        $data->slidesToConvert = $this->getSlidesToConvert();
        $data->convertedFiles = $this->getConvertedFiles();
        return $data;
    }

    public static function fromArray(array $parameters, bool $assignId = false): static
    {
        $instance = parent::fromArray($parameters, $assignId);
        if (isset($parameters['target'])) {
            $instance->setTarget(File::fromArray($parameters['target']));
        }
        if (isset($parameters['convertedFiles'])) {
            $convertedFiles = [];
            foreach ($parameters['convertedFiles'] as $convertedFile) {
                $convertedFiles[] = PresentationConvertedFile::fromArray($convertedFile);
            }
            $instance->setConvertedFiles($convertedFiles);
        }
        return $instance;
    }
}
