<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert;

use QBNK\JobQueue\Job\Document\CombineSlideStructure;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\Storage\File;

class PowerPointCombineJob extends JobAbstract
{
    public const FORMAT_PPTX = 'PPTX';

    /** @var File */
    private $target;

    /** @var string */
    private $format;

    /** @var CombineSlideStructure[] */
    private $structure;

    /** @var string[] */
    private $fontPaths = [];

    /**
     * @return string
     */
    public function getTarget(): File
    {
        return $this->target;
    }

    public function setTarget(File $target): static
    {
        $this->target = $target;
        return $this;
    }

    public function getFormat(): string
    {
        return $this->format;
    }

    public function setFormat(string $format): static
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @return CombineSlideStructure[]
     */
    public function getSlidesStructure(): array
    {
        return $this->structure ?? [];
    }

    /**
     * @param CombineSlideStructure[] $structure
     * @return $this
     */
    public function setSlidesStructure(array $structure): static
    {
        $this->structure = [];
        foreach ($structure as $slide) {
            if ($slide instanceof CombineSlideStructure) {
                $this->structure[] = $slide;
            }
        }
        return $this;
    }

    /**
     * @param CombineSlideStructure $structure
     * @return $this
     */
    public function addSlideStructure(CombineSlideStructure $structure): static
    {
        $slides = $this->getSlidesStructure();
        $slides[] = $structure;
        $this->setSlidesStructure($slides);
        return $this;
    }

    /**
     * @return string[]
     */
    public function getFontPaths(): array
    {
        return $this->fontPaths ?? [];
    }

    /**
     * @param string[] $fontPaths
     * @return $this
     */
    public function setFontPaths(array $fontPaths): static
    {
        $this->fontPaths = $fontPaths;
        return $this;
    }

    /**
     * @param string $fontPath
     * @return $this
     */
    public function addFontPath(string $fontPath): static
    {
        $fontPaths = $this->getFontPaths();
        $fontPaths[] = $fontPath;
        $this->setFontPaths($fontPaths);
        return $this;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? 'pptcombine';
    }

    public function jsonSerialize(): \stdClass
    {
        $data = parent::jsonSerialize();
        $data->target = $this->getTarget();
        $data->format = $this->getFormat();
        $data->fontPaths = $this->getFontPaths();
        $data->structure = $this->getSlidesStructure();
        return $data;
    }

    public static function fromArray(array $parameters, bool $assignId = false): static
    {
        $instance = parent::fromArray($parameters, $assignId);
        if (isset($parameters['structure'])) {
            $slides = [];
            foreach ($parameters['structure'] as $structure) {
                $slides = CombineSlideStructure::fromArray($structure);
            }
            $instance->setSlidesStructure($slides);
        }
        return $instance;
    }
}
