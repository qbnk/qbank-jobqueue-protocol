<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert;

use QBNK\JobQueue\Job\Convert\Model\AudioConvertTarget;
use QBNK\JobQueue\Job\Convert\Model\TargetAbstract;
use QBNK\JobQueue\Job\Convert\Model\VideoConvertTarget;
use QBNK\JobQueue\Job\JobQueueException;
use QBNK\JobQueue\Job\Storage\File;

/**
 * A job that converts videos between formats and/or sizes. May do some other stuff to.
 * @author Björn Hjortsten, Henrik Malmberg
 * @since 2012-10-19
 * @package JobQueue
 * @subpackage Model\Video
 */
class FFmpegJob extends ConvertAbstract implements PersistConvertInterface
{
    use PersistConvertTrait;

    public const QUEUE_NAME = 'ffmpeg';

    /**
     * A list of different conversions that should be performed.
     * @var TargetAbstract[]
     */
    protected array $targets = [];

    /**
     * @var File[]
     */
    protected array $convertedFiles = [];

    /**
     * Adds a new target to the job.
     * @param VideoConvertTarget|AudioConvertTarget $target
     * @return $this
     * @throws JobQueueException Thrown if a target with the same name already is attached to the job.
     * @author Björn Hjortsten
     * @since 2012-10-19
     */
    public function addTarget(TargetAbstract $target): static
    {
        if (isset($this->targets[$target->getIdentifier()])) {
            throw new JobQueueException(
                'Could not add FFmpegTarget to FFmpegJob.'
                . 'Another target with the same name is already added.',
                JobQueueException::ERR_TARGETEXISTS
            );
        }
        $this->targets[$target->getIdentifier()] = $target;
        return $this;
    }

    /**
     * @return TargetAbstract[]
     */
    public function getTargets(): array
    {
        return $this->targets;
    }

    /**
     * @param TargetAbstract[] $targets
     * @return FFmpegJob
     */
    public function setTargets(array $targets): FFmpegJob
    {
        $this->targets = $this->ensureIndexedTargets($targets, VideoConvertTarget::class);
        return $this;
    }

    /**
     * @return File[]
     */
    public function getConvertedFiles(): array
    {
        return $this->convertedFiles;
    }

    /**
     * @param File[] $convertedFiles
     * @return FFmpegJob
     */
    public function setConvertedFiles(array $convertedFiles): FFmpegJob
    {
        foreach ($convertedFiles as $targetName => &$file) {
            if (is_array($file)) {
                $file = File::fromArray($file);
            }
        }
        $this->convertedFiles = $convertedFiles;
        return $this;
    }

    public function getQueueName(): string
    {
        return $this->queueName ?? self::QUEUE_NAME;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->targets = $this->getTargets();
        $json->convertedFiles = [];
        foreach ($this->getConvertedFiles() as $templateName => $file) {
            $json->convertedFiles[$templateName] = $file->jsonSerialize();
        }
        return $json;
    }

    public function reset(): static
    {
        $resetJob = parent::reset();
        return $resetJob->setConvertedFiles([]);
    }
}
