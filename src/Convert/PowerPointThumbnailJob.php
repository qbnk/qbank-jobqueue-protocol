<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert;

use QBNK\JobQueue\Job\Storage\File;

class PowerPointThumbnailJob extends ConvertAbstract
{
    /**
     * @var File
     */
    protected $convertedThumbnail;

    /**
     * @return File
     */
    public function getConvertedThumbnail(): ?File
    {
        return $this->convertedThumbnail;
    }

    /**
     * @param File $convertedThumbnail
     * @return PowerPointThumbnailJob
     */
    public function setConvertedThumbnail($convertedThumbnail): PowerPointThumbnailJob
    {
        if (is_array($convertedThumbnail)) {
            $convertedThumbnail = File::fromArray($convertedThumbnail);
        }

        $this->convertedThumbnail = $convertedThumbnail;
        return $this;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName ?? 'pptthumbnail';
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->convertedThumbnail = $this->getConvertedThumbnail();
        return $json;
    }
}
