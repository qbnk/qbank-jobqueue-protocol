<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Convert;

class PdfExtractTextJob extends ConvertAbstract
{
    /**
     * Indicates if we want xml back instead of pure text
     * @var boolean
     */
    protected bool $asXml = false;

    protected ?string $outputData = null;

    public function setAsXml(bool $asXml): static
    {
        $this->asXml = $asXml;
        return $this;
    }

    public function getAsXml(): bool
    {
        return $this->asXml;
    }

    public function getOutputData(): ?string
    {
        return $this->outputData;
    }

    public function setOutputData(?string $outputData): PdfExtractTextJob
    {
        $this->outputData = $outputData;
        return $this;
    }

    public function getQueueName(): string
    {
        return $this->queueName ?? 'pdfextracttext';
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->asXml = $this->getAsXml();
        $json->outputData = $this->getOutputData();
        return $json;
    }

    public function reset(): static
    {
        $resetJob = parent::reset();
        return $resetJob->setOutputData(null);
    }
}
