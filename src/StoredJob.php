<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job;

use DateTime;
use Exception;
use JsonSerializable;
use RuntimeException;
use stdClass;
use Throwable;

/**
 * Class StoredJob
 * Represents a persisted JobAbstract along with time stamps
 * @package QBNK\JobQueue\Job
 */
class StoredJob implements JsonSerializable
{
    /** @var string The index to use when storing job */
    public const DEFAULT_INDEX = 'jobqueue';

    public const QUEUE_STATUS_CREATED = 'createdDate';
    public const QUEUE_STATUS_BLOCKED = 'blockedDate';
    public const QUEUE_STATUS_QUEUED = 'queuedDate';
    public const QUEUE_STATUS_STARTED = 'startedDate';
    public const QUEUE_STATUS_PROCESSED = 'processedDate';
    public const QUEUE_STATUS_CALLBACK_QUEUED = 'callbackQueuedDate';
    public const QUEUE_STATUS_CALLBACK_RUNNING = 'callbackRunningDate';
    public const QUEUE_STATUS_CALLBACK_FAILED = 'callbackFailedDate';
    public const QUEUE_STATUS_CALLBACK_COMPLETED = 'callbackCompletedDate';
    public const QUEUE_STATUS_CANCELED = 'canceledDate';

    protected ?DateTime $createdDate = null;

    protected ?DateTime $blockedDate = null;

    protected ?DateTime $queuedDate = null;

    protected ?DateTime $startedDate = null;

    protected ?DateTime $processedDate = null;

    protected ?DateTime $callbackQueuedDate = null;

    protected ?DateTime $callbackRunningDate = null;

    protected ?DateTime $callbackFailedDate = null;

    protected ?DateTime $callbackCompletedDate = null;

    protected ?DateTime $canceledDate = null;

    /** @var JobAbstract The actual job a worker executes */
    protected JobAbstract $qbankJob;

    /** @var int[] */
    protected array $mediaIds = [];

    protected ?string $elasticIndex = null;

    public function jsonSerialize(): \stdClass
    {
        $return = new stdClass();
        $return->mediaIds = $this->getMediaIds();
        $return->qbankJob = $this->getQbankJob();
        foreach (
            [
                'createdDate',
                'blockedDate',
                'queuedDate',
                'startedDate',
                'processedDate',
                'callbackQueuedDate',
                'callbackRunningDate',
                'callbackFailedDate',
                'callbackCompletedDate',
                'canceledDate'
            ] as $timeStamp
        ) {
            $getter = 'get' . $timeStamp;
            $return->$timeStamp = $this->$getter() ? $this->$getter()->format(DATE_ATOM) : null;
        }

        return $return;
    }

    /**
     * @param array $data
     * @return StoredJob
     * @throws JobQueueException
     * @throws Exception
     */
    public static function fromArray(array $data): StoredJob
    {
        $storedJob = new static();

        if (is_array($data['qbankJob'])) {
            $storedJob->setQbankJob(JobAbstract::fromArray($data['qbankJob']));
        } else {
            if ($data['qbankJob'] instanceof JobAbstract) {
                $storedJob->setQbankJob($data['qbankJob']);
            }
        }

        if ($data['mediaIds']) {
            $storedJob->setMediaIds($data['mediaIds']);
        }

        foreach (
            [
                'createdDate',
                'blockedDate',
                'queuedDate',
                'startedDate',
                'processedDate',
                'callbackQueuedDate',
                'callbackRunningDate',
                'callbackFailedDate',
                'callbackCompletedDate',
                'canceledDate'
            ] as $timeStamp
        ) {
            if (isset($data[$timeStamp])) {
                $setter = 'set' . $timeStamp;
                if (!$data[$timeStamp] instanceof DateTime) {
                    try {
                        $storedJob->$setter(new DateTime($data[$timeStamp]));
                    } catch (Throwable $t) {
                        throw new RuntimeException(
                            sprintf(
                                'Cannot set timestamp for status "%s", malformed date given: "%s"',
                                $timeStamp,
                                $data[$timeStamp]
                            )
                        );
                    }
                } else {
                    $storedJob->$setter($data[$timeStamp]);
                }
            }
        }

        return $storedJob;
    }

    public function getLastStatus(): ?string
    {
        $lastStatus = null;
        $lastDate = null;
        foreach (
            [
                self::QUEUE_STATUS_CREATED,
                self::QUEUE_STATUS_BLOCKED,
                self::QUEUE_STATUS_QUEUED,
                self::QUEUE_STATUS_STARTED,
                self::QUEUE_STATUS_PROCESSED,
                self::QUEUE_STATUS_CALLBACK_QUEUED,
                self::QUEUE_STATUS_CALLBACK_RUNNING,
                self::QUEUE_STATUS_CALLBACK_COMPLETED,
                self::QUEUE_STATUS_CALLBACK_FAILED,
                self::QUEUE_STATUS_CANCELED
            ] as $status
        ) {
            $getter = 'get' . $status;
            if ($lastDate && $this->$getter() && $this->$getter() >= $lastDate) {
                $lastStatus = $status;
            }
            if ($this->$getter()) {
                $lastDate = $this->$getter();
            }
        }
        return $lastStatus;
    }

    public function getLastUpdated(): ?DateTime
    {
        if ($this->getLastStatus()) {
            $getter = 'get' . $this->getLastStatus();
            return $this->$getter();
        }
        return null;
    }

    public function getQbankJob(): JobAbstract
    {
        return $this->qbankJob;
    }

    public function setQbankJob(JobAbstract $qbankJob): StoredJob
    {
        $this->qbankJob = $qbankJob;
        return $this;
    }

    /**
     * @return int[]
     */
    public function getMediaIds(): array
    {
        return $this->mediaIds;
    }

    /**
     * @param int[] $mediaIds
     * @return StoredJob
     */
    public function setMediaIds(array $mediaIds): StoredJob
    {
        $this->mediaIds = $mediaIds;
        return $this;
    }

    public function getCreatedDate(): ?DateTime
    {
        return $this->createdDate;
    }

    public function setCreatedDate(DateTime $createdDate): StoredJob
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    public function getBlockedDate(): ?DateTime
    {
        return $this->blockedDate;
    }

    public function setBlockedDate(DateTime $blockedDate): StoredJob
    {
        $this->blockedDate = $blockedDate;
        return $this;
    }

    public function getQueuedDate(): ?DateTime
    {
        return $this->queuedDate;
    }

    public function setQueuedDate(DateTime $queuedDate): StoredJob
    {
        $this->queuedDate = $queuedDate;
        return $this;
    }

    public function getStartedDate(): ?DateTime
    {
        return $this->startedDate;
    }

    public function setStartedDate(DateTime $startedDate): StoredJob
    {
        $this->startedDate = $startedDate;
        return $this;
    }

    public function getProcessedDate(): ?DateTime
    {
        return $this->processedDate;
    }

    public function setProcessedDate(DateTime $processedDate): StoredJob
    {
        $this->processedDate = $processedDate;
        return $this;
    }

    public function getCallbackQueuedDate(): ?DateTime
    {
        return $this->callbackQueuedDate;
    }

    public function setCallbackQueuedDate(DateTime $callbackQueuedDate): StoredJob
    {
        $this->callbackQueuedDate = $callbackQueuedDate;
        return $this;
    }

    public function getCallbackRunningDate(): ?DateTime
    {
        return $this->callbackRunningDate;
    }

    public function setCallbackRunningDate(DateTime $callbackRunningDate): StoredJob
    {
        $this->callbackRunningDate = $callbackRunningDate;
        return $this;
    }

    public function getCallbackFailedDate(): ?DateTime
    {
        return $this->callbackFailedDate;
    }

    public function setCallbackFailedDate(DateTime $callbackFailedDate): StoredJob
    {
        $this->callbackFailedDate = $callbackFailedDate;
        return $this;
    }

    public function getCallbackCompletedDate(): ?DateTime
    {
        return $this->callbackCompletedDate;
    }

    public function setCallbackCompletedDate(DateTime $callbackCompletedDate): StoredJob
    {
        $this->callbackCompletedDate = $callbackCompletedDate;
        return $this;
    }

    public function getCanceledDate(): ?DateTime
    {
        return $this->canceledDate;
    }

    public function setCanceledDate(DateTime $canceledDate): StoredJob
    {
        $this->canceledDate = $canceledDate;
        return $this;
    }

    public function getElasticIndex(): ?string
    {
        return $this->elasticIndex;
    }

    public function setElasticIndex(string $index): StoredJob
    {
        $this->elasticIndex = $index;
        return $this;
    }
}
