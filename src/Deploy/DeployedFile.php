<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Deploy;

use JsonSerializable;
use stdClass;

class DeployedFile implements JsonSerializable
{
    public const STATUS_UNKNOWN = 0;
    public const STATUS_UNDEPLOYED = 1;
    public const STATUS_DEPLOYED = 2;

    /** @var int */
    protected $size = 0;

    /** @var string */
    protected $remotePath = '';

    /** @var string */
    protected $errorMessage;

    /** @var int */
    protected $status = self::STATUS_UNKNOWN;

    /** @var array */
    protected $metadata = [];

    /**
     * @var string {templatetype}_{templateId} An identifier to match which template (if any) that was used to create this file.
     */
    protected $identifier;

    /**
     * @param int $size
     * @return $this
     */
    public function setSize(int $size): DeployedFile
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @param string $remotePath
     * @return $this
     */
    public function setRemotePath(string $remotePath): DeployedFile
    {
        $this->remotePath = $remotePath;
        return $this;
    }

    /**
     * @return string
     */
    public function getRemotePath(): string
    {
        return $this->remotePath;
    }

    /**
     * @param string $errorMessage
     * @return $this
     */
    public function setErrorMessage(string $errorMessage): DeployedFile
    {
        $this->errorMessage = $errorMessage;
        return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus(int $status): DeployedFile
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    public function jsonSerialize(): \stdClass
    {
        $data = new stdClass();
        $data->size = $this->getSize();
        $data->remotePath = $this->getRemotePath();
        $data->errorMessage = $this->getErrorMessage();
        $data->status = $this->getStatus();
        $data->identifier = $this->getIdentifier();
        $data->metadata = $this->getMetadata();
        return $data;
    }

    public static function fromArray(array $parameters): DeployedFile
    {
        $instance = new static();
        if (isset($parameters['size'])) {
            $instance->setSize((int)$parameters['size']);
        }
        if (isset($parameters['remotePath'])) {
            $instance->setRemotePath($parameters['remotePath']);
        }
        if (isset($parameters['errorMessage'])) {
            $instance->setErrorMessage($parameters['errorMessage']);
        }
        if (isset($parameters['status'])) {
            $instance->setStatus($parameters['status']);
        }
        if (isset($parameters['identifier'])) {
            $instance->setIdentifier($parameters['identifier']);
        }
        if (isset($parameters['metadata'])) {
            $instance->setMetadata($parameters['metadata']);
        }
        return $instance;
    }

    /**
     * @return string
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     * @return DeployedFile
     */
    public function setIdentifier(string $identifier): DeployedFile
    {
        $this->identifier = $identifier;
        return $this;
    }

    /**
     * @return array string => string Key-value array
     */
    public function getMetadata(): array
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata string => string Key-value array
     * @return DeployedFile
     */
    public function setMetadata(array $metadata): DeployedFile
    {
        $this->metadata = $metadata;
        return $this;
    }
}
