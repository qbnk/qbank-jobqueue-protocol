<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Deploy\Protocol;

use stdClass;

trait DynamicSubDirectoryTrait
{
    protected int $subDirectoryDepth = 0;

    protected string $subDirectoryType = DynamicSubDirectoryInterface::SUBDIR_TYPE_HASH;

    public function getSubDirectoryType(): string
    {
        return $this->subDirectoryType;
    }

    public function setSubDirectoryType(string $subDirectoryType): static
    {
        $this->subDirectoryType = $subDirectoryType;
        return $this;
    }

    public function getSubDirectoryDepth(): int
    {
        return $this->subDirectoryDepth;
    }

    public function setSubDirectoryDepth(int $subDirectoryDepth): static
    {
        $this->subDirectoryDepth = $subDirectoryDepth;
        return $this;
    }

    public function dynamicSubDirectoryConstruct(array $params = null): void
    {
        if (isset($params[self::SYSTEMNAME_SUBDIR_DEPTH])) {
            $this->subDirectoryDepth = (int)$params[self::SYSTEMNAME_SUBDIR_DEPTH];
        }
        if (isset($params[self::SYSTEMNAME_SUBDIR_TYPE])) {
            $this->subDirectoryType = $params[self::SYSTEMNAME_SUBDIR_TYPE];
        }
    }

    public function getDynamicSubDirectoryProperties(): array
    {
        return [
            [
                'name' => 'Dynamic subdirectory pattern',
                'systemname' => self::SYSTEMNAME_SUBDIR_TYPE,
                'description' => 'Decide how optional dynamic subdirectories will be named.',
                'datatype_id' => 6,
                'definition' => [
                    'options' => [self::SUBDIR_TYPE_MEDIAID, self::SUBDIR_TYPE_HASH],
                    'array' => true,
                    'mandatory' => true
                ]
            ],
            [
                'name' => 'Dynamic subdirectory depth',
                'systemname' => self::SYSTEMNAME_SUBDIR_DEPTH,
                'description' => 'Optional depth of dynamic subdirectories to append to target directory on publish.',
                'datatype_id' => 5,
            ]
        ];
    }

    public function getDynamicSubDirectoryJson(stdClass $json): stdClass
    {
        $json->{self::SYSTEMNAME_SUBDIR_TYPE} = $this->getSubDirectoryType();
        $json->{self::SYSTEMNAME_SUBDIR_DEPTH} = $this->getSubDirectoryDepth();
        return $json;
    }
}
