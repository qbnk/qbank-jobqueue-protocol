<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Deploy\Protocol;

use League\Flysystem\FilesystemAdapter;
use League\Flysystem\Ftp\FtpAdapter;
use League\Flysystem\Ftp\FtpConnectionOptions;

class Ftp extends FtpAbstract
{
    public const NAME = 'FTP';
    public const DESCRIPTION = 'Connects to a FTP-server and transfers the published files there.';
    public const DEFAULT_PORT = 21;

    protected int $protocolPort = self::DEFAULT_PORT;

    protected bool $passive = true;

    protected bool $ssl = false;

    public function validateConnection(): bool
    {
        $this->getFlySystemAdapter()->listContents('/', false);

        return true;
    }

    public function isPassive(): bool
    {
        return $this->passive;
    }

    public function setPassive(bool $passive): static
    {
        $this->passive = $passive;
        return $this;
    }

    public function isSsl(): bool
    {
        return $this->ssl;
    }

    public function setSsl(bool $ssl): static
    {
        $this->ssl = $ssl;
        return $this;
    }

    public function getFlySystemAdapter(): FilesystemAdapter
    {
        return new FtpAdapter(
            FtpConnectionOptions::fromArray([
                'host' => $this->getHostname(),
                'port' => $this->getPort(),
                'ssl' => $this->isSsl(),
                'timeout' => $this->getTimeout(),
                'utf8' => $this->isUtf8(),
                'passive' => $this->isPassive(),
                'transferMode' => FTP_BINARY,
                'systemType' => null, //"windows" // "unix"
                'username' => $this->getUsername(),
                'password' => $this->getPassword(),
                'root' => $this->getSubDirectory(),
            ])
        );
    }

    public function getProperties(): array
    {
        return array_merge(
            parent::getProperties(),
            [
                [
                    [
                        'name' => 'Use SSL',
                        'systemname' => 'protocol_ssl',
                        'datatype_id' => 1,
                        'definition' => [
                            'defaultvalue' => false
                        ],
                    ]
                ]
            ]
        );
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->protocol_hostname = $this->getHostname();
        $json->protocol_port = $this->getPort();
        $json->protocol_username = $this->getUsername();
        $json->protocol_password = $this->getPassword();
        $json->protocol_ssl = $this->isSsl();

        return $json;
    }

    public static function fromArray(array $data): static
    {
        $instance = parent::fromArray($data);
        if (isset($data['protocol_ssl'])) {
            $instance->setSsl((bool)$data['protocol_ssl']);
        }
        return $instance;
    }
}
