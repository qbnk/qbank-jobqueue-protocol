<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Deploy\Protocol;

use stdClass;

interface SubDirectoryInterface
{
    public const SYSTEMNAME_SUBDIR = 'protocol_deploydir';

    public function getSubDirectory(): string;

    public function setSubDirectory(string $subDirectory): static;

    public function subDirectoryConstruct(array $params = null): void;

    public function getSubDirectoryProperties(): array;

    public function getSubDirectoryJson(stdClass $json): stdClass;
}
