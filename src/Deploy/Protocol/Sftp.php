<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Deploy\Protocol;

use League\Flysystem\PhpseclibV3\SftpAdapter;
use League\Flysystem\PhpseclibV3\SftpConnectionProvider;
use League\Flysystem\UnixVisibility\PortableVisibilityConverter;
use League\Flysystem\Visibility;
use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class Sftp extends FtpAbstract
{
    public const NAME = 'SFTP';
    public const DESCRIPTION = 'Connects to an SFTP-server and transfers the published files there.';

    protected ?string $privateKey = null;

    public function getProperties(): array
    {
        return array_merge(
            parent::getProperties(),
            [
                [
                    [
                        'name' => 'Private key',
                        'systemname' => 'protocol_privatekey',
                        'datatype_id' => PropertyTypeEnum::STRING,
                        'definition' => [
                            'multiline' => true,
                            'password' => true,
                        ]
                    ]
                ]
            ]
        );
    }

    public function getFlySystemAdapter(): SftpAdapter
    {
        return new SftpAdapter(
            SftpConnectionProvider::fromArray([
                'host' => $this->getHostname(),
                'port' => $this->getPort(),
                'timeout' => 90,
                'username' => $this->getUsername(),
                'password' => $this->getPassword(),
                'privateKey' => $this->getPrivateKey()
            ]),
            $this->getSubDirectory(),
            PortableVisibilityConverter::fromArray([
                'file' => [
                    Visibility::PUBLIC => octdec($this->getDefaultFilePermissions())
                ],
                'dir' => [
                    Visibility::PUBLIC => octdec($this->getDefaultFolderPermissions())
                ]
            ], Visibility::PUBLIC)
        );
    }

    public function getPrivateKey(): ?string
    {
        return $this->privateKey;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->protocol_privatekey = $this->getPrivateKey();
        return $json;
    }

    public static function fromArray(array $data): static
    {
        $instance = parent::fromArray($data);
        if (isset($data['protocol_privatekey'])) {
            $instance->setPrivateKey($data['protocol_privatekey']);
        }
        return $instance;
    }

    public function setPrivateKey(?string $privateKey): static
    {
        $this->privateKey = empty($privateKey) ? null : $privateKey;
        return $this;
    }
}
