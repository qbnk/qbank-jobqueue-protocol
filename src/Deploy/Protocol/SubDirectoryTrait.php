<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Deploy\Protocol;

use stdClass;

trait SubDirectoryTrait
{
    protected string $subDirectory = '';

    public function getSubDirectory(): string
    {
        return $this->subDirectory;
    }

    public function subDirectoryConstruct(array $params = null): void
    {
        if (isset($params[self::SYSTEMNAME_SUBDIR])) {
            $this->subDirectory = $params[self::SYSTEMNAME_SUBDIR];
        }
    }

    public function getSubDirectoryProperties(): array
    {
        return [
            [
                'name' => 'Subdirectory',
                'systemname' => self::SYSTEMNAME_SUBDIR,
                'datatype_id' => 6,
                'definition' => [
                    'mandatory' => true,
                ],
            ]
        ];
    }

    public function getSubDirectoryJson(stdClass $json): stdClass
    {
        $json->{self::SYSTEMNAME_SUBDIR} = $this->getSubDirectory();
        return $json;
    }

    /**
     * @param string $subDirectory
     * @return $this
     */
    public function setSubDirectory(string $subDirectory): static
    {
        $this->subDirectory = $subDirectory;
        return $this;
    }
}
