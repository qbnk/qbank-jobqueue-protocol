<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Deploy\Protocol;

use League\Flysystem\AzureBlobStorage\AzureBlobStorageAdapter;
use MicrosoftAzure\Storage\Blob\BlobRestProxy;

class Azure extends Copy
{
    public const NAME = 'Azure';
    public const DESCRIPTION = 'Publish files to an Azure container';

    protected string $accountName;

    protected string $accountKey;

    protected string $container;

    public function validateConnection(): bool
    {
        $this->getBlobService()->getContainerProperties($this->getContainer());

        return true;
    }

    public function getProperties(): array
    {
        return [
            array_merge(
                $this->getSubDirectoryProperties(),
                $this->getDynamicSubDirectoryProperties(),
                [
                    [
                        'name' => 'Account name',
                        'systemname' => 'protocol_accountname',
                        'datatype_id' => 6,
                        'definition' => [
                            'mandatory' => true,
                        ],
                    ],
                    [
                        'name' => 'Account key',
                        'systemname' => 'protocol_accountkey',
                        'datatype_id' => 6,
                        'definition' => [
                            'mandatory' => true,
                            'password' => true,
                        ],
                    ],
                    [
                        'name' => 'Container',
                        'systemname' => 'protocol_container',
                        'datatype_id' => 6,
                        'definition' => [
                            'mandatory' => true,
                        ],
                    ],
                ]
            )
        ];
    }

    public function getAccountName(): string
    {
        return $this->accountName;
    }

    public function getAccountKey(): string
    {
        return $this->accountKey;
    }

    public function getContainer(): string
    {
        return $this->container;
    }

    public function getFlySystemAdapter(): AzureBlobStorageAdapter
    {
        return new AzureBlobStorageAdapter(
            $this->getBlobService(),
            $this->getContainer()
        );
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->protocol_accountname = $this->getAccountName();
        $json->protocol_accountkey = $this->getAccountKey();
        $json->protocol_container = $this->getContainer();
        return $json;
    }

    public static function fromArray(array $data): Azure
    {
        $instance = new static();

        if (isset($data['protocol_accountname'])) {
            $instance->setAccountName($data['protocol_accountname']);
        }
        if (isset($data['protocol_accountkey'])) {
            $instance->setAccountKey($data['protocol_accountkey']);
        }
        if (isset($data['protocol_container'])) {
            $instance->setContainer($data['protocol_container']);
        }

        return $instance;
    }

    private function getBlobService(): BlobRestProxy
    {
        return BlobRestProxy::createBlobService(
            sprintf(
                'DefaultEndpointsProtocol=https;AccountName=%s;AccountKey=%s',
                $this->getAccountName(),
                $this->getAccountKey()
            )
        );
    }

    public function setAccountName(string $accountName): Azure
    {
        $this->accountName = $accountName;
        return $this;
    }

    public function setAccountKey(string $accountKey): Azure
    {
        $this->accountKey = $accountKey;
        return $this;
    }

    public function setContainer(string $container): Azure
    {
        $this->container = $container;
        return $this;
    }
}
