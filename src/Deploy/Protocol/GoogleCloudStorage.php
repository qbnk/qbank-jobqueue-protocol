<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Deploy\Protocol;

use Google\Cloud\Storage\StorageClient;
use League\Flysystem\GoogleCloudStorage\GoogleCloudStorageAdapter;

class GoogleCloudStorage extends Copy
{
    public const NAME = 'Google Cloud Storage';
    public const DESCRIPTION = 'Copies the published files to a Google bucket storage.';

    protected string $bucket;

    protected array $credentials = [];

    public function validateConnection(): bool
    {
        return $this->getStorageClient()->bucket($this->bucket)->exists();
    }

    /**
     * @return array
     */
    public function getProperties(): array
    {
        $propertySets = array_merge(
            [
                [
                    [
                        'name' => 'Credentials',
                        'systemname' => 'credentials',
                        'description' => 'The credentials of the service account can be obtained via the Google developer console.',
                        'datatype_id' => 6,
                        'definition' => [
                            'stringformat' => 'json',
                            'multiline' => true,
                            'mandatory' => true,
                        ],
                    ],
                    [
                        'name' => 'Bucket name',
                        'systemname' => 'bucket',
                        'description' => 'Where assets should be stored.',
                        'datatype_id' => 6,
                        'definition' => [
                            'mandatory' => true,
                        ],
                    ]
                ],
            ],
            parent::getProperties()
        );

        // Sub dir is not mandatory for Google Storage
        foreach ($propertySets as $key => $propertyset) {
            foreach ($propertyset as $subKey => $property) {
                if ($property['systemname'] === self::SYSTEMNAME_SUBDIR) {
                    unset($propertySets[$key][$subKey]['definition']['mandatory']);
                }
            }
        }

        return $propertySets;
    }

    public function getFlySystemAdapter(): GoogleCloudStorageAdapter
    {
        $client = $this->getStorageClient();
        return new GoogleCloudStorageAdapter($client->bucket($this->bucket), $this->getSubDirectory());
    }

    private function getStorageClient(): StorageClient
    {
        return new StorageClient(['keyFile' => $this->getCredentials()]);
    }

    public function getBucket(): ?string
    {
        return $this->bucket;
    }

    public function getCredentials(): array
    {
        return $this->credentials;
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->credentials = json_encode($this->getCredentials());
        $json->bucket = $this->getBucket();

        return $json;
    }

    public static function fromArray(array $data): GoogleCloudStorage
    {
        $instance = parent::fromArray($data);

        if (isset($data['credentials'])) {
            $instance->setCredentials($data['credentials']);
        }

        if (isset($data['bucket'])) {
            $instance->setBucket($data['bucket']);
        }

        return $instance;
    }

    public function setBucket(string $bucket): GoogleCloudStorage
    {
        $this->bucket = $bucket;
        return $this;
    }

    public function setCredentials($credentials): GoogleCloudStorage
    {
        if (is_string($credentials)) {
            $this->credentials = json_decode($credentials, true);
        } elseif (is_array($credentials)) {
            $this->credentials = $credentials;
        }
        return $this;
    }
}
