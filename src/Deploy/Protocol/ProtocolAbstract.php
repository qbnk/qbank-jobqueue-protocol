<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Deploy\Protocol;

use JsonSerializable;
use League\Flysystem\FilesystemAdapter;
use RuntimeException;
use stdClass;

abstract class ProtocolAbstract implements JsonSerializable
{
    public const NAME = '_unset';
    public const DESCRIPTION = '_unset';

    protected array $filesystemOptions = [];

    /**
     * Verify that this protocol configuration is valid by endpoint service. Return true or exception with error message
     * @return bool
     */
    public function validateConnection(): bool
    {
        return true;
    }

    public function getProperties(): array
    {
        return [];
    }

    abstract public function getFlySystemAdapter(): FilesystemAdapter;

    public function getDescription(): string
    {
        return $this::DESCRIPTION;
    }

    public function getName(): string
    {
        return $this::NAME;
    }

    /**
     * Flysystem Adapters handle base paths differently, so these two sibling methods exist to make sure that we apply paths correctly in the right situation
     * @param string $filename
     * @return string
     */
    public function preparePath(string $filename): string
    {
        return $this->applyPath($filename);
    }

    /**
     * Flysystem Adapters handle base paths differently, so these two sibling methods exist to make sure that we apply paths correctly in the right situation
     * @param string $filename
     * @return string
     */
    public function applyPath(string $filename): string
    {
        return $filename;
    }

    public function getFilesystemOptions(): array
    {
        return $this->filesystemOptions;
    }

    public function setFilesystemOptions(array $filesystemOptions): static
    {
        $this->filesystemOptions = $filesystemOptions;
        return $this;
    }

    /**
     * Gets all data that should be available in a json representation.
     *
     * @return stdClass
     */
    public function jsonSerialize(): \stdClass
    {
        $protocol = new stdClass();
        $protocol->name = $this->getName();
        $protocol->filesystemOptions = $this->getFilesystemOptions();
        return $protocol;
    }

    /**
     * From associative array derived from json_decode()
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data): ProtocolAbstract
    {
        try {
            $instance = match ($data['name'] ?? '') {
                Copy::NAME => Copy::fromArray($data),
                CopyDynamic::NAME => CopyDynamic::fromArray($data),
                Ftp::NAME => Ftp::fromArray($data),
                Sftp::NAME => Sftp::fromArray($data),
                WebDAV::NAME => WebDAV::fromArray($data),
                Azure::NAME => Azure::fromArray($data),
                Dropbox::NAME => Dropbox::fromArray($data),
                Aws::NAME => Aws::fromArray($data),
                GoogleCloudStorage::NAME => GoogleCloudStorage::fromArray($data)
            };
            $instance->setFilesystemOptions($data['filesystemOptions'] ?? []);
        } catch (\UnhandledMatchError $ume) {
            throw new RuntimeException(sprintf('Unknown protocol class: "%s"', $data['name'] ?? ''));
        }
        return $instance;
    }
}
