<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Deploy\Protocol;

use Spatie\Dropbox\Client;
use Spatie\FlysystemDropbox\DropboxAdapter;

class Dropbox extends Copy
{
    public const NAME = 'Dropbox';
    public const DESCRIPTION = 'Publish files to a Dropbox account';

    protected string $authToken;

    public function preparePath(string $filename): string
    {
        return $this->applyPath($filename);
    }

    public function validateConnection(): bool
    {
        $this->getClient()->getAccountInfo();

        return true;
    }

    public function getProperties(): array
    {
        return array_merge([
            [
                [
                    'name' => 'Authorization token',
                    'description' => 'A token can be generated in the App Console for any Dropbox API app. You’ll find more info at the Dropbox Developer Blog.',
                    'systemname' => 'protocol_authtoken',
                    'datatype_id' => 6,
                    'definition' => [
                        'mandatory' => true,
                        'password' => true,
                    ],
                ]
            ]
        ], parent::getProperties());
    }

    /**
     * @return string
     */
    public function getAuthorizationToken(): string
    {
        return $this->authToken;
    }

    public function getFlySystemAdapter(): DropboxAdapter
    {
        return new DropboxAdapter($this->getClient());
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->protocol_authtoken = $this->getAuthorizationToken();
        return $json;
    }

    public static function fromArray(array $data): Dropbox
    {
        $instance = parent::fromArray($data);

        if (isset($data['protocol_authtoken'])) {
            $instance->setAuthToken($data['protocol_authtoken']);
        }

        return $instance;
    }

    private function getClient(): Client
    {
        return new Client($this->getAuthorizationToken());
    }

    public function setAuthToken(string $authToken): Dropbox
    {
        $this->authToken = $authToken;
        return $this;
    }
}
