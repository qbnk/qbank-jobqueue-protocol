<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Deploy\Protocol;

use stdClass;

interface DynamicSubDirectoryInterface
{
    public const SYSTEMNAME_SUBDIR_DEPTH = 'subdirdepth';
    public const SYSTEMNAME_SUBDIR_TYPE = 'subdirtype';
    public const SUBDIR_TYPE_MEDIAID = 'Media ID';
    public const SUBDIR_TYPE_HASH = 'Hash';

    public function getSubDirectoryType(): string;

    public function setSubDirectoryType(string $subDirectoryType): static;

    public function getSubDirectoryDepth(): int;

    public function setSubDirectoryDepth(int $subDirectoryDepth): static;

    public function dynamicSubDirectoryConstruct(array $params = null): void;

    public function getDynamicSubDirectoryProperties(): array;

    public function getDynamicSubDirectoryJson(stdClass $json): stdClass;
}
