<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Deploy\Protocol;

use League\Flysystem\FilesystemAdapter;
use League\Flysystem\WebDAV\WebDAVAdapter;
use Sabre\DAV\Client;

class WebDAV extends Copy
{
    public const NAME = 'WebDAV';
    public const DESCRIPTION = 'Connects to a WebDav and transfers the published files there.';

    protected string $protocolHostname;

    protected string $protocolUsername;

    protected string $protocolPassword;

    public function setHostname(string $hostname): static
    {
        $this->protocolHostname = $hostname;
        return $this;
    }

    public function setUsername(string $username): static
    {
        $this->protocolUsername = $username;
        return $this;
    }

    public function setPassword(string $password): static
    {
        $this->protocolPassword = $password;
        return $this;
    }

    public function getHostname(): string
    {
        return $this->protocolHostname;
    }

    public function getUsername(): string
    {
        return $this->protocolUsername;
    }

    public function getPassword(): string
    {
        return $this->protocolPassword;
    }

    public function getFlySystemAdapter(): FilesystemAdapter
    {
        $client = new Client([
            'baseUri' => $this->getHostname(),
            'userName' => $this->getUsername(),
            'password' => $this->getPassword(),
            'authType' => Client::AUTH_BASIC
        ]);
        return new WebDAVAdapter($client, $this->getSubDirectory());
    }

    public function getProperties(): array
    {
        return array_merge([
            [
                [
                    'name' => 'Server hostname',
                    'systemname' => 'protocol_hostname',
                    'datatype_id' => 6,
                    'definition' => [
                        'mandatory' => true,
                    ],
                ],
                [
                    'name' => 'Username',
                    'systemname' => 'protocol_username',
                    'datatype_id' => 6,
                    'definition' => [
                        'mandatory' => true,
                    ],
                ],
                [
                    'name' => 'Password',
                    'systemname' => 'protocol_password',
                    'datatype_id' => 6,
                    'definition' => [
                        'mandatory' => true,
                        'password' => true
                    ],
                ]
            ]
        ], parent::getProperties());
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->protocol_hostname = $this->getHostname();
        $json->protocol_username = $this->getUsername();
        $json->protocol_password = $this->getPassword();

        return $json;
    }

    public static function fromArray(array $data): WebDAV
    {
        $instance = parent::fromArray($data);
        if (isset($data['protocol_hostname'])) {
            $instance->setHostname($data['protocol_hostname']);
        }
        if (isset($data['protocol_username'])) {
            $instance->setUsername($data['protocol_username']);
        }
        if (isset($data['protocol_password'])) {
            $instance->setPassword($data['protocol_password']);
        }
        return $instance;
    }
}
