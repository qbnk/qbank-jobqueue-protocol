<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Deploy\Protocol;

use Aws\S3\S3Client;
use League\Flysystem\AwsS3V3\AwsS3V3Adapter;

class Aws extends Copy
{
    public const NAME = 'AWS S3';
    public const DESCRIPTION = 'Publish files to an Amazon S3 bucket';
    protected const REGION_MAPPING = [
        ['US East (Ohio)', 'us-east-2'],
        ['US East (N. Virginia)', 'us-east-1'],
        ['US West (N. California)', 'us-west-1'],
        ['US West (Oregon)', 'us-west-2'],
        ['Asia Pacific (Tokyo)', 'ap-northeast-1'],
        ['Asia Pacific (Seoul)', 'ap-northeast-2'],
        ['Asia Pacific (Osaka-Local)', 'ap-northeast-3'],
        ['Asia Pacific (Mumbai)', 'ap-south-1'],
        ['Asia Pacific (Singapore)', 'ap-southeast-1'],
        ['Asia Pacific (Sydney)', 'ap-southeast-2'],
        ['Canada (Central)', 'ca-central-1'],
        ['China (Beijing)', 'cn-north-1'],
        ['China (Ningxia)', 'cn-northwest-1'],
        ['EU (Stockholm)', 'eu-north-1'],
        ['EU (Frankfurt)', 'eu-central-1'],
        ['EU (Ireland)', 'eu-west-1'],
        ['EU (London)', 'eu-west-2'],
        ['EU (Paris)', 'eu-west-3'],
        ['South America (Sao Paulo)', 'sa-east-1']
    ];

    /**
     * https://docs.aws.amazon.com/aws-sdk-php/v3/api/index.html
     */
    public const API_VERSION = '2006-03-01';

    protected string $accountKey;

    protected string $accountSecret;

    protected string $region;

    protected string $bucket;

    protected array $clientOptions = [];
    protected array $adapterOptions = [];

    public const CLIENT_OPTIONS = 'protocol_client_options';
    public const ADAPTER_OPTIONS = 'protocol_adapter_options';

    public function validateConnection(): bool
    {
        $this->getS3Client()->headBucket(['Bucket' => $this->getBucket()]);

        return true;
    }

    public function getProperties(): array
    {
        return array_merge(
            [
                [
                    [
                        'name' => 'Access key',
                        'systemname' => 'protocol_accountkey',
                        'datatype_id' => 6,
                        'definition' => [
                            'mandatory' => true,
                        ],
                    ],
                    [
                        'name' => 'Access secret',
                        'systemname' => 'protocol_accountsecret',
                        'datatype_id' => 6,
                        'definition' => [
                            'mandatory' => true,
                            'password' => true,
                        ],
                    ],
                    [
                        'name' => 'Bucket',
                        'systemname' => 'protocol_bucket',
                        'datatype_id' => 6,
                        'definition' => [
                            'mandatory' => true,
                        ],
                    ],
                    [
                        'name' => 'Region',
                        'systemname' => 'protocol_region',
                        'datatype_id' => 6,
                        'definition' => [
                            'mandatory' => true,
                            'array' => true,
                            'options' => $this->getRegions()
                        ],
                    ],
                    [
                        'name' => 'Set tags',
                        'systemname' => 'protocol_set_tenant_tags',
                        'datatype_id' => 1,
                        'definition' => [
                            'description' => 'Will set tags on the uploaded files with information about the QBank instance',
                            'mandatory' => false,
                        ],
                    ],
                ],
            ],
            parent::getProperties()
        );
    }

    public function getRegions(): array
    {
        return array_map(static function ($region) {
            return ['key' => $region[1], 'value' => $region[0]];
        }, self::REGION_MAPPING);
    }

    public function getAccountKey(): string
    {
        return $this->accountKey;
    }

    public function getAccountSecret(): string
    {
        return $this->accountSecret;
    }

    public function getRegion(): string
    {
        return $this->region;
    }

    public function getBucket(): string
    {
        return $this->bucket;
    }

    public function getFlySystemAdapter(): AwsS3V3Adapter
    {
        return new AwsS3V3Adapter(
            $this->getS3Client(),
            $this->getBucket(),
            $this->getSubDirectory(),
            options: $this->adapterOptions
        );
    }

    private function getS3Client()
    {
        $options = array_merge(
            [
                'credentials' => [
                    'key' => $this->getAccountKey(),
                    'secret' => $this->getAccountSecret(),
                ],
                'region' => $this->getRegion(),
                'version' => self::API_VERSION
            ],
            $this->clientOptions
        );

        if (empty($options['credentials']['key'])) {
            unset($options['credentials']);
        }

        return new S3Client($options);
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->protocol_accountsecret = $this->getAccountSecret();
        $json->protocol_accountkey = $this->getAccountKey();
        $json->protocol_bucket = $this->getBucket();
        $json->protocol_region = $this->getRegion();
        $json->protocol_client_options = $this->clientOptions;
        $json->protocol_adapter_options = $this->adapterOptions;
        return $json;
    }

    public static function fromArray(array $data): Aws
    {
        $instance = parent::fromArray($data);

        if (isset($data['protocol_accountsecret'])) {
            $instance->setAccountSecret($data['protocol_accountsecret']);
        }
        if (isset($data['protocol_accountkey'])) {
            $instance->setAccountKey($data['protocol_accountkey']);
        }
        if (isset($data['protocol_bucket'])) {
            $instance->setBucket($data['protocol_bucket']);
        }
        if (isset($data['protocol_region'])) {
            $instance->setRegion($data['protocol_region']);
        }
        if (isset($data[self::CLIENT_OPTIONS])) {
            $instance->clientOptions = is_string($data[self::CLIENT_OPTIONS])
                ? json_decode($data[self::CLIENT_OPTIONS], true)
                : $data[self::CLIENT_OPTIONS];
        }
        if (isset($data[self::ADAPTER_OPTIONS])) {
            $instance->adapterOptions = is_string($data[self::ADAPTER_OPTIONS])
                ? json_decode($data[self::ADAPTER_OPTIONS], true)
                : $data[self::ADAPTER_OPTIONS];
        }

        return $instance;
    }

    public function setAccountKey(string $accountKey): Aws
    {
        $this->accountKey = $accountKey;
        return $this;
    }

    public function setAccountSecret(string $accountSecret): Aws
    {
        $this->accountSecret = $accountSecret;
        return $this;
    }

    public function setRegion(string $region): Aws
    {
        $this->region = $region;
        return $this;
    }

    public function setBucket(string $bucket): Aws
    {
        $this->bucket = $bucket;
        return $this;
    }
}
