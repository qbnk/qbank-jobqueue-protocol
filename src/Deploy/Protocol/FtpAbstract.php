<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Deploy\Protocol;

use QBNK\JobQueue\Job\Convert\Command\PropertyTypeEnum;

class FtpAbstract extends Copy
{
    protected const DEFAULT_FOLDER_PERMISSIONS = '755';

    protected const DEFAULT_FILE_PERMISSIONS = '644';

    protected string $protocolHostname;

    protected string $protocolUsername;

    protected string $protocolPassword;

    protected int $protocolPort;

    protected bool $utf8 = false;

    protected int $timeout = 90;

    protected ?string $protocolFolderPermissions = null;

    protected ?string $protocolFilePermissions = null;

    public function setHostname(string $hostname): static
    {
        $this->protocolHostname = $hostname;
        return $this;
    }

    public function setPort(int $port): static
    {
        $this->protocolPort = $port;
        return $this;
    }

    public function setUsername(string $username): static
    {
        $this->protocolUsername = $username;
        return $this;
    }

    public function setPassword(string $password): static
    {
        $this->protocolPassword = $password;
        return $this;
    }

    public function getHostname(): string
    {
        return $this->protocolHostname;
    }

    public function getPort(): int
    {
        return $this->protocolPort;
    }

    public function getUsername(): string
    {
        return $this->protocolUsername;
    }

    public function getPassword(): string
    {
        return $this->protocolPassword;
    }

    public function isUtf8(): bool
    {
        return $this->utf8;
    }

    public function setUtf8(bool $utf8): static
    {
        $this->utf8 = $utf8;
        return $this;
    }

    public function getTimeout(): int
    {
        return $this->timeout;
    }

    public function setTimeout(int $timeout): static
    {
        $this->timeout = $timeout;
        return $this;
    }

    public function getDefaultFolderPermissions(): string
    {
        return $this->protocolFolderPermissions ?? self::DEFAULT_FOLDER_PERMISSIONS;
    }

    public function setFolderPermissions(string $folderPermissions): static
    {
        $this->protocolFolderPermissions = $folderPermissions;
        return $this;
    }

    public function getDefaultFilePermissions(): string
    {
        return $this->protocolFilePermissions ?? self::DEFAULT_FILE_PERMISSIONS;
    }

    public function setFilePermissions(string $filePermissions): static
    {
        $this->protocolFilePermissions = $filePermissions;
        return $this;
    }

    public function getProperties(): array
    {
        return array_merge([
            [
                [
                    'name' => 'Server hostname',
                    'systemname' => 'protocol_hostname',
                    'datatype_id' => PropertyTypeEnum::STRING,
                    'definition' => [
                        'mandatory' => true,
                    ],
                ],
                [
                    'name' => 'Server port',
                    'systemname' => 'protocol_port',
                    'datatype_id' => PropertyTypeEnum::INTEGER,
                    'definition' => [
                        'mandatory' => false,
                    ],
                ],
                [
                    'name' => 'Username',
                    'systemname' => 'protocol_username',
                    'datatype_id' => PropertyTypeEnum::STRING,
                    'definition' => [
                        'mandatory' => true,
                    ],
                ],
                [
                    'name' => 'Password',
                    'systemname' => 'protocol_password',
                    'datatype_id' => PropertyTypeEnum::STRING,
                    'definition' => [
                        'mandatory' => true,
                        'password' => true
                    ],
                ],
                [
                    'name' => 'Folder permissions',
                    'systemname' => 'protocol_folder_permissions',
                    'datatype_id' => PropertyTypeEnum::STRING,
                    'definition' => [
                        'pattern' => '[0-7]{3}'
                    ],
                ],
                [
                    'name' => 'File permissions',
                    'systemname' => 'protocol_file_permissions',
                    'datatype_id' => PropertyTypeEnum::STRING,
                    'definition' => [
                        'pattern' => '[0-7]{3}'
                    ],
                ],
            ]
        ], parent::getProperties());
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json->protocol_hostname = $this->getHostname();
        $json->protocol_port = $this->getPort();
        $json->protocol_username = $this->getUsername();
        $json->protocol_password = $this->getPassword();
        $json->protocol_folder_permissions = $this->getDefaultFolderPermissions();
        $json->protocol_file_permissions = $this->getDefaultFilePermissions();

        return $json;
    }

    public static function fromArray(array $data): static
    {
        $instance = parent::fromArray($data);
        if (isset($data['protocol_hostname'])) {
            $instance->setHostname($data['protocol_hostname']);
        }
        if (isset($data['protocol_port'])) {
            $instance->setPort((int)$data['protocol_port']);
        }
        if (isset($data['protocol_username'])) {
            $instance->setUsername($data['protocol_username']);
        }
        if (isset($data['protocol_password'])) {
            $instance->setPassword($data['protocol_password']);
        }
        if (isset($data['protocol_folder_permissions'])) {
            $instance->setFolderPermissions((string)$data['protocol_folder_permissions']);
        }
        if (isset($data['protocol_file_permissions'])) {
            $instance->setFilePermissions((string)$data['protocol_file_permissions']);
        }
        return $instance;
    }
}
