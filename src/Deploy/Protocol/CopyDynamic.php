<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Deploy\Protocol;

use League\Flysystem\FilesystemAdapter;
use League\Flysystem\Local\LocalFilesystemAdapter;

class CopyDynamic extends ProtocolAbstract implements DynamicSubDirectoryInterface, SubDirectoryInterface
{
    use DynamicSubDirectoryTrait;
    use SubDirectoryTrait;

    public const NAME = 'Copy Dynamic';
    public const DESCRIPTION = 'Copies the published files to another location on the filesystem and allows for dynamic generation of images.';

    public function getProperties(): array
    {
        return [
            array_merge(
                $this->getSubDirectoryProperties(),
                $this->getDynamicSubDirectoryProperties()
            )
        ];
    }

    /**
     * Adapter prefixes $filename internally so we need to return it un-prefixed before handling
     */
    public function preparePath(string $filename): string
    {
        return ltrim(str_replace($this->getSubDirectory(), '', $filename), DIRECTORY_SEPARATOR);
    }

    public function applyPath(string $filename): string
    {
        return rtrim($this->getSubDirectory(), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $filename;
    }

    public function getFlySystemAdapter(): FilesystemAdapter
    {
        return new LocalFilesystemAdapter($this->getSubDirectory());
    }

    public function jsonSerialize(): \stdClass
    {
        $json = parent::jsonSerialize();
        $json = $this->getSubDirectoryJson($json);
        $json = $this->getDynamicSubDirectoryJson($json);
        return $json;
    }

    /**
     * @return static
     */
    public static function fromArray(array $data): CopyDynamic
    {
        $instance = new static();
        $instance->subDirectoryConstruct($data);
        $instance->dynamicSubDirectoryConstruct($data);
        return $instance;
    }
}
