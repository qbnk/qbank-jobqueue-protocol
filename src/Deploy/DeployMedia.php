<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Deploy;

use JsonSerializable;
use QBNK\JobQueue\Job\Convert\ConvertAbstract;
use QBNK\JobQueue\Job\Storage\File;
use stdClass;

class DeployMedia implements JsonSerializable
{
    protected int $mediaId = 0;

    protected ?string $subDirectory = null;

    protected ?string $primaryClippingPathName = null;

    /** @var ConvertAbstract[] */
    protected array $convertJobs = [];

    /** @var DeployedFile[] */
    protected array $deployedFiles = [];

    protected ?string $errorMessage = null;

    public function setMediaId(int $mediaId): DeployMedia
    {
        $this->mediaId = $mediaId;
        return $this;
    }

    public function getMediaId(): int
    {
        return $this->mediaId;
    }

    public function setSubDirectory(string $subDirectory): DeployMedia
    {
        $this->subDirectory = $subDirectory;
        return $this;
    }

    public function getSubDirectory(): ?string
    {
        return $this->subDirectory;
    }

    public function setPrimaryClippingPathName(?string $primaryClippingPathName): DeployMedia
    {
        $this->primaryClippingPathName = $primaryClippingPathName;
        return $this;
    }

    public function getPrimaryClippingPathName(): ?string
    {
        return $this->primaryClippingPathName;
    }

    /**
     * @param ConvertAbstract[] $convertJobs
     * @return $this
     */
    public function setConvertJobs(array $convertJobs): DeployMedia
    {
        $this->convertJobs = $convertJobs;
        return $this;
    }

    /**
     * @return ConvertAbstract[]
     */
    public function getConvertJobs(): array
    {
        return $this->convertJobs;
    }

    /**
     * @param DeployedFile[] $files
     * @return $this
     */
    public function setDeployedFiles(array $files): DeployMedia
    {
        $this->deployedFiles = [];
        foreach ($files as $file) {
            if (is_array($file)) {
                $file = DeployedFile::fromArray($file);
            }

            if ($file instanceof DeployedFile) {
                $this->deployedFiles[] = $file;
            }
        }

        return $this;
    }

    /**
     * @return DeployedFile[]
     */
    public function getDeployedFiles(): array
    {
        return $this->deployedFiles;
    }

    public function setErrorMessage(?string $errorMessage): DeployMedia
    {
        $this->errorMessage = $errorMessage;
        return $this;
    }

    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }

    public function jsonSerialize(): \stdClass
    {
        $data = new stdClass();
        $data->class = static::class;
        $data->mediaId = $this->getMediaId();
        $data->subDirectory = $this->getSubDirectory();
        $data->primaryClippingPathName = $this->getPrimaryClippingPathName();
        $data->convertJobs = $this->getConvertJobs();
        $data->deployedFiles = $this->getDeployedFiles();
        $data->errorMessage = $this->getErrorMessage();
        return $data;
    }

    public static function fromArray(array $parameters): DeployMedia
    {
        $instance = new static();
        if (isset($parameters['mediaId'])) {
            $instance->setMediaId((int)$parameters['mediaId']);
        }

        if (isset($parameters['subDirectory'])) {
            $instance->setSubDirectory($parameters['subDirectory']);
        }

        if (isset($parameters['primaryClippingPathName'])) {
            $instance->setPrimaryClippingPathName($parameters['primaryClippingPathName']);
        }

        if (isset($parameters['errorMessage'])) {
            $instance->setErrorMessage($parameters['errorMessage']);
        }
        if (isset($parameters['convertJobs'])) {
            $convertJobs = [];
            foreach ($parameters['convertJobs'] as $convertJob) {
                if ($convertJob instanceof ConvertAbstract) {
                    $convertJobs[] = $convertJob;
                } else {
                    $convertJobs[] = ConvertAbstract::fromArray($convertJob);
                }
            }
            $instance->setConvertJobs($convertJobs);
        }

        if (isset($parameters['deployedFiles'])) {
            $files = [];
            foreach ($parameters['deployedFiles'] as $file) {
                if ($file instanceof File) {
                    $files[] = $file;
                } else {
                    $files[] = DeployedFile::fromArray($file);
                }
            }
            $instance->setDeployedFiles($files);
        }

        return $instance;
    }
}
