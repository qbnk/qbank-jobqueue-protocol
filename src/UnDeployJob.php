<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job;

use QBNK\JobQueue\Job\Deploy\DeployMedia;
use QBNK\JobQueue\Job\Deploy\Protocol\ProtocolAbstract;
use QBNK\JobQueue\Job\Enums\JobStatus;
use TypeError;

class UnDeployJob extends JobAbstract
{
    /** @var DeployMedia[] */
    private array $media = [];

    private int $deploymentSiteId;

    private ProtocolAbstract $protocol;

    /**
     * @param DeployMedia[] $media
     * @return $this
     */
    public function setMedia(array $media): UnDeployJob
    {
        $this->media = [];
        foreach ($media as $mediaCandidate) {
            if (is_array($mediaCandidate)) {
                $mediaCandidate = DeployMedia::fromArray($mediaCandidate);
            }

            if ($mediaCandidate instanceof DeployMedia) {
                $this->media[] = $mediaCandidate;
            }
        }
        return $this;
    }

    /**
     * @return DeployMedia[]
     */
    public function getMedia(): array
    {
        return $this->media;
    }

    public function setDeploymentSiteId(int $deploymentSiteId): UnDeployJob
    {
        $this->deploymentSiteId = $deploymentSiteId;
        return $this;
    }

    public function getDeploymentSiteId(): int
    {
        return $this->deploymentSiteId;
    }

    /**
     * @param ProtocolAbstract|array $protocol
     * @return $this
     */
    public function setProtocol($protocol): UnDeployJob
    {
        if (!($protocol instanceof ProtocolAbstract) && !is_array($protocol)) {
            throw new TypeError(
                sprintf('Incorrect type, ProtocolAbstract or array expected, got %s instead', json_encode($protocol))
            );
        }

        if (is_array($protocol)) {
            $protocol = ProtocolAbstract::fromArray($protocol);
        }

        $this->protocol = $protocol;
        return $this;
    }

    public function getProtocol(): ProtocolAbstract
    {
        return $this->protocol;
    }

    public function jsonSerialize(): \stdClass
    {
        $data = parent::jsonSerialize();
        $data->media = $this->getMedia();
        $data->deploymentSiteId = $this->getDeploymentSiteId();
        $data->protocol = $this->getProtocol();
        return $data;
    }

    public function getQueueName(): string
    {
        return $this->queueName ?? 'undeploy';
    }

    public static function fromArray(array $parameters, bool $assignId = false): static
    {
        $instance = parent::fromArray($parameters, $assignId);

        if (isset($parameters['media'])) {
            $media = [];
            foreach ($parameters['media'] as $mediaData) {
                if ($mediaData instanceof DeployMedia) {
                    $media[] = $mediaData;
                } else {
                    $media[] = DeployMedia::fromArray($mediaData);
                }
            }
            $instance->setMedia($media);
        }

        return $instance;
    }

    public function asLegacy(): array
    {
        $data = [
            'deploymentSiteId' => $this->getDeploymentSiteId(),
            'undeployedFiles' => [],
            'success' => $this->getStatus() === JobStatus::Success
        ];

        foreach ($this->getMedia() as $deployMedia) {
            $data['undeployedFiles'][$deployMedia->getMediaId()] = [];
            foreach ($deployMedia->getDeployedFiles() as $deployedFile) {
                $data['undeployedFiles'][$deployMedia->getMediaId()][] = [
                    'remoteFilePath' => $deployedFile->getRemotePath()
                ];
            }
        }

        return $data;
    }
}
