<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Examination\MimeTypeJob;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\Storage\CombineChunksJob;
use QBNK\JobQueue\Job\Storage\File;

class CombineChunksJobTest extends TestCase
{

    function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => CombineChunksJob::class,
            'queueName' => 'combinechunks',
            'chunkDirectory' => '/tmp/chunkdirtest/',
            'checkSum' => 'eb62f6b9306db575c2d596b1279627a4',
            'combinedFile' => [
                'source' => '/dummydir/dummyinputfile.tmp',
                'mimeType' => 'test/tmp',
                'classification' => 'Document',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],
            'mimeTypeJob' => MimeTypeJobTest::getDataModel(),
            'createThumbnails' => false,
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var CombineChunksJob $combineChunksJob */
        $combineChunksJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        self::assertInstanceOf(CombineChunksJob::class, $combineChunksJob);
        self::assertInstanceOf(MimeTypeJob::class, $combineChunksJob->getMimeTypeJob());
        self::assertInstanceOf(File::class, $combineChunksJob->getCombinedFile());
        self::assertEquals($dataModel['checkSum'], $combineChunksJob->getCheckSum());
        self::assertEquals($dataModel['chunkDirectory'], $combineChunksJob->getChunkDirectory());
        self::assertFalse($combineChunksJob->shouldCreateThumbnails());

        //Test serialize
        self::assertJsonStringEqualsJsonString($jsonModel, json_encode($combineChunksJob));
    }
}
