<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video\Aspect;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video\CodecOptionsX264;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video\ConstantRateFactor;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video\Deinterlace;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video\FrameRate;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video\Pass;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video\Preset;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video\Profile;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video\Scale;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video\ScoreTrim;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video\VideoCodec;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video\VideoQuality;
use QBNK\JobQueue\Job\Convert\Model\TargetAbstract;
use QBNK\JobQueue\Job\Convert\Model\VideoConvertTarget;
use QBNK\JobQueue\Job\Storage\File;

class VideoTargetTest extends TestCase
{

    function testSerialization()
    {
        $dataModel = [
            'templateId' => 1,
            'class' => VideoConvertTarget::class,
            'identifier' => 'TestTarget MP4',
            'target' => new File(
                '/dummydir/dummyoutputfile.mp4',
                'video/mp4',
                'video',
                ['keywords' => ['Test', 'Dummy']]
            ),
            'commands' => [
                [
                    'aspect' => Aspect::ASPECT_STANDARD,
                    'class' => 'Aspect'
                ],
                [
                    'keyInt' => 1,
                    'minKeyInt' => 1,
                    'sceneCut' => 1,
                    'bFrames' => 1,
                    'aqMode' => 1,
                    'weightB' => true,
                    'noCabac' => true,
                    'class' => 'CodecOptionsX264'
                ],
                [
                    'crf' => 35,
                    'class' => 'ConstantRateFactor'
                ],
                [
                    'mode' => Deinterlace::MODE_SENDFIELD,
                    'parity' => Deinterlace::PARITY_BOTTOMFIELDFIRST,
                    'force' => true,
                    'class' => 'Deinterlace'
                ],
                [
                    'framerate' => 24,
                    'class' => 'FrameRate'
                ],
                [
                    'pass' => 123,
                    'class' => 'Pass'
                ],
                [
                    'preset' => Preset::PRESET_FASTER,
                    'class' => 'Preset'
                ],
                [
                    'profile' => Profile::PROFILE_HIGH,
                    'level' => Profile::LEVEL_31,
                    'class' => 'Profile'
                ],
                [
                    'width' => 720,
                    'height' => 480,
                    'scalingOption' => Scale::OPTION_FASTBILINEAR,
                    'class' => 'Scale'
                ],
                [
                    'width' => 720,
                    'height' => 480,
                    'originalLength' => 1337,
                    'class' => 'ScoreTrim'
                ],
                [
                    'codec' => VideoCodec::CODEC_LIBX264,
                    'class' => 'VideoCodec'
                ],
                [
                    'quality' => 80,
                    'class' => 'VideoQuality'
                ],
            ]
        ];

        $jsonModel = json_encode($dataModel);

        /** @var VideoConvertTarget $videoTarget */
        $videoTarget = TargetAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(VideoConvertTarget::class, $videoTarget);

        $this->assertInstanceOf(Aspect::class, $videoTarget->getCommands()[0]);
        $this->assertEquals($dataModel['commands'][0]['aspect'], $videoTarget->getCommands()[0]->getAspect());
        $this->assertInstanceOf(CodecOptionsX264::class, $videoTarget->getCommands()[1]);
        $this->assertInstanceOf(ConstantRateFactor::class, $videoTarget->getCommands()[2]);
        $this->assertEquals($dataModel['commands'][2]['crf'], $videoTarget->getCommands()[2]->getCrf());
        $this->assertInstanceOf(Deinterlace::class, $videoTarget->getCommands()[3]);
        $this->assertInstanceOf(FrameRate::class, $videoTarget->getCommands()[4]);
        $this->assertEquals($dataModel['commands'][4]['framerate'], $videoTarget->getCommands()[4]->getFramerate());
        $this->assertInstanceOf(Pass::class, $videoTarget->getCommands()[5]);
        $this->assertInstanceOf(Preset::class, $videoTarget->getCommands()[6]);
        $this->assertInstanceOf(Profile::class, $videoTarget->getCommands()[7]);
        $this->assertEquals($dataModel['commands'][7]['profile'], $videoTarget->getCommands()[7]->getProfile());
        $this->assertInstanceOf(Scale::class, $videoTarget->getCommands()[8]);
        $this->assertInstanceOf(ScoreTrim::class, $videoTarget->getCommands()[9]);
        $this->assertEquals(
            $dataModel['commands'][9]['originalLength'],
            $videoTarget->getCommands()[9]->getOriginalLength()
        );
        $this->assertInstanceOf(VideoCodec::class, $videoTarget->getCommands()[10]);
        $this->assertInstanceOf(VideoQuality::class, $videoTarget->getCommands()[11]);
        $this->assertEquals($dataModel['commands'][11]['quality'], $videoTarget->getCommands()[11]->getQuality());

        //Test serialize
        $this->assertEquals($jsonModel, json_encode($videoTarget));
    }
}
