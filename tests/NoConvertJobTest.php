<?php
declare(strict_types=1);

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Convert\Model\NoConvertTarget;
use QBNK\JobQueue\Job\Convert\NoConvertJob;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\Storage\File;

class NoConvertJobTest extends TestCase
{

    function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => NoConvertJob::class,
            'queueName' => 'imageconvert',
            'source' => [
                'source' => '/dummydir/dummyinputfile.tmp',
                'mimeType' => 'test/tmp',
                'classification' => 'Document',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],
            'removeSource' => false,
            'targets' => [
				'TestTarget' => [
                    'class' => NoConvertTarget::class,
                    'identifier' => 'TestTarget',
                    'target' => new File(
                        '/dummydir/dummyoutputfile.tmp',
                        'test/tmp',
                        'Document',
                        ['keywords' => ['Test', 'Dummy']]
                    ),
					'templateId' => null, //Not used by NoConvert but required to match json
					'commands' => [] //Not used by NoConvert but required to match json
                ]
            ]
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var NoConvertJob $noConvertJob */
        $noConvertJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(NoConvertJob::class, $noConvertJob);

        //Some spot tests
        $this->assertEquals($dataModel['queueName'], $noConvertJob->getQueueName());
        $this->assertEquals($dataModel['source']['source'], $noConvertJob->getSource()->getPathname());
        $this->assertArrayHasKey('TestTarget', $noConvertJob->getTargets());
        $this->assertInstanceOf(NoConvertTarget::class, $noConvertJob->getTargets()['TestTarget']);

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($noConvertJob));
    }
}
