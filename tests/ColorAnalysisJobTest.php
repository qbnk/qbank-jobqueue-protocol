<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Examination\ColorAnalysisJob;
use QBNK\JobQueue\Job\JobAbstract;

class ColorAnalysisJobTest extends TestCase
{

    function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => ColorAnalysisJob::class,
            'queueName' => 'coloranalysis',
            'source' => [
                'source' => '/dummydir/dummyinputfile.tmp',
                'mimeType' => 'test/tmp',
                'classification' => 'Document',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],
            'colorData' => ['mixed' => ['foo', 'bar']]
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var ColorAnalysisJob $colorAnalysisJob */
        $colorAnalysisJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(ColorAnalysisJob::class, $colorAnalysisJob);

        $this->assertEquals($dataModel['customerDomain'], $colorAnalysisJob->getCustomer());
        $this->assertEquals($dataModel['priority'], $colorAnalysisJob->getPriority());
        $this->assertArrayHasKey(
            key($dataModel['source']['properties']),
            $colorAnalysisJob->getSource()->getProperties()
        );
        $this->assertArrayHasKey(key($dataModel['colorData']), $colorAnalysisJob->getColorData());

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($colorAnalysisJob));
    }
}
