<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Convert\Command\Image\Resize;
use QBNK\JobQueue\Job\Convert\Command\Image\ModifySVGColor;
use QBNK\JobQueue\Job\Convert\ImageConvertJob;
use QBNK\JobQueue\Job\Convert\Model\ImageConvertTarget;
use QBNK\JobQueue\Job\Enums\JobStatus;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\Storage\File;

class ImageConvertJobTest extends TestCase
{
    public static function getCommands(?array $commands): array
    {
        $defaultCommand = [[
            'width' => 500,
            'height' => 400,
            'proportional' => true,
            'scaleUp' => false,
            'allowOverflow' => false,
            'class' => 'Resize'
        ]];
        return $commands ?? $defaultCommand;
    }

    public static function getJobModel(string $targetIdentifier, ?array $commands = null): array
    {
        return TestUtility::getJobAbstractTestParams([
            'class' => ImageConvertJob::class,
            'queueName' => 'imageconvert',
            'source' => [
                'source' => '/dummydir/dummyinputfile.tmp',
                'mimeType' => 'test/tmp',
                'classification' => 'Document',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],
            'removeSource' => false,
            'persistProtocol' => null,
            'sourceIsVector' => true,
            'targets' => [
                [ //Test array index fix
                    'templateId' => 1,
                    'class' => ImageConvertTarget::class,
                    'identifier' => $targetIdentifier,
                    'target' => new File(
                        '/dummydir/dummyoutputfile.tmp',
                        'test/tmp',
                        'Document',
                        ['keywords' => ['Test', 'Dummy']]
                    ),
                    'commands' => self::getCommands($commands),
                ],
                [ //Test no-identifier fix
                    'templateId' => 1,
                    'class' => ImageConvertTarget::class,
                    'target' => new File(
                        '/dummydir/dummyoutputfile.tmp'
                    ),
                    'commands' => []
                ],
            ],
            'convertedImages' => [
                $targetIdentifier => new File(
                    '/dummydir/dummyoutputfile.tmp',
                    'test/tmp',
                    'Document',
                    ['keywords' => ['Test', 'Dummy']]
                )
            ]
        ]);
    }

    function testModifySVGColorReplaceAll()
    {
        $targetIdentifier = 'TestSVGReplaceAll';
        $commands = [
            [
                'class' => 'ModifySVGColor',
                'svgColor' => '', // Search for all colors
                'svgReplaceColor' => '#123456',
                'svgIgnoreColor' => false // Only works for positive search
            ],
        ];
        $svgContent = <<<EOL
            <style type="text/css">
                .st2{fill: #0000ff;stroke:#B3B3B3;stroke-miterlimit:10;}
            </style>
            <svg width="200" height ="200" viewBox ="-100 -100 200 200">
                <circle cx="0" cy="50" r="40" fill="#000000" />
                <rect x="17.5" y="-65" width="35" height="20" fill="#00ff00" />
                <line x1="25" y1="5" x2="1" y2="0" stroke="#faa" stroke-width="5" />
            </svg>
        EOL;
        $svgModifiedContent = <<<EOL
            <style type="text/css">
                .st2{fill: #123456;stroke:#123456;stroke-miterlimit:10;}
            </style>
            <svg width="200" height ="200" viewBox ="-100 -100 200 200">
                <circle cx="0" cy="50" r="40" fill="#123456" />
                <rect x="17.5" y="-65" width="35" height="20" fill="#123456" />
                <line x1="25" y1="5" x2="1" y2="0" stroke="#123456" stroke-width="5" />
            </svg>
        EOL;

        $dataModel = self::getJobModel($targetIdentifier, $commands);
        $jsonModel = json_encode($dataModel);

        /** @var ImageConvertJob $imageConvertJob */
        $imageConvertJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(ImageConvertJob::class, $imageConvertJob);

        //Some spot tests
        $this->assertEquals($dataModel['queueName'], $imageConvertJob->getQueueName());
        $this->assertEquals($dataModel['source']['source'], $imageConvertJob->getSource()->getPathname());

        $targetIdentifiers = array_flip(array_keys($imageConvertJob->getTargets()));
        $automaticIdentifier = key($targetIdentifiers);
        $command = $imageConvertJob->getTargets()[$targetIdentifier]->getCommands()[0];

        $this->assertInstanceOf(ImageConvertTarget::class, $imageConvertJob->getTargets()[$targetIdentifier]);
        $this->assertInstanceOf(ImageConvertTarget::class, $imageConvertJob->getTargets()[$automaticIdentifier]);
        $this->assertEquals($automaticIdentifier, $imageConvertJob->getTargets()[$automaticIdentifier]->getIdentifier());
        $this->assertInstanceOf(ModifySVGColor::class, $command);
        $this->assertEquals($svgModifiedContent, $command->modifySource($svgContent));
    }

    function testModifySVGColor()
    {
        $targetIdentifier = 'TestSVG';
        $commands = [
            [
                'class' => 'ModifySVGColor',
                'svgColor' => '#3aa,#faa',
                'svgReplaceColor' => '#aaa',
                'svgIgnoreColor' => true
            ],
            [
                'class' => 'ModifySVGColor',
                'svgColor' => '#3aa',
                'svgReplaceColor' => '#00ff00',
                'svgIgnoreColor' => false
            ]
        ];
        $svgContent = <<<EOL
            <style type="text/css">
                .st2{fill:#3aa;stroke : #000;stroke-miterlimit:10;}
            </style>
            <svg width="200" height ="200" viewBox ="-100 -100 200 200">
                <circle cx="0" cy="20" r="80" fill="#000000" />
                <rect x="-17.5" y="-65" width="35" height="20" fill="#00ff00" />
                <polygon points="0,-40 60,60 -60,60" fill="#0000ff" stroke="#3aa" stroke-width="5" />
                <line x1="25" y1="5" x2="1" y2="0" stroke="#faa" stroke-width="5" />
            </svg>
        EOL;
        $svgModifiedStep1 = <<<EOL
            <style type="text/css">
                .st2{fill:#3aa;stroke : #aaa;stroke-miterlimit:10;}
            </style>
            <svg width="200" height ="200" viewBox ="-100 -100 200 200">
                <circle cx="0" cy="20" r="80" fill="#aaa" />
                <rect x="-17.5" y="-65" width="35" height="20" fill="#aaa" />
                <polygon points="0,-40 60,60 -60,60" fill="#aaa" stroke="#3aa" stroke-width="5" />
                <line x1="25" y1="5" x2="1" y2="0" stroke="#faa" stroke-width="5" />
            </svg>
        EOL;
        $svgModifiedStep2 = <<<EOL
            <style type="text/css">
                .st2{fill:#00ff00;stroke : #aaa;stroke-miterlimit:10;}
            </style>
            <svg width="200" height ="200" viewBox ="-100 -100 200 200">
                <circle cx="0" cy="20" r="80" fill="#aaa" />
                <rect x="-17.5" y="-65" width="35" height="20" fill="#aaa" />
                <polygon points="0,-40 60,60 -60,60" fill="#aaa" stroke="#00ff00" stroke-width="5" />
                <line x1="25" y1="5" x2="1" y2="0" stroke="#faa" stroke-width="5" />
            </svg>
        EOL;

        $dataModel = self::getJobModel($targetIdentifier, $commands);
        $jsonModel = json_encode($dataModel);

        /** @var ImageConvertJob $imageConvertJob */
        $imageConvertJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(ImageConvertJob::class, $imageConvertJob);

        //Some spot tests
        $this->assertEquals($dataModel['queueName'], $imageConvertJob->getQueueName());
        $this->assertEquals($dataModel['source']['source'], $imageConvertJob->getSource()->getPathname());

        $targetIdentifiers = array_flip(array_keys($imageConvertJob->getTargets()));
        $automaticIdentifier = key($targetIdentifiers);
        $command1 = $imageConvertJob->getTargets()[$targetIdentifier]->getCommands()[0];
        $command2 = $imageConvertJob->getTargets()[$targetIdentifier]->getCommands()[1];

        $this->assertInstanceOf(ImageConvertTarget::class, $imageConvertJob->getTargets()[$targetIdentifier]);
        $this->assertInstanceOf(ImageConvertTarget::class, $imageConvertJob->getTargets()[$automaticIdentifier]);
        $this->assertEquals($automaticIdentifier, $imageConvertJob->getTargets()[$automaticIdentifier]->getIdentifier());
        $this->assertInstanceOf(ModifySVGColor::class, $command1);
        $this->assertInstanceOf(ModifySVGColor::class, $command2);
        $this->assertEquals($svgModifiedStep1, $command1->modifySource($svgContent));
        $this->assertEquals($svgModifiedStep2, $command2->modifySource($svgModifiedStep1));
    }

    function testSerialization()
    {
        $targetIdentifier = 'TestTarget';
        $dataModel = self::getJobModel($targetIdentifier);

        $jsonModel = json_encode($dataModel);

        /** @var ImageConvertJob $imageConvertJob */
        $imageConvertJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(ImageConvertJob::class, $imageConvertJob);

        //Some spot tests
        $this->assertEquals($dataModel['queueName'], $imageConvertJob->getQueueName());
        $this->assertEquals($dataModel['source']['source'], $imageConvertJob->getSource()->getPathname());

        $targetIdentifiers = array_flip(array_keys($imageConvertJob->getTargets()));
        $this->assertArrayHasKey($targetIdentifier, $targetIdentifiers);
        unset($targetIdentifiers[$targetIdentifier]);
        $automaticIdentifier = key($targetIdentifiers);
        $this->assertStringStartsWith('target-', $automaticIdentifier);

        $this->assertInstanceOf(ImageConvertTarget::class, $imageConvertJob->getTargets()[$targetIdentifier]);
        $this->assertInstanceOf(ImageConvertTarget::class, $imageConvertJob->getTargets()[$automaticIdentifier]);
        $this->assertEquals($automaticIdentifier, $imageConvertJob->getTargets()[$automaticIdentifier]->getIdentifier());
        $this->assertInstanceOf(Resize::class, $imageConvertJob->getTargets()[$targetIdentifier]->getCommands()[0]);
        $this->assertEquals(
            $dataModel['targets'][0]['commands'][0]['width'],
            $imageConvertJob->getTargets()[$targetIdentifier]->getCommands()[0]->getWidth()
        );
        $this->assertArrayHasKey(key($dataModel['convertedImages']), $imageConvertJob->getConvertedImages());
        $this->assertInstanceOf(File::class, current($imageConvertJob->getConvertedImages()));

        //Test serialize, handle target array being indexed by setter
        $dataModel['targets'][1]['identifier'] = $automaticIdentifier;
        $dataModel['targets'] = [
            $targetIdentifier => $dataModel['targets'][0],
            $automaticIdentifier => $dataModel['targets'][1]
        ];
        unset($dataModel['targets'][0]);
        $this->assertJsonStringEqualsJsonString(json_encode($dataModel), json_encode($imageConvertJob));
    }

    public function testReset()
    {
        $job = (ImageConvertJob::fromArray($this->getJobModel('foo')))
            ->setStatus(JobStatus::Success)
            ->setBlockingJobIds(['foo', 'bar'])
            ->setMessage('baz');

        $reset = $job->reset();

        $this->assertNotEmpty($job->getCallbackQueue());
        $this->assertNotEmpty($job->getBlockingJobIds());
        $this->assertEquals(JobStatus::Success, $job->getStatus());
        $this->assertEquals('baz', $job->getMessage());
        $this->assertNotEmpty($job->getConvertedImages());

        $this->assertInstanceOf(ImageConvertJob::class, $reset);
        $this->assertIsString($reset->getId());
        $this->assertNotEquals($job->getId(), $reset->getId());
        $this->assertEquals(JobStatus::New, $reset->getStatus());
        $this->assertEmpty($reset->getCallbackQueue());
        $this->assertEmpty($reset->getBlockingJobIds());
        $this->assertEmpty($reset->getMessage());
        $this->assertEmpty($reset->getConvertedImages());
    }
}
