<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Convert\DocumentConvertJob;
use QBNK\JobQueue\Job\Convert\Model\DocumentConvertTarget;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\Storage\File;

class DocumentConvertJobTest extends TestCase
{

    public static function getJobModel(): array
    {
        return TestUtility::getJobAbstractTestParams([
            'class' => DocumentConvertJob::class,
            'queueName' => 'documentconvert',
            'source' => [
                'source' => '/dummydir/dummyinputfile.tmp',
                'mimeType' => 'test/tmp',
                'classification' => 'Document',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],
            'removeSource' => false,
            'persistProtocol' => null,
            'targets' => [
                'TestTarget' => [
                    'templateId' => null,
                    'class' => DocumentConvertTarget::class,
                    'identifier' => 'TestTarget',
                    'target' => new File(
                        '/dummydir/dummyoutputfile.tmp',
                        'test/tmp',
                        'Document',
                        ['keywords' => ['Test', 'Dummy']]
                    ),
                    'commands' => []
                ]
            ],
            'convertedDocuments' => [
                'TestTarget' => new File(
                    '/dummydir/dummyoutputfile.tmp',
                    'test/tmp',
                    'Document',
                    ['keywords' => ['Test', 'Dummy']]
                )
            ]
        ]);
    }

    function testSerialization()
    {
        $dataModel = self::getJobModel();

        $jsonModel = json_encode($dataModel);

        /** @var DocumentConvertJob $documentConvertJob */
        $documentConvertJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(DocumentConvertJob::class, $documentConvertJob);

        //Some spot tests
        $this->assertEquals($dataModel['queueName'], $documentConvertJob->getQueueName());
        $this->assertInstanceOf(DocumentConvertTarget::class, $documentConvertJob->getTargets()['TestTarget']);
        $this->assertInstanceOf(File::class, current($documentConvertJob->getConvertedDocuments()));

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($documentConvertJob));
    }
}
