<?php


namespace QBNK\JobQueue\Job\Test;


use Mockery;
use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Examination\MimeTypeJob;
use QBNK\JobQueue\Job\JobAbstract;

class JobAbstractTest extends TestCase
{

    public function testId()
    {
        /** @var JobAbstract|Mockery\MockInterface $abstract */
        $abstract = Mockery::mock(JobAbstract::class)->makePartial();
        $abstract->__construct();
        $this->assertInstanceOf(JobAbstract::class, $abstract);
        $this->assertNotEmpty($abstract->getId());
        $this->assertIsString($abstract->getId());

        $abstract->shouldReceive('getQueueName')->andReturn('test')
        ;
        $abstract->shouldReceive('getUserId')->andReturn(123)
        ;
        $abstract->setCustomer('QBNK');
        $jsonData = (array)$abstract->jsonSerialize();
        $this->assertArrayHasKey('id', $jsonData);
        $this->assertEquals($abstract->getId(), $jsonData['id']);

        $jsonData['class'] = MimeTypeJob::class;
        $deserialized = JobAbstract::fromArray($jsonData);
        $this->assertInstanceOf(JobAbstract::class, $deserialized);
        $this->assertEquals($abstract->getId(), $deserialized->getId());
    }

    public function testAssignmentWithoutId()
    {
        $jobConstructedWithoutId = JobAbstract::fromArray(['class' => MimeTypeJob::class]);
        $this->expectException(\Error::class);
        $jobConstructedWithoutId->getId();
    }

    public function testAssignmentWithId()
    {
        $jobConstructedWithId = JobAbstract::fromArray(['class' => MimeTypeJob::class, 'id' => 'test']);
        $this->assertEquals('test', $jobConstructedWithId->getId());

        $jobAssignedId = JobAbstract::fromArray(['class' => MimeTypeJob::class], true);
        $this->assertIsString($jobAssignedId->getId());
    }
}
