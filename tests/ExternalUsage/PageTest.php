<?php

namespace QBNK\JobQueue\Job\Test\ExternalUsage;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Examination\Model\ExternalUsage\Image;
use QBNK\JobQueue\Job\Examination\Model\ExternalUsage\Page;

class PageTest extends TestCase
{

    /** @var Page */
    protected $page;

    public function setUp(): void
    {
        $this->page = (new Page())
            ->setUrl('https://example.com')
            ->setTitle('Test')
            ->setImages([
                (new Image)
                    ->setUrl('https://example.com/image.jpg')
                    ->setType(Image::TYPE_FULL_MATCH)
            ]);
    }

    public function testJsonSerialize()
    {
        $json = json_encode($this->page);
        self::assertNotEmpty($json);
        self::assertIsString($json);
        self::assertJson($json);
    }

    /**
     * @depends testJsonSerialize
     */
    public function testFromArray()
    {
        $array = (array)$this->page->jsonSerialize();
        self::assertArrayHasKey('url', $array);
        self::assertEquals($this->page->getUrl(), $array['url']);
        self::assertArrayHasKey('title', $array);
        self::assertEquals($this->page->getTitle(), $array['title']);
        self::assertArrayHasKey('images', $array);
        self::assertIsArray($array['images']);
    }
}
