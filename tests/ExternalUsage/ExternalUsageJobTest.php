<?php

namespace QBNK\JobQueue\Job\Test\ExternalUsage;

use Generator;
use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Examination\ExternalUsageJob;
use QBNK\JobQueue\Job\Examination\Model\ExternalUsage\ExternalUsageAbstract;
use QBNK\JobQueue\Job\Examination\Model\ExternalUsage\Image;
use QBNK\JobQueue\Job\Examination\Model\ExternalUsage\Page;

class ExternalUsageJobTest extends TestCase
{

    public function testGetQueueName()
    {
        $job = new ExternalUsageJob();
        self::assertEquals('externalusage', $job->getQueueName());
    }

    public function testGetExternalUseResult()
    {
        $job = (new ExternalUsageJob())
            ->setAnalysisData(json_decode(file_get_contents(__DIR__ . '/response.json'), true));
        $externalUses = $job->getExternalUseResult();
        self::assertInstanceOf(Generator::class, $externalUses);
        foreach ($externalUses as $externalUse) {
            self::assertInstanceOf(ExternalUsageAbstract::class, $externalUse);
            self::assertContains(get_class($externalUse), [Page::class, Image::class]);
            self::assertIsString($externalUse->getUrl());
            self::assertNotEmpty($externalUse->getUrl());
            if ($externalUse instanceof Page) {
                self::assertIsString($externalUse->getTitle());
                self::assertNotEmpty($externalUse->getTitle());
                self::assertIsArray($externalUse->getImages());
                self::assertContainsOnly(Image::class, $externalUse->getImages());
            }
            if ($externalUse instanceof Image) {
                self::assertIsString($externalUse->getType());
                self::assertNotEmpty($externalUse->getType());
                self::assertContains(
                    $externalUse->getType(),
                    [
                        Image::TYPE_FULL_MATCH,
                        Image::TYPE_PARTIAL_MATCH,
                        Image::TYPE_VISUALLY_SIMILAR
                    ]
                );
            }
        }
        // Did we generate all pages and images?
        self::assertCount(38, $job->getExternalUseResult(), false);
    }
}
