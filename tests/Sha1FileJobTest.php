<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Examination\Sha1FileJob;
use QBNK\JobQueue\Job\JobAbstract;

class Sha1FileJobTest extends TestCase
{

    function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => Sha1FileJob::class,
            'queueName' => 'sha1file',
            'source' => [
                'source' => '/dummydir/dummyinputfile.tmp',
                'mimeType' => 'test/tmp',
                'classification' => 'Document',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],

            'sha1sum' => '12345678asdfghjk',
            'calculateDuplicates' => true
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var Sha1FileJob $sha1fileJob */
        $sha1fileJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(Sha1FileJob::class, $sha1fileJob);

        $this->assertEquals($dataModel['sha1sum'], $sha1fileJob->getSha1sum());
        $this->assertTrue($sha1fileJob->isCalculateDuplicates());

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($sha1fileJob));
    }
}
