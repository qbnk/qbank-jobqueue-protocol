<?php

namespace QBNK\JobQueue\Job\Test;

use QBNK\JobQueue\Job\Enums\DataProcessingRegion;
use QBNK\JobQueue\Job\Enums\JobPriority;
use QBNK\JobQueue\Job\Enums\JobStatus;
use QBNK\JobQueue\Job\JobAbstract;

class TestUtility
{

    public static function getJobAbstractTestParams(array $specifics = []): array
    {
        return array_merge([
            'id' => 'dummyId',
            'sessionId' => 1,
            'userId' => 123,
            'priority' => JobPriority::High,
            'status' => JobStatus::Running,
            'message' => 'Dummy message',
            'customerDomain' => 'test.customer.test',
            'storageConfig' => null,
            'callbackQueue' => JobAbstract::DEFAULT_CALLBACK_QUEUE,
            'callbackHandler' => 'dummy',
            'callbackParameters' => ['foo' => 'baz'],
            'delayedPrepareHandler' => 'dummy2',
            'blockingJobIds' => ['foo', 'bar'],
            'triggeredByJobId' => '1a2b3c',
            'dataProcessingRegion' => DataProcessingRegion::Global
        ], $specifics);
    }
}
