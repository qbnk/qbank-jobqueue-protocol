<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Convert\PdfExtractPagesJob;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\Storage\File;

class PdfExtractPagesJobTest extends TestCase
{

    function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => PdfExtractPagesJob::class,
            'queueName' => 'pdfextractpages',
            'source' => [
                'source' => '/dummydir/dummyinputfile.tmp',
                'mimeType' => 'test/tmp',
                'classification' => 'Document',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],
            'removeSource' => false,
            'targetExtension' => 'bar',
            'page' => 5,
            'width' => 300,
            'height' => 500,
            'outputPages' => [
                new File('/dummydir/foo-1.bar', 'image/jpg', 'image', ['keywords' => ['Test', 'Dummy']])
            ]
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var PdfExtractPagesJob $pdfExtractPagesJob */
        $pdfExtractPagesJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(PdfExtractPagesJob::class, $pdfExtractPagesJob);

        $this->assertEquals($dataModel['targetExtension'], $pdfExtractPagesJob->getTargetExtension());
        $this->assertEquals($dataModel['page'], $pdfExtractPagesJob->getPage());
        $this->assertInstanceOf(File::class, $pdfExtractPagesJob->getOutputPages()[0]);

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($pdfExtractPagesJob));
    }
}
