<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Examination\MimeTypeJob;
use QBNK\JobQueue\Job\JobAbstract;

class MimeTypeJobTest extends TestCase
{

    public static function getDataModel()
    {
        return TestUtility::getJobAbstractTestParams([
            'class' => MimeTypeJob::class,
            'queueName' => 'mimetype',
            'source' => [
                'source' => '/dummydir/dummyinputfile.tmp',
                'mimeType' => 'test/tmp',
                'classification' => 'Document',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],

            'extractedMimeType' => 'foo/bar',
            'extractedDescription' => 'faz baz'
        ]);
    }

    function testSerialization()
    {
        $dataModel = self::getDataModel();

        $jsonModel = json_encode($dataModel);

        /** @var MimeTypeJob $mimeTypeJob */
        $mimeTypeJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(MimeTypeJob::class, $mimeTypeJob);

        $this->assertEquals($dataModel['extractedMimeType'], $mimeTypeJob->getExtractedMimeType());
        $this->assertEquals($dataModel['extractedDescription'], $mimeTypeJob->getExtractedDescription());

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($mimeTypeJob));
    }
}
