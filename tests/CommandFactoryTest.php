<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Convert\Command\CommandAbstract;
use QBNK\JobQueue\Job\Convert\Command\CommandFactory;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Audio\AudioCodec;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video\Profile;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video\VideoCodec;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video\VideoQuality;
use QBNK\JobQueue\Job\Convert\Command\Image\AutoOrient;
use QBNK\JobQueue\Job\Convert\Command\Image\Background;
use QBNK\JobQueue\Job\Convert\Command\Image\Crop;
use ReflectionClass;

class CommandFactoryTest extends TestCase
{

    /** @var CommandFactory */
    private $commandFactory;

    public function setUp(): void
    {
        parent::setUp();
        $this->commandFactory = new CommandFactory();
    }

    public function testFactory(): void
    {
        /** @var CommandAbstract[] $commands */
        $commands = [
            new AutoOrient(),
            new Background('#000'),
            new Crop(123, 321, 100, 100),
            new VideoQuality(100),
            new Profile(Profile::PROFILE_MAIN, Profile::LEVEL_30),
            new AudioCodec(AudioCodec::AAC),
            new VideoCodec(VideoCodec::CODEC_LIBX264)
        ];

        $asserts = 0;
        foreach ($commands as $command) {
            $factoryOutput = $this->commandFactory->fromArray((array)$command->jsonSerialize());

            if ($factoryOutput instanceof Background) {
                self::assertEquals('#000', $factoryOutput->getBackground());
                $asserts++;
            } elseif ($factoryOutput instanceof Crop) {
                self::assertEquals(321, $factoryOutput->getY());
                $asserts++;
            } elseif ($factoryOutput instanceof VideoQuality) {
                self::assertEquals(100, $factoryOutput->getQuality());
                $asserts++;
            } elseif ($factoryOutput instanceof AudioCodec) {
                self::assertEquals(AudioCodec::AAC, $factoryOutput->getCodec());
                $asserts++;
            } elseif ($factoryOutput instanceof VideoCodec) {
                self::assertEquals(VideoCodec::CODEC_LIBX264, $factoryOutput->getCodec());
                $asserts++;
            }
        }

        self::assertEquals(5, $asserts);
    }

    public function testGetDirectories(): void
    {
        $class = new ReflectionClass(CommandFactory::class);
        $method = $class->getMethod('getDirectories');
        $method->setAccessible(true);
        $directories = $method->invoke($this->commandFactory);
        self::assertNotEmpty($directories);
    }
}
