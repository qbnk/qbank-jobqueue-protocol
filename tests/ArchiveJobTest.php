<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Convert\ArchiveJob;
use QBNK\JobQueue\Job\Convert\DocumentConvertJob;
use QBNK\JobQueue\Job\Convert\FFmpegJob;
use QBNK\JobQueue\Job\Convert\ImageConvertJob;
use QBNK\JobQueue\Job\Examination\ArchiveListJob;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\JobQueueException;
use QBNK\JobQueue\Job\Storage\File;

class ArchiveJobTest extends TestCase
{

    public function testSerialization(): void
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => ArchiveJob::class,
            'queueName' => 'archive',
            'format' => ArchiveListJob::FORMAT_TAR,
            'sourceFiles' => [
                [
                    'source' => '/dummydir/dummyfile.jpg',
                    'mimeType' => 'test/tmp',
                    'classification' => 'Document',
                    'properties' => ['keywords' => ['Test', 'Dummy']]
                ],
                new File('/dummydir/dummyfile2.jpg')
            ],
            'convertJobs' => [
                ImageConvertJobTest::getJobModel('foo'),
                ImageConvertJob::fromArray(ImageConvertJobTest::getJobModel('bar')),
                DocumentConvertJobTest::getJobModel(),
                FFmpegJobTest::getJobModel()
            ],

            'archive' => new File('/dummydir/dummyoutput.zip')
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var ArchiveJob $archiveJob */
        $archiveJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test construct
        $this->assertInstanceOf(ArchiveJob::class, $archiveJob);

        //Test reconstruct
        $this->assertInstanceOf(
            ArchiveJob::class,
            JobAbstract::fromArray(json_decode(json_encode($archiveJob), true))
        );

        $this->assertEquals($dataModel['format'], $archiveJob->getFormat());

        $this->assertCount(count($dataModel['sourceFiles']), $archiveJob->getSourceFiles());
        foreach ($archiveJob->getSourceFiles() as $sourceFile) {
            $this->assertInstanceOf(File::class, $sourceFile);
        }
        $this->assertCount(count($dataModel['convertJobs']), $archiveJob->getConvertJobs());
        $this->assertInstanceOf(ImageConvertJob::class, $archiveJob->getConvertJobs()[0]);
        $this->assertInstanceOf(ImageConvertJob::class, $archiveJob->getConvertJobs()[1]);
        $this->assertInstanceOf(DocumentConvertJob::class, $archiveJob->getConvertJobs()[2]);
        $this->assertInstanceOf(FFmpegJob::class, $archiveJob->getConvertJobs()[3]);
    }

    public function testCannotPassNonConvertJob(): void
    {
        $archiveJob = new ArchiveJob();
        $this->expectException(JobQueueException::class);
        $archiveJob->setConvertJobs([new ArchiveJob()]);
    }
}
