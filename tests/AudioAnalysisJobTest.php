<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Examination\AudioAnalysisJob;
use QBNK\JobQueue\Job\JobAbstract;

class AudioAnalysisJobTest extends TestCase
{

    function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => AudioAnalysisJob::class,
            'queueName' => 'audioanalysis',
            'source' => [
                'source' => '/dummydir/dummyinputfile.mp4',
                'mimeType' => 'audio/mp4',
                'classification' => 'audio',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],

            'analysisData' => [
                'audioTranscripts' => ['faz'],
                'audioWords' => ['bar', 'baz'],
            ],
            'selectedLanguage' => 'en-US'
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var AudioAnalysisJob $audioAnalysisJob */
        $audioAnalysisJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        self::assertInstanceOf(AudioAnalysisJob::class, $audioAnalysisJob);
        self::assertEquals(
            $dataModel['analysisData']['audioWords'],
            $audioAnalysisJob->getAnalysisData()->getAudioWords()
        );

        //Test serialize
        self::assertJsonStringEqualsJsonString($jsonModel, json_encode($audioAnalysisJob));
    }
}
