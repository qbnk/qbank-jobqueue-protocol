<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video\Scale;
use QBNK\JobQueue\Job\Convert\Model\DocumentConvertTarget;
use QBNK\JobQueue\Job\Convert\Model\ImageConvertTarget;
use QBNK\JobQueue\Job\Convert\Model\VideoConvertTarget;
use QBNK\JobQueue\Job\Integration\DeployJob;
use QBNK\JobQueue\Job\Integration\Model\IntegratedFile;
use QBNK\JobQueue\Job\Integration\Model\SubtitleFile;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\Storage\File;

class IntegrationDeployJobTest extends TestCase
{

    function testSerialization()
    {
        $mediaId = 123;
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => DeployJob::class,
            'queueName' => 'integration_deploy_job',
            'integrationId' => 1337,
            'authData' => '{"json":["foo","bar"]}',
            'jobParameters' => ['faz', 'baz'],
            'subChannels' => [['id' => 1338, 'name' => 'test', 'definition' => ['externalId' => '123ABC']]],
            'publishProtocol' => null,
            'mediaUsages' => [],
            'sourceFiles' => [
                $mediaId => new File(
                    '/dummydir/dummyinput.jpg',
                    'image/jpeg',
                    'image',
                    ['keywords' => ['Test', 'Dummy']]
                ),
                124 => new File('/dummydir/dummyinput.mp4', 'video/mp4', 'video', ['keywords' => ['Test', 'Dummy']])
            ],
            'convertTargets' => [
                [
                    'templateId' => 1,
                    'class' => ImageConvertTarget::class,
                    'identifier' => 'TestTarget',
                    'target' => new File(
                        '/dummydir/dummyoutputfile.tmp',
                        'test/tmp',
                        'Document',
                        ['keywords' => ['Test', 'Dummy']]
                    ),
                    'commands' => [
                        [
                            'width' => 500,
                            'height' => 400,
                            'proportional' => true,
                            'scaleUp' => false,
                            'allowOverflow' => false,
                            'class' => 'Resize'
                        ]
                    ]
                ],
                [
                    'templateId' => 2,
                    'class' => VideoConvertTarget::class,
                    'identifier' => 'TestTarget MP4',
                    'target' => new File(
                        '/dummydir/dummyoutputfile.tmp',
                        'test/tmp',
                        'Document',
                        ['keywords' => ['Test', 'Dummy']]
                    ),
                    'commands' => [
                        [
                            'quality' => 80,
                            'class' => 'VideoQuality'
                        ],
                        [
                            'width' => 720,
                            'height' => 480,
                            'scalingOption' => Scale::OPTION_FASTBILINEAR,
                            'class' => 'Scale'
                        ]
                    ]
                ],
                [
                    'templateId' => null,
                    'class' => DocumentConvertTarget::class,
                    'identifier' => 'TestTarget',
                    'target' => new File(
                        '/dummydir/dummyoutputfile.tmp',
                        'test/tmp',
                        'Document',
                        ['keywords' => ['Test', 'Dummy']]
                    ),
                    'commands' => []
                ]
            ],
            'integratedFiles' => [
                $mediaId => [
                    ['success' => false, 'message' => 'Test false', 'payload' => []],
                    ['success' => true, 'message' => '', 'payload' => ['lots' => 'of data', 'foo' => 'bar']]
                ]
                ],
            'subtitleFiles' => [
                ['id' => 1, 'mediaId' => 1, 'path' => '/dummydir/dummyinput.srt', 'language' => 'en'],
                ['id' => 1, 'mediaId' => 2, 'path' => '/dummydir/dummyinput.srt', 'language' => 'de']
            ]
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var DeployJob $integrationDeployJob */
        $integrationDeployJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(DeployJob::class, $integrationDeployJob);

        //Some spot tests
        $this->assertArrayHasKey($mediaId, $integrationDeployJob->getSourceFiles());
        $this->assertInstanceOf(File::class, $integrationDeployJob->getSourceFiles()[$mediaId]);
        $this->assertCount(count($dataModel['sourceFiles']), $integrationDeployJob->getSourceFiles());
        $this->assertInstanceOf(ImageConvertTarget::class, $integrationDeployJob->getConvertTargets()[0]);
        $this->assertInstanceOf(VideoConvertTarget::class, $integrationDeployJob->getConvertTargets()[1]);
        $this->assertInstanceOf(DocumentConvertTarget::class, $integrationDeployJob->getConvertTargets()[2]);
        $this->assertCount(count($dataModel['convertTargets']), $integrationDeployJob->getConvertTargets());
        $this->assertArrayHasKey($mediaId, $integrationDeployJob->getIntegratedFiles());
        $this->assertInstanceOf(IntegratedFile::class, $integrationDeployJob->getIntegratedFiles()[$mediaId][0]);
        $this->assertInstanceOf(SubtitleFile::class, $integrationDeployJob->getSubtitleFiles()[0]);
        $this->assertFalse($integrationDeployJob->getIntegratedFiles()[$mediaId][0]->isSuccess());
        $this->assertTrue($integrationDeployJob->getIntegratedFiles()[$mediaId][1]->isSuccess());
        $this->assertCount(
            count($dataModel['integratedFiles'][$mediaId][1]['payload']),
            $integrationDeployJob->getIntegratedFiles()[$mediaId][1]->getPayload()
        );

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($integrationDeployJob));
    }
}
