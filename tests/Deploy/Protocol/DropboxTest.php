<?php


namespace QBNK\JobQueue\Job\Test\Deploy\Protocol;


use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Deploy\Protocol\Dropbox;
use QBNK\JobQueue\Job\Deploy\Protocol\ProtocolAbstract;

class DropboxTest extends TestCase
{

    /** @var array An associative array representation of the expected JSON */
    protected $jsonExpected;

    /** @var Dropbox An instance filled with data to test against */
    protected $testInstance;

    public function setUp(): void
    {
        parent::setUp();
        $this->jsonExpected = json_encode([
            'name' => 'Dropbox',
            'filesystemOptions' => [],
            'protocol_deploydir' => '/subdir',
            'subdirtype' => 'Hash',
            'subdirdepth' => 0,
            'protocol_authtoken' => 'noauth'
        ]);
        $this->testInstance = (new Dropbox())
            ->setSubDirectory('/subdir')
            ->setSubDirectoryType(Dropbox::SUBDIR_TYPE_HASH)
            ->setSubDirectoryDepth(0)
            ->setAuthToken('noauth');
    }

    public function testSerialization()
    {
        $json = json_encode($this->testInstance);
        $this->assertIsString($json);
        $this->assertEquals($this->jsonExpected, $json);
    }

    /**
     * @depends testSerialization
     */
    public function testDeserialization()
    {
        $deserializedData = json_decode(json_encode($this->testInstance), true);
        $this->assertNotNull($deserializedData);
        $deserializedInstance = ProtocolAbstract::fromArray($deserializedData);
        $this->assertInstanceOf(Dropbox::class, $deserializedInstance);
    }
}