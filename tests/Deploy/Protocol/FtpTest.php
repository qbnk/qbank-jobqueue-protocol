<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Test\Deploy\Protocol;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Deploy\Protocol\Ftp;
use QBNK\JobQueue\Job\Deploy\Protocol\ProtocolAbstract;

class FtpTest extends TestCase
{

    public function testFromArray()
    {
        $testData = [
            'name' => 'FTP',
            'filesystemOptions' => [],
            Ftp::SYSTEMNAME_SUBDIR => 'dummy/path',
            'protocol_file_permissions' => '644',
            'protocol_folder_permissions' => '755',
            Ftp::SYSTEMNAME_SUBDIR_TYPE => Ftp::SUBDIR_TYPE_MEDIAID,
            Ftp::SYSTEMNAME_SUBDIR_DEPTH => 2,
            'protocol_hostname' => '1.1.1.1',
            'protocol_port' => 21,
            'protocol_ssl' => false,
            'protocol_username' => 'qbank',
            'protocol_password' => '****************'
        ];
        $protocol = ProtocolAbstract::fromArray($testData);
        $this->assertInstanceOf(Ftp::class, $protocol);
        $this->assertJsonStringEqualsJsonString(json_encode($testData), json_encode($protocol));
    }
}
