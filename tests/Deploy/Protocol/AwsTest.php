<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Test\Deploy\Protocol;

use Monolog\Test\TestCase;
use QBNK\JobQueue\Job\Deploy\Protocol\Aws;
use QBNK\JobQueue\Job\Deploy\Protocol\DynamicSubDirectoryInterface;
use QBNK\JobQueue\Job\Deploy\Protocol\ProtocolAbstract;
use QBNK\JobQueue\Job\Deploy\Protocol\SubDirectoryInterface;

class AwsTest extends TestCase
{
    public function testFromArray()
    {
        $testData = [
            'name' => Aws::NAME,
            SubDirectoryInterface::SYSTEMNAME_SUBDIR => 'dummy/path',
            DynamicSubDirectoryInterface::SYSTEMNAME_SUBDIR_TYPE => DynamicSubDirectoryInterface::SUBDIR_TYPE_MEDIAID,
            DynamicSubDirectoryInterface::SYSTEMNAME_SUBDIR_DEPTH => 2,
            'protocol_accountsecret' => 'foo',
            "protocol_adapter_options" => [],
            'protocol_accountkey' => 's3cR3T',
            'protocol_bucket' => 'bar',
            'protocol_region' => 'baz',
            Aws::CLIENT_OPTIONS => [
                'faz' => 'qux'
            ],
            'filesystemOptions' => [
                'StorageClass' => 'INTELLIGENT_TIERING'
            ]
        ];
        $protocol = ProtocolAbstract::fromArray($testData);
        $this->assertInstanceOf(Aws::class, $protocol);
        $this->assertJsonStringEqualsJsonString(json_encode($testData), json_encode($protocol));
        $this->assertArrayHasKey('StorageClass', $protocol->getFilesystemOptions());
        $this->assertEquals('INTELLIGENT_TIERING', $protocol->getFilesystemOptions()['StorageClass']);
    }
}