<?php

namespace QBNK\JobQueue\Job\Test\Deploy;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Deploy\DeployedFile;
use QBNK\JobQueue\Job\Deploy\DeployMedia;
use QBNK\JobQueue\Job\Deploy\Protocol\Copy;
use QBNK\JobQueue\Job\Enums\JobPriority;
use QBNK\JobQueue\Job\Enums\JobStatus;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\UnDeployJob;

class UnDeployJobTest extends TestCase
{

    /** @var string */
    protected $json;

    /** @var UnDeployJob An instance filled with data to test against */
    protected $testInstance;

    public function setUp(): void
    {
        parent::setUp();

        $this->testInstance = JobAbstract::fromArray($this->getDataModel());
        $this->testInstance
            ->setDeploymentSiteId(1)
            ->setProtocol(new Copy())
        ;
    }

    protected function getDataModel(): array
    {
        return [
            'id' => 'dummyId',
            'userId' => 123,
            'class' => UnDeployJob::class,
            'priority' => JobPriority::High,
            'status' => JobStatus::Running,
            'message' => 'Dummy message',
            'customerDomain' => 'test.customer.test',
            'tempDirectoryPath' => '/dummydir/temppath/',
            'deploymentSiteId' => 5,
            'media' => [
                [
                    'mediaId' => 1337,
                    'deployedFiles' => [
                        [
                            'remotePath' => '/dummydir/dummyinputfile.tmp',
                            'status' => DeployedFile::STATUS_DEPLOYED
                        ]
                    ],
                    'mimeType' => 'test/tmp',
                    'classification' => 'Document',
                    'properties' => ['keywords' => ['Test', 'Dummy']]
                ]
            ]
        ];
    }

    public function testClass(): void
    {
        $this->assertInstanceOf(UnDeployJob::class, $this->testInstance);
    }

    /**
     * @depends testClass
     */
    public function testSerialization(): void
    {
        $json = json_encode($this->testInstance);
        $this->assertIsString($json);
    }

    /**
     * @depends testSerialization
     */
    public function testDeserialization()
    {
        $deserializedData = json_decode(json_encode($this->testInstance), true);
        $this->assertNotNull($deserializedData);
        /** @var UnDeployJob $deserializedInstance */
        $deserializedInstance = JobAbstract::fromArray($deserializedData);
        $this->assertInstanceOf(UnDeployJob::class, $deserializedInstance);
        $this->assertInstanceOf(Copy::class, $deserializedInstance->getProtocol());
        $this->assertNotEmpty($deserializedInstance->getMedia());
        foreach ($deserializedInstance->getMedia() as $source) {
            $this->assertInstanceOf(DeployMedia::class, $source);
        }
    }

    public function testAsLegacy(): void
    {
        $this->assertArrayHasKey('deploymentSiteId', $this->testInstance->asLegacy());
        $this->assertArrayHasKey('success', $this->testInstance->asLegacy());
        $this->assertArrayHasKey('undeployedFiles', $this->testInstance->asLegacy());
        $this->assertArrayHasKey('remoteFilePath', $this->testInstance->asLegacy()['undeployedFiles'][1337][0]);

        $this->assertEquals(
            $this->testInstance->getDeploymentSiteId(),
            $this->testInstance->asLegacy()['deploymentSiteId']
        );
        $this->assertIsBool($this->testInstance->asLegacy()['success']);
    }
}
