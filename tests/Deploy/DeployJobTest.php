<?php

namespace QBNK\JobQueue\Job\Test\Deploy;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Convert\ImageConvertJob;
use QBNK\JobQueue\Job\Deploy\DeployMedia;
use QBNK\JobQueue\Job\Deploy\Protocol\Copy;
use QBNK\JobQueue\Job\DeployJob;
use QBNK\JobQueue\Job\Enums\JobPriority;
use QBNK\JobQueue\Job\Enums\JobStatus;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\Test\ImageConvertJobTest;

class DeployJobTest extends TestCase
{

    /** @var string */
    protected $json;

    /** @var DeployJob An instance filled with data to test against */
    protected $testInstance;

    public function setUp(): void
    {
        parent::setUp();

        $this->testInstance = JobAbstract::fromArray($this->getDataModel());
        $this->testInstance
            ->setDeploymentSiteId(1)
            ->setProtocol(new Copy())
        ;
    }

    protected function getDataModel(): array
    {
        return [
            'id' => 'dummyId',
            'userId' => 123,
            'class' => DeployJob::class,
            'priority' => JobPriority::High,
            'status' => JobStatus::Running,
            'message' => 'Dummy message',
            'customerDomain' => 'test.customer.test',
            'tempDirectoryPath' => '/dummydir/temppath/',
            'sources' => [
                [
                    'mediaId' => 1,
                    'source' => '/dummydir/dummyinputfile.tmp',
                    'mimeType' => 'test/tmp',
                    'classification' => 'Document',
                    'properties' => ['keywords' => ['Test', 'Dummy']],
                    'deployedFiles' => [
                        [
                            'size' => 100,
                            'remotePath' => 'remote/path/to/file',
                            'identifier' => 'foo',
                            'metadata' => []
                        ]
                    ],
                    'convertJobs' => [
                        ImageConvertJob::fromArray(ImageConvertJobTest::getJobModel('foo'))
                    ]
                ]
            ]
        ];
    }

    public function testClass(): void
    {
        $this->assertInstanceOf(DeployJob::class, $this->testInstance);
    }

    /**
     * @depends testClass
     */
    public function testSerialization(): void
    {
        $json = json_encode($this->testInstance);
        $this->assertIsString($json);
    }

    /**
     * @depends testSerialization
     */
    public function testDeserialization()
    {
        $deserializedData = json_decode(json_encode($this->testInstance), true);
        $this->assertNotNull($deserializedData);
        /** @var DeployJob $deserializedInstance */
        $deserializedInstance = JobAbstract::fromArray($deserializedData);
        $this->assertInstanceOf(DeployJob::class, $deserializedInstance);
        $this->assertInstanceOf(Copy::class, $deserializedInstance->getProtocol());
        $this->assertNotEmpty($deserializedInstance->getSources());
        foreach ($deserializedInstance->getSources() as $source) {
            $this->assertInstanceOf(DeployMedia::class, $source);
        }
    }

    public function testAsLegacy(): void
    {
        $asLegacy = $this->testInstance->asLegacy();
        $this->assertArrayHasKey('deploymentSiteId', $asLegacy);
        $this->assertArrayHasKey('success', $asLegacy);
        $this->assertArrayHasKey('deployedFiles', $asLegacy);
        $this->assertArrayHasKey('files', $asLegacy['deployedFiles'][0]);
        $this->assertArrayHasKey('mediaId', $asLegacy['deployedFiles'][0]);

        $this->assertEquals(
            $this->testInstance->getDeploymentSiteId(),
            $asLegacy['deploymentSiteId']
        );
        $this->assertIsBool($asLegacy['success']);
        $this->assertIsInt($asLegacy['deployedFiles'][0]['mediaId']);
        $this->assertIsArray($asLegacy['deployedFiles'][0]['files']);
        $this->assertEquals('foo', $asLegacy['deployedFiles'][0]['files'][0]['identifier']);
    }

    public function testReset() {
        $deployJob = DeployJob::fromArray($this->getDataModel());
        $resetJob = $deployJob->reset();

        $this->assertNotEquals(
            $deployJob->getSources()[0]->getConvertJobs()[0]->getId(),
            $resetJob->getSources()[0]->getConvertJobs()[0]->getId()
        );
        $this->assertEquals(
            JobStatus::Running,
            $deployJob->getSources()[0]->getConvertJobs()[0]->getStatus()
        );
        $this->assertEquals(
            JobStatus::New,
            $resetJob->getSources()[0]->getConvertJobs()[0]->getStatus()
        );
    }
}
