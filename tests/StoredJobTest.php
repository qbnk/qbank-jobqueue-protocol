<?php

declare(strict_types=1);

namespace QBNK\JobQueue\Job\Test;

use DateTime;
use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Examination\ColorAnalysisJob;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\StoredJob;

class StoredJobTest extends TestCase
{

    /** @var StoredJob */
    protected $storedJob;

    /** @var JobAbstract */
    protected $job;

    public function setUp(): void
    {
        // We need to use a random actual job model since reconstruct
        // is using an Instantiator which Mockery cannot handle
        $this->job = ColorAnalysisJob::fromArray(TestUtility::getJobAbstractTestParams([
            'class' => ColorAnalysisJob::class,
            'queueName' => 'coloranalysis',
            'source' => [
                'source' => '/dummydir/dummyinputfile.tmp',
                'mimeType' => 'test/tmp',
                'classification' => 'Document',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],
            'colorData' => ['mixed' => ['foo', 'bar']]
        ]));
    }

    public function testSerialization(): void
    {
        $dates = [
            'createdDate' => (new DateTime('2000-01-01 13:36'))->format(DATE_ATOM),
            'blockedDate' => (new DateTime('2000-01-01 13:37'))->format(DATE_ATOM),
            'queuedDate' => (new DateTime('2000-01-01 13:38'))->format(DATE_ATOM),
            'startedDate' => (new DateTime('2000-01-01 13:39'))->format(DATE_ATOM),
            'processedDate' => (new DateTime('2000-01-01 13:40'))->format(DATE_ATOM),
            'callbackQueuedDate' => (new DateTime('2000-01-01 13:41'))->format(DATE_ATOM),
            'callbackRunningDate' => (new DateTime('2000-01-01 13:42'))->format(DATE_ATOM),
            'callbackFailedDate' => (new DateTime('2000-01-01 13:43'))->format(DATE_ATOM),
            'callbackCompletedDate' => (new DateTime('2000-01-01 13:44'))->format(DATE_ATOM),
            'canceledDate' => (new DateTime('2000-01-01 13:45'))->format(DATE_ATOM)
        ];

        $dataModel = array_merge([
            'mediaIds' => [1, 2],
            'qbankJob' => $this->job,
        ], $dates);

        //Test reconstruct
        $storedJob = StoredJob::fromArray($dataModel);

        $this->assertEquals($this->job->getId(), $storedJob->getQbankJob()->getId());
        foreach ($dates as $date => $expectedValue) {
            $getter = 'get' . $date;
            $this->assertEquals($expectedValue, $storedJob->$getter()->format(DATE_ATOM));
        }
        $this->assertEquals(StoredJob::QUEUE_STATUS_CANCELED, $storedJob->getLastStatus());
        $this->assertEquals($storedJob->getCanceledDate(), $storedJob->getLastUpdated());

        //Test serialize
        $this->assertJsonStringEqualsJsonString(json_encode($dataModel), json_encode($storedJob));
    }
}
