<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Audio\AudioCodec;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video\Scale;
use QBNK\JobQueue\Job\Convert\Command\FFmpeg\Video\VideoCodec;
use QBNK\JobQueue\Job\Convert\FFmpegJob;
use QBNK\JobQueue\Job\Convert\Model\VideoConvertTarget;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\Storage\File;

class FFmpegJobTest extends TestCase
{

    public static function getJobModel(): array
    {
        return TestUtility::getJobAbstractTestParams([
            'class' => FFmpegJob::class,
            'queueName' => 'ffmpeg',
            'source' => [
                'source' => '/dummydir/dummyinputfile.tmp',
                'mimeType' => 'test/tmp',
                'classification' => 'Document',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],
            'removeSource' => false,
            'persistProtocol' => null,
            'targets' => [
                'TestTarget' => [
                    'templateId' => 1,
                    'class' => VideoConvertTarget::class,
                    'identifier' => 'TestTarget',
                    'target' => new File(
                        '/dummydir/dummyoutputfile.tmp',
                        'test/tmp',
                        'Document',
                        ['keywords' => ['Test', 'Dummy']]
                    ),
                    'commands' => [
                        [
                            'width' => 720,
                            'height' => 480,
                            'scalingOption' => Scale::OPTION_FASTBILINEAR,
                            'class' => 'Scale'
                        ],
                        new AudioCodec(AudioCodec::AAC),
                        new VideoCodec(VideoCodec::CODEC_LIBX264)
                    ]
                ]
            ],
            'convertedFiles' => [
                'TestTarget' => new File(
                    '/dummydir/dummyoutputfile.tmp',
                    'test/tmp',
                    'Document',
                    ['keywords' => ['Test', 'Dummy']]
                )
            ]
        ]);
    }

    function testSerialization()
    {
        $dataModel = self::getJobModel();

        $jsonModel = json_encode($dataModel);

        /** @var FFmpegJob $ffmpegJob */
        $ffmpegJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(FFmpegJob::class, $ffmpegJob);

        //Some spot tests
        $this->assertEquals($dataModel['queueName'], $ffmpegJob->getQueueName());
        $this->assertInstanceOf(VideoConvertTarget::class, $ffmpegJob->getTargets()['TestTarget']);
        $this->assertInstanceOf(Scale::class, $ffmpegJob->getTargets()['TestTarget']->getCommands()[0]);
        $this->assertInstanceOf(AudioCodec::class, $ffmpegJob->getTargets()['TestTarget']->getCommands()[1]);
        $this->assertInstanceOf(VideoCodec::class, $ffmpegJob->getTargets()['TestTarget']->getCommands()[2]);
        $this->assertInstanceOf(File::class, current($ffmpegJob->getConvertedFiles()));

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($ffmpegJob));
    }
}
