<?php


namespace QBNK\JobQueue\Job\Test;


use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Convert\AudioThumbnailJob;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\Storage\File;

class AudioThumbnailJobTest extends TestCase
{
    function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => AudioThumbnailJob::class,
            'queueName' => 'audiothumbnail',
            'source' => [
                'source' => '/dummydir/dummyinputfile.tmp',
                'mimeType' => 'test/tmp',
                'classification' => 'Document',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],
            'removeSource' => false,
            'convertedWaveformImage' => new File(
                '/dummydir/dummyoutputfile.tmp',
                'test/tmp',
                'Document',
                ['keywords' => ['Test', 'Dummy']]
            )
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var AudioThumbnailJob $audioThumbnailJob */
        $audioThumbnailJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(AudioThumbnailJob::class, $audioThumbnailJob);

        //Some spot tests
        $this->assertEquals($dataModel['queueName'], $audioThumbnailJob->getQueueName());
        $this->assertEquals($dataModel['source']['source'], $audioThumbnailJob->getSource()->getPathname());

//		$this->assertInstanceOf(File::class, $audioThumbnailJob->getConvertedWaveformImage());

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($audioThumbnailJob));
    }
}
