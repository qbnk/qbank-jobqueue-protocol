<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Examination\WordExtractTextJob;
use QBNK\JobQueue\Job\JobAbstract;

class WordExtractTextJobTest extends TestCase
{

    function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => WordExtractTextJob::class,
            'queueName' => 'wordextracttext',
            'source' => [
                'source' => '/dummydir/dummyinputfile.tmp',
                'mimeType' => 'test/tmp',
                'classification' => 'Document',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],
            'extractedText' => 'testing'
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var WordExtractTextJob $wordExtractTextJob */
        $wordExtractTextJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(WordExtractTextJob::class, $wordExtractTextJob);

        $this->assertEquals($dataModel['extractedText'], $wordExtractTextJob->getExtractedText());

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($wordExtractTextJob));
    }
}
