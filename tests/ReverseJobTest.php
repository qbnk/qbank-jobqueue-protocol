<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Examination\ReverseJob;
use QBNK\JobQueue\Job\JobAbstract;

class ReverseJobTest extends TestCase
{

    function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => ReverseJob::class,
            'queueName' => 'locationreverse',
            'latitude' => 1111.1111,
            'longitude' => 2222.2222,
            'locationDetails' => ['foo' => 'bar']
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var ReverseJob $reverseJob */
        $reverseJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(ReverseJob::class, $reverseJob);

        $this->assertEquals($dataModel['latitude'], $reverseJob->getLatitude());
        $this->assertEquals($dataModel['longitude'], $reverseJob->getLongitude());
        $this->assertArrayHasKey(key($dataModel['locationDetails']), $reverseJob->getLocationDetails());

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($reverseJob));
    }
}
