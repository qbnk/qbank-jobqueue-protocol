<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Examination\Model\Entity;
use QBNK\JobQueue\Job\Examination\VideoAnalysisJob;
use QBNK\JobQueue\Job\JobAbstract;

class VideoAnalysisJobTest extends TestCase
{

    function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => VideoAnalysisJob::class,
            'queueName' => 'videoanalysis',
            'source' => [
                'source' => '/dummydir/dummyinputfile.mp4',
                'mimeType' => 'video/mp4',
                'classification' => 'video',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],

            // Feature::SPEECH_TRANSCRIPTION, Feature::LABEL_DETECTION
            'features' => [1, 6],
            'analysisData' => [
                'videoData' => ['foo'],
                'explicit' => [],
                'logos' => [
                    Entity::fromArray([
                        'value' => 'foo',
                        'segments' => [
                            [
                                'startTime' => 5,
                                'endTime' => 6
                            ]
                        ],
                        'detectedObjects' => [
                            [
                                'timeOffset' => 5,
                                'normalizedBoundingBox' => [
                                    'left' => 0.1,
                                    'top' => 0.2,
                                    'right' => 0.3,
                                    'bottom' => 0.4
                                ]
                            ]
                        ]
                    ])
                ],
                'texts' => [
                    Entity::fromArray([
                        'value' => 'bar',
                        'confidence' => 0.9,
                        'segments' => [
                            [
                                'startTime' => 10,
                                'endTime' => 15
                            ]
                        ],
                        'detectedObjects' => [
                            [
                                'timeOffset' => 10,
                                'normalizedBoundingBox' => [
                                    'left' => 0.6,
                                    'top' => 0.7,
                                    'right' => 0.8,
                                    'bottom' => 0.9
                                ]
                            ]
                        ]
                    ])
                ]
            ]
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var VideoAnalysisJob $videoAnalysisJob */
        $videoAnalysisJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        self::assertInstanceOf(VideoAnalysisJob::class, $videoAnalysisJob);

        self::assertEquals($dataModel['features'], $videoAnalysisJob->getFeatures());
        self::assertInstanceOf(Entity::class, $videoAnalysisJob->getAnalysisData()->getLogos()[0]);
        self::assertEquals('foo', $videoAnalysisJob->getAnalysisData()->getLogos()[0]->getValue());
        self::assertEquals('bar', $videoAnalysisJob->getAnalysisData()->getTexts()[0]->getValue());

        //Test serialize
        self::assertJsonStringEqualsJsonString($jsonModel, json_encode($videoAnalysisJob));
    }
}
