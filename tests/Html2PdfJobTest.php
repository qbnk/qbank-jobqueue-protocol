<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Convert\Html2PdfJob;
use QBNK\JobQueue\Job\Convert\Model\PdfHeader;
use QBNK\JobQueue\Job\Convert\Model\PdfFooter;
use QBNK\JobQueue\Job\Convert\Model\PdfText;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\Storage\File;

class Html2PdfJobTest extends TestCase
{
    public function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => Html2PdfJob::class,
            'queueName' => 'htmltopdf',
            'source' => [
                'source' => '/dummydir/dummyinputfile.tmp',
                'mimeType' => 'test/tmp',
                'classification' => 'Document',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],
            'removeSource' => false,
            'header' => [
              'class' => PdfHeader::class,
              'baseStyles' => 'testBaseStyles',
              'extraStyles' => 'testExtraStyles',
              'id' => 'testHeaderId',
              'texts' => [
                ['content' => 'testText', 'alignment' => 'right', 'float' => true, 'class' => PdfText::class, 'alignmentStyles' => 'float: right;']
              ]
            ],
            'footer' => [
              'class' => PdfFooter::class,
              'baseStyles' => 'testBaseStyles',
              'extraStyles' => 'testExtraStyles',
              'id' => 'testFooterId',
              'texts' => [
                ['content' => 'testText', 'alignment' => 'right', 'float' => true, 'class' => PdfText::class, 'alignmentStyles' => 'float: right;']
              ],
              'includePageCount' => true
            ],
            'documentSize' => 'A4',
            'orientation' => Html2PdfJob::ORIENTATION_PORTRAIT,
            'margins' => [20, 10, 20, 10],
            'outputPdf' => new File(
                '/dummydir/dummyoutputfile.pdf',
                'application/pdf',
                'Document',
                ['keywords' => ['Test', 'Dummy']]
            )
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var Html2PdfJob $html2pdfJob */
        $html2pdfJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(Html2PdfJob::class, $html2pdfJob);

        $this->assertEquals($dataModel['documentSize'], $html2pdfJob->getDocumentSize());
        $this->assertEquals($dataModel['outputPdf']->getPathname(), $html2pdfJob->getOutputPdf()->getPathname());
        $this->assertEquals($dataModel['orientation'], $html2pdfJob->getOrientation());

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($html2pdfJob));
    }
}
