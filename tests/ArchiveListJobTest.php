<?php

namespace QBNK\JobQueue\Job\Test;

use DateTime;
use Phar;
use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Examination\ArchiveListJob;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\JobQueueException;
use QBNK\JobQueue\Job\Storage\CompressedFile;

class ArchiveListJobTest extends TestCase
{

    public function getCompressedFile()
    {
        return [
            'archivePath' => 'dummy/path/file.jpg',
            'compressedSize' => 100,
            'realSize' => 200,
            'created' => new DateTime('2020-04-21 16:33:00'),
            'modified' => new DateTime('2020-04-22 16:34:00'),
            'compressionAlgorithm' => Phar::ZIP
        ];
    }

    public function testCompressedFiles()
    {
        $baseData = $this->getCompressedFile();
        $jsonData = json_encode($baseData);

        $compressedFile = CompressedFile::fromArray(json_decode($jsonData, true));

        $this->assertEquals($baseData['archivePath'], $compressedFile->getArchivePath());
        $this->assertEquals($baseData['compressedSize'], $compressedFile->getCompressedSize());
        $this->assertEquals($baseData['realSize'], $compressedFile->getRealSize());
        $this->assertEquals($baseData['created']->format('c'), $compressedFile->getCreated()->format('c'));
        $this->assertEquals($baseData['modified']->format('c'), $compressedFile->getModified()->format('c'));
        $this->assertEquals($baseData['compressionAlgorithm'], $compressedFile->getCompressionAlgorithm());

        $this->assertEquals($jsonData, json_encode($compressedFile));
    }

    /**
     * @throws JobQueueException
     * @depends testCompressedFiles
     */
    public function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => ArchiveListJob::class,
            'queueName' => 'archivelist',
            'source' => [
                'source' => '/dummydir/dummyinputfile.zip',
                'mimeType' => 'test/tmp',
                'classification' => 'Document',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],

            'format' => ArchiveListJob::FORMAT_ZIP,
            'compressedFiles' => [
                $this->getCompressedFile(),
                $this->getCompressedFile()
            ]
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var ArchiveListJob $archiveListJob */
        $archiveListJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(ArchiveListJob::class, $archiveListJob);

        //Some spot tests
        $this->assertEquals($dataModel['format'], $archiveListJob->getFormat());
        $this->assertCount(count($dataModel['compressedFiles']), $archiveListJob->getCompressedFiles());
        $this->assertInstanceOf(CompressedFile::class, current($archiveListJob->getCompressedFiles()));

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($archiveListJob));
    }
}
