<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Convert\PdfExtractTextJob;
use QBNK\JobQueue\Job\JobAbstract;

class PdfExtractTextJobTest extends TestCase
{

    function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => PdfExtractTextJob::class,
            'queueName' => 'pdfextracttext',
            'source' => [
                'source' => '/dummydir/dummyinputfile.tmp',
                'mimeType' => 'test/tmp',
                'classification' => 'Document',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],
            'removeSource' => false,
            'asXml' => true,
            'outputData' => 'lorem ipsum'
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var PdfExtractTextJob $pdfExtractTextJob */
        $pdfExtractTextJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(PdfExtractTextJob::class, $pdfExtractTextJob);

        $this->assertEquals($dataModel['asXml'], $pdfExtractTextJob->getAsXml());
        $this->assertEquals($dataModel['outputData'], $pdfExtractTextJob->getOutputData());

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($pdfExtractTextJob));
    }
}
