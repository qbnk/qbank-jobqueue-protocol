<?php

namespace QBNK\JobQueue\Job\Test\Examination\Model;

use QBNK\JobQueue\Job\Examination\Model\DetectedObject;
use PHPUnit\Framework\TestCase;

class DetectedObjectTest extends TestCase
{
    public function testValidateBoundingBox()
    {
        $box = ['top' => 0.1234, 'bottom' => 0.2343, 'left' => 0.1233, 'right' => 0.3456];
        $this->assertTrue(DetectedObject::validateBoundingBox($box));
    }

    public function testValditateBoundingBoxWithZeroValues()
    {
        $box = ['top' => 0, 'bottom' => 0.2343, 'left' => 0, 'right' => 0.3456];
        $this->assertTrue(DetectedObject::validateBoundingBox($box));
    }

    public function testValidateBoundingBoxWithMissingValues()
    {
        $box = ['top' => 0.1234, 'bottom' => 0.2343, 'left' => 0.1233];
        $message = '';
        $this->assertFalse(DetectedObject::validateBoundingBox($box, $message));
        $this->assertStringContainsString('right', $message);
    }

    public function test__construct()
    {
        $box = ['top' => 0.1234, 'bottom' => 0.2343, 'left' => 0.1233, 'right' => 0.3456];
        $detectedObject = new DetectedObject($box, 1337);
        $this->assertEquals($box, $detectedObject->getNormalizedBoundingBox());
    }
}
