<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Convert\VideoThumbnailJob;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\Storage\File;

class VideoThumbnailJobTest extends TestCase
{

    function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => VideoThumbnailJob::class,
            'queueName' => 'videothumbnail',
            'source' => [
                'source' => '/dummydir/dummyinputfile.tmp',
                'mimeType' => 'test/tmp',
                'classification' => 'Document',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],
            'removeSource' => false,
            'width' => 200,
            'height' => 100,
            'numThumbs' => 6,
            'convertedThumbnails' => [
                new File('/dummydir/dummyoutputfile.jpg', 'image/jpg', 'image', ['keywords' => ['Test', 'Dummy']]),
                new File('/dummydir/dummyoutputfile2.jpg', 'image/jpg', 'image', ['keywords' => ['Test', 'Dummy']])
            ]
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var VideoThumbnailJob $videoThumbnailJob */
        $videoThumbnailJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(VideoThumbnailJob::class, $videoThumbnailJob);

        $this->assertEquals($dataModel['width'], $videoThumbnailJob->getWidth());
        $this->assertEquals($dataModel['height'], $videoThumbnailJob->getHeight());
        $this->assertEquals($dataModel['numThumbs'], $videoThumbnailJob->getNumThumbs());
        $this->assertCount(count($dataModel['convertedThumbnails']), $videoThumbnailJob->getConvertedThumbnails());
        $this->assertInstanceOf(File::class, current($videoThumbnailJob->getConvertedThumbnails()));

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($videoThumbnailJob));
    }
}
