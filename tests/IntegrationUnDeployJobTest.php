<?php

namespace QBNK\JobQueue\Job\Test;

use DateTime;
use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Integration\Model\MediaUsage;
use QBNK\JobQueue\Job\Integration\Model\UnDeployResult;
use QBNK\JobQueue\Job\Integration\UnDeployJob;
use QBNK\JobQueue\Job\JobAbstract;

class IntegrationUnDeployJobTest extends TestCase
{

    function testSerialization()
    {
        $usageModel = [
            'id' => 123,
            'sessionId' => 12345678,
            'created' => new DateTime('2020-01-01'),
            'updated' => new DateTime('2020-01-02'),
            'deleted' => new DateTime('2020-01-03'),
            'pageUrl' => 'https://baconmockup.com/300/200',
            'mediaUrl' => 'https://www.stevensegallery.com/200/300',
            'context' => ['foo' => 'bar'],
            'mediaId' => 321,
            'language' => 'en_US',
            'updatedBy' => 1,
            'integrationId' => 1
        ];
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => UnDeployJob::class,
            'queueName' => 'integration_undeploy_job',
            'integrationId' => 1337,
            'authData' => '{"json":["foo","bar"]}',
            'jobParameters' => ['faz', 'baz'],
            'subChannels' => [['id' => 1338, 'name' => 'test', 'definition' => ['externalId' => '123ABC']]],
            'publishProtocol' => null,

            'mediaUsages' => [$usageModel],
            'result' => [
                [
                    'usage' => $usageModel,
                    'message' => 'Hello',
                    'success' => true
                ]
            ]
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var UnDeployJob $integrationUndeployJob */
        $integrationUndeployJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(UnDeployJob::class, $integrationUndeployJob);
        $this->assertInstanceOf(MediaUsage::class, $integrationUndeployJob->getMediaUsages()[0]);
        $this->assertEquals($usageModel['id'], $integrationUndeployJob->getMediaUsages()[0]->getId());
        $this->assertEquals(
            $usageModel['integrationId'],
            $integrationUndeployJob->getMediaUsages()[0]->getIntegrationId()
        );
        $this->assertEquals(
            $usageModel['created']->format('Ymd'),
            $integrationUndeployJob->getMediaUsages()[0]->getCreated()->format('Ymd')
        );
        $this->assertEquals(
            $usageModel['updated']->format('Ymd'),
            $integrationUndeployJob->getMediaUsages()[0]->getUpdated()->format('Ymd')
        );
        $this->assertEquals(
            $usageModel['deleted']->format('Ymd'),
            $integrationUndeployJob->getMediaUsages()[0]->getDeleted()->format('Ymd')
        );

        $this->assertInstanceOf(UnDeployResult::class, $integrationUndeployJob->getResult()[0]);
        $this->assertEquals($dataModel['result'][0]['message'], $integrationUndeployJob->getResult()[0]->getMessage());
        $this->assertEquals($dataModel['result'][0]['success'], $integrationUndeployJob->getResult()[0]->isSuccess());

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($integrationUndeployJob));
    }
}
