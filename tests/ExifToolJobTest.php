<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Examination\ExifToolJob;
use QBNK\JobQueue\Job\Examination\Model\ClippingPath;
use QBNK\JobQueue\Job\JobAbstract;

class ExifToolJobTest extends TestCase
{

    function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => ExifToolJob::class,
            'queueName' => 'exiftool',
            'source' => [
                'source' => '/dummydir/dummyinputfile.tmp',
                'mimeType' => 'test/tmp',
                'classification' => 'Document',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],

            'extractedExifData' => ['foo', 'bar'],
            'extractedPaths' => [
                [
                    'name' => 'faz',
                    'isPrimaryPath' => false,
                    'path' => '<svg></svg>',
                    'pngFile' => null
                ]
            ],
            'locationReverseJob' => null
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var ExifToolJob $exifToolJob */
        $exifToolJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(ExifToolJob::class, $exifToolJob);

        $this->assertEquals($dataModel['extractedExifData'], $exifToolJob->getExtractedExifData());
        $this->assertInstanceOf(ClippingPath::class, $exifToolJob->getExtractedPaths()[0]);

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($exifToolJob));
    }
}
