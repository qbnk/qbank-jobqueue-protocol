<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Examination\GoogleVisionJob;
use QBNK\JobQueue\Job\JobAbstract;

class GoogleVisionJobTest extends TestCase
{

    function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => GoogleVisionJob::class,
            'queueName' => 'googlevision',
            'source' => [
                'source' => '/dummydir/dummyinputfile.tmp',
                'mimeType' => 'test/tmp',
                'classification' => 'Document',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],
            'analysisFeatures' => [GoogleVisionJob::FEATURE_CROP_HINTS],
            'analysisData' => ['response' => ['foo', 'bar']]
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var GoogleVisionJob $googleVisionJob */
        $googleVisionJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(GoogleVisionJob::class, $googleVisionJob);

        $this->assertEquals($dataModel['analysisFeatures'][0], $googleVisionJob->getAnalysisFeatures()[0]);
        $this->assertArrayHasKey(key($dataModel['analysisData']), $googleVisionJob->getAnalysisData());

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($googleVisionJob));
    }
}
