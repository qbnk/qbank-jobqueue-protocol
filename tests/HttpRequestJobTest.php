<?php

namespace QBNK\JobQueue\Job\Test;

use InvalidArgumentException;
use JsonSerializable;
use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\Misc\HttpRequestJob;
use QBNK\JobQueue\Job\Misc\IncompatibleDataTypesException;

class HttpRequestJobTest extends TestCase
{

    function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => HttpRequestJob::class,
            'queueName' => 'httprequest',
            'method' => HttpRequestJob::METHOD_POST,
            'uri' => 'https://123.123.123.123/dummy',
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'httpData' => [
                'dummy_data_key' => 'dummy_data_value'
            ],
            'dataType' => HttpRequestJob::DATATYPE_JSON,
            'files' => [
                'dummyinputfile.tmp' => [
                    'source' => '/dummydir/dummyinputfile.tmp',
                    'mimeType' => 'test/tmp',
                    'classification' => 'Document',
                    'properties' => ['keywords' => ['Test', 'Dummy']]
                ]
            ],
            'timeout' => 33.3,
            'readTimeout' => 0.5,
            'responseStatusCode' => 200,
            'responseReasonPhrase' => 'OK',
            'webhookId' => null,
            'saveResponse' => false,
            'responseHeaders' => []
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var HttpRequestJob $job */
        $job = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        self::assertInstanceOf(HttpRequestJob::class, $job);

        //Test serialize
        self::assertJsonStringEqualsJsonString($jsonModel, json_encode($job));
    }

    function testNoDataType()
    {
        $job = new HttpRequestJob();
        $this->assertEquals(null, $job->getDataType());

        $jsonData = json_decode(json_encode($job), true);
        $this->assertArrayNotHasKey('dataType', $jsonData);
    }

    function testBadData()
    {
        $job = new HttpRequestJob();
        $this->expectException(InvalidArgumentException::class);
        $job->setHttpData('data');
    }

    function testJsonData()
    {
        $data = [
            'foo' => 'bar',
            'nested' => [
                'key' => 'value'
            ]
        ];
        $job = new HttpRequestJob();
        $job->setHttpData($data, HttpRequestJob::DATATYPE_JSON);
        $this->assertEquals(HttpRequestJob::DATATYPE_JSON, $job->getDataType());
        $this->assertEquals($data, $job->getHttpData());

        $data = new HttpRequestJob();
        $data->setMethod(HttpRequestJob::METHOD_GET);
        $data->setUri('https://example.com');
        $job = new HttpRequestJob();
        $job->setHttpData($data, HttpRequestJob::DATATYPE_JSON);
        $this->assertEquals(HttpRequestJob::DATATYPE_JSON, $job->getDataType());
        $this->assertInstanceOf(HttpRequestJob::class, $job->getHttpData());
        $this->assertInstanceOf(JsonSerializable::class, $job->getHttpData());
    }

    function testFormData()
    {
        $data = [
            'foo' => 'bar',
            'key' => 'value'
        ];
        $job = new HttpRequestJob();
        $job->setHttpData($data, HttpRequestJob::DATATYPE_FORM);
        $this->assertEquals(HttpRequestJob::DATATYPE_FORM, $job->getDataType());
        $this->assertEquals($data, $job->getHttpData());
    }

    function testMultipartData()
    {
        $data = [
            'foo' => 'bar',
            'key' => 'value'
        ];
        $job = new HttpRequestJob();
        $job->setHttpData($data, HttpRequestJob::DATATYPE_MULTIPART);
        $this->assertEquals(HttpRequestJob::DATATYPE_MULTIPART, $job->getDataType());
        $this->assertEquals($data, $job->getHttpData());
    }

    function testAddData()
    {
        $data = [
            'foo' => 'bar',
            'key' => 'value'
        ];
        $job = new HttpRequestJob();
        $job->addHttpData('foo', 'bar', HttpRequestJob::DATATYPE_FORM);
        $job->addHttpData('key', 'value', HttpRequestJob::DATATYPE_FORM);
        $this->assertEquals($data, $job->getHttpData());

        $job = new HttpRequestJob();
        $job->setHttpData($data, HttpRequestJob::DATATYPE_FORM);
        $job->addHttpData('company', 'QBNK', HttpRequestJob::DATATYPE_FORM);
        $this->assertEquals(array_merge($data, ['company' => 'QBNK']), $job->getHttpData());
    }

    function testAddWrongDataType()
    {
        $job = new HttpRequestJob();
        $job->addHttpData('foo', 'bar', HttpRequestJob::DATATYPE_FORM);
        $this->expectException(IncompatibleDataTypesException::class);
        $job->addHttpData('key', 'value', HttpRequestJob::DATATYPE_MULTIPART);
    }

    function testAddToJsonData()
    {
        $data = new HttpRequestJob();
        $data->setMethod(HttpRequestJob::METHOD_GET);
        $data->setUri('https://example.com');
        $job = new HttpRequestJob();
        $job->setHttpData($data, HttpRequestJob::DATATYPE_JSON);
        $this->expectException(IncompatibleDataTypesException::class);
        $job->addHttpData('foo', 'bar', HttpRequestJob::DATATYPE_JSON);
    }
}
