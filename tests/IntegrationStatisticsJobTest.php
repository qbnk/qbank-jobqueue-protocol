<?php

namespace QBNK\JobQueue\Job\Test;

use DateTime;
use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Integration\Model\Statistic;
use QBNK\JobQueue\Job\Integration\StatisticsJob;
use QBNK\JobQueue\Job\JobAbstract;

class IntegrationStatisticsJobTest extends TestCase
{

    function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => StatisticsJob::class,
            'queueName' => 'integration_statistics_job',
            'integrationId' => 1337,
            'authData' => '{"json":["foo","bar"]}',
            'jobParameters' => ['faz', 'baz'],
            'subChannels' => [['id' => 1338, 'name' => 'test', 'definition' => ['externalId' => '123ABC']]],
            'publishProtocol' => null,

            'mediaIds' => [123, 345],
            'context' => ['foo' => ['bar']],
            'startDate' => new DateTime('2020-01-01'),
            'endDate' => new DateTime('2020-02-01'),
            'statistics' => [
                [
                    'mediaId' => 123,
                    'total' => ['views' => 100],
                    'details' => ['customChannelDetails' => 100]
                ]
            ]
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var StatisticsJob $integrationsStatisticsJob */
        $integrationsStatisticsJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(StatisticsJob::class, $integrationsStatisticsJob);

        $this->assertEquals(
            $dataModel['startDate']->format('Ymd'),
            $integrationsStatisticsJob->getStartDate()->format('Ymd')
        );
        $this->assertEquals(
            $dataModel['endDate']->format('Ymd'),
            $integrationsStatisticsJob->getEndDate()->format('Ymd')
        );
        $this->assertInstanceOf(Statistic::class, $integrationsStatisticsJob->getStatistics()[0]);
        $this->assertArrayHasKey(
            key($dataModel['statistics'][0]['details']),
            $integrationsStatisticsJob->getStatistics()[0]->getDetails()
        );
        $this->assertEquals(
            $dataModel['statistics'][0]['mediaId'],
            $integrationsStatisticsJob->getStatistics()[0]->getMediaId()
        );
        $this->assertCount(count($dataModel['mediaIds']), $integrationsStatisticsJob->getMediaIds());

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($integrationsStatisticsJob));
    }
}
