<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Convert\PowerPointThumbnailJob;
use QBNK\JobQueue\Job\JobAbstract;
use QBNK\JobQueue\Job\Storage\File;

class PowerPointThumbnailJobTest extends TestCase
{

    function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => PowerPointThumbnailJob::class,
            'queueName' => 'pptthumbnail',
            'source' => [
                'source' => '/dummydir/dummyinputfile.tmp',
                'mimeType' => 'test/tmp',
                'classification' => 'Document',
                'properties' => ['keywords' => ['Test', 'Dummy']]
            ],
            'removeSource' => null,
            'convertedThumbnail' => new File('/dummydir/dummyoutputfile.jpg', 'image/jpg', 'image'),

        ]);

        $jsonModel = json_encode($dataModel);

        /** @var PowerPointThumbnailJob $pptThumbnailJob */
        $pptThumbnailJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        self::assertInstanceOf(PowerPointThumbnailJob::class, $pptThumbnailJob);
        self::assertInstanceOf(File::class, $pptThumbnailJob->getConvertedThumbnail());

        //Test serialize
        self::assertJsonStringEqualsJsonString($jsonModel, json_encode($pptThumbnailJob));
    }
}
