<?php

namespace QBNK\JobQueue\Job\Test;

use PHPUnit\Framework\TestCase;
use QBNK\JobQueue\Job\Integration\ImportJob;
use QBNK\JobQueue\Job\Integration\Model\ImportFile;
use QBNK\JobQueue\Job\JobAbstract;

class IntegrationImportJobTest extends TestCase
{

    function testSerialization()
    {
        $dataModel = TestUtility::getJobAbstractTestParams([
            'class' => ImportJob::class,
            'queueName' => 'integration_import_job',
            'integrationId' => 1337,
            'authData' => '{"json":["foo","bar"]}',
            'jobParameters' => ['faz', 'baz'],
            'subChannels' => [['id' => 1338, 'name' => 'test', 'definition' => ['externalId' => '123ABC']]],
            'publishProtocol' => null,

            'externalIds' => ['12345', '23456'],
            'filters' => ['foo', 'bar'],
            'importedFiles' => [
                [
                    'name' => 'dummy file',
                    'filename' => 'dummyfile.tmp',
                    'metadata' => ['width' => 'baz'],
                    'pageUrl' => 'https://www.placecage.com/1600/900'
                ]
            ]
        ]);

        $jsonModel = json_encode($dataModel);

        /** @var ImportJob $integrationImportJob */
        $integrationImportJob = JobAbstract::fromArray(json_decode($jsonModel, true));

        //Test reconstruct
        $this->assertInstanceOf(ImportJob::class, $integrationImportJob);

        //Some spot tests
        $this->assertInstanceOf(ImportFile::class, $integrationImportJob->getImportedFiles()[0]);
        $this->assertCount(count($dataModel['externalIds']), $integrationImportJob->getExternalIds());

        //Test serialize
        $this->assertJsonStringEqualsJsonString($jsonModel, json_encode($integrationImportJob));
    }
}
